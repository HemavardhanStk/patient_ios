//
//  WebViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 21/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    var webViewer: WKWebView!
    var pageTitle: String = ""
    var urlString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.pageTitle, controller: self)
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []
        webConfiguration.allowsInlineMediaPlayback = true
        webViewer = WKWebView(frame: .zero, configuration: webConfiguration)
        webViewer.navigationDelegate = self
        self.view = webViewer
        if let url = URL.init(string: urlString) {
            let request = URLRequest.init(url: url)
            self.webViewer.load(request)
            LoadingIndicator.shared.show(self)
        }
    }
    

}

extension WebViewController: UIWebViewDelegate, WKNavigationDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingIndicator.shared.hide(self)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        LoadingIndicator.shared.hide(self)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        LoadingIndicator.shared.hide(self)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        LoadingIndicator.shared.hide(self)
    }
    
}
