//
//  Navigation.swift
//  MedHos
//
//  Created by Hemavardhan on 03/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class Navigation {
    
    static let shared: Navigation = Navigation()
    
    // MARK: - Navigation Bar
    
    func setTitle(title: String, controller: UIViewController) {

        self.setDefaultNavigationProperties(controller: controller)
        self.setDefaultTitleProperties(title: title, controller: controller)
    }
    
    func setTitlewithRightBarButton(_ title: String, badge: String = "", image: String, text: String, controller: UIViewController) {
        
        self.setTitle(title: title, controller: controller)
        
        let button = UIButton.init(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width: 60, height: 40)
        button.contentHorizontalAlignment = .right
        button.addTarget(controller, action:#selector(rightBarButtonTapped), for: .touchUpInside)

        if text.count != 0 {
            button.setTitle(text, for: .normal)
            button.titleLabel?.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 15)
        } else if image.count != 0 {
            let image = UIImage(named: image)
            let imageView = UIImageView.init(image: image)
            imageView.frame = CGRect.init(x: button.frame.width - 25, y: button.frame.midY - 8, width: 25, height: 25)
            imageView.contentMode = .scaleAspectFit

            button.addSubview(imageView)
        }

        let barButton = UIBarButtonItem(customView: button)
        if badge.count != 0 {
            barButton.setBadge(text: badge , withOffsetFromTopRight: CGPoint(x: 8, y: 2))
        }
        controller.navigationItem.rightBarButtonItem = barButton
    }
    
    func addPageNumber(_ pageNumber: Int, totalPages: Int, controller: UIViewController) {
        
        let label = UILabel.init()
        label.frame = CGRect.init(x: 0, y: 0, width: 60, height: 40)
        label.text = "\(pageNumber)/\(totalPages)"
        label.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        label.textColor = UIColor.white
        label.textAlignment = .right
        
        let barButton = UIBarButtonItem(customView: label)
        controller.navigationItem.rightBarButtonItem = barButton
    }
    
    func hideNavigation(_ controller: UIViewController) {
        
        self.setDefaultNavigationProperties(controller: controller)
        controller.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Private Methods
    
    private func setDefaultNavigationProperties(controller: UIViewController) {
        
        controller.navigationController?.isNavigationBarHidden = false
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
        controller.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color: UIColor.init(netHex: APP_PRIMARY_COLOR)), for: .default)
        controller.navigationController?.navigationBar.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        controller.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    private func setDefaultTitleProperties(title: String, controller: UIViewController) {
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.init(name: APP_FONT_NAME_NORMAL, size: 18) as Any]
        controller.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        controller.title = title
    }
    
    // MARK: - Button Listeners
    
    @objc func rightBarButtonTapped() {
        
    }
}
