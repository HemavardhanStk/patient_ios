//
//  ActivityIndicator.swift
//  MedHos
//
//  Created by Hemavardhan on 11/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingIndicator: NVActivityIndicatorViewable {
    
    static let shared: LoadingIndicator = LoadingIndicator()
    
    func show(_ controller: UIViewController) {
        
        let size = CGSize(width: 30, height: 30)

        controller.startAnimating(size, message: "Loading", type: NVActivityIndicatorType.ballClipRotate, fadeInAnimation: nil)
    }
    
    func hide(_ controller: UIViewController) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            controller.stopAnimating(nil)
        }
    }
    
}

extension UIViewController: NVActivityIndicatorViewable {
    
}
