//
//  RadioButtonTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 18/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class RadioButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var checkBoxImageViewOne: UIImageView!
    @IBOutlet weak var checkBoxImageViewTwo: UIImageView!
    @IBOutlet weak var checkBoxImageViewThree: UIImageView!
    
    @IBOutlet weak var radioButtonOne: UIButton!
    @IBOutlet weak var radioButtonTwo: UIButton!
    @IBOutlet weak var radioButtonThree: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
