//
//  LabelTableViewCell.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 25/02/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {

    @IBOutlet weak var itemFrontImageView: UIImageView!
    @IBOutlet weak var itemFrontImageViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var itemFrontImageViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var Switcher: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
