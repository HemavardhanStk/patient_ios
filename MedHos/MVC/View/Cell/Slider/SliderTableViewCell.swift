//
//  SliderTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 20/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell {

    @IBOutlet weak var SliderCount: UILabel!
    @IBOutlet weak var SliderView: UISlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        SliderCount.layer.borderWidth = 1
        SliderCount.layer.borderColor = UIColor.darkGray.cgColor
        SliderCount.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func CountChanger(_ sender:UISlider)
    {
          SliderCount.text = String(Int(sender.value))
    }
}
