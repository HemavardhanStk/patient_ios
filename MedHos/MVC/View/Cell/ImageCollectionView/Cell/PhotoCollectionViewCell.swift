//
//  PhotoCollectionViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 25/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol PhotoCollectionViewCellDelegate : class {
    func deleteButtondidPress(_ tag: Int)
}

class PhotoCollectionViewCell: UICollectionViewCell {

    var PhotoCollectionViewDelegate : PhotoCollectionViewCellDelegate?
    
    @IBOutlet weak var IconImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBAction func DeleteCall(_ sender:UIButton)
    {
        PhotoCollectionViewDelegate?.deleteButtondidPress(sender.tag)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
