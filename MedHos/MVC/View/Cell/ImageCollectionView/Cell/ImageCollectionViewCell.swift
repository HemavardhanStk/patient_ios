//
//  ImageCollectionViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ImageViewWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
