//
//  CollectionTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class CollectionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var CollectionView: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.CollectionView.layer.masksToBounds = true
        self.CollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        self.CollectionView.register(UINib.init(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCollectionID")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
