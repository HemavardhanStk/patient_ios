//
//  BasicSearchTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 21/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol BasicSearchTableViewCellDelegate : class {
    func BasicSearchCellButtondidPressButton(_ tag: Int)
}

class BasicSearchTableViewCell: UITableViewCell {

    var BasicSearchDelegate : BasicSearchTableViewCellDelegate?
    
    @IBOutlet weak var TextLabel1: UILabel!
    @IBOutlet weak var TextLabel2: UILabel!
    @IBOutlet weak var TextLabel3: UILabel!
    @IBOutlet weak var TextLabel4: UILabel!
    @IBOutlet weak var TextLabel5: UILabel!
    
    @IBOutlet weak var TextLabel1BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var TextLabel2BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var TextLabel3BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var TextLabel4BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var TextLabel5BottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ProfileimageView: CustomImageView!
    
    @IBOutlet weak var bottomTextLabel: UILabel!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var bottomButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBAction func BottomButtonFunc(_ sender: UIButton) {
        
        BasicSearchDelegate?.BasicSearchCellButtondidPressButton(sender.tag)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
