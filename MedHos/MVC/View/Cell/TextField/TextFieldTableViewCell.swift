//
//  TextFieldTableViewCell.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 13/02/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var defaultTextIcon: UIImageView!
    @IBOutlet weak var defaultTextIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var defaultTextField: ACFloatingTextfield!
    @IBOutlet weak var DefaultTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var DefaultTextFieldTraillingConstraint: NSLayoutConstraint!
    @IBOutlet weak var TextIconRight: UIImageView!
    @IBOutlet weak var TextIconRightHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var defaultButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
