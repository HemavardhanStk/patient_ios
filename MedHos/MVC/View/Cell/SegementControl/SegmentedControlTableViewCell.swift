//
//  SegmentedControlTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 09/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class SegmentedControlTableViewCell: UITableViewCell {

    @IBOutlet weak var segmentedControl: MRSegmentedControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
