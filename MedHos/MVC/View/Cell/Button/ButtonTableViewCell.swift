//
//  ButtonTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/03/19.
//  Copyright © 2019 Hemavardhan O. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var defaultButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
