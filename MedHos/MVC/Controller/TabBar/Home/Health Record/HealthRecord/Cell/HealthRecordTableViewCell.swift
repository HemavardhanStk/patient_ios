//
//  HealthRecordTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 22/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class HealthRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var ReportTypeImage: UIImageView!
    @IBOutlet weak var ReportTypeLabel: UILabel!
    @IBOutlet weak var ClockImage: UIImageView!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var ReportName: UILabel!
    @IBOutlet weak var ReportIdImage: UIImageView!
    //@IBOutlet weak var ReportIdImageHeight: NSLayoutConstraint!
    @IBOutlet weak var ReportIdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
