//
//  HealthRecordHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HealthRecordViewController {
    
    func getRecordCell(_ indexPath: IndexPath) -> HealthRecordTableViewCell {
        
        let cell: HealthRecordTableViewCell = Bundle.main.loadNibNamed("HealthRecordTableViewCell", owner: nil, options: nil)![0] as! HealthRecordTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.recordView.setRoundedCorner()
        
        cell.ReportTypeLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.ReportName.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.ReportIdLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.DateLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        
        let item = self.infoArray[indexPath.row]
        
        switch item["DocumentType"] as? String {
            
        case "Bills" :
            cell.ReportTypeImage.image = UIImage(named: "ic_bill_blue")
            break
        case "Report" :
            cell.ReportTypeImage.image = UIImage(named: "ic_test_blue")
            break
        case "Prescription" :
            cell.ReportTypeImage.image = UIImage(named: "ic_prescription_blue")
            break
        case "Others" :
            cell.ReportTypeImage.image = UIImage(named: "ic_other_blue")
            break
            
        default:break
        }
        
        if let value = item["DocumentType"] as? String {
            cell.ReportTypeLabel.text = value
        }
        
        if let value = item["DocumentDate"] as? String {
            cell.DateLabel.text = value
        }
        
        if let value = item["PatientName"] as? String {
            cell.ReportName.text = "Report for \(value)"
        }
        
        if let value = item["AppointmentNo"] as? String,value != "" {
            cell.ReportIdImage.alpha = 1
            cell.ReportIdImage.image = UIImage(named: "ic_token_no")
            cell.ReportIdLabel.text = value
        } else {
            cell.ReportIdImage.alpha = 0
            cell.ReportIdLabel.text = ""
        }
        
        return cell
    }

    func isFiltering() -> Bool {
        return  !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return self.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        if isFiltering() && self.infoArray.count == 0 {
            self.infoArray = self.infoArrayBackup
        }
        
        self.filteredInfoArray = self.infoArray.filter({( dict : Dictionary<String, Any> ) -> Bool in
            if self.searchBarIsEmpty() {
                return true
            } else {
                let PatientName: String = dict["PatientName"] as! String
                let DocumentType: String = dict["DocumentType"] as! String
                let AppointmentNo: String = dict["AppointmentNo"] as! String
                return PatientName.lowercased().contains(searchText.lowercased()) || DocumentType.lowercased().contains(searchText.lowercased()) || AppointmentNo.lowercased().contains(searchText.lowercased())
            }
        })
        
        if isFiltering() {
            self.infoArray = self.filteredInfoArray
        }
        else{
            self.infoArray = self.infoArrayBackup
        }
        self.tableView.reloadData()
    }

}

