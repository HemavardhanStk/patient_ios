//
//  HealthRecordAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HealthRecordViewController {
    
    @objc func getHealthRecordsAPICall()
    {
        let user = Session.shared.getUserInfo()
        
        let params = ["UserId":user.id,
                      "FilterRecord":"",
                      "Language":"English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetMedicalRecords, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["MedRecList"] as? [Dictionary<String, Any>] {
                    self.infoArray = value
                    self.infoArrayBackup = value
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
    
}

