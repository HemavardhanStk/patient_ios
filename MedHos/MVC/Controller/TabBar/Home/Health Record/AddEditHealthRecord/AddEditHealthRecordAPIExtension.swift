//
//  AddEditHealthRecordAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 11/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddEditHealthRecordViewController {
    
    @objc func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        
        var isValidationSuccess = true
        var nonMandatoryField = [Int]()
        
        self.infoArray = [[PLACEHOLDER:"Record Type",TEXT: self.typeOfRecord],
                          [PLACEHOLDER:"Test Report",TEXT: self.recordTitle],
                          [PLACEHOLDER:"Document title",TEXT: self.DocumentTitle],
                          [PLACEHOLDER:"Appointment Number",TEXT: self.appNo],
                          [PLACEHOLDER:"Patient name",TEXT: self.patientName],
                          [PLACEHOLDER:"Doctor name",TEXT: self.doctorName]]

        if self.typeOfRecord == type[0] {
            nonMandatoryField = [2]
        } else if self.typeOfRecord == type[3] {
            nonMandatoryField = [1]
        } else {
            nonMandatoryField = [1,2]
        }
        
        if self.mapYN == "Y" {
            nonMandatoryField.append(contentsOf: 4...5)
        } else if self.mapYN == "N" {
            nonMandatoryField.append(3)
        } else {
            nonMandatoryField.append(contentsOf: 3...5)
        }
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: self.infoArray, nonMandatoryFields: nonMandatoryField, controller: self)
        
        if isValidationSuccess {
            
            if self.existingDataArray.count == 0 && self.filesArray.count == 0 {
                AlertHelper.shared.showAlert(message: "Select at least one file to upload", controller: self)
                isValidationSuccess = false
            }
            
            if mapYN == "" && isValidationSuccess {
                AlertHelper.shared.showAlert(message: "Select map with medhos", controller: self)
                isValidationSuccess = false
            }
            
        }
        
        return isValidationSuccess
    }
    
    func insertUpdateHealthRecordAPICall() {
        
        let params = getParams()

        HttpManager.shared.uploadImageWithParametersAnd(endUrl: PATH_AddUpdateMedicalRecords, photo: self.filesArray as? [UIImage] ?? [], dataArray: self.filesArray as? [Data] ?? [], parameters: params, headers: nil, jsonName: "MedicalInputData", controller: self, isBackgroundCall: false) { (response, InvoiceResponse, responseValue) in
            
            
            if InvoiceResponse?.Success == 0 {
                AlertHelper.shared.showAlertWithHandler(message: InvoiceResponse?.Message ?? "Record has been Uploaded Successfully", handler: "popViewController", withCancel: false, controller: self)
                
            } else {
                AlertHelper.shared.showAlert(message: InvoiceResponse?.Message ?? "Something went wrong! Try again later", controller: self)
            }
        }
        
    }
    
    func deleteFileAPICall( index: Int) {
        
        let user = Session.shared.getUserInfo()
        
        let fileId = self.existingDataArray[index]["FileNo"] as? Int ?? 0
        
        let params = ["FileRecordID"    : "\(fileId)",
                      "UserID"          : user.id,
                      "Language"        : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_DeleteMedRecordFiles, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                self.existingDataArray.remove(at: index)
                self.tableView.reloadSections([1], with: .automatic)
            }
        }) {}
    }
    
    func getParams() -> Dictionary<String,String> {
        
        let user = Session.shared.getUserInfo()
        
        var params = Dictionary<String,String>()
        
         if let AppointmentNo = self.infoArray[3][TEXT],
            let DoctorName = self.infoArray[5][TEXT],
            let fileName = self.infoArray[1][TEXT],
            let DocumentName = self.infoArray[2][TEXT],
            let DocumentType = self.infoArray[0][TEXT],
            let PatinetName = self.infoArray[4][TEXT] {
            
            var ParentUserNo = ""
            var UserID = ""
            var DocumentNameValue = ""
            var ConsultingDateValue = ""
            var ConsultingTimeValue = ""
            
            if self.typeOfRecord == self.type[0] {
                DocumentNameValue = fileName
            } else if self.typeOfRecord == self.type[3] {
                DocumentNameValue = DocumentName
            } else {
                DocumentNameValue = self.typeOfRecord
            }
            
            if self.mapYN == "Y" {
                ConsultingDateValue = self.ConsultingDate
                ConsultingTimeValue = self.ConsultingTime
            }
            
            if self.patientId != user.id {
                ParentUserNo = user.id
                UserID = "\(self.patientId)"
            } else {
                ParentUserNo = "0"
                UserID = user.id
            }
            
               params = ["AppointmentNo"        : AppointmentNo,
                        "ConsultingDate"        : ConsultingDateValue,
                        "ConsultingTime"        : ConsultingTimeValue,
                        "DoctorName"            : DoctorName,
                        "DocumentDate"          : self.documentDate,
                        "DocumentName"          : DocumentNameValue,
                        "DocumentType"          : DocumentType,
                        "FamilyNewName"         : PatinetName,
                        "Language"              : "English",
                        "filename"              : DocumentNameValue,
                        "MedicalRecordID"       : self.recordId,
                        "ParentUserNo"          : ParentUserNo,
                        "UserID"                : UserID]
        }
        
        return params
    }
}
