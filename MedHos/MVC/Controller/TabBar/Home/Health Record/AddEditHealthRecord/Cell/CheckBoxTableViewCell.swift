//
//  CheckBoxTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 26/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol CheckBoxTableViewCellDelegate : class {
    func CheckerdidPressButton(_ tag: Int,Button: UIButton)
}


class CheckBoxTableViewCell: UITableViewCell { 

    @IBOutlet weak var YesImageView: UIImageView!
    @IBOutlet weak var YesButton: UIButton!
    @IBOutlet weak var NoImageView: UIImageView!
    @IBOutlet weak var NoButton: UIButton!
    @IBAction func CheckBox(_ sender:UIButton)
    {
        CheckBoxDelegate?.CheckerdidPressButton(sender.tag, Button: sender)
    }
    
    var CheckBoxDelegate : CheckBoxTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
