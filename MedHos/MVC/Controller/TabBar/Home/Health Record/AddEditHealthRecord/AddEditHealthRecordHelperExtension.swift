//
//  AddEditHealthRecordHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 11/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import MobileCoreServices

extension AddEditHealthRecordViewController {
    
    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        if indexPath.section == 2 {
            cell.isUserInteractionEnabled = self.canChangeMapping ? true : false
        }
        
        cell.defaultTextIconWidthConstraint.constant = 0
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        
        var placeholderText = ""
        var text = ""
        
        switch indexPath {
            
        case [0,1] :
            if self.typeOfRecord == self.type[0] {
                
                placeholderText = "Test Name"
                text = self.recordTitle
                cell.defaultTextField.tag = self.recordTitleTag
                cell.defaultTextField.addPickerView(self)
                
            } else {
                
                placeholderText = "Document Name"
                text = self.DocumentTitle
                cell.defaultTextField.tag = self.documentTitleTag
            }
            
        case [2,1] :
            if self.mapYN == "Y" {
                
                placeholderText = "Appointment Number"
                text = self.appNo
                cell.defaultTextField.tag = self.appNoTag
                
            } else if self.mapYN == "N" {
                
                placeholderText = "Report for"
                text = self.patientName
                cell.defaultTextField.tag = self.ReportForTag
            }
            cell.defaultTextField.setDropDownStyle()
            
        case[2,2] :
            
            placeholderText = "Referred Doctor Name"
            text = self.doctorName
            cell.defaultTextField.tag = self.doctorNameTag
            
        default: break
        }
        
        cell.defaultTextField.placeholder = placeholderText
        cell.defaultTextField.text = text
        
        return cell
    }
    
    // MARK: - Health_Record_Type Cell
    
    func getHealthRecordTypeCell() -> HealthReminderTypeTableViewCell {
        
        let cell: HealthReminderTypeTableViewCell = Bundle.main.loadNibNamed("HealthReminderTypeTableViewCell", owner: nil, options: nil)![0] as! HealthReminderTypeTableViewCell
        
        cell.MedicineButton.tag = 10
        cell.TestButton.tag = 11
        cell.DoctorAppointmentButton.tag = 12
        cell.OthersButton.tag = 13
        
        cell.MedicineImage.tag = 14
        cell.TestImage.tag = 15
        cell.DoctorImage.tag = 16
        cell.OthersImage.tag = 17
        
        cell.MedicineLabel.tag = 18
        cell.TestLabel.tag = 19
        cell.DoctorLabel.tag = 20
        cell.OthersLabel.tag = 21
        
        cell.MedicineImage.image = UIImage(named: "ic_test_gray")
        cell.TestImage.image = UIImage(named: "ic_prescription_gray")
        cell.DoctorImage.image = UIImage(named: "ic_bill_gray")
        cell.OthersImage.image = UIImage(named: "ic_other_gray")
        
        cell.MedicineLabel.text = "Test\nReport"
        cell.TestLabel.text = "Prescription"
        cell.DoctorLabel.text = "Bills"
        cell.OthersLabel.text = "Others"

        cell.MedicineButton.addTarget(self, action: #selector(changeRecordType(_:)), for: .touchUpInside)
        cell.TestButton.addTarget(self, action: #selector(changeRecordType(_:)), for: .touchUpInside)
        cell.DoctorAppointmentButton.addTarget(self, action: #selector(changeRecordType(_:)), for: .touchUpInside)
        cell.OthersButton.addTarget(self, action: #selector(changeRecordType(_:)), for: .touchUpInside)
        
        return cell
    }

    // MARK: - File_Viewer Cell
    
    func getFilesCell() -> CollectionTableViewCell {
        
        let cell: CollectionTableViewCell = Bundle.main.loadNibNamed("CollectionTableViewCell", owner: nil, options: nil)![0] as! CollectionTableViewCell
     
        cell.CollectionView.delegate = self
        cell.CollectionView.dataSource = self
        
        return cell
    }
    
//     MARK: - Check_Box Cell
    
    func getCheckBoxCell() -> CheckBoxTableViewCell {
        
        let cell: CheckBoxTableViewCell = Bundle.main.loadNibNamed("CheckBoxTableViewCell", owner: nil, options: nil)![0] as! CheckBoxTableViewCell
        
        cell.YesImageView.tag = 25
        cell.NoImageView.tag = 26
        
        cell.YesButton.tag = 23
        cell.NoButton.tag = 24
        
        cell.YesButton.addTarget(self, action: #selector(changeMapWithMedhos(_:)), for: .touchUpInside)
        cell.NoButton.addTarget(self, action: #selector(changeMapWithMedhos(_:)), for: .touchUpInside)
        
        cell.isUserInteractionEnabled = self.canChangeMapping ? true : false
        
        return cell
    }

    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            if textField.text?.count == 0 {
                self.recordTitle = self.testsPickerValues[0]
            }
            textField.resignFirstResponder()
        }
    }

    @objc func changeRecordType(_ sender: UIButton) {
        
        self.typeOfRecord = type[sender.tag - 10]
        self.recordTitle = ""
        self.DocumentTitle = ""
        self.tableView.reloadSections([0], with: .automatic)
    }
    
    func setRecordTypeSelection() {
        
        for i in 0..<type.count {
            if type[i] == self.typeOfRecord {
                if let imageView = self.tableView.viewWithTag(i + 14) as? UIImageView {
                    imageView.setImageColor(color: UIColor(netHex: APP_PRIMARY_COLOR))
                }
                
                if let label = self.tableView.viewWithTag(i + 18) as? UILabel {
                    label.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
                }
                
            } else {
                
                if let imageView = self.tableView.viewWithTag(i + 14) as? UIImageView {
                    imageView.setImageColor(color: UIColor.darkGray)
                }
                
                if let label = self.tableView.viewWithTag(i + 18) as? UILabel {
                    label.textColor = UIColor.darkGray
                }
                
            }
        }
    }
    
    @objc func changeMapWithMedhos(_ sender: UIButton) {
        
        let user = Session.shared.getUserInfo()
        
        if sender.tag == 23 {
            self.mapYN = "Y"
            
            self.patientName = ""
            self.patientId = "0"
            
        } else if sender.tag == 24 {
            self.mapYN = "N"
            
            self.appNo = ""
            self.doctorName = ""
            self.ConsultingDate = ""
            self.ConsultingTime = ""
            self.patientName = user.name
            self.patientId = user.id
            
        }
        
        self.tableView.reloadSections([2], with: .automatic)
    }
    
    func setMapWithMedhos() {
        
        if mapYN == "Y" {
            if let imageView = self.tableView.viewWithTag(25) as? UIImageView {
                imageView.image = UIImage(named: "ic_check_box_selected")
            }
            if let imageView = self.tableView.viewWithTag(26) as? UIImageView {
                imageView.image = UIImage(named: "ic_check_box_empty")
            }
        } else if mapYN == "N" {
            if let imageView = self.tableView.viewWithTag(25) as? UIImageView {
                imageView.image = UIImage(named: "ic_check_box_empty")
            }
            if let imageView = self.tableView.viewWithTag(26) as? UIImageView {
                imageView.image = UIImage(named: "ic_check_box_selected")
            }
        } else {
            if let imageView = self.tableView.viewWithTag(25) as? UIImageView {
                imageView.image = UIImage(named: "ic_check_box_empty")
            }
            if let imageView = self.tableView.viewWithTag(26) as? UIImageView {
                imageView.image = UIImage(named: "ic_check_box_empty")
            }

        }
        
    }
    
    @objc func uploadFilesTapped() {
        
        let alert = UIAlertController(title: APP_NAME, message: "Choose Image from...", preferredStyle: .actionSheet);
        
        if self.filesArray.count == 0 || ((self.filesArray[0] as? UIImage) != nil) {
            
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in
                
                self.pickImageFromCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Photo Gallery", style: .default, handler: {(action:UIAlertAction) in
                
                self.pickImageFromPhotos()
            }))
        }
        
        if self.filesArray.count == 0 || ((self.filesArray[0] as? UIImage) == nil) {
            
            alert.addAction(UIAlertAction(title: "Upload PDF", style: .default, handler: {(action:UIAlertAction) in
                
                self.attachDocument()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func pickImageFromCamera() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .camera
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    func pickImageFromPhotos() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .photoLibrary
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    func attachDocument() {

        let types: [String] = [kUTTypePDF as String]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        self.present(documentPicker, animated: true, completion: nil)

    }

    func downloadfile(URL: NSURL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL as URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = response?.mimeType
                print("Success: \(String(describing: statusCode))")
                DispatchQueue.main.async(execute: {
                    self.filesArray.append(data as Any)
                    self.tableView.reloadSections([1], with: .automatic)
                })
            } else {
                // Failure
                print("Failure: %@", error!.localizedDescription)
            }
        })
        task.resume()
    }
    
    @objc func deleteFile(_ sender: UIButton) {
        
        if sender.tag < self.existingDataArray.count {
            self.showDeletionAlert(index: sender.tag)
        } else {
            self.filesArray.remove(at: sender.tag - self.existingDataArray.count)
            self.tableView.reloadSections([1], with: .automatic)
        }
    }
    
    func showDeletionAlert(index: Int) {
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure to delete this file", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: {(action:UIAlertAction) in
            self.deleteFileAPICall(index: index)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
