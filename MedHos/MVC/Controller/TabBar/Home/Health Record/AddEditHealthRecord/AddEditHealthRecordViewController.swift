//
//  AddEditHealthRecordViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 11/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AddEditHealthRecordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    
    var infoArray = [Dictionary<String, String>]()
    
    var type = ["Report","Prescription","Bills","Others"]
    var testsPickerValues = ["Blood Test","Urine Test","Stool Test","X-Ray","MRI","CT-Scan","Others"]
    var sectionHeaders = ["Choose Record Type","Upload Files","Do you want to map with MedHos Appointment"]
    
    var imagePickerController: UIImagePickerController?
    
    var filesArray = [Any]()
    var existingDataArray = [Dictionary<String, Any>]()
    
    var typeOfRecord = ""
    var recordTitle = ""
    var DocumentTitle = ""
    var mapYN = ""
    var appNo = ""
    var patientName = Session.shared.getUserInfo().name
    var patientId = Session.shared.getUserInfo().id
    var doctorName = ""
    
    var ConsultingDate = ""
    var ConsultingTime = ""
    var documentDate = Helper.shared.getCurrentDateString("dd MMM yyyy")
    
    var recordTitleTag = 43
    var documentTitleTag = 44
    var appNoTag = 45
    var ReportForTag = 46
    var doctorNameTag = 47
    
    var recordId = "0"
    var canChangeMapping = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        if self.navigationController!.viewControllers[viewControllers.count - 2] as? EditRecordViewController != nil {
            self.navigationController!.viewControllers.remove(at: viewControllers.count - 2)
        }
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.recordId == "0" ? "Add HealthRecord":"Edit HealthRecord", controller: self)
        
        self.setupColors()

        self.imagePickerController = UIImagePickerController.init()
        self.imagePickerController?.delegate = self
        
        self.button.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "HealthReminderTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "HealthReminderTypeTableViewCell")
        self.tableView.register(UINib(nibName: "CollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "CollectionTableViewCell")
        self.tableView.register(UINib(nibName: "CheckBoxTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckBoxTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.allowsSelection = false
    }

    func setupColors() {
        
        self.view.backgroundColor = UIColor.white
        self.tableView.backgroundColor = UIColor.clear
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        let viewControllers = self.navigationController!.viewControllers
        for controller in viewControllers.reversed() {
            if controller.isKind(of: HealthRecordViewController.self) {
                NotificationCenter.default.post(name: Notification.Name.Task.ReloadHealthRecord, object: self, userInfo: nil)
                self.navigationController?.popToViewController(controller, animated: true)
                break
            } else if controller.isKind(of: TabBarViewController.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
    }

    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            
            self.insertUpdateHealthRecordAPICall()
        }
    }
    
}
