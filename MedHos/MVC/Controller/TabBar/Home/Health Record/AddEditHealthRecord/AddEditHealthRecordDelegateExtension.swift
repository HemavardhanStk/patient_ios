//
//  AddEditHealthRecordDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 11/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import MobileCoreServices

extension AddEditHealthRecordViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0 :
            return self.typeOfRecord == self.type[0] || self.typeOfRecord == self.type[3] ? 2:1
            
        case 1:
            return 1
            
        case 2:
            if self.mapYN == "Y" {
                return 2
            } else if self.mapYN == "N" {
                return 3
            } else {
                return 1
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == [0,0] || indexPath == [1,0] {
            return 100
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let Header = UIView(frame: CGRect.zero)
        let HeaderLabel = UILabel()
        HeaderLabel.text = self.sectionHeaders[section]
        HeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        HeaderLabel.font = HeaderLabel.font.withSize(13)
        HeaderLabel.frame.origin.x = Header.frame.minX + 5
        HeaderLabel.frame.origin.y = Header.frame.minY + 10
        HeaderLabel.frame.size = CGSize(width: 300, height: 20)
        Header.backgroundColor = UIColor.white
        Header.addSubview(HeaderLabel)
        return Header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerview = UIView()
        return footerview
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath {
            
        case [0,0]:
            return self.getHealthRecordTypeCell()
            
        case [0,1],[2,1],[2,2]:
            return self.getTextFieldCell(indexPath)
            
        case [1,0]:
            return self.getFilesCell()
            
        case [2,0]:
            return self.getCheckBoxCell()
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.setRecordTypeSelection()
        } else if indexPath.section == 2 {
            self.setMapWithMedhos()
        }
    }
    
}

extension AddEditHealthRecordViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        let count = self.filesArray.count + self.existingDataArray.count
        
        return count == 5 ? 5 : count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionID", for: indexPath as IndexPath) as! PhotoCollectionViewCell
        
        let count = self.filesArray.count + self.existingDataArray.count
        
        if indexPath.row == count && count != 5 {
            cell.IconImageView.image = UIImage(named: "camera1")
            cell.deleteButton.isHidden = true
        } else {
            
            if indexPath.row < self.existingDataArray.count {
                
                if let urlString = self.existingDataArray[indexPath.row]["FileLocation"] as? String,
                    let url = URL(string: urlString) {
                    
                    if String(urlString.suffix(3)) == "pdf" {
                        
                        cell.IconImageView.image = #imageLiteral(resourceName: "ic_pdf")
                        
                    } else {
                        
                        cell.IconImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "ic_add_image"))
                        
                    }
                }
            } else {
                cell.IconImageView.image = self.filesArray[indexPath.row - self.existingDataArray.count] as? UIImage ?? #imageLiteral(resourceName: "ic_pdf")
            }
            cell.deleteButton.isHidden = false
        }
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteFile), for: .touchUpInside)
        cell.IconImageView.setRoundedCorner()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let count = self.filesArray.count + self.existingDataArray.count
        
        if indexPath.row == count {
            self.uploadFilesTapped()
        }
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
}

extension AddEditHealthRecordViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == self.appNoTag {
            let viewController = AppController.shared.getSingleSelectionViewController()
            viewController.pageTitle = TITLE_CHOOSE_APPOINTMENT
            viewController.delegate = self
            let navigationViewController = UINavigationController.init(rootViewController: viewController)
            self.present(navigationViewController, animated: true, completion: nil)
            return false
        } else if textField.tag == self.ReportForTag {
            let viewController = AppController.shared.getSingleSelectionViewController()
            viewController.pageTitle = TITLE_APPOINTMENT_FOR
            viewController.delegate = self
            let navigationViewController = UINavigationController.init(rootViewController: viewController)
            self.present(navigationViewController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
        
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == self.recordTitleTag {
            if let text = textField.text {
                self.recordTitle = text
            }
        } else if textField.tag == self.documentTitleTag {
            if let text = textField.text {
                self.DocumentTitle = text
            }
        } else if textField.tag == self.doctorNameTag {
            if let text = textField.text {
                self.doctorName = text
            }
        }
    }
}

extension AddEditHealthRecordViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == self.recordTitleTag {
            return self.testsPickerValues.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == self.recordTitleTag {
            
            return self.testsPickerValues[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == self.recordTitleTag {
            
            if let textField = self.tableView.viewWithTag(pickerView.tag) as? UITextField {
                
                textField.text = self.testsPickerValues[row]
                self.recordTitle = self.testsPickerValues[row]
                
            }
        }
    }
    
}

extension AddEditHealthRecordViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imager = info[.originalImage] as? UIImage {
            self.filesArray.append(imager)
        }
        
        self.tableView.reloadSections(IndexSet([1]), with: .automatic)
        
        picker.dismiss(animated: true, completion: nil)
    }
 
}

extension AddEditHealthRecordViewController: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.downloadfile(URL: myURL as NSURL)//loadpdf("\(myURL)")
        print("import result : \(myURL)")
    }
    
    func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {

        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
}

extension AddEditHealthRecordViewController: SingleSelectionDelegate {
    
    func didSingleSelected(_ selectedItem: String, IDs: String, Value3: String, AppointmentDate: String, AppointmentTime: String, PatientId: String) {
        
        if self.mapYN == "Y" {
            
            self.appNo = IDs
            self.doctorName = Value3
            self.ConsultingDate = AppointmentDate
            self.ConsultingTime = AppointmentTime
            self.patientName = selectedItem
            self.patientId = PatientId
        } else if self.mapYN == "N" {
            
            if selectedItem != "" {
                self.patientName = selectedItem
                self.patientId = IDs
            }
        }
        
        self.tableView.reloadSections([2], with: .automatic)
    }
    
}
