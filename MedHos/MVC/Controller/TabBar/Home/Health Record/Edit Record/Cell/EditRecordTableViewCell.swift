//
//  EditRecordTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 22/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class EditRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var ReferIdImage: UIImageView!
    @IBOutlet weak var ReferId: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var PatientName: UILabel!
    @IBOutlet weak var DoctorName: UILabel!
    @IBOutlet weak var titleReportFor: UILabel!
    @IBOutlet weak var titleReferedDoctor: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var EditButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
