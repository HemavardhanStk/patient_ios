//
//  EditRecordDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension EditRecordViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getEditRecordCell(indexPath)
    }
}

extension EditRecordViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let value = self.infoArray["filename"] as? [Dictionary<String, Any>] {
            return value.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 99, height: 99)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionID", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        if let value = self.infoArray["filename"] as? [Dictionary<String, Any>] {
            if let urlString = value[indexPath.row]["FileLocation"] as? String,
                let url = URL(string: urlString) {
                cell.imageView.image = UIImage(named: "loading")
                cell.imageView.af_setImage(withURL: url)
            }
        }
        cell.imageView.setRoundedCorner()
        cell.imageView.viewInFullScreen()
        return cell
    }
}
