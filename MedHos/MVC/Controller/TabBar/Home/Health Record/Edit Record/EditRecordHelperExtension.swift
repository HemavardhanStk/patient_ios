//
//  EditRecordHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension EditRecordViewController {
    
    func getEditRecordCell(_ indexPath: IndexPath) -> EditRecordTableViewCell {
        
        let cell: EditRecordTableViewCell = Bundle.main.loadNibNamed("EditRecordTableViewCell", owner: nil, options: nil)![0] as! EditRecordTableViewCell
        
        cell.setRoundedCorner()
        
        cell.DeleteButton.addTarget(self, action: #selector(deleteRecordAlert), for: .touchUpInside)
        cell.EditButton.addTarget(self, action: #selector(gotoEditHealthRecord), for: .touchUpInside)
        
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        cell.collectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        cell.collectionView.showsHorizontalScrollIndicator = false
        
        cell.DateLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.ReferId.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.PatientName.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.DoctorName.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.titleReportFor.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.titleReferedDoctor.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        if let value = self.infoArray["DocumentDate"] as? String {
            cell.DateLabel.text = value
        }
        
        if let value = self.infoArray["AppointmentNo"] as? String, value != "" {
            cell.ReferId.text = value
        } else {
            cell.ReferId.text = ""
            cell.ReferIdImage.alpha = 0
        }

        if let value = self.infoArray["PatientName"] as? String {
            cell.PatientName.text = value
        }

        if let value = self.infoArray["DoctorName"] as? String, value != "" {
            cell.DoctorName.text = value
        } else {
            cell.DoctorName.text = ""
            cell.titleReferedDoctor.text = ""
        }
            
        return cell
    }
    
    
    
    @objc func deleteRecordAlert()
    {
        AlertHelper.shared.showAlertWithHandler(message: "Are you sure you want to delete this record?", handler: "loadDeleteRecordAPICall", withCancel: true, controller: self)
    }
    
    
    @objc func gotoEditHealthRecord()
    {
        let viewController = AppController.shared.getAddHealthRecordViewController()
        viewController.appNo = self.infoArray["AppointmentNo"] as? String ?? ""
        viewController.recordId = "\(self.infoArray["MedRecordId"] as? Int ?? 0)"
        viewController.DocumentTitle = self.infoArray["DocumentName"] as? String ?? ""
        viewController.recordTitle = self.infoArray["DocumentName"] as? String ?? ""
        viewController.typeOfRecord = self.infoArray["DocumentType"] as? String ?? ""
        viewController.documentDate = self.infoArray["DocumentDate"] as? String ?? ""
        viewController.ConsultingDate = self.infoArray["ConsultingDate"] as? String ?? ""
        viewController.ConsultingTime = self.infoArray["ConsultingTime"] as? String ?? ""
        viewController.doctorName = self.infoArray["DoctorName"] as? String ?? ""
        viewController.patientName = self.infoArray["PatientName"] as? String ?? ""
        viewController.patientId = "\(self.infoArray["UserID"] as? Int ?? 0)"
        viewController.canChangeMapping = false
        if let value = self.infoArray["AppointmentNo"] as? String {
            if value == "" {
                viewController.mapYN = "N"
            } else {
                viewController.mapYN = "Y"
            }
        }
        
        if let value = self.infoArray["filename"] as? [Dictionary<String,Any>] {
            viewController.existingDataArray = value
        }
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
}
