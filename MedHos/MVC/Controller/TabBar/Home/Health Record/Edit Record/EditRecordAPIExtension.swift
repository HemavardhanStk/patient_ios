//
//  EditRecordAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension EditRecordViewController {
    
    @objc func loadDeleteRecordAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        if let recordId = self.infoArray["MedRecordId"] as? Int {
            
            let params = ["FileRecordID": "\(recordId)",
                          "UserID"      : user.id,
                          "Language"    : "English" ]
            
            HttpManager.shared.loadAPICall(path: PATH_DeactivateMedicalRecord, params: params, httpMethod: .post, controller: self, completion: { (response) in
                if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                    self.popViewController()
                }
            }) {}
        }
    }
    
}
