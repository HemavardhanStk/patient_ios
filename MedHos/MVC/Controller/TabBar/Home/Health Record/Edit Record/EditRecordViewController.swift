//
//  EditRecordViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 22/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class EditRecordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String, Any>()
    var titleName = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.infoArray)
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.titleName.count == 0 ? TITLE_EDITRECORD : self.titleName, controller: self)
        
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "EditRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "EditRecordTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
        
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func popViewController() {
        NotificationCenter.default.post(name: Notification.Name.Task.ReloadHealthRecord, object: self, userInfo: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
}
