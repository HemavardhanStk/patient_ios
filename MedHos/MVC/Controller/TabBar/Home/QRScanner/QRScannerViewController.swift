//
//  QRScannerViewController.swift
//  tests
//
//  Created by Hemavardhan on 12/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import AVFoundation

class QRScannerViewController: UIViewController ,AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var BlurLayer: UIView!
    @IBOutlet weak var BottomBlurLayer: UIView!
    @IBOutlet weak var LeftBlurLayer: UIView!
    @IBOutlet weak var RightBlurLayer: UIView!
    @IBOutlet weak var CenterView: UIView!
    
    var video = AVCaptureVideoPreviewLayer()
    let QrSession = AVCaptureSession()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "QR Scanner", controller: self)
        
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            QrSession.addInput(input)
        } catch {
            print(error)
        }
        
        let output = AVCaptureMetadataOutput()
        QrSession.addOutput(output)
        
        output.setMetadataObjectsDelegate(self as? AVCaptureMetadataOutputObjectsDelegate, queue: DispatchQueue.main)
        
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: QrSession)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        self.view.bringSubviewToFront(CenterView)
        self.view.bringSubviewToFront(BlurLayer)
        self.view.bringSubviewToFront(BottomBlurLayer)
        self.view.bringSubviewToFront(LeftBlurLayer)
        self.view.bringSubviewToFront(RightBlurLayer)
        
        self.QrSession.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
 
        if metadataObjects != nil && metadataObjects.count != 0,
            let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject,
            object.type == AVMetadataObject.ObjectType.qr,
            let qrString = object.stringValue {
            
            self.QrSession.stopRunning()
            self.getDoctorIdAPICall(qrString)
        } else {
            AlertHelper.shared.showAlert(title: APP_NAME, message: "Try again later", controller: self)
        }
    }

    func gotoDoctorSlotsController(_ id: Int, name: String) {
        
        let viewController = AppController.shared.getCreateAppointmentViewController()
        viewController.DoctorId = id
        viewController.DoctorName = name
        self.navigationController!.pushViewController(viewController, animated: true)
    }
}
