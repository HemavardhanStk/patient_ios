//
//  QRScannerAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension QRScannerViewController {
    
    func getDoctorIdAPICall(_ string: String) {
        
        let params = ["PlaintextToEnc"  : "",
                      "Language"        : "English",
                      "TextToDecrypt"   : string]
        
        HttpManager.shared.loadAPICall(path: PATH_ScanDoctorProfileQR, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let id = value["DoctorNo"] as? Int,
                    let name = value["Name"] as? String {
                    
                    self.gotoDoctorSlotsController(id, name: name)
                }
            }
        }) {}
    }
    
}
