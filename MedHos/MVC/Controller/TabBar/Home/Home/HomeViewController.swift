//
//  HomeViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 16/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import FSPagerView

var LanguagesArray = [String]()

class HomeViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userImageButton: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var doctorButton: UIButton!
    @IBOutlet weak var qrScannerButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cartDetailsView: UIView!
    @IBOutlet weak var cartDetailsLabelOne: UILabel!
    @IBOutlet weak var cartDetailsLabelTwo: UILabel!
    @IBOutlet weak var cartDetailsViewCartButton: UIButton!
    @IBOutlet weak var cartDetailsCloseButton: UIButton!
    
    let pageindicator = UIPageControl()
    let pagerView = FSPagerView(frame: CGRect(x: 0, y: 0, width: screenWidth - 20, height: screenHeight/4))
    
    var infoArray = [Dictionary<String, Any>]()
    var infoDataArray = Dictionary<String, Any>()
    let SECTION = "Section"
    var apiCallIsMade = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadHomeAPICalls()
        self.apiCallIsMade = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !self.apiCallIsMade {
            self.loadHomeAPICalls()
        }
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.hideNavigation(self)
        
        self.setupColors()
        self.setTopView()
        self.setUpCartDetails()
        self.loadInfoData()
        
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.automaticSlidingInterval = 2.0
        self.pagerView.interitemSpacing = 30
        self.pagerView.itemSize = CGSize(width: screenWidth - 50, height: screenHeight/4 - 20)
        self.pagerView.backgroundColor = UIColor.white
        self.pagerView.layer.cornerRadius = 15
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ExploreMedHosTableViewCell", bundle: nil), forCellReuseIdentifier: "ExploreMedHosTableViewCell")
        self.tableView.register(UINib(nibName: "DualLabelTabBarTableViewCell", bundle: nil), forCellReuseIdentifier: "DualLabelTabBarTableViewCell")
        self.tableView.register(UINib(nibName: "CollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "CollectionTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: -10, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
    }

    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }

    func setTopView() {
        
        let user = Session.shared.getUserInfo()
        
        self.userNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 17)
        self.cartCountLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        self.userNameLabel.textColor = .white
        self.cartCountLabel.textColor = .white
        
        self.cartCountLabel.backgroundColor = UIColor(netHex: APP_LIGHT_RED_COLOR)
        self.cartCountLabel.layer.borderColor = UIColor.white.cgColor
        self.cartCountLabel.layer.borderWidth = 2
        
        self.userNameLabel.text = user.name
        
        if let url = URL(string: user.image) {
            self.userImageButton.af_setImage(for: .normal, url: url, placeholderImage: #imageLiteral(resourceName: "login"))
        } else {
            self.userImageButton.setImage(#imageLiteral(resourceName: "login"), for: .normal)
        }
        
        self.cartCountLabel.setRoundedCorner(11)
        self.userImageButton.setRoundedCorner(17.5)
        self.doctorButton.setRoundedCorner(2)
        self.qrScannerButton.imageView?.setImageColor(color: UIColor.black)
        
        self.userImageButton.addTarget(self, action: #selector(gotoProfile), for: .touchUpInside)
        self.doctorButton.addTarget(self, action: #selector(gotoDoctors), for: .touchUpInside)
        self.qrScannerButton.addTarget(self, action: #selector(gotoQrSacanner), for: .touchUpInside)
        self.cartButton.addTarget(self, action: #selector(gotoViewCart), for: .touchUpInside)
    }
    
    func setUpCartDetails() {
        
        self.cartDetailsView.alpha = 0
        self.cartDetailsView.backgroundColor = UIColor(netHex: APP_CART_DETAILS_COLOR)
        self.cartDetailsViewCartButton.backgroundColor = UIColor(netHex: APP_GREEN)
        
        self.cartDetailsCloseButton.imageView?.setImageColor(color: UIColor(netHex: APP_PRIMARY_COLOR))
        
        self.cartDetailsLabelOne.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        self.cartDetailsLabelTwo.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        self.cartDetailsLabelOne.textColor = UIColor.darkGray
        self.cartDetailsLabelTwo.textColor = UIColor.darkGray
        
        self.cartDetailsViewCartButton.addTarget(self, action: #selector(gotoViewCart), for: .touchUpInside)
        self.cartDetailsCloseButton.addTarget(self, action: #selector(closeCartDetails), for: .touchUpInside)
        
    }
    
    func loadInfoData() {
        
        self.infoArray = []
        var subinfoArray = [Dictionary<String, String>]()
        
        var dict = [IMAGE: "", TITLE: "", DESCRIPTION: ""]
        subinfoArray.append(dict)
        
        var subDict = [self.SECTION: subinfoArray, TITLE : "", DESCRIPTION: ""] as [String : Any]
        self.infoArray.append(subDict)

        subinfoArray = []
        dict = ["IMAGEONE": "doctor-2", "TITLEONE": "Doctors", "DESCRIPTIONONE": "Book Appointment", "IMAGETWO": "lab-1","TITLETWO": "Diagnostic Lab", "DESCRIPTIONTWO": "Book Test"]
        subinfoArray.append(dict)
        
        subDict = [self.SECTION: subinfoArray, TITLE : "Explore MedHos", DESCRIPTION: "Find nearest HealthCare Providers"] as [String : Any]
        self.infoArray.append(subDict)
        
        subinfoArray = []
        dict = [IMAGE: "ic_myfav_icon", TITLE : "My Favorites", DESCRIPTION: "Frequently viewed providers"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_upcoming_icon", TITLE : "Upcoming Appointment", DESCRIPTION: "Doctor Consultation followup"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_upcoming_icon", TITLE : "Booked Tests", DESCRIPTION: "Booked tests followup"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_healthrecord_icon", TITLE : "Health Records", DESCRIPTION: "All health info at one place"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_reminder_icon", TITLE : "Health Reminders", DESCRIPTION: "Your health followup alarm"]
        subinfoArray.append(dict)
        
        subDict = [self.SECTION: subinfoArray, TITLE : "Quick Access", DESCRIPTION: "Frequently used features"] as [String : Any]
        self.infoArray.append(subDict)
        
        subinfoArray = []
        dict = [IMAGE: "", TITLE : "", DESCRIPTION: ""]
        subinfoArray.append(dict)
        
        subDict = [self.SECTION: subinfoArray, TITLE : "Upcoming MedHos", DESCRIPTION: "Checkout other services to be released"] as [String : Any]
        self.infoArray.append(subDict)
        
        self.tableView.reloadData()
    }
    
    func loadHomeAPICalls() {
        
        self.loadHomeDetailsAPICall(self.infoDataArray.count != 0)
        self.getCartCountAPICall(self.infoDataArray.count != 0)
        
        if LanguagesArray.count == 0 {
            self.getLanguagesAPICall()
        }
    }
    
    @objc func gotoDoctors() {
        let viewController = AppController.shared.getDoctorsViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoQrSacanner() {
        let viewController = AppController.shared.getQRScannerViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoProfile() {
        let viewController = AppController.shared.getPatientProfileViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    @objc func gotoViewCart() {
        let viewController = AppController.shared.getLabChartViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
        if self.cartDetailsView != nil {
            self.cartDetailsView.removeFromSuperview()
            self.tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: -10, right: 0)
        }
    }
    
    @objc func closeCartDetails() {
        
        self.tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: -10, right: 0)
        
//        let xPosition = self.cartDetailsView.frame.origin.x
        let yPosition = self.cartDetailsView.frame.origin.y
        
        let width = self.cartDetailsView.frame.size.width
        let height = self.cartDetailsView.frame.size.height
        
        UIView.animate(withDuration: 1, animations: {
            
            self.cartDetailsView.frame = CGRect(x: -width, y: yPosition, width: width, height: height)
        }) {(isFinished) in
            self.cartDetailsView.removeFromSuperview()
        }        
    }
}
