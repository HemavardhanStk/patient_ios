//
//  HomeAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 16/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HomeViewController {
    
    func loadHomeDetailsAPICall(_ isBackgroundCall: Bool = false) {
        
        let (mobile, userId) = Session.shared.getUserMobileAndUserId()
        
        let params = ["UserId"          : userId,
                      "MobileNumber"    : mobile,
                      "Language"        : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetSearchSummaryDetails, params: params, httpMethod: .post, controller: self,isBackgroundCall: isBackgroundCall, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response,controller: self, isBackgroundCall: isBackgroundCall) {
                self.infoDataArray = response
                
                if let value = self.infoDataArray["RecentResult"] as? Dictionary<String, Any>,
                    let upcomingCount = value["UpcomingAppCount"] as? Int,
                    let dueCount = value["DueAppCount"] as? Int {
                    
                    Session.shared.setUpcomingCount("\(upcomingCount)")
                    Session.shared.setDueCount("\(dueCount)")
                }
                
                // MARK :- AD View Call
                
                if let adDict = response["HomePopup"] as? Dictionary<String, Any>,
                    !isBackgroundCall {
                    
                    if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AdPopOverViewController") as? AdPopOverViewController {
                        
                        viewController.infoArray = adDict
                        self.present(viewController, animated: true, completion: nil)
                    }
                }
                
                UserInfo.shared.loadUserBasicInfo(response)
                UserAddress.shared.loadUserDefaultAddress(response)
                
                self.setTopView()
                
                self.apiCallIsMade = false
                self.tableView.reloadData()
                self.pagerView.reloadData()
            } else {
                UserInfo.shared.logoutUser(self)
            }
        }) {}
    }

    func getCartCountAPICall(_ isBackgroundCall: Bool = false) {
        
        let (mobile, userId) = Session.shared.getUserMobileAndUserId()
        
        let params = ["UserId"          : userId,
                      "MobileNumber"    : mobile,
                      "Language"        : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetCartBucketResult, params: params, httpMethod: .post, controller: self, isBackgroundCall: isBackgroundCall, completion: { (response) in
            
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as! Dictionary<String, AnyObject>, controller: self, isBackgroundCall: true) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let cartCount = value["CartCount"] as? Int {
                    
                    if cartCount == 0 {
                        self.cartButton.alpha = 0
                        self.cartCountLabel.alpha = 0
                        self.cartDetailsView.alpha = 0
                    } else {
                        self.cartButton.alpha = 1
                        self.cartCountLabel.alpha = 1
                        
                        self.cartCountLabel.text = "\(cartCount)"
                        
                        if let labName = value["LabFirmName"] as? String,
                            let testType = value["TestType"] as? String,
                            self.cartDetailsView != nil {
                            
                            self.cartDetailsLabelOne.text = labName
                            self.cartDetailsLabelTwo.text = "You have \(cartCount) \(testType) in your cart"
                            self.tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: 50, right: 0)
                            self.cartDetailsView.alpha = 1
                        }
                    }
//                    Session.shared.setCartCount("\(cartCount)")
                }
            } else {
                self.cartButton.alpha = 0
                self.cartCountLabel.alpha = 0
                self.cartDetailsView.alpha = 0
            }
        }) {}
    }
    
    func getLanguagesAPICall() {
        
        let params = ["AppLanguage" : "English",
                      "Language"    : "English" ]

        HttpManager.shared.loadAPICall(path: PATH_GetLanguages, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["LangList"] as? [Dictionary<String, Any>] {
                    LanguagesArray.append("All")
                    for i in 0..<value.count {
                        LanguagesArray.append(value[i]["DropText"] as? String ?? "")
                    }
                }
            }
        }) {}
    }
}
