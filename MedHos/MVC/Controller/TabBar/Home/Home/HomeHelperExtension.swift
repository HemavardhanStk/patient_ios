//
//  HomeHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 16/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HomeViewController {
    
    func getTopAdView() -> UITableViewCell {
        let cell = UITableViewCell()
        cell.setRoundedCorner()
        self.pagerView.frame.origin = cell.frame.origin
        cell.addSubview(self.pagerView)
        
        return cell
    }
    
    func getExploreMedhosCell(_ indexPath: IndexPath) -> ExploreMedHosTableViewCell {
        
        let cell: ExploreMedHosTableViewCell = Bundle.main.loadNibNamed("ExploreMedHosTableViewCell", owner: nil, options: nil)![0] as! ExploreMedHosTableViewCell
        
        cell.backgroundColor = .clear
        cell.exploreView.setRoundedCorner()
        
        cell.itemOneNameLabel.textColor = UIColor.black
        cell.itemOneSubLabel.textColor = UIColor.darkGray
        
        cell.itemTwoNameLabel.textColor = UIColor.black
        cell.itemTwoSubLabel.textColor = UIColor.darkGray
        
        cell.itemOneNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.itemOneSubLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.itemTwoNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.itemTwoSubLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)

        cell.itemOneButton.addTarget(self, action: #selector(gotoDoctors), for: .touchUpInside)
        cell.itemTwoButton.addTarget(self, action: #selector(gotoLab), for: .touchUpInside)
        
        if let array = self.infoArray[indexPath.section][self.SECTION] as? [Dictionary<String, String>] {
            
            let item = array[indexPath.row]
            
            cell.itemOneImageView.image = UIImage(named: item["IMAGEONE"]!)
            cell.itemOneNameLabel.text = item["TITLEONE"]
            cell.itemOneSubLabel.text = item["DESCRIPTIONONE"]
            
            cell.itemTwoImageView.image = UIImage(named: item["IMAGETWO"]!)
            cell.itemTwoNameLabel.text = item["TITLETWO"]
            cell.itemTwoSubLabel.text = item["DESCRIPTIONTWO"]
        }
        
        return cell
    }
    
    func getQuickAccessTableViewCell(_ indexPath: IndexPath) -> DualLabelTabBarTableViewCell {
        
        let cell: DualLabelTabBarTableViewCell = Bundle.main.loadNibNamed("DualLabelTabBarTableViewCell", owner: nil, options: nil)![0] as! DualLabelTabBarTableViewCell
        
        cell.selectionStyle = .none
        cell.itemNameLabel.textColor = UIColor.black
        cell.itemSubLabel.textColor = UIColor.darkGray
        cell.itemBadgeLabel.textColor = UIColor.white
        
        cell.itemNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.itemSubLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.itemBadgeLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        if let array = self.infoArray[indexPath.section][self.SECTION] as? [Dictionary<String, String>] {
            
            let item = array[indexPath.row]
            
            cell.itemImageView.image = UIImage(named: item[IMAGE]!)
            cell.itemNameLabel.text = item[TITLE]
            cell.itemSubLabel.text = item[DESCRIPTION]
            cell.arrowImageView.image = UIImage(named: "ic_right_arrow_blue")
            cell.bottomLineView.alpha = indexPath.row == array.count - 1 ? 0 : 1
            
            if indexPath.row == 0 {
                cell.backgroundColor = .clear
                cell.dualLabelView.backgroundColor = .clear
                cell.dualLabelBgImageView.alpha = 1
                cell.dualLabelBgImageView.image = UIImage(named: "ic_view_bg")
            } else if indexPath.row == 1 {
                if let value = self.infoDataArray["RecentResult"] as? Dictionary<String, Any>,
                    let upcomingCount = value["UpcomingAppCount"] as? Int,
                    upcomingCount > 0 {
                    cell.itemBadgeLabel.text = "\(upcomingCount)"
                    cell.itemBadgeLabel.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
                    cell.itemBadgeLabel.setRoundedCorner(11)
                }
            } else if indexPath.row == array.count - 1 {
                cell.backgroundColor = .clear
                cell.dualLabelView.backgroundColor = .clear
                cell.dualLabelBgImageView.alpha = 1
                cell.dualLabelBgImageView.image = UIImage(cgImage: UIImage(named: "ic_view_bg")!.cgImage!, scale: 1.0, orientation: .downMirrored)
            }
            
        }
        return cell
    }
    
    func getCollectionViewCell() -> CollectionTableViewCell {
        
        let cell: CollectionTableViewCell = Bundle.main.loadNibNamed("CollectionTableViewCell", owner: nil, options: nil)![0] as! CollectionTableViewCell
        
        cell.backgroundColor = .clear
        cell.CollectionView.backgroundColor = .white
        cell.CollectionView.setRoundedCorner()

        cell.CollectionView.delegate = self
        cell.CollectionView.dataSource = self
        cell.CollectionView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        
        return cell
    }
    
    @objc func gotoLab() {
        let viewController = AppController.shared.getLabBasicSearchViewController()
        self.navigationController!.pushViewController(viewController, animated: true)
    }

    func navigate(_ indexPath: IndexPath) {
        
        switch indexPath.section {
        case 2:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getMyFavoriteViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let viewController = AppController.shared.getUpcomingAppointmentViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 2:
                let viewController = AppController.shared.getBookedTestListViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break

            case 3:
                let viewController = AppController.shared.getHealthRecordViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 4:
                let viewController = AppController.shared.getHealthReminderViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break

            default: break
            }
            break
            
        default: break
        }
    }
    
}
