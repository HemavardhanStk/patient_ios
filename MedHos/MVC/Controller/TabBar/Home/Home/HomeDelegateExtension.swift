//
//  HomeDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 16/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import FSPagerView

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.infoArray[section][self.SECTION] as? [Dictionary<String, String>] {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return screenHeight/4
        } else if indexPath.section == 3 {
            return screenHeight/5
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let item = self.infoArray[section]
        
        let headerView = UIView(frame: CGRect.zero)
        let headerLabel = UILabel()
        
        let attString = NSMutableAttributedString(string: "\(item[TITLE]!)\n\(item[DESCRIPTION]!)")
        attString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_BOLD, size: 15)!, range: NSMakeRange(0, (item[TITLE] as! String).count))
        attString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, (item[TITLE] as! String).count))
        attString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_NORMAL, size: 14)!, range: NSMakeRange((item[TITLE] as! String).count + 1,(item[DESCRIPTION] as! String).count))
        attString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSMakeRange((item[TITLE] as! String).count + 1, (item[DESCRIPTION] as! String).count))
        
        headerLabel.attributedText = attString
        headerLabel.numberOfLines = 2
        headerLabel.frame.origin.x = headerView.frame.minX + 5
        headerLabel.frame.origin.y = headerView.frame.minY + 10
        headerLabel.frame.size = CGSize(width: 300, height: 40)
        headerView.backgroundColor = UIColor.clear
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            return self.getTopAdView()
            
        case 1:
            return self.getExploreMedhosCell(indexPath)
            
        case 2:
            return self.getQuickAccessTableViewCell(indexPath)
            
        case 3:
            return self.getCollectionViewCell()
            
        default:
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2 {
            self.navigate(indexPath)
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let array = self.infoDataArray["UpcomingBannerList"] as? [Dictionary<String, Any>] {
            return array.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth/1.4, height: screenHeight/5 - 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionID", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        if let array = self.infoDataArray["UpcomingBannerList"] as? [Dictionary<String, Any>],
            let urlString = array[indexPath.row]["BannerLocation"] as? String,
            let url = URL(string: urlString) {
            
            cell.imageView.af_setImage(withURL: url)
        }
        cell.imageView.contentMode = .scaleToFill
        cell.imageView.layer.cornerRadius = 8
        cell.imageView.layer.masksToBounds = true
        cell.ImageViewWidthConstraint.constant = screenWidth/1.4 - 5
        
        return cell
    }
    
    
}

extension HomeViewController : FSPagerViewDelegate, FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        
        if let array = self.infoDataArray["TopBannerList"] as? [Dictionary<String, Any>] {
            
            self.pageindicator.numberOfPages = array.count
            self.pageindicator.frame = CGRect(x: 0, y: self.pagerView.frame.maxY - 25, width: screenWidth, height: 10)
            
            self.pagerView.addSubview(self.pageindicator)
            
            return array.count
        }
        return 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        if let array = self.infoDataArray["TopBannerList"] as? [Dictionary<String, Any>],
            let urlString = array[index]["BannerLocation"] as? String,
            let url = URL(string: urlString) {
            cell.imageView?.af_setImage(withURL: url, placeholderImage: UIImage(named: "loading"))
        }
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        
        pageindicator.currentPage = index
    }
    
}
