//
//  MultiProfileHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 07/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps
import SafariServices

extension MultiProfileViewController {
    
    func getProfileCell(_ indexpath:IndexPath) -> CommonProfileViewTableViewCell {
        
        let cell: CommonProfileViewTableViewCell = Bundle.main.loadNibNamed("CommonProfileViewTableViewCell", owner: nil, options: nil)![0] as! CommonProfileViewTableViewCell
        
        //cell.CommonProfileDelegate = self
        
        if self.infoArray[0][TEXT] != "" {
            cell.ProfileImageView.af_setImage(withURL: URL(string: infoArray[0][TEXT]!)!)
            cell.ProfileImageView.layer.cornerRadius = 15
            cell.ProfileImageView.layer.masksToBounds = true
        } else {
            cell.ProfileImageView.image = cell.ProfileImageView.textToImage(drawText: "\(self.infoArray[1][TEXT]!.prefix(1))", inImage: UIImage(named: "ic_replacement_image")!, atPoint: CGPoint(x: 140, y: 20))
        }
        
        cell.ProfileName.text = self.infoArray[1][TEXT]
        cell.ProfileName.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        
        cell.LocationLabel.text = self.infoArray[2][TEXT]
        cell.LocationLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.LocationLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[2][BOTTOM_SPACE]!)!)
        cell.LocationLabel.numberOfLines = 2
        cell.LocationImageView.image = UIImage(named: self.infoArray[2][IMAGE]!)
        cell.LocationImageView.setImageColor(color: UIColor.darkGray)
        
        cell.TimingLabel.text = self.infoArray[3][TEXT]
        cell.TimingLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.TimingLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[3][BOTTOM_SPACE]!)!)
        cell.TimingLabel.numberOfLines = 0
        cell.TimingImageView.image = UIImage(named: self.infoArray[3][IMAGE]!)
        cell.TimingImageView.setImageColor(color: UIColor.darkGray)
        
        cell.InfoLabel.text = self.infoArray[4][TEXT]
        cell.InfoLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.InfoLabel.numberOfLines = infoRows
        cell.InfoLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[4][BOTTOM_SPACE]!)!)
        cell.InfoImageView.image = UIImage(named: self.infoArray[4][IMAGE]!)
        cell.InfoImageView.setImageColor(color: UIColor.darkGray)
        
        let rowcount = cell.InfoLabel.calculateMaxLines()
        if rowcount > 2 {
            cell.SeeMoreButton.alpha = 1
            cell.SeeMoreButton.setTitle(button_Title, for: .normal)
            cell.InfoLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[4][BOTTOM_SPACE]!)!) + 8
            cell.SeeMoreButton.addTarget(self, action: #selector(infoRowIncrementor), for: .touchUpInside)
        } else {
            cell.SeeMoreButton.alpha = 0
        }
        
        cell.WebLinkLabel.text = self.infoArray[5][TEXT]
        cell.WebLinkLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.WebLinkLabel.textColor = UIColor.blue
        cell.WebLinkLabel.isUserInteractionEnabled = true
        cell.WebLinkLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(loadWebPage)))
        cell.WebLinkLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[5][BOTTOM_SPACE]!)!)
        cell.WebLinkImageView.image = UIImage(named: self.infoArray[5][IMAGE]!)
        cell.WebLinkImageView.setImageColor(color: UIColor.darkGray)
        
        cell.LabFacilitiesLabel.text = self.infoArray[6][TEXT]
        cell.LocationLabel.numberOfLines = 0
        cell.LabFacilitiesLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.LabFacilitiesLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[6][BOTTOM_SPACE]!)!)
        cell.LabFacilitiesImageView.image = UIImage(named: self.infoArray[6][IMAGE]!)
        cell.LabFacilitiesImageView.setImageColor(color: UIColor.darkGray)
        
        cell.PhoneNumberLabel.text = self.infoArray[7][TEXT]
        cell.PhoneNumberLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.PhoneNumberLabelBottomConstraint.constant = CGFloat(Int(self.infoArray[7][BOTTOM_SPACE]!)!)
        cell.PhoneNumberImageView.image = UIImage(named: self.infoArray[7][IMAGE]!)
        cell.PhoneNumberImageView.setImageColor(color: UIColor.darkGray)
        
        cell.PhoneOneLabel.text = self.infoArray[8][TEXT]
        cell.PhoneOneLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.PhoneOneBottomConstraint.constant = CGFloat(Int(self.infoArray[8][BOTTOM_SPACE]!)!)
        cell.PhoneOneCallButton.alpha = CGFloat(Int(self.infoArray[8]["ALPHA"]!)!)
        cell.PhoneOneCallButton.tag = 8
        cell.PhoneOneCallButton.addTarget(self, action: #selector(loadCallToFirm), for: .touchUpInside)
        //cell.PhoneOneBottomLine.alpha = CGFloat(Int(self.sinfoArray[8]["ALPHA"]!)!)
        
        cell.PhoneTwoLabel.text = self.infoArray[9][TEXT]
        cell.PhoneTwoLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.PhoneTwoBottomConstraint.constant = CGFloat(Int(self.infoArray[9][BOTTOM_SPACE]!)!)
        cell.PhoneTwoCallButton.alpha = CGFloat(Int(self.infoArray[9]["ALPHA"]!)!)
        cell.PhoneTwoCallButton.tag = 9
        cell.PhoneTwoCallButton.addTarget(self, action: #selector(loadCallToFirm), for: .touchUpInside)
        //cell.PhoneTwoBottomLine.alpha = CGFloat(Int(self.sinfoArray[9]["ALPHA"]!)!)
        
        cell.PhoneThreeLabel.text = self.infoArray[10][TEXT]
        cell.PhoneThreeLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.PhoneThreeSubBottomConstraint.constant = CGFloat(Int(self.infoArray[10][BOTTOM_SPACE]!)!)
        cell.PhoneThreeCallButton.alpha = CGFloat(Int(self.infoArray[10]["ALPHA"]!)!)
        cell.PhoneThreeCallButton.tag = 10
        cell.PhoneThreeCallButton.addTarget(self, action: #selector(loadCallToFirm), for: .touchUpInside)
        cell.PhoneThreeSubLabel.text = self.infoArray[11][TEXT]
        cell.PhoneThreeSubLabel.font = UIFont(name: APP_FONT_NAME_LIGHT, size: 12)
        cell.PhoneThreeSubLabel.textColor = .darkGray
        
        self.sloadView(lat: Double(self.infoArray[12]["Lat"]!)!, long: Double(self.infoArray[12]["Long"]!)!, View: cell.mapView)
        cell.MapImage.image = UIImage(named: self.infoArray[12][IMAGE]!)
        cell.MapImage.setImageColor(color: UIColor.darkGray)
        cell.mapView.layer.cornerRadius = 5
        cell.mapView.layer.masksToBounds = true
        cell.mapView.isUserInteractionEnabled = false
        
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    @objc func loadWebPage() {
        let urlString = "http://\(self.infoArray[5][TEXT]!)"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true, completion: nil)
    }

    @objc func infoRowIncrementor() {
        
        if infoRows == 2 {
            button_Title = "See less"
            infoRows = 0
        } else {
            button_Title = "See more"
            infoRows = 2
        }
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: self.sectionIndex)], with: .automatic)
        }
    }
    
    @objc func sloadView(lat:Double,long:Double,View:GMSMapView) {
        
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        View.camera = GMSCameraPosition(target: coord, zoom: 14, bearing: 0, viewingAngle: 0)
        
        let marker = GMSMarker()
        
        marker.appearAnimation = GMSMarkerAnimation(rawValue: 1)!
        marker.position = coord
        marker.map = View
    }
    
    func parseInitInfoArray() {
        
        var dictTag = ""
        var imageArrayTag = ""
        var facilitiesTag = ""
        var thirdContactTag = ""
        var subTextForThirdContact = ""
        
        switch self.TypeOfID {
            
            case "Lab":
                dictTag = "LabData"
                imageArrayTag = "LABIMAGES"
                facilitiesTag = "LABFACILITIES"
                thirdContactTag = "HOMESAMPLECONTACTNO"
                subTextForThirdContact = "(Home Sample Available)"
            
            case "Medical":
                dictTag = "MedicalData"
                imageArrayTag = "MEDICALSHOPIMAGES"
                thirdContactTag = "HOMEDELIVERYCONTACTNO"
                subTextForThirdContact = "(Home Delivery Available)"
            
            case "Optical":
                dictTag = "OpticalData"
                imageArrayTag = "LABIMAGES"
                thirdContactTag = "TESTINGCONTACTNO"
                subTextForThirdContact = "(Computerized Testing Available)"
            
            default:
                break
        }

        self.setNavigationBar()
        
        if let item = self.initInfoArray[dictTag] as? Dictionary<String, Any> {
            
            if let value = item[imageArrayTag] as? [Dictionary<String, Any>] {
                for i in 0..<value.count
                {
                    self.ImageUrlArray.append(value[i]["ImageUrl"] as? String ?? "")
                }
                self.pagerView.reloadData()
            }
            
            var dict = [IMAGE:"",TEXT:""]
            
            if let value = item["LOGOFILENAME"] as? String {
                dict = [IMAGE:"",TEXT:value ,TYPE:"ProfileImageUrl"]
                self.infoArray.append(dict)
            }
            
            if let value = item["FIRMNAME"] as? String {
                dict = [IMAGE:"ic_Location_icon",TEXT:value]
                self.infoArray.append(dict)
            }
            
            if let value1 = item["ADDRESSONE"] as? String,
                let value2 = item["ADDRESSTWO"] as? String {
                dict = [IMAGE:"ic_map_blue",TEXT:"\(value1), \(value2)",BOTTOM_SPACE:"8"]
                self.infoArray.append(dict)
            }
            
            if let value = item["TIMINGS"] as? [String] , value.count != 0 {
                var text = ""
                for i in 0..<value.count
                {
                    if i != value.count - 1 {
                        text.append("\(value[i])\n")
                    } else {
                        text.append(value[i])
                    }
                }
                dict = [IMAGE:"ic_clock",TEXT:text,BOTTOM_SPACE:"8"]
                self.infoArray.append(dict)
                
            } else {
                dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
                self.infoArray.append(dict)
            }
            
            if let value = item["ABOUT"] as? String, value != "" {
                dict = [IMAGE: "ic_about_blue",TEXT: value,BOTTOM_SPACE: "8"]
                self.infoArray.append(dict)
            } else {
                dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
                self.infoArray.append(dict)
            }
            
            if let value = item["WEBSITE"] as? String, value != "" {
                dict = [IMAGE:"ic_website_icon",TEXT: value,BOTTOM_SPACE:"8"]
                self.infoArray.append(dict)
            } else {
                dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
                self.infoArray.append(dict)
            }
            
            if let value = item[facilitiesTag] as? [String], value.count != 0 {
                var text = ""
                for i in 0..<value.count
                {
                    if i != value.count - 1 {
                        text.append("\(value[i])\n")
                    } else {
                        text.append(value[i])
                    }
                }
                dict = [IMAGE:"ic_lab_facility",TEXT:text,BOTTOM_SPACE:"8"]
                self.infoArray.append(dict)
                
            } else {
                dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
                self.infoArray.append(dict)
            }

//            if item["PHONENO1"] as? String != "" || item["PHONENO2"] as? String != "" || item["HOMESAMPLECONTACTNO"] as? String != "" {
//                dict = [IMAGE:"ic_mobile_blue",TEXT:"Phone Number",BOTTOM_SPACE:"8"]
//                self.infoArray.append(dict)
//            } else {
                dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
                self.infoArray.append(dict)
//            }
            
            if let value = item["PHONENO1"] as? String, value != "" {
                dict = [IMAGE: "ic_call",TEXT: value,BOTTOM_SPACE: "18","ALPHA": "1"]
                self.infoArray.append(dict)
            } else {
                dict = [IMAGE: "",TEXT: "",BOTTOM_SPACE: "0","ALPHA": "0"]
                self.infoArray.append(dict)
            }
            
            if let value = item["PHONENO2"] as? String, value != "" {
                dict = [IMAGE: "ic_call",TEXT: value,BOTTOM_SPACE: "15","ALPHA": "1"]
                self.infoArray.append(dict)
            } else {
                dict = [IMAGE: "",TEXT: "",BOTTOM_SPACE: "0","ALPHA": "0"]
                self.infoArray.append(dict)
            }
            
            if let value = item[thirdContactTag] as? String, value != "" {
                dict = [IMAGE: "ic_call",TEXT: value,BOTTOM_SPACE: "8","ALPHA": "1"]
                self.infoArray.append(dict)
                dict = [IMAGE:"",TEXT:subTextForThirdContact,TYPE:"Stamp Text"]
                self.infoArray.append(dict)
            } else {
                dict = [IMAGE: "",TEXT: "",BOTTOM_SPACE: "0","ALPHA": "0"]
                self.infoArray.append(dict)
                dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
                self.infoArray.append(dict)
            }
            
            if let lat = item["LATITUDE"] as? Double ,
                let long = item["LONGITUDE"] as? Double {
                dict = [IMAGE:"ic_map_location_blue","Lat":"\(lat)","Long":"\(long)"]
                self.infoArray.append(dict)
            }
            self.tableView.reloadData()
        }
        
    }
    
    func setNavigationBar() {
        
        var dictTag = ""
        
        switch self.TypeOfID {
            
        case "Lab":
            dictTag = "LabData"
            
        case "Medical":
            dictTag = "MedicalData"
           
        case "Optical":
            dictTag = "OpticalData"
            
        default:
            break
        }

        if let item = self.initInfoArray[dictTag] as? Dictionary<String, Any>,
            let value = item["FIRMNAME"] as? String {
            if item["ISFAVOURITE"] as? String == "Y" {
                Navigation.shared.setTitlewithRightBarButton(value, image: "ic_heart_like" , text: "", controller: self)
            } else {
                Navigation.shared.setTitlewithRightBarButton(value, image: "ic_heart_like_white" , text: "", controller: self)
            }
        }
    }
    
    @objc func loadCallToFirm(_ sender: UIButton) {
        if let value = self.infoArray[sender.tag][TEXT], value != "" {
            if let url = URL(string: "tel://\(value)"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    @objc func gotoBookTest() {
        
        if let item = self.initInfoArray["LabData"] as? Dictionary<String, Any> {
            let viewController = AppController.shared.getLabPackageTestViewController()
            viewController.LabGUID = item["APPGUID"] as? String ?? ""
            viewController.LabId = "\(item["DIAGNOSTICLABNO"] as? Int ?? 0)"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func gotoBookTestWithPrescription() {
        
        if let item = self.initInfoArray["LabData"] as? Dictionary<String, Any> {
            
            let viewController = AppController.shared.getLabSlotPickerViewController()
            viewController.labName = item["FIRMNAME"] as? String ?? ""
            
            if let value1 = item["AREANAME"] as? String,
                let value2 = item["ADDRESSTWO"] as? String {
                
                viewController.labAddress = "\(value1), \(value2)"
                
            }
            
            viewController.labLogo = item["LOGOFILENAME"] as? String ?? ""
            viewController.labGUID = item["APPGUID"] as? String ?? ""
            
            if let value = item["ISHOMESAMPLE"] as? Bool, value {
                viewController.isHomeSampleAvailable = "Y"
            } else {
                viewController.isHomeSampleAvailable = "N"
            }
            
            if let value = item["REPORTDELIVERYATHOME"] as? String, value == "Y" {
                viewController.isReportDeliveryAvailable = "Y"
            } else {
                viewController.isReportDeliveryAvailable = "N"
            }
            
            viewController.homeSampleCharge = "\(item["HOMEVISITMINAMOUNT"] as? Int ?? 0)"
            viewController.reportDeliveryCharges = "\(item["REPORTDELIVERYMINAMOUNT"] as? Int ?? 0)"
            viewController.isPrescriptionMandatory = true
            self.navigationController?.pushViewController(viewController, animated: true)

        }
    }
    
}


