//
//  MultiProfileAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 07/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension MultiProfileViewController {
    
    func getFirmDetailsAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        var urlString = ""
       
        switch self.TypeOfID {
            
            case "Lab":
                urlString = PATH_GetLabDetailsByID
            
            case "Medical":
                urlString = PATH_GetMedicalShopDetailsByID
            
            case "Optical":
                urlString = PATH_GetOpticalDetailsByID
            
            default:
                break
        }

        
        let params = ["UniqueID": "\(self.UniqueID)",
                     "Language": "English",
                     "UserNo": user.id]

        HttpManager.shared.loadAPICall(path: urlString, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any> {
                    self.NeedToListFromID = false
                    self.initInfoArray = value
                    self.setUpButtons()
                    self.parseInitInfoArray()
                }
            }
        }) {}
    }
    
    func loadEditFavAPICall() {
        
        var urlString = ""
        var idTag = ""
        var idValue = 0
        var updateTag = ""
        var dictTag = ""
        
        switch self.TypeOfID {
            
            case "Lab":
                
                urlString = PATH_AddUpdateLabFavourites
                idTag = "DiagnosticLabNo"
                if let item = self.initInfoArray["LabData"] as? Dictionary<String, Any>,
                    let value = item["DIAGNOSTICLABNO"] as? Int,
                    let value2 = item["ISFAVOURITE"] as? String {
                    idValue = value
                    dictTag = "LabData"
                    if value2 == "Y" {
                        updateTag = "remove"
                    } else {
                        updateTag = "add"
                    }
                }
            
            case "Medical":
                
                urlString = PATH_AddUpdateMedicalFavourites
                idTag = "MedicalNo"
                if let item = self.initInfoArray["MedicalData"] as? Dictionary<String, Any>,
                    let value = item["MEDICALNO"] as? Int,
                    let value2 = item["ISFAVOURITE"] as? String {
                    idValue = value
                    dictTag = "MedicalData"
                    if value2 == "Y" {
                        updateTag = "remove"
                    } else {
                        updateTag = "add"
                    }
                }
            
            case "Optical":
                
                urlString = PATH_AddUpdateOpticalsFavourites
                idTag = "OpticalsNo"
                if let item = self.initInfoArray["OpticalData"] as? Dictionary<String, Any>,
                    let value = item["OPTICALSNO"] as? Int,
                    let value2 = item["ISFAVOURITE"] as? String {
                    idValue = value
                    dictTag = "OpticalData"
                    if value2 == "Y" {
                        updateTag = "remove"
                    } else {
                        updateTag = "add"
                    }
                }
            
            default:
                break
        }

        let user = Session.shared.getUserInfo()
        
        let params = [
                      idTag             :"\(idValue)",
                      "UserDetailNo"    :user.id,
                      "UpdateCommand"   :updateTag,
                      "Language"        :"English"]
        
        HttpManager.shared.loadAPICall(path: urlString, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if var item = self.initInfoArray[dictTag] as? Dictionary<String, Any> {
                    if updateTag == "add" {
                        item["ISFAVOURITE"] = "Y"
                    } else {
                        item["ISFAVOURITE"] = "N"
                    }
                    self.initInfoArray[dictTag] = item
                }
                self.setNavigationBar()
            }
        }) {}
    }
    
}
