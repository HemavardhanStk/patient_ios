//
//  CommonProfileViewTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 20/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

protocol CommonProfileViewTableViewCellDelegate : class {
    func CallButtondidPressButton(_ tag: Int,Button: UIButton)
}

class CommonProfileViewTableViewCell: UITableViewCell {

    @IBOutlet weak var ProfileImageView: CustomImageView!
    @IBOutlet weak var ProfileName: UILabel!
    
    @IBOutlet weak var LocationImageView: UIImageView!
    @IBOutlet weak var TimingImageView: UIImageView!
    @IBOutlet weak var InfoImageView: UIImageView!
    @IBOutlet weak var WebLinkImageView: UIImageView!
    @IBOutlet weak var LabFacilitiesImageView: UIImageView!
    @IBOutlet weak var PhoneNumberImageView: UIImageView!
    @IBOutlet weak var MapImage: UIImageView!
    
    @IBOutlet weak var LocationLabel: UILabel!
    @IBOutlet weak var TimingLabel: UILabel!
    @IBOutlet weak var InfoLabel: UILabel!
    @IBOutlet weak var WebLinkLabel: UILabel!
    @IBOutlet weak var LabFacilitiesLabel: UILabel!
    @IBOutlet weak var PhoneNumberLabel: UILabel!
    @IBOutlet weak var PhoneOneLabel: UILabel!
    @IBOutlet weak var PhoneTwoLabel: UILabel!
    @IBOutlet weak var PhoneThreeLabel: UILabel!
    @IBOutlet weak var PhoneThreeSubLabel: UILabel!
    
    @IBOutlet weak var LocationLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var TimingLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var InfoLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var WebLinkLabelBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var LabFacilitiesLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var PhoneNumberLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var PhoneOneBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var PhoneTwoBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var PhoneThreeSubBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var SeeMoreButton: UIButton!
    @IBOutlet weak var PhoneOneCallButton: UIButton!
    @IBOutlet weak var PhoneTwoCallButton: UIButton!
    @IBOutlet weak var PhoneThreeCallButton: UIButton!
    
    @IBOutlet weak var PhoneOneBottomLine: UIView!
    @IBOutlet weak var PhoneTwoBottomLine: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBAction func CallButton(_ sender:UIButton)
    {
        CommonProfileDelegate?.CallButtondidPressButton(sender.tag, Button: sender)
    }
    
    var CommonProfileDelegate : CommonProfileViewTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
