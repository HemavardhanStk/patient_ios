//
//  MultipProfileViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 20/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps
import FSPagerView

class MultiProfileViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonOneWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonOneHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTwo: UIButton!
    
    let pagerView = FSPagerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height/12, width: 0, height: 0))
    let pageindicator = UIPageControl()
    
    var NeedToListFromID = false
    var initInfoArray = Dictionary<String,Any>()
    var infoArray = [Dictionary<String, String>]()
    var UniqueID = Int()
    var TypeOfID = ""
    var LabDetailedArray : LabData? = nil
    var MedicalDetailedArray : MedicalData? = nil
    var OpticalDetailedArray : OpticalData? = nil
    var ImageUrlArray = [String]()
    let BOTTOM_SPACE = "BottomSpace"
    var Title_Name = ""
    var infoRows = 2
    var sectionIndex = 0
    var button_Title = "See more"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Navigation.shared.setTitle(title: Title_Name, controller: self)
        
        self.setUpButtons()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.register(UINib(nibName: "ProfileImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileImageTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.backgroundColor = .clear
        
        if Title_Name == TITLE_DIAGONSTIC_LAB
        {
            parseInitInfoArray()
        }
        else if Title_Name == TITLE_PHARMACY
        {
            StructMedicalDetails()
        }
        else if Title_Name == TITLE_OPTICALS
        {
            StructOpticalsDetails()
        }
        else if NeedToListFromID
        {
            getFirmDetailsAPICall()
        }
        
        topadviewer()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        pageindicator.numberOfPages = ImageUrlArray.count
        pageindicator.frame = CGRect(x: 0, y: pagerView.frame.maxY - 25, width: screenWidth, height: 10)
        pagerView.addSubview(pageindicator)
    }
    
    func topadviewer()
    {
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        pagerView.automaticSlidingInterval = 2.0
        pagerView.interitemSpacing = 30
        pagerView.itemSize = CGSize(width: screenWidth - 50, height: screenHeight/4 - 20)
        pagerView.backgroundColor = UIColor.white
        pagerView.layer.cornerRadius = 15
    }
    
    func setUpButtons() {
        
        if let item = self.initInfoArray["LabData"] as? Dictionary<String, Any>,
            let value1 = item["BOOKTESTONLINE"] as? Bool,
            let value2 = item["BOOKTESTWITHPRESCRIPTION"] as? Bool {
            
            self.buttonOne.layer.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
            self.buttonTwo.layer.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
            
            self.buttonOne.setTitleColor(UIColor.white, for: .normal)
            self.buttonTwo.setTitleColor(UIColor.white, for: .normal)
            
            self.buttonOneHeightConstraint.constant = 50
            
            if value1 && value2 {
                self.buttonOne.setTitle("Book With Prescription", for: .normal)
                self.buttonTwo.setTitle("Book Test", for: .normal)
                
                self.buttonOneWidthConstraint.constant = screenWidth/2 - 2
                
                self.buttonOne.addTarget(self, action: #selector(gotoBookTestWithPrescription), for: .touchUpInside)
                self.buttonTwo.addTarget(self, action: #selector(gotoBookTest), for: .touchUpInside)
                
            } else if value1 {
                self.buttonOne.setTitle("Book Test", for: .normal)
                self.buttonOneWidthConstraint.constant = screenWidth - 2
                self.buttonOne.addTarget(self, action: #selector(gotoBookTest), for: .touchUpInside)
            } else if value2 {
                self.buttonOne.setTitle("Book With Prescription", for: .normal)
                self.buttonOneWidthConstraint.constant = screenWidth - 2
                self.buttonOne.addTarget(self, action: #selector(gotoBookTestWithPrescription), for: .touchUpInside)
            } else {
                self.buttonOneHeightConstraint.constant = 0
            }
        }
        
    }
    
    func StructLabDetails()
    {
        
        if LabDetailedArray?.ISFAVOURITE == "Y"
        {
            DispatchQueue.main.async {
                Navigation.shared.setTitlewithRightBarButton(self.LabDetailedArray!.FIRMNAME!, image: "ic_heart_like" , text: "", controller: self)
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                Navigation.shared.setTitlewithRightBarButton(self.LabDetailedArray!.FIRMNAME!, image: "ic_heart_like_white", text: "", controller: self)
            }
        }
        
        
        if LabDetailedArray?.LABIMAGES?.count != 0
        {
            for i in 0...(LabDetailedArray?.LABIMAGES?.count)! - 1
            {
                self.ImageUrlArray.append(LabDetailedArray!.LABIMAGES![i].ImageUrl!)
            }
            DispatchQueue.main.async {
                self.pagerView.reloadData()
            }
            print(self.ImageUrlArray)
        }
        
        var dict = [IMAGE:"",TEXT:""]
        
        dict = [IMAGE:"",TEXT:LabDetailedArray!.LOGOFILENAME!,TYPE:"ProfileImageUrl"]
        self.infoArray.append(dict)
        
        dict = [IMAGE:"ic_Location_icon",TEXT:LabDetailedArray!.FIRMNAME!]
        self.infoArray.append(dict)
        
        dict = [IMAGE:"ic_map_blue",TEXT:"\(LabDetailedArray!.ADDRESSONE!), \(LabDetailedArray!.ADDRESSTWO!)",BOTTOM_SPACE:"8"]
        self.infoArray.append(dict)
        
        if LabDetailedArray?.TIMINGS?.count != 0
        {
            var text = ""
            for i in 0...(LabDetailedArray?.TIMINGS?.count)! - 1
            {
                if i != (LabDetailedArray?.TIMINGS?.count)! - 1
                {
                    text.append("\(LabDetailedArray!.TIMINGS![i])\n")
                }
                else
                {
                    text.append(LabDetailedArray!.TIMINGS![i])
                }
            }
            dict = [IMAGE:"ic_clock",TEXT:text,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        if LabDetailedArray!.ABOUT! != "" {
        dict = [IMAGE:"ic_about_blue",TEXT:LabDetailedArray!.ABOUT!,BOTTOM_SPACE:"8"]
        self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if LabDetailedArray!.WEBSITE! != "" {
        dict = [IMAGE:"ic_website_icon",TEXT:LabDetailedArray!.WEBSITE!,BOTTOM_SPACE:"8"]
        self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        
        if LabDetailedArray?.LABFACILITIES?.count != 0
        {
            var text = ""
            for i in 0...(LabDetailedArray?.LABFACILITIES?.count)! - 1
            {
                if i != (LabDetailedArray?.LABFACILITIES?.count)! - 1
                {
                    text.append("\(LabDetailedArray!.LABFACILITIES![i])\n")
                }
                else
                {
                    text.append(LabDetailedArray!.LABFACILITIES![i])
                }
            }
            dict = [IMAGE:"ic_lab_facility",TEXT:text,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if LabDetailedArray?.PHONENO1 != "" || LabDetailedArray?.PHONENO2 != "" || LabDetailedArray?.HOMESAMPLECONTACTNO != ""
        {
            dict = [IMAGE:"ic_mobile_blue",TEXT:"Phone Number",BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if LabDetailedArray!.PHONENO1! != "" {
            dict = [IMAGE:"ic_call",TEXT:LabDetailedArray!.PHONENO1!,BOTTOM_SPACE:"18","ALPHA":"1"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
        }
        
        if LabDetailedArray!.PHONENO2! != "" {
            
        dict = [IMAGE:"ic_call",TEXT:LabDetailedArray!.PHONENO2!,BOTTOM_SPACE:"15","ALPHA":"1"]
        self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
        }
        
        if LabDetailedArray!.HOMESAMPLECONTACTNO! != "" {
            
            dict = [IMAGE:"ic_call",TEXT:LabDetailedArray!.HOMESAMPLECONTACTNO!,BOTTOM_SPACE:"8","ALPHA":"1"]
            self.infoArray.append(dict)
            dict = [IMAGE:"",TEXT:"(Home Sample Available)",TYPE:"Stamp Text"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        dict = [IMAGE:"ic_map_location_blue","Lat":"\(LabDetailedArray!.LATITUDE!)","Long":"\(LabDetailedArray!.LONGITUDE!)"]
        self.infoArray.append(dict)
        
        print(self.infoArray)
    }
    
    func StructMedicalDetails()
    {
        if MedicalDetailedArray?.ISFAVOURITE == "Y"
        {
            DispatchQueue.main.async {
                Navigation.shared.setTitlewithRightBarButton(self.MedicalDetailedArray!.FIRMNAME!, image: "ic_heart_like" , text: "", controller: self)
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                Navigation.shared.setTitlewithRightBarButton(self.MedicalDetailedArray!.FIRMNAME!, image: "ic_heart_like_white", text: "", controller: self)
            }
        }
        
        if MedicalDetailedArray?.MEDICALSHOPIMAGES?.count != 0
        {
            for i in 0...(MedicalDetailedArray?.MEDICALSHOPIMAGES?.count)! - 1
            {
                self.ImageUrlArray.append(MedicalDetailedArray!.MEDICALSHOPIMAGES![i].ImageUrl!)
            }
        }
        
        var dict = [IMAGE:"",TEXT:""]
        
        dict = [IMAGE:"",TEXT:MedicalDetailedArray!.LOGOFILENAME!,TYPE:"ProfileImageUrl"]
        self.infoArray.append(dict)
        
        dict = [IMAGE:"ic_Location_icon",TEXT:MedicalDetailedArray!.FIRMNAME!]
        self.infoArray.append(dict)
        
        dict = [IMAGE:"ic_map_blue",TEXT:"\(MedicalDetailedArray!.ADDRESSONE!), \(MedicalDetailedArray!.ADDRESSTWO!)",BOTTOM_SPACE:"8"]
        self.infoArray.append(dict)
        
        if MedicalDetailedArray?.TIMINGS?.count != 0
        {
            var text = ""
            for i in 0...(MedicalDetailedArray?.TIMINGS?.count)! - 1
            {
                if i != (MedicalDetailedArray?.TIMINGS?.count)! - 1
                {
                    text.append("\(MedicalDetailedArray!.TIMINGS![i])\n")
                }
                else
                {
                    text.append(MedicalDetailedArray!.TIMINGS![i])
                }
            }
            dict = [IMAGE:"ic_clock",TEXT:text,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        if MedicalDetailedArray!.ABOUT! != "" {
            dict = [IMAGE:"ic_about_blue",TEXT:MedicalDetailedArray!.ABOUT!,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if MedicalDetailedArray!.WEBSITE! != ""{
            dict = [IMAGE:"ic_website_icon",TEXT:MedicalDetailedArray!.WEBSITE!,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
        self.infoArray.append(dict)
        
        if MedicalDetailedArray?.PHONENO1 != "" || MedicalDetailedArray?.PHONENO2 != "" || MedicalDetailedArray?.HOMEDELIVERYCONTACTNO != ""
        {
            dict = [IMAGE:"ic_mobile_blue",TEXT:"Phone Number",BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if MedicalDetailedArray!.PHONENO1! != "" {
            dict = [IMAGE:"ic_call",TEXT:MedicalDetailedArray!.PHONENO1!,BOTTOM_SPACE:"18","ALPHA":"1"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
        }
        
        if MedicalDetailedArray!.PHONENO2! != "" {
            
            dict = [IMAGE:"ic_call",TEXT:MedicalDetailedArray!.PHONENO2!,BOTTOM_SPACE:"15","ALPHA":"1"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
        }
        
        if MedicalDetailedArray!.HOMEDELIVERYCONTACTNO! != "" {
            
            dict = [IMAGE:"ic_call",TEXT:MedicalDetailedArray!.HOMEDELIVERYCONTACTNO!,BOTTOM_SPACE:"8","ALPHA":"1"]
            self.infoArray.append(dict)
            dict = [IMAGE:"",TEXT:"(Home Delivery Available)",TYPE:"Stamp Text"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        dict = [IMAGE:"ic_map_location_blue","Lat":"\(MedicalDetailedArray!.LATITUDE!)","Long":"\(MedicalDetailedArray!.LONGITUDE!)"]
        self.infoArray.append(dict)
        
        print(self.infoArray)
    }
    
    func StructOpticalsDetails()
    {
        
        if OpticalDetailedArray?.ISFAVOURITE == "Y"
        {
            DispatchQueue.main.async {
                Navigation.shared.setTitlewithRightBarButton(self.OpticalDetailedArray!.FIRMNAME!, image: "ic_heart_like" , text: "", controller: self)
            }
        }
        else
        {
            DispatchQueue.main.async {
                Navigation.shared.setTitlewithRightBarButton(self.OpticalDetailedArray!.FIRMNAME!, image: "ic_heart_like_white", text: "", controller: self)
            }
        }
        
        if OpticalDetailedArray?.LABIMAGES?.count != 0
        {
            for i in 0...(OpticalDetailedArray?.LABIMAGES?.count)! - 1
            {
                self.ImageUrlArray.append(OpticalDetailedArray!.LABIMAGES![i].ImageUrl!)
            }
        }
        
        var dict = [IMAGE:"",TEXT:""]
        
        dict = [IMAGE:"",TEXT:OpticalDetailedArray!.LOGOFILENAME!,TYPE:"ProfileImageUrl"]
        self.infoArray.append(dict)
        
        dict = [IMAGE:"ic_Location_icon",TEXT:OpticalDetailedArray!.FIRMNAME!]
        self.infoArray.append(dict)
        
        dict = [IMAGE:"ic_map_blue",TEXT:"\(OpticalDetailedArray!.ADDRESSONE!), \(OpticalDetailedArray!.ADDRESSTWO!)",BOTTOM_SPACE:"8"]
        self.infoArray.append(dict)
        
        if OpticalDetailedArray?.TIMINGS?.count != 0
        {
            var text = ""
            for i in 0...(OpticalDetailedArray?.TIMINGS?.count)! - 1
            {
                if i != (OpticalDetailedArray?.TIMINGS?.count)! - 1
                {
                    text.append("\(OpticalDetailedArray!.TIMINGS![i])\n")
                }
                else
                {
                    text.append(OpticalDetailedArray!.TIMINGS![i])
                }
            }
            dict = [IMAGE:"ic_clock",TEXT:text,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        if OpticalDetailedArray!.ABOUT! != "" {
            dict = [IMAGE:"ic_about_blue",TEXT:OpticalDetailedArray!.ABOUT!,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if OpticalDetailedArray!.WEBSITE! != ""{
            dict = [IMAGE:"ic_website_icon",TEXT:OpticalDetailedArray!.WEBSITE!,BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
        self.infoArray.append(dict)
        
        if OpticalDetailedArray?.PHONENO1 != "" || OpticalDetailedArray?.PHONENO2 != "" || OpticalDetailedArray?.TESTINGCONTACTNO != ""
        {
            dict = [IMAGE:"ic_mobile_blue",TEXT:"Phone Number",BOTTOM_SPACE:"8"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        if OpticalDetailedArray!.PHONENO1! != "" {
            dict = [IMAGE:"ic_call",TEXT:OpticalDetailedArray!.PHONENO1!,BOTTOM_SPACE:"18","ALPHA":"1"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
        }
        
        if OpticalDetailedArray!.PHONENO2! != "" {
            
            dict = [IMAGE:"ic_call",TEXT:OpticalDetailedArray!.PHONENO2!,BOTTOM_SPACE:"15","ALPHA":"1"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
        }
        
        if OpticalDetailedArray!.TESTINGCONTACTNO! != "" {
            
            dict = [IMAGE:"ic_call",TEXT:OpticalDetailedArray!.TESTINGCONTACTNO!,BOTTOM_SPACE:"8","ALPHA":"1"]
            self.infoArray.append(dict)
            dict = [IMAGE:"",TEXT:"(Computerized Testing Available)",TYPE:"Stamp Text"]
            self.infoArray.append(dict)
        }
        else
        {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0","ALPHA":"0"]
            self.infoArray.append(dict)
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.infoArray.append(dict)
        }
        
        dict = [IMAGE:"ic_map_location_blue","Lat":"\(OpticalDetailedArray!.LATITUDE!)","Long":"\(OpticalDetailedArray!.LONGITUDE!)"]
        self.infoArray.append(dict)
        
        print(self.infoArray)
    }
    
    @objc func rightBarButtonTapped() {
        self.loadEditFavAPICall()
    }
}
