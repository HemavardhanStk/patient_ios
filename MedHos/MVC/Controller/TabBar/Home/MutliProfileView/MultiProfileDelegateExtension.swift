//
//  MultiProfileDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 07/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import FSPagerView

extension MultiProfileViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if NeedToListFromID
        {
            return 0
        }
        if ImageUrlArray.count != 0
        {
            sectionIndex = 1
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if ImageUrlArray.count != 0 && section == 0
        {
            return pagerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if ImageUrlArray.count != 0 && section == 0
        {
            return screenHeight/4
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if ImageUrlArray.count != 0 && section == 0
        {
            return 10
        }
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ImageUrlArray.count != 0 && section == 0
        {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getProfileCell(indexPath)
    }
    
}

extension MultiProfileViewController:  FSPagerViewDelegate, FSPagerViewDataSource, UIPageViewControllerDelegate {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return ImageUrlArray.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: "loading")!
        cell.imageView?.af_setImage(withURL: URL(string: ImageUrlArray[index])!)
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        
        pageindicator.currentPage = index
    }
    
}
