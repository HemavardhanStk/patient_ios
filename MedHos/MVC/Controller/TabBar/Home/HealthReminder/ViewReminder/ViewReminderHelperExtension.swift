//
//  ViewReminderHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewReminderViewController {
    
    func getItemCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        if indexPath.row == 0 {
            cell.itemTitleLabel.textColor = #colorLiteral(red: 0, green: 0.4085357051, blue: 0.8441912375, alpha: 1)
            cell.itemImageViewWidthConstraint.constant = 8
            cell.Switcher.alpha = 1
            cell.Switcher.addTarget(self, action: #selector(buttonClicked), for: .valueChanged)
            
            let booler = self.existingInfoArray["Status"] as? String == "Pending"
            
            cell.itemTitleLabel.text = "Reminder Status"
            cell.Switcher.isOn = booler
            cell.Switcher.isEnabled = booler
            
        } else {
            cell.itemTitleLabel.attributedText = self.infoArray[indexPath.row - 1]
            cell.itemTitleLabel.numberOfLines = 0
        }
        
        return cell
    }

    func parseExistingInfoArray() {
        
        let string1Array = ["Type of Reminders","Title","Dosage","Timings","Frequency","Days","Start Date","End Date","Remind Before","Comments"]
        
        if let value = self.existingInfoArray["ReminderType"] as? String {
            let attString = value.convertToAttributedString(string1Array[0])
            self.infoArray.append(attString)
        }
        
        if let value = self.existingInfoArray["ReminderTitle"] as? String {
            let attString = value.convertToAttributedString(string1Array[1])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["Dosage"] as? String, value != "0" {
            let attString = value.convertToAttributedString(string1Array[2])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["Timings"] as? String {
            let attString = value.convertToAttributedString(string1Array[3])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["Frequency"] as? String {
            let attString = value.convertToAttributedString(string1Array[4])
            self.infoArray.append(attString)
        }
        
        if let value = self.existingInfoArray["FrequencyDetails"] as? String, value != "" {
            let attString = value.convertToAttributedString(string1Array[5])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["StartDate"] as? String {
            let attString = value.convertToAttributedString(string1Array[6])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["EndDate"] as? String {
            let attString = value.convertToAttributedString(string1Array[7])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["Duration"] as? Int {
            var value1 = ""
            if value != 0 {
                value1 = "\(value) mins"
            } else {
                value1 = "On Time"
            }
            let attString = value1.convertToAttributedString(string1Array[8])
            self.infoArray.append(attString)
        }

        if let value = self.existingInfoArray["Comments"] as? String {
            let attString = value.convertToAttributedString(string1Array[9])
            self.infoArray.append(attString)
        }

        self.tableView.reloadData()
        
    }

    @objc func buttonClicked(_ sender: UISwitch) {
        if !sender.isOn {
            AlertHelper.shared.showAlertWithHandler(message: "Do you Wish to Cancel the alert", handler: "loadDeActivateReminder", withCancel: true, controller: self)
            sender.setOn(true, animated:true)
        }
    }
    
    
}
