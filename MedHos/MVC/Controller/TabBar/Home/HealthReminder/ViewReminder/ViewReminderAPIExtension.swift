//
//  ViewReminderAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewReminderViewController {
    
    @objc func loadDeActivateReminder() {
        
        var params = self.existingInfoArray
        params["Status"] = "cancelled"
        
        HttpManager.shared.loadAPICall(path: PATH_AddUpdateHealthReminder, params: params, httpMethod: .post, controller: self, completion: { (response) in
                self.popViewController()
        }) {}
    }
}
