//
//  ViewReminderViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 02/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ViewReminderViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var EditButtonHeightConstraint: NSLayoutConstraint!
    
    var existingInfoArray = Dictionary<String, Any>()
    var infoArray = [NSMutableAttributedString]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        
        // Do any additional setup after loading the view.
    }

    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: TITLE_MEDICAL_REMINDER, controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        if self.existingInfoArray["Status"] as? String != "Pending" {
            self.EditButtonHeightConstraint.constant = 0
        }
        self.EditButton.addTarget(self, action: #selector(gotoEditReminder), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoData() {
        self.parseExistingInfoArray()
    }
    
    @objc func gotoEditReminder() {
        
        let viewController = AppController.shared.getAddHealthReminderViewController()
        
        if let value = self.existingInfoArray["ReminderType"] as? String {
            viewController.typeOfReminder = value
        }
        
        if let value = self.existingInfoArray["ReminderTitle"] as? String {
            viewController.reminderTitle = value
        }
        
        if let value = self.existingInfoArray["Dosage"] as? String, value != "0" {
            viewController.dosageCount = value
        }
        
        if let value = self.existingInfoArray["Timings"] as? String {
            let data = value.components(separatedBy: ",")
            viewController.timings = data
        }
        
        if let value = self.existingInfoArray["Frequency"] as? String {
            viewController.frequencyType = value
        }
        
        if let value = self.existingInfoArray["FrequencyDetails"] as? String, value != "" {
            let data = value.components(separatedBy: ",")
            viewController.actualdays = data
        }
        
        if let value = self.existingInfoArray["NoofTimesperday"] as? Int {
            viewController.IntervalCount = "\(value) day(s)"
        }

        if let value = self.existingInfoArray["StartDate"] as? String {
            viewController.startDate = value
        }
        
        if let value = self.existingInfoArray["EndDate"] as? String {
            viewController.endDate = value
        }
        
        if let value = self.existingInfoArray["Duration"] as? Int {
            var value1 = ""
            if value != 0 {
                value1 = "\(value) mins"
            } else {
                value1 = "On Time"
            }
            viewController.remindBefore = value1
        }
        
        if let value = self.existingInfoArray["Comments"] as? String {
            viewController.comments = value
        }

        if let value = self.existingInfoArray["HReminderID"] as? Int {
            viewController.reminderId = "\(value)"
        }

        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
}
