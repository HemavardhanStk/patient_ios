//
//  HealthReminderViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 19/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class HealthReminderViewController: UIViewController {
    
    @IBOutlet weak var segmentcontroller: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, Any>]()
    var segmentIndex = 0
    var TypeOfRemindersImages = ["ic_medicine_blue","ic_test_blue","ic_doctor_blue","ic_other_blue"]
    var titleName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitlewithRightBarButton(self.titleName.count == 0 ? TITLE_HEALTHREMINDER : self.titleName, image: "ic_plus_white", text: "", controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.segmentcontroller.addTarget(self, action: #selector(segmentControl), for: .valueChanged)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "HealthReminderTableViewCell", bundle: nil), forCellReuseIdentifier: "HealthReminderTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(loadInfoData), name: Notification.Name.Task.ReloadHealthReminder, object: nil)
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
       self.getHealthRemindersAPICall()
    }
    
    @objc func rightBarButtonTapped() {
        let viewController = AppController.shared.getAddHealthReminderViewController()
        self.navigationController!.pushViewController(viewController, animated: true)
    }
}
