//
//  HealthReminderDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HealthReminderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infoArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getReminderCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = AppController.shared.getViewReminderViewController()
        viewController.existingInfoArray = self.infoArray[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
