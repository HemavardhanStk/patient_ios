//
//  HealthReminderTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 19/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class HealthReminderTableViewCell: UITableViewCell {

    @IBOutlet weak var reminderView: UIView!
    @IBOutlet weak var HealthReminderTypeImage: UIImageView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var MedicineCount: UILabel!
    @IBOutlet weak var DurationLabel: UILabel!
    @IBOutlet weak var TimingLabel: UILabel!
    @IBOutlet weak var StateSwitcher: UISwitch!
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var MedicineCountLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var MedicineCountTop: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
