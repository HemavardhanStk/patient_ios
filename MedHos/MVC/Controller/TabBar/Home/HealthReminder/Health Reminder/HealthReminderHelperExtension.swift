//
//  HealthReminderHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HealthReminderViewController {
    
    func getReminderCell(_ indexPath: IndexPath) -> HealthReminderTableViewCell {
        
        let cell: HealthReminderTableViewCell = Bundle.main.loadNibNamed("HealthReminderTableViewCell", owner: nil, options: nil)![0] as! HealthReminderTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.reminderView.setRoundedCorner()
        
        cell.TimingLabel.textColor = UIColor.darkGray
        
        cell.TitleLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.MedicineCount.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.DurationLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.TimingLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        
        cell.StateSwitcher.tag = indexPath.row
        cell.StateSwitcher.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        cell.StateSwitcher.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
        
        let item = self.infoArray[indexPath.row]
        
        switch item["ReminderType"] as? String {
            
        case "Medicine" :
            cell.HealthReminderTypeImage.image = UIImage(named: TypeOfRemindersImages[0])
            if let value = item["Dosage"] as? String {
                 cell.MedicineCount.text = value + " tablets"
            }
            break
        case "Test" :
            cell.HealthReminderTypeImage.image = UIImage(named: TypeOfRemindersImages[1])
            cell.MedicineCount.text = ""
            break
        case "Doctor Appointment" :
            cell.HealthReminderTypeImage.image = UIImage(named: TypeOfRemindersImages[2])
            cell.MedicineCount.text = ""
            break
        case "Others" :
            cell.HealthReminderTypeImage.image = UIImage(named: TypeOfRemindersImages[3])
            cell.MedicineCount.text = ""
            break
        default:break
            
        }
       
        if let value = item["Frequency"] as? String,
            let value2 = item["FrequencyDetails"] as? String {
            
            if value == "Interval Day" {
                cell.DurationLabel.text = "Interval of Days | \(value2)"
            } else {
                cell.DurationLabel.text = "\(value) | \(value2)"
            }
        }
        
        if let value = item["Status"] as? String {
            let booler = value == "Pending"
            
            cell.StateSwitcher.isOn = booler
            cell.StateSwitcher.isEnabled = booler
        }
        
        if let value = item["ReminderTitle"] as? String {
            cell.TitleLabel.text = value
        }
        
        if let value = item["Timings"] as? String {
            cell.TimingLabel.text = value
        }

        return cell
    }
    
    @objc func buttonClicked(_ sender: UISwitch) {
        
        if !sender.isOn {
            
            let alert = UIAlertController(title: APP_NAME, message: "Do you Wish to Cancel the alert", preferredStyle: UIAlertController.Style.alert);
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {(action:UIAlertAction) in
                self.loadDeActivateReminder(sender.tag)
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            sender.setOn(true, animated:true)
        }
    }

    @objc func segmentControl(sender: UISegmentedControl) {
        
        self.segmentIndex = sender.selectedSegmentIndex
        self.getHealthRemindersAPICall()
    }

    
    
}
