//
//  HealthReminderAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension HealthReminderViewController {
    
    func getHealthRemindersAPICall() {
        
        let user = Session.shared.getUserInfo()
        let type = self.segmentIndex == 0 ? "P":""
        
        let params = ["UserId"              : user.id,
                      "ReminderCommand"     : type,
                      "Language"            : "English"]
        
        self.infoArray = []
        
        HttpManager.shared.loadAPICall(path: PATH_GetReminderList, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["ReminderList"] as? [Dictionary<String, Any>] {
                    self.infoArray = value
                }
            }
            self.tableView.reloadData()
        }) {}
    }
    
    @objc func loadDeActivateReminder(_ tag: Int) {
        
        var params = self.infoArray[tag]
        params["Status"] = "cancelled"
        
        HttpManager.shared.loadAPICall(path: PATH_AddUpdateHealthReminder, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                 self.getHealthRemindersAPICall()
            }
        }) {}
    }
    
}
