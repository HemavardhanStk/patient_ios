//
//  AddEditHealthReminderAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 10/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddEditHealthReminderViewController {
    
    @objc func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        
        var isValidationSuccess = true
        var dosageValue = "0"
        var actualDayString = ""
        var reminderTimeString = ""
        var noOfDays = ""
        var remindBeforeString = ""
        var nonMandatoryField = [Int]()
        
        if self.typeOfReminder == type[0] {
            dosageValue = self.dosageCount
        }
        
        if self.IntervalCount != "" {
            noOfDays = String(self.IntervalCount.prefix(1))
        }
        
        if self.actualdays.count != 0 {
            for i in 0..<self.actualdays.count {
                if i != self.actualdays.count - 1 {
                    actualDayString.append("\(self.actualdays[i]),")
                } else {
                    actualDayString.append(self.actualdays[i])
                }
            }
        }
        
        if self.timings.count != 0 {
            for i in 0..<self.timings.count {
                if i != self.timings.count - 1 {
                    reminderTimeString.append("\(self.timings[i]),")
                } else {
                    reminderTimeString.append(self.timings[i])
                }
            }
        }
        
        if self.frequencyType == frequencyTags[0] {
            self.endDate = self.startDate
        }
        
        if self.remindBefore == RemindBeforePickerArray[0] {
            remindBeforeString = "0"
        } else if self.remindBefore != "" {
            remindBeforeString = self.remindBefore.replacingOccurrences(of: " mins", with: "")
        }
        
        self.infoArray = [[PLACEHOLDER:"Reminder Type",TEXT: self.typeOfReminder],
                          [PLACEHOLDER:"Reminder Title",TEXT: self.reminderTitle],
                          [PLACEHOLDER:"dosage",TEXT: dosageValue],
                          [PLACEHOLDER:"Frequency Type",TEXT: self.frequencyType],
                          [PLACEHOLDER:"Actual days",TEXT: actualDayString],
                          [PLACEHOLDER:"Interval of days",TEXT: noOfDays],
                          [PLACEHOLDER:"Reminder Timings",TEXT: reminderTimeString],
                          [PLACEHOLDER:"Start date",TEXT: self.startDate],
                          [PLACEHOLDER:"End date",TEXT: self.endDate],
                          [PLACEHOLDER:"Remind Before Time",TEXT: remindBeforeString],
                          [PLACEHOLDER:"Comments",TEXT: self.comments]]
        
        if self.frequencyType == frequencyTags[2] {
            nonMandatoryField = [2,5,10]
        } else if self.frequencyType == frequencyTags[3] {
            nonMandatoryField = [2,4,10]
        } else {
            nonMandatoryField = [2,4,5,10]
        }
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: self.infoArray, nonMandatoryFields: nonMandatoryField, controller: self)
        
        return isValidationSuccess
    }
    
    func insertUpdateHealthReminderAPICall() {
        
        let user = Session.shared.getUserInfo()
        
            if let ReminderType         = self.infoArray[0][TEXT],
                let ReminderTitle       = self.infoArray[1][TEXT],
                let Dosage              = self.infoArray[2][TEXT],
                let Frequency           = self.infoArray[3][TEXT],
                let FrequencyDetails    = self.infoArray[4][TEXT],
                let NoofTimesperday     = self.infoArray[5][TEXT],
                let Timings             = self.infoArray[6][TEXT],
                let StartDate           = self.infoArray[7][TEXT],
                let EndDate             = self.infoArray[8][TEXT],
                let Duration            = self.infoArray[9][TEXT],
                let Comments            = self.infoArray[10][TEXT] {
            
                var params = ["ReminderType"        : ReminderType,
                              "ReminderTitle"       : ReminderTitle,
                              "Dosage"              : Dosage,
                              "Frequency"           : Frequency,
                              "FrequencyDetails"    : FrequencyDetails,
                              "NoofTimesperday"     : NoofTimesperday,
                              "Timings"             : Timings,
                              "StartDate"           : StartDate,
                              "EndDate"             : EndDate,
                              "Duration"            : Duration,
                              "Comments"            : Comments,
                              "Language"            : "English",
                              "Status"              : "",
                              "HReminderID"         : self.reminderId,
                              "UserID"              : user.id]
                
                if params["NoofTimesperday"] == "" {
                    params["NoofTimesperday"] = "0"
                }
                
                HttpManager.shared.loadAPICall(path: PATH_AddUpdateHealthReminder, params: params as Dictionary<String, Any>, httpMethod: .post, controller: self, completion: { (response) in
                    if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                        if let message = response["Message"] as? String {
                            AlertHelper.shared.showAlertWithHandler(message: message, handler: "popViewController", withCancel: false, controller: self)
                        }
                    }
                }) {}
        }
        
        
        
    }
    
}
