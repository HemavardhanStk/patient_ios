//
//  AddEditHealthReminderHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 10/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddEditHealthReminderViewController {
    
    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        
        cell.defaultTextIconWidthConstraint.constant = 0
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row
        
        var placeholderText = ""
        var text = ""
        
        switch indexPath {
        
        case [0,1] :
            placeholderText = "Title/Medicine Name"
            text = self.reminderTitle
            cell.defaultTextField.tag = self.ReminderTitleTag
            
        case [2,1]:
            placeholderText = "Select interval"
            text = self.IntervalCount
            cell.defaultTextField.tag = self.IntervalOfDaysTag
            cell.defaultTextField.addPickerView(self)
            
        case [3,1]:
            placeholderText = "Start date"
            text = self.startDate
            cell.defaultTextField.tag = self.StartDateTag
            let datePicker = cell.defaultTextField.addDatePickerView(self)
            datePicker.minimumDate = Date()
            
        case [3,2]:
            placeholderText = "End date"
            text = self.endDate
            cell.defaultTextField.tag = self.EndDateTag
            let datePicker = cell.defaultTextField.addDatePickerView(self)
            datePicker.minimumDate = Date()
            
        case [4,0]:
            placeholderText = "Select duration"
            text = self.remindBefore
            cell.defaultTextField.tag = self.RemindBeforeTag
            cell.defaultTextField.addPickerView(self)
            
        default: break
        }
        
        cell.defaultTextField.placeholder = placeholderText
        cell.defaultTextField.text = text
        
        return cell
    }

    // MARK: - TextView Cell
    
    func getTextViewCell() -> TextViewTableViewCell {
        
        let cell: TextViewTableViewCell = Bundle.main.loadNibNamed("TextViewTableViewCell", owner: nil, options: nil)![0] as! TextViewTableViewCell
        
        cell.textView.delegate = self
        cell.textView.tag = self.commentTag
        cell.textView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        cell.textView.setRoundedCorner()
        
        self.setupTextViewText(self.comments, textView: cell.textView)
        
        return cell
    }
    
    // MARK: - HealthReminderType Cell
    
    func getHealthReminderTypeCell() -> HealthReminderTypeTableViewCell {
        
        let cell: HealthReminderTypeTableViewCell = Bundle.main.loadNibNamed("HealthReminderTypeTableViewCell", owner: nil, options: nil)![0] as! HealthReminderTypeTableViewCell
        
        cell.MedicineButton.tag = 10
        cell.TestButton.tag = 11
        cell.DoctorAppointmentButton.tag = 12
        cell.OthersButton.tag = 13
        
        cell.MedicineImage.tag = 14
        cell.TestImage.tag = 15
        cell.DoctorImage.tag = 16
        cell.OthersImage.tag = 17
        
        cell.MedicineLabel.tag = 18
        cell.TestLabel.tag = 19
        cell.DoctorLabel.tag = 20
        cell.OthersLabel.tag = 21
        
        cell.MedicineButton.addTarget(self, action: #selector(changeReminderType(_:)), for: .touchUpInside)
        cell.TestButton.addTarget(self, action: #selector(changeReminderType(_:)), for: .touchUpInside)
        cell.DoctorAppointmentButton.addTarget(self, action: #selector(changeReminderType(_:)), for: .touchUpInside)
        cell.OthersButton.addTarget(self, action: #selector(changeReminderType(_:)), for: .touchUpInside)

        return cell
    }

    // MARK: - Slider Cell
    
    func getDosageCell() -> SliderTableViewCell {
        
        let cell: SliderTableViewCell = Bundle.main.loadNibNamed("SliderTableViewCell", owner: nil, options: nil)![0] as! SliderTableViewCell
        
        let floatValue = Float(Int(self.dosageCount) ?? 1)
        
        cell.SliderCount.tag = 22
        cell.SliderCount.text = self.dosageCount
        cell.SliderView.minimumValue = 1
        cell.SliderView.setValue(floatValue, animated: true)
        cell.SliderView.addTarget(self, action: #selector(dosageListener), for: .valueChanged)
        cell.layer.masksToBounds = true
        
        return cell
    }

    // MARK: - Frequency Type Cell
    
    func getFrequencyTypeCell() -> FrequencyTableViewCell {
        
        let cell: FrequencyTableViewCell = Bundle.main.loadNibNamed("FrequencyTableViewCell", owner: nil, options: nil)![0] as! FrequencyTableViewCell
        
        cell.OnceCheckImage.tag = 27
        cell.EveryDayCheckImage.tag = 28
        cell.ActualDayCheckImage.tag = 29
        cell.IntervalOfDaysCheckImage.tag = 30
        
        cell.OnceButton.tag = 23
        cell.EveryDayButton.tag = 24
        cell.ActualDayButton.tag = 25
        cell.IntervalOfDaysButton.tag = 26

        cell.OnceButton.addTarget(self, action: #selector(changeFrequencyType(_:)), for: .touchUpInside)
        cell.EveryDayButton.addTarget(self, action: #selector(changeFrequencyType(_:)), for: .touchUpInside)
        cell.ActualDayButton.addTarget(self, action: #selector(changeFrequencyType(_:)), for: .touchUpInside)
        cell.IntervalOfDaysButton.addTarget(self, action: #selector(changeFrequencyType(_:)), for: .touchUpInside)

        return cell
    }

    // MARK: - ActualDays Cell
    
    func getActuallDaysCell(_ indexPath:IndexPath) -> ReminderTimeTableViewCell {
        
        let cell: ReminderTimeTableViewCell = Bundle.main.loadNibNamed("ReminderTimeTableViewCell", owner: nil, options: nil)![0] as! ReminderTimeTableViewCell
        
        cell.EightAMButton.tag = 31
        cell.TenAMButton.tag = 32
        cell.OnePMButton.tag = 33
        cell.FourPMButton.tag = 34
        cell.SixPMButton.tag = 35
        cell.EightPMButton.tag = 36
        cell.TenPMButton.tag = 37
        
        cell.EightAMButton.setTitle("SUN", for: .normal)
        cell.TenAMButton.setTitle("MON", for: .normal)
        cell.OnePMButton.setTitle("TUE", for: .normal)
        cell.FourPMButton.setTitle("WED", for: .normal)
        cell.SixPMButton.setTitle("THU", for: .normal)
        cell.EightPMButton.setTitle("FRI", for: .normal)
        cell.TenPMButton.setTitle("SAT", for: .normal)
        
        cell.EightAMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        cell.TenAMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        cell.OnePMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        cell.FourPMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        cell.SixPMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        cell.EightPMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        cell.TenPMButton.addTarget(self, action: #selector(changeActualDaySelection(_:)), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - Timings Cell
    
    func getTimingCell(_ indexPath:IndexPath) -> ReminderTimeTableViewCell {
        
        //let cell: ReminderTimeTableViewCell = Bundle.main.loadNibNamed("ReminderTimeTableViewCell1", owner: nil, options: nil)![0] as! ReminderTimeTableViewCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderTimeTableViewCell1", for: indexPath) as! ReminderTimeTableViewCell
        
        cell.EightAMButton.tag = 1
        cell.TenAMButton.tag = 2
        cell.OnePMButton.tag = 3
        cell.FourPMButton.tag = 4
        cell.SixPMButton.tag = 5
        cell.EightPMButton.tag = 6
        cell.TenPMButton.tag = 7
        
        cell.EightAMButton.setTitle("8AM", for: .normal)
        cell.TenAMButton.setTitle("10AM", for: .normal)
        cell.OnePMButton.setTitle("1PM", for: .normal)
        cell.FourPMButton.setTitle("4PM", for: .normal)
        cell.SixPMButton.setTitle("6PM", for: .normal)
        cell.EightPMButton.setTitle("8PM", for: .normal)
        cell.TenPMButton.setTitle("10PM", for: .normal)
        
        cell.EightAMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        cell.TenAMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        cell.OnePMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        cell.FourPMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        cell.SixPMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        cell.EightPMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        cell.TenPMButton.addTarget(self, action: #selector(changeTimingSelection(_:)), for: .touchUpInside)
        
        return cell
    }

    @objc func changeReminderType(_ sender: UIButton) {
        
        self.typeOfReminder = type[sender.tag - 10]
        self.setReminderTypeSelection()
        self.tableView.reloadSections([1,2,3,4], with: .automatic)
    }
    
    func setReminderTypeSelection() {
        for i in 0..<type.count {
            if type[i] == self.typeOfReminder {
              if let imageView = self.tableView.viewWithTag(i + 14) as? UIImageView {
                 imageView.setImageColor(color: UIColor(netHex: APP_PRIMARY_COLOR))
                }
                
                if let label = self.tableView.viewWithTag(i + 18) as? UILabel {
                    label.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
                }
                
            } else {
                
                if let imageView = self.tableView.viewWithTag(i + 14) as? UIImageView {
                    imageView.setImageColor(color: UIColor.darkGray)
                }
                
                if let label = self.tableView.viewWithTag(i + 18) as? UILabel {
                    label.textColor = UIColor.darkGray
                }

            }
        }
    }
    
    @objc func dosageListener(_ sender:UISlider) {
        self.dosageCount = String(Int(sender.value))
        if let label = self.tableView.viewWithTag(22) as? UILabel {
            label.text = self.dosageCount
        }
    }
 
    @objc func changeFrequencyType(_ sender: UIButton) {
        
        self.frequencyType = self.frequencyTags[sender.tag - 23]
        self.tableView.reloadSections([2,3,4], with: .automatic)
    }
    
    func setFrequencySelection() {
        for i in 0..<self.frequencyTags.count {
            if self.frequencyTags[i] == self.frequencyType {
                if let imageView = self.tableView.viewWithTag(i + 27) as? UIImageView {
                    imageView.image = UIImage(named: "ic_check_box_selected")
                }
            } else {
                if let imageView = self.tableView.viewWithTag(i + 27) as? UIImageView {
                    imageView.image = UIImage(named: "ic_check_box_empty")
                }
            }
        }
    }
    
    @objc func changeActualDaySelection(_ sender: UIButton) {
        
        if let button = self.tableView.viewWithTag(sender.tag) as? UIButton {
            if let textLabel = button.titleLabel,
                let text = textLabel.text {
                if self.actualdays.contains(text) {
                    if let index = self.actualdays.firstIndex(of: text) {
                        self.actualdays.remove(at: index)
                    }
                } else {
                    self.actualdays.append(text)
                }
            }
            self.setActualDaySelection()
        }
    }
    
    func setActualDaySelection() {
        
        for i in 31...37 {
            if let button = self.tableView.viewWithTag(i) as? UIButton {
                if let textLabel = button.titleLabel,
                    let text = textLabel.text {
                    if self.actualdays.contains(text) {
                        button.setTitleColor(UIColor(netHex: APP_PRIMARY_COLOR), for: .normal)
                    } else {
                        button.setTitleColor(UIColor.darkGray, for: .normal)
                    }
                }
            }
        }
    }
    
    @objc func changeTimingSelection(_ sender: UIButton) {
        
        if let button = self.tableView.viewWithTag(sender.tag) as? UIButton {
            if let textLabel = button.titleLabel,
                let text = textLabel.text {
                if self.timings.contains(text) {
                    if let index = self.timings.firstIndex(of: text) {
                        self.timings.remove(at: index)
                    }
                } else {
                    self.timings.append(text)
                }
            }
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 3)], with: .automatic)
        }
    }
    
    func setTimingSelection() {
        
        for i in 1...7 {
            if let button = self.tableView.viewWithTag(i) as? UIButton {
                if let textLabel = button.titleLabel,
                    let text = textLabel.text {
                    if self.timings.contains(text) {
                        button.setTitleColor(UIColor(netHex: APP_PRIMARY_COLOR), for: .normal)
                    } else {
                        button.setTitleColor(UIColor.darkGray, for: .normal)
                    }
                }
            }
        }
    }

    func setupTextViewText(_ text: String, textView: UITextView) {
        
        if text != "" {
            textView.text = text
            textView.textColor = UIColor.black
        } else {
            textView.text = self.commentsPlaceholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    // MARK: - Picker Listeners
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            textField.text = Helper.shared.getString(sender.date, format: "dd-MMM-yyyy")
            if let text = textField.text, text != "" {
                if sender.tag == self.StartDateTag {
                    self.startDate = text
                } else if sender.tag == self.EndDateTag {
                    self.endDate = text
                }
            }
        }
    }
    
    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            if textField.text?.count == 0 {
                self.updatePickerValues(senderTag: sender.tag, selectedRow: 0)
            }
            textField.resignFirstResponder()
        }
    }
    
    func updatePickerValues(senderTag: Int, selectedRow: Int) {
        if let textField = self.tableView.viewWithTag(senderTag) as? UITextField {
            if textField.tag == self.IntervalOfDaysTag {
                if IntervalPickerArray.count != 0 {
                    textField.text = IntervalPickerArray[selectedRow]
                    self.IntervalCount = IntervalPickerArray[selectedRow]
                }
            } else if textField.tag == self.RemindBeforeTag {
                if self.RemindBeforePickerArray.count != 0 {
                    textField.text = RemindBeforePickerArray[selectedRow]
                    self.remindBefore = RemindBeforePickerArray[selectedRow]
                }
            } else if textField.tag == self.StartDateTag {
                if self.startDate == "" {
                    textField.text = Helper.shared.getCurrentDateString("dd-MMM-yyyy")
                    self.startDate = Helper.shared.getCurrentDateString("dd-MMM-yyyy")
                }
            } else if textField.tag == self.EndDateTag {
                if self.endDate == "" {
                    textField.text = Helper.shared.getCurrentDateString("dd-MMM-yyyy")
                    self.endDate = Helper.shared.getCurrentDateString("dd-MMM-yyyy")
                }
            }
        }
    }
    
}
