//
//  AddEditHealthReminderViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 04/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AddEditHealthReminderViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    
    var infoArray = [Dictionary<String, String>]()
    
    var type = ["Medicine","Test","Doctor Appointment","Others"]
    var frequencyTags = ["Once","Every day","Actual Day","Interval Day"]
    var sectionHeaders = ["Type Of Reminders","Dosage","Frequency","Reminder Time","Remind Before"]
    var commentsPlaceholder = "Comments"
    
    var IntervalPickerArray = ["1 day","2 days","3 days","4 days","5 days","6 days","7 days"]
    var RemindBeforePickerArray = ["On Time","5 mins","10 mins","15 mins","20 mins","30 mins","45 mins","60 mins"]
    
    var reminderId = "0"
    
    var typeOfReminder = ""
    var reminderTitle = ""
    var dosageCount = "1"
    var frequencyType = ""
    var actualdays = [String]()
    var IntervalCount = ""
    var timings = [String]()
    var startDate = ""
    var endDate = ""
    var remindBefore = ""
    var comments = ""
    
    var ReminderTitleTag = 43
    var IntervalOfDaysTag = 44
    var StartDateTag = 45
    var EndDateTag = 46
    var RemindBeforeTag = 47
    var commentTag = 48
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.reminderId == "0" ? "Add HealthReminder":"Edit HealthReminder", controller: self)
        
        self.button.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "HealthRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "HealthRecordTableViewCell")
        self.tableView.register(UINib(nibName: "ReminderTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ReminderTimeTableViewCell")
        self.tableView.register(UINib(nibName: "ReminderTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ReminderTimeTableViewCell1")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.allowsSelection = false
    }

    // MARK: - Handlers
    
    @objc func popViewController() {
        
        NotificationCenter.default.post(name: Notification.Name.Task.ReloadHealthReminder, object: self, userInfo: nil)
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HealthReminderViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            
            self.insertUpdateHealthReminderAPICall()
        }
    }

    
}
