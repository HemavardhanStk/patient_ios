//
//  ReminderTimeTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 16/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol ReminderTimeTableViewCellDelegate : class {
    func didPressButton(_ tag: Int,Button: UIButton)
}

class ReminderTimeTableViewCell: UITableViewCell {

    var ReminderDelegate : ReminderTimeTableViewCellDelegate?
    
    @IBOutlet weak var EightAMButton: UIButton!
    @IBOutlet weak var TenAMButton: UIButton!
    @IBOutlet weak var OnePMButton: UIButton!
    @IBOutlet weak var FourPMButton: UIButton!
    @IBOutlet weak var SixPMButton: UIButton!
    @IBOutlet weak var EightPMButton: UIButton!
    @IBOutlet weak var TenPMButton: UIButton!
    
    @IBAction func remindertype(_ sender : UIButton)
        {
            ReminderDelegate?.didPressButton(sender.tag,Button: sender)
        }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
