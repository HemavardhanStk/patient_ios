//
//  FrequencyTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 16/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol FrequencyTableViewCellDelegate : class {
    func FrequencydidPressButton(_ tag: Int)
}

class FrequencyTableViewCell: UITableViewCell {

    var FrequencyDelegate : FrequencyTableViewCellDelegate?
    
    @IBOutlet weak var OnceButton: UIButton!
    @IBOutlet weak var OnceCheckImage: UIImageView!
    @IBOutlet weak var EveryDayCheckImage: UIImageView!
    @IBOutlet weak var EveryDayButton: UIButton!
    @IBOutlet weak var ActualDayButton: UIButton!
    @IBOutlet weak var ActualDayCheckImage: UIImageView!
    @IBOutlet weak var IntervalOfDaysButton: UIButton!
    @IBOutlet weak var IntervalOfDaysCheckImage: UIImageView!
    @IBAction func FrequencyType(_ sender:UIButton)
    {
        FrequencyDelegate?.FrequencydidPressButton(sender.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
