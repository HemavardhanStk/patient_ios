//
//  DosageTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 16/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol DosageTableViewCellDelegate : class {
    func DosagedidPressButton(_ tag: Int)
}


class DosageTableViewCell: UITableViewCell {

    var DosageCellDelegate : DosageTableViewCellDelegate?
    
    @IBOutlet weak var MinusImageLeading: NSLayoutConstraint!
    @IBOutlet weak var MinusButton: UIButton!
    @IBOutlet weak var PlusButton: UIButton!
    @IBOutlet weak var CountLabel: UILabel!
    @IBAction func DosageCounter(_ sender:UIButton)
    {
        DosageCellDelegate?.DosagedidPressButton(sender.tag)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
