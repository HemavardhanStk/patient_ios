//
//  HealthReminderTypeTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol HealthReminderTypeTableViewCellDelegate : class {
    func HealthReminderTypedidPressButton(_ tag: Int)
}


class HealthReminderTypeTableViewCell: UITableViewCell {

    var HealthReminderDelegate : HealthReminderTypeTableViewCellDelegate?
    
    @IBOutlet weak var MedicineButton: UIButton!
    @IBOutlet weak var MedicineImage: UIImageView!
    @IBOutlet weak var MedicineLabel: UILabel!
    @IBOutlet weak var TestButton: UIButton!
    @IBOutlet weak var TestImage: UIImageView!
    @IBOutlet weak var TestLabel: UILabel!
    @IBOutlet weak var DoctorAppointmentButton: UIButton!
    @IBOutlet weak var DoctorImage: UIImageView!
    @IBOutlet weak var DoctorLabel: UILabel!
    @IBOutlet weak var OthersButton: UIButton!
    @IBOutlet weak var OthersImage: UIImageView!
    @IBOutlet weak var OthersLabel: UILabel!
    @IBAction func HealthReminderTypeFunction(_ sender:UIButton)
    {
        HealthReminderDelegate?.HealthReminderTypedidPressButton(sender.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
