//
//  AddEditHealthReminderDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 10/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddEditHealthReminderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0,4:
            return 2
          
        case 1:
            return 1
            
        case 2:
            if self.frequencyType == self.frequencyTags[0] || self.frequencyType == self.frequencyTags[1] || self.frequencyType == "" {
                return 1
            } else {
                return 2
            }
            
        case 3:
            if self.frequencyType == self.frequencyTags[0] {
                return 2
            } else {
                return 3
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.typeOfReminder != self.type[0] && indexPath.section == 1 {
            return CGFloat.leastNonzeroMagnitude
        }
        
        if indexPath == [4,1] {
            return 100
        }
        
        if indexPath != [0,0] && indexPath != [2,0] {
            return 60
        }
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.typeOfReminder != type[0] && section == 1 {
            return CGFloat.leastNonzeroMagnitude
        }
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let Header = UIView(frame: CGRect.zero)
        let HeaderLabel = UILabel()
        HeaderLabel.text = sectionHeaders[section]
        HeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        HeaderLabel.frame.origin.x = Header.frame.minX + 8
        HeaderLabel.frame.origin.y = Header.frame.minY + 5
        HeaderLabel.frame.size = CGSize(width: 150, height: 20)
        Header.backgroundColor = UIColor.white
        Header.addSubview(HeaderLabel)
        Header.layer.masksToBounds = true
        return Header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerview = UIView()
        return footerview
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath {
        case [0,0]:
            return self.getHealthReminderTypeCell()
            
        case [1,0]:
            return self.getDosageCell()
            
        case [2,0]:
            return self.getFrequencyTypeCell()
            
        case [2,1]:
            if frequencyType == self.frequencyTags[2] {
                return self.getActuallDaysCell(indexPath)
            } else {
                return self.getTextFieldCell(indexPath)
            }
            
        case [3,0]:
            return self.getTimingCell(indexPath)
            
        case [0,1],[3,1],[3,2],[4,0] :
            return self.getTextFieldCell(indexPath)
            
        case [4,1]:
            return self.getTextViewCell()
            
        default: break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.setReminderTypeSelection()
        } else if indexPath.section == 2 {
            self.setFrequencySelection()
            self.setActualDaySelection()
        } else if indexPath.section == 3 {
            self.setTimingSelection()
        }
    }
    
}

extension AddEditHealthReminderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == self.IntervalOfDaysTag {
            return self.IntervalPickerArray.count
        } else if pickerView.tag == self.RemindBeforeTag {
            return self.RemindBeforePickerArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == self.IntervalOfDaysTag {
            return self.IntervalPickerArray[row]
        } else if pickerView.tag == self.RemindBeforeTag {
            return self.RemindBeforePickerArray[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.updatePickerValues(senderTag: pickerView.tag, selectedRow: row)
    }
}


extension AddEditHealthReminderViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == self.ReminderTitleTag {
            self.reminderTitle = textField.text!
        }
    }
}

extension AddEditHealthReminderViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.setupTextViewText("", textView: textView)
        }
        
        if textView.text != self.commentsPlaceholder {
            self.comments = textView.text
        }
    }   
}
