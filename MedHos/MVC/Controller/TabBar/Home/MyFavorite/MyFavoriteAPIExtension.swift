//
//  MyFavoriteAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension MyFavoriteViewController {
    
    func getMyFavouritesAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = [ "UniqueID": user.id,
                        "UserNo": "0",
                        "Language": "English"]
        
        self.infoArray = [:]
        
        HttpManager.shared.loadAPICall(path: PATH_GetUserMyFavourites, params: params, httpMethod: .post, controller: self,completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as! Dictionary<String, AnyObject>, controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any> {
                    self.infoArray = value
                    self.tableView.reloadData()
                } 
            }
        }) {}
    }
    
    @objc func loadFavouriteChangeAPICall(_ sender: UIButton) {

        let user = Session.shared.getUserInfo()
        
        if var value = self.infoArray["DoctorFavourites"] as? [Dictionary<String,Any>] {
            
            let isFav = value[sender.tag]["DrFavourite"] as? String  == "Y"
            
            let doctorId = value[sender.tag]["DoctorNo"] as? Int ?? 0
            let updateCommand = isFav ? "remove":"add"
            
            let params = ["UserNo": user.id,
                            "DoctorNo": "\(doctorId)",
                            "UpdateCommand": updateCommand,
                            "Language": "English"]
            
            
            HttpManager.shared.loadAPICall(path: PATH_AddDeleteDrFavourites, params: params, httpMethod: .post, controller: self, completion: { (response) in
                if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                    value[sender.tag]["DrFavourite"] = isFav ? "N" : "Y"
                    self.infoArray["DoctorFavourites"] = value
                    self.tableView.reloadSections(([0]), with: .automatic)
                }
            }) {}
        }
    }
    
}
