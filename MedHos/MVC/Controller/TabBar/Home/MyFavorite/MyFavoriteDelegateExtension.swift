//
//  MyFavoriteDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension MyFavoriteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        currentArray.removeAll()
        
        if let value = self.infoArray["DoctorFavourites"] as? [Dictionary<String, Any>], value.count != 0 {
            currentArray.append(HeadersList[0])
        }
        if let value = self.infoArray["LabFavourites"] as? [Dictionary<String, Any>], value.count != 0 {
            currentArray.append(HeadersList[1])
        }
//        if let value = self.infoArray["MedicalFavourites"] as? [Dictionary<String, Any>], value.count != 0 {
//            currentArray.append(HeadersList[2])
//        }
//        if let value = self.infoArray["OpticalsFavourites"] as? [Dictionary<String, Any>], value.count != 0 {
//            currentArray.append(HeadersList[3])
//        }
        return currentArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let Header = UIView(frame: CGRect.zero)
        let HeaderLabel = UILabel()
        
        HeaderLabel.text = currentArray[section]
        HeaderLabel.textColor = #colorLiteral(red: 0, green: 0.4085357051, blue: 0.8441912375, alpha: 1)
        
        HeaderLabel.frame.origin.x = Header.frame.minX + 10
        HeaderLabel.frame.origin.y = Header.frame.minY + 5
        HeaderLabel.frame.size = CGSize(width: 150, height: 20)
        Header.backgroundColor = UIColor.groupTableViewBackground
        Header.addSubview(HeaderLabel)
        return Header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFavouriteTableViewCell", for: indexPath) as! MyFavouriteTableViewCell
        
        if currentArray[indexPath.section]  == HeadersList[0]
        {
            cell.MyFavouriteCollectionView.tag = 0
        }
        if currentArray[indexPath.section]  == HeadersList[1]
        {
            cell.MyFavouriteCollectionView.tag = 1
        }
        if currentArray[indexPath.section]  == HeadersList[2]
        {
            cell.MyFavouriteCollectionView.tag = 2
        }
        if currentArray[indexPath.section]  == HeadersList[3]
        {
            cell.MyFavouriteCollectionView.tag = 3
        }
        cell.MyFavouriteCollectionView.reloadData()
        
        return cell
    }
    
}

extension MyFavoriteViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let value = self.infoArray["DoctorFavourites"] as? [Dictionary<String,Any>], collectionView.tag == 0 {
            return value.count
        }
        
        if let value = self.infoArray["LabFavourites"] as? [Dictionary<String,Any>], collectionView.tag == 1 {
            return value.count
        }
        
        if let value = self.infoArray["MedicalFavourites"] as? [Dictionary<String,Any>], collectionView.tag == 2 {
            return value.count
        }
        
        if let value = self.infoArray["OpticalsFavourites"] as? [Dictionary<String,Any>], collectionView.tag == 3 {
            return value.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return getMyFavouriteCollectionViewCell(indexPath,collectionView: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectionHandler(indexPath,collectionView: collectionView)
    }
    
}

