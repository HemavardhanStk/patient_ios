//
//  MyFavouriteCollectionViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 28/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit


class MyFavouriteCollectionViewCell: UICollectionViewCell { 
    
    @IBOutlet weak var ProfileImageView: CustomImageView!
    @IBOutlet weak var FavouriteStatusButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detailLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomButton: UIButton!
    
}
