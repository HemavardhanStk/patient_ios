//
//  MyFavouriteTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 28/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class MyFavouriteTableViewCell: UITableViewCell {

    
    @IBOutlet weak var MyFavouriteCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.MyFavouriteCollectionView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension MyFavouriteTableViewCell
{
    func SetCollectionViewDataSourceDelegate
        <D: UICollectionViewDelegate & UICollectionViewDataSource>
        (_ datasourceDelegate: D, forRow row:Int)
    {
        MyFavouriteCollectionView.delegate = datasourceDelegate
        MyFavouriteCollectionView.dataSource = datasourceDelegate
        
        MyFavouriteCollectionView.reloadData()
    }
    
}

