//
//  MyFavoriteHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension MyFavoriteViewController {
    
    // MARK: - MyFavouriteCollectionViewCell Cell
    
    func getMyFavouriteCollectionViewCell(_ indexPath: IndexPath,collectionView: UICollectionView) -> MyFavouriteCollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyFavouriteCollectionViewCell", for: indexPath) as! MyFavouriteCollectionViewCell
        
        
        cell.FavouriteStatusButton.addTarget(self, action: #selector(loadFavouriteChangeAPICall), for: .touchUpInside)
        
        cell.bottomButton.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.bottomButton.addTarget(self, action: #selector(gotoBookProfileView), for: .touchUpInside)
        
        if collectionView.tag == 0 {
            
            cell.FavouriteStatusButton.alpha = 1
            cell.FavouriteStatusButton.tag = indexPath.row
            cell.detailLabelHeight.constant = 20
            cell.bottomButton.setTitle("Book", for: .normal)
            cell.bottomButton.tag = indexPath.row
            
            if let value = self.infoArray["DoctorFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                if item["DrFavourite"] as? String == "Y" {
                    cell.FavouriteStatusButton.setImage(UIImage(named: "ic_heart_like"), for: .normal)
                } else {
                    cell.FavouriteStatusButton.setImage(UIImage(named: "ic_heart_unlike"), for: .normal)
                }
                
                cell.ProfileImageView.image = UIImage(named: "ic_male_avatar")
                
                if let url = URL(string: item["DoctorImage"] as? String ?? "" ) {
                    cell.ProfileImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "ic_doctor_avatar"))
                }
                
                cell.nameLabel.text = item["DoctorName"] as? String ?? ""
                cell.detailLabel.text = item["Speciality"] as? String ?? ""
                
            }
            
        } else if collectionView.tag == 1 {
            
            cell.FavouriteStatusButton.alpha = 0
            cell.detailLabelHeight.constant = 0
            cell.bottomButton.setTitle("View", for: .normal)
            cell.bottomButton.tag = indexPath.row + 1000
            
            
            if let value = self.infoArray["LabFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                cell.ProfileImageView.image = cell.ProfileImageView.textToImage(drawText: "\((item["FIRMNAME"] as? String ?? "M").prefix(1))", inImage: UIImage(named: "ic_replacement_image")!, atPoint: CGPoint(x: 140, y: 20))
                
                if let url = URL(string: item["LOGOFILENAME"] as? String ?? "" ) {
                    cell.ProfileImageView.af_setImage(withURL: url)
                }
                
                cell.nameLabel.text = item["FIRMNAME"] as? String ?? ""
                
            }
            
        } else if collectionView.tag == 2 {
            
            cell.FavouriteStatusButton.alpha = 0
            cell.detailLabelHeight.constant = 0
            cell.bottomButton.setTitle("View", for: .normal)
            cell.bottomButton.tag = indexPath.row + 2000
            
            if let value = self.infoArray["MedicalFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                cell.ProfileImageView.image = cell.ProfileImageView.textToImage(drawText: "\((item["FIRMNAME"] as? String ?? "M").prefix(1))", inImage: UIImage(named: "ic_replacement_image")!, atPoint: CGPoint(x: 140, y: 20))
                
                if let url = URL(string: item["LOGOFILENAME"] as? String ?? "" ) {
                    cell.ProfileImageView.af_setImage(withURL: url)
                }
                
                cell.nameLabel.text = item["FIRMNAME"] as? String ?? ""
                
            }
            
        } else if collectionView.tag == 3 {
            
            cell.FavouriteStatusButton.alpha = 0
            cell.detailLabelHeight.constant = 0
            cell.bottomButton.setTitle("View", for: .normal)
            cell.bottomButton.tag = indexPath.row + 3000
            
            if let value = self.infoArray["OpticalsFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                cell.ProfileImageView.image = cell.ProfileImageView.textToImage(drawText: "\((item["FIRMNAME"] as? String ?? "M").prefix(1))", inImage: UIImage(named: "ic_replacement_image")!, atPoint: CGPoint(x: 140, y: 20))
                
                if let url = URL(string: item["LOGOFILENAME"] as? String ?? "" ) {
                    cell.ProfileImageView.af_setImage(withURL: url)
                }
                
                cell.nameLabel.text = item["FIRMNAME"] as? String ?? ""
                
            }
            
        }
        
        return cell
    }
    
    func selectionHandler(_ indexPath: IndexPath,collectionView: UICollectionView) {
        
        if collectionView.tag == 0
        {
            if let value = self.infoArray["DoctorFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                let viewController = AppController.shared.getDoctorProfileViewController()
                viewController.DoctorId = item["DoctorNo"] as? Int ?? 0
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        }
        else if collectionView.tag == 1
        {
            if let value = self.infoArray["LabFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                let viewController = AppController.shared.getMultiProfileViewController()
                viewController.NeedToListFromID = true
                viewController.Title_Name = item["FIRMNAME"] as? String ?? ""
                viewController.UniqueID = item["DIAGNOSTICLABNO"] as? Int ?? 0
                viewController.TypeOfID = "Lab"
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        }
        else if collectionView.tag == 2
        {
            if let value = self.infoArray["MedicalFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                let viewController = AppController.shared.getMultiProfileViewController()
                viewController.NeedToListFromID = true
                viewController.Title_Name = item["FIRMNAME"] as? String ?? ""
                viewController.UniqueID = item["MEDICALNO"] as? Int ?? 0
                viewController.TypeOfID = "Medical"
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        }
        else if collectionView.tag == 3
        {
            if let value = self.infoArray["OpticalsFavourites"] as? [Dictionary<String, Any>] {
                let item = value[indexPath.row]
                
                let viewController = AppController.shared.getMultiProfileViewController()
                viewController.NeedToListFromID = true
                viewController.Title_Name = item["FIRMNAME"] as? String ?? ""
                viewController.UniqueID = item["OPTICALSNO"] as? Int ?? 0
                viewController.TypeOfID = "Optical"
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        }
        
    }
    
    @objc func gotoBookProfileView(_ sender: UIButton) {
        
        let tag = sender.tag
        
        if tag < 1000 {
            if let value = self.infoArray["DoctorFavourites"] as? [Dictionary<String, Any>] {
                let item = value[sender.tag]
                
                let viewController = AppController.shared.getCreateAppointmentViewController()
                viewController.DoctorId = item["DoctorNo"] as? Int ?? 0
                viewController.DoctorName = item["DoctorName"] as? String ?? ""
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        } else if tag < 2000 {
            if let value = self.infoArray["LabFavourites"] as? [Dictionary<String, Any>] {
                let item = value[tag - 1000]
                
                let viewController = AppController.shared.getMultiProfileViewController()
                viewController.NeedToListFromID = true
                viewController.Title_Name = item["FIRMNAME"] as? String ?? ""
                viewController.UniqueID = item["DIAGNOSTICLABNO"] as? Int ?? 0
                viewController.TypeOfID = "Lab"
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        } else if tag < 3000 {
            if let value = self.infoArray["MedicalFavourites"] as? [Dictionary<String, Any>] {
                let item = value[tag - 2000]
                
                let viewController = AppController.shared.getMultiProfileViewController()
                viewController.NeedToListFromID = true
                viewController.Title_Name = item["FIRMNAME"] as? String ?? ""
                viewController.UniqueID = item["MEDICALNO"] as? Int ?? 0
                viewController.TypeOfID = "Medical"
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        } else if tag < 4000 {
            if let value = self.infoArray["OpticalsFavourites"] as? [Dictionary<String, Any>] {
                let item = value[tag - 3000]
                
                let viewController = AppController.shared.getMultiProfileViewController()
                viewController.NeedToListFromID = true
                viewController.Title_Name = item["FIRMNAME"] as? String ?? ""
                viewController.UniqueID = item["OPTICALSNO"] as? Int ?? 0
                viewController.TypeOfID = "Optical"
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        }
    }
    
}
