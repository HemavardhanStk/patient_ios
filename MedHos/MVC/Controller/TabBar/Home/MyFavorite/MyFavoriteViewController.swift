//
//  MyFavoriteViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 28/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class MyFavoriteViewController: UIViewController  {

    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String, Any>()
    
    var HeadersList = ["My Doctors","My Lab","My Pharmacy","My Opticals"]
    var currentArray = [String]()
    var titleName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }  
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.titleName.count == 0 ? TITLE_MYFAVOURITE : self.titleName, controller: self)
        
        self.loadInfoData()
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoData() {
        self.getMyFavouritesAPICall()
    }
    
}
