//
//  ClinicProfileAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ClinicProfileViewController {
    
    func getClinicDetailsAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["HospitalID": self.clinicId,
                      "Language": "English",
                      "UserNo": user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_GetClinicProfile, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["ProfileDetails"] as? Dictionary<String, Any> {
                    self.infoArray = value
                    self.parseClinicData()
                    self.tableView.reloadData()
                    self.pagerView.reloadData()
                }
            }
        }) {}
    }
    
    func loadFavouriteChangeAPICall(_ tag: Int) {
        
        if var value = self.infoArray["DoctorList"] as? [Dictionary<String,Any>] {
            
            let user = Session.shared.getUserInfo()
            let isFav = value[tag]["DrFavourite"] as? String  == "Y"
            let doctorId = value[tag]["ID"] as? Int ?? 0
            let updateCommand = isFav ? "remove":"add"
            
            let params = ["UserNo": user.id,
                          "DoctorNo": "\(doctorId)",
                          "UpdateCommand": updateCommand,
                          "Language": "English"]
            
            HttpManager.shared.loadAPICall(path: PATH_AddDeleteDrFavourites, params: params, httpMethod: .post, controller: self, completion: { (response) in
                if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {

                    value[tag]["DrFavourite"] = isFav ? "N" : "Y"
                    self.infoArray["DoctorList"] = value
                    self.tableView.reloadData()
                }
            }) {}
        }
    }
    
}
