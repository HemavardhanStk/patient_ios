//
//  ClinicProfileHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

extension ClinicProfileViewController {
    
    func getClinicProfileCell(_ indexpath:IndexPath) -> ClinicProfileTableViewCell {
        
        let cell: ClinicProfileTableViewCell = Bundle.main.loadNibNamed("ClinicProfileTableViewCell", owner: nil, options: nil)![0] as! ClinicProfileTableViewCell
        
        if let value = self.infoArray["ClinicLogo"] as? String,
            let url = URL(string: value) {
            cell.ClinicProfileImageView.af_setImage(withURL: url)
        }
        
        cell.ClinicNameLabel.text = self.clinicInfoArray[0][TEXT]
        cell.ClinicNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.ClinicNameLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.ClinicIconImageView.image = UIImage(named: self.clinicInfoArray[0][IMAGE]!)
        cell.ClinicNameLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[0][BOTTOM_SPACE]!)!)
        
        cell.ClinicAddressLabel.text = self.clinicInfoArray[1][TEXT]
        cell.ClinicAddressLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.ClinicAddressLabel.numberOfLines = 3
        
        cell.MobileNumberLabel.text = self.clinicInfoArray[2][TEXT]
        cell.MobileNumberLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.MobileNumberIconImageView.image = UIImage(named: self.clinicInfoArray[2][IMAGE]!)
        cell.MobileNumberLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[2][BOTTOM_SPACE]!)!)
        
        self.sloadView(lat: Double(self.clinicInfoArray[3]["lat"]!)!, long: Double(self.clinicInfoArray[3]["long"]!)!, View: cell.MapView)
        cell.MapIconImageView.image = UIImage(named: self.clinicInfoArray[3][IMAGE]!)
        cell.MapView.layer.cornerRadius = 5
        cell.MapView.layer.masksToBounds = true
        cell.MapView.isUserInteractionEnabled = false
        
        cell.TimingsLabel.text = self.clinicInfoArray[4][TEXT]
        cell.TimingsLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.TimingsLabel.numberOfLines = 0
        cell.TimingsLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[4][BOTTOM_SPACE]!)!)
        cell.TimingsIconImageView.image = UIImage(named: self.clinicInfoArray[4][IMAGE]!)
        
        cell.AboutHeaderLabel.text = self.clinicInfoArray[5][TEXT]
        cell.AboutHeaderLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.AboutHeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.AboutIconImageView.image = UIImage(named: self.clinicInfoArray[5][IMAGE]!)
        cell.AboutHeaderLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[5][BOTTOM_SPACE]!)!)
        
        cell.AboutTextLabel.text = self.clinicInfoArray[6][TEXT]
        cell.AboutTextLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.AboutTextLabel.numberOfLines = self.infoRows
        cell.AboutTextBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[6][BOTTOM_SPACE]!)!)
        
        let rowcount = cell.AboutTextLabel.calculateMaxLines()
        if rowcount > 2
        {
            cell.SeeMoreButton.alpha = 1
            cell.SeeMoreButton.setTitle(button_Title, for: .normal)
            cell.AboutTextBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[4][BOTTOM_SPACE]!)!) + 8
            cell.SeeMoreButton.addTarget(self, action: #selector(infoRowIncrementor), for: .touchUpInside)
        }
        else
        {
            cell.SeeMoreButton.alpha = 0
        }
        
        cell.SpecializationHeaderLabel.text = self.clinicInfoArray[7][TEXT]
        cell.SpecializationHeaderLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.SpecializationHeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.SpecializationHeaderLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[7][BOTTOM_SPACE]!)!)
        cell.SpecializationIconImageView.image = UIImage(named: self.clinicInfoArray[7][IMAGE]!)
        
        cell.SpecializationTextLabel.text = self.clinicInfoArray[8][TEXT]
        cell.SpecializationTextLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.SpecializationTextLabel.numberOfLines = 0
        cell.SpecializationTextLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[8][BOTTOM_SPACE]!)!)
        
        cell.ServiceHeaderLabel.text = self.clinicInfoArray[9][TEXT]
        cell.ServiceHeaderLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.ServiceHeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.ServiceHeaderLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[9][BOTTOM_SPACE]!)!)
        cell.ServiceIconImageView.image = UIImage(named: self.clinicInfoArray[9][IMAGE]!)
        
        cell.ServiceTextLabel.text = self.clinicInfoArray[10][TEXT]
        cell.ServiceTextLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.ServiceTextLabel.numberOfLines = 0
        cell.ServiceTextLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[10][BOTTOM_SPACE]!)!)
        
        cell.FacilitiesHeaderLabel.text = self.clinicInfoArray[11][TEXT]
        cell.FacilitiesHeaderLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.FacilitiesHeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.FacilitiesHeaderLabelBottomConstraint.constant = CGFloat(Int(self.clinicInfoArray[11][BOTTOM_SPACE]!)!)
        cell.FacilitiesIconImageView.image = UIImage(named: self.clinicInfoArray[11][IMAGE]!)
        
        cell.FacilitiesTextLabel.text = self.clinicInfoArray[12][TEXT]
        cell.FacilitiesTextLabel.numberOfLines = 0
        cell.FacilitiesTextLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        return cell
    }
    
    func getClinicDoctorsTableViewCell(_ indexpath:IndexPath) -> ClinicDoctorsTableViewCell {
        let cell: ClinicDoctorsTableViewCell = Bundle.main.loadNibNamed("ClinicDoctorsTableViewCell", owner: nil, options: nil)![0] as! ClinicDoctorsTableViewCell
        
        //cell.ClinicDoctorsDelegate = self
        
        cell.DoctorNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.SpecializationLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.ExperienceLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.LanguageLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.FeeLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.DoctorNameLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.SpecializationLabel.textColor = UIColor.black
        cell.ExperienceLabel.textColor = UIColor.black
        cell.LanguageLabel.textColor = UIColor.black
        cell.FeeLabel.textColor = UIColor.black
        
        cell.BookButton.layer.cornerRadius = 3
        cell.BookButton.layer.masksToBounds = true
        
        cell.BookButton.tag = indexpath.row
        cell.ViewProfileButton.tag = indexpath.row
        cell.FavouriteButton.tag = indexpath.row
        
        cell.BookButton.addTarget(self, action: #selector(gotoBookAppointment(_:)), for: .touchUpInside)
        cell.ViewProfileButton.addTarget(self, action: #selector(gotoViewProfile(_:)), for: .touchUpInside)
        cell.FavouriteButton.addTarget(self, action: #selector(addDeleteFavorite(_:)), for: .touchUpInside)
        
        cell.DoctorProfileImageView.layer.cornerRadius = 35
        cell.DoctorProfileImageView.layer.masksToBounds = true
        
        if let doctorList = self.infoArray["DoctorList"] as? [Dictionary<String, Any>] {
            let item = doctorList[indexpath.row]
            
            if let value = item["Image"] as? String,
                let url = URL(string: value) {
                cell.DoctorProfileImageView.af_setImage(withURL: url)
            }
            
            if let value = item["Name"] as? String {
                 cell.DoctorNameLabel.text = value
            }
            
            if let value = item["Speciality"] as? String, value != "" {
                cell.SpecializationLabel.text = value
                cell.SpecializationIconImageView.image = UIImage(named: "ic_specialization_gray")
                cell.SpecializationIconImageView.alpha = 1
                cell.SpecializationLabelBottomConstraint.constant = 5
            } else {
                cell.SpecializationLabel.text = ""
                cell.SpecializationIconImageView.alpha = 0
                cell.SpecializationLabelBottomConstraint.constant = 0
            }
            
            if let value = item["Expereience"] as? Int, value != 0 {
                
                cell.ExperienceLabel.text = "\(value) year(s) of Experience"
                cell.ExperienceIconImageView.image = UIImage(named: "ic_briefcase_gray")
                cell.ExperienceIconImageView.alpha = 1
                cell.ExperienceLabelBottomConstraint.constant = 5
            } else {
                
                cell.ExperienceLabel.text = ""
                cell.ExperienceIconImageView.alpha = 0
                cell.ExperienceLabelBottomConstraint.constant = 0
            }
            
            if let value = item["Language"] as? String, value != "" {
                
                cell.LanguageLabel.text = value
                cell.LanguageIconImageView.image = UIImage(named: "ic_language_gray")
                cell.LanguageIconImageView.alpha = 1
            } else {
                cell.LanguageLabel.text = ""
                cell.LanguageIconImageView.alpha = 0
            }
            
            if let value = item["Fees"] as? Int, value != 0 {
                
                cell.FeeLabel.text = "₹ \(value)"
                cell.FeeLabelBottomConstraint.constant = 5
            } else {
                
                cell.FeeLabel.text = ""
                cell.FeeLabelBottomConstraint.constant = 0
            }
            
            if let value = item["DrFavourite"] as? String, value == "Y" {
                cell.FavouriteButton.setImage(UIImage(named: "ic_heart_like"), for: .normal)
            } else {
                cell.FavouriteButton.setImage(UIImage(named: "ic_heart_unlike"), for: .normal)
            }
            
        }
        
        return cell
    }
    
    func getSegmentHeaderViewCell() -> SegmentedControlTableViewCell {
        
        let cell: SegmentedControlTableViewCell = Bundle.main.loadNibNamed("SegmentedControlTableViewCell", owner: nil, options: nil)![0] as! SegmentedControlTableViewCell
        cell.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        
        self.setupSegmentControl(cell.segmentedControl)
        
        return cell
    }
    
    func setupSegmentControl(_ segmentedControl: MRSegmentedControl) {
        
        segmentedControl.sectionTitles = ["Clinic", "Doctors"]
        segmentedControl.selectedSegmentIndex = self.selectedSegmentIndex
        
        segmentedControl.indexChangeBlock = { (index) in
            
            self.selectedSegmentIndex = index
            
            switch index {
            case 0:
                
                break
                
            case 1:
                
                break
                
            default:
                break
            }
            
            self.tableView.reloadData()
            var isAllow = false
            if self.selectedSegmentIndex == 0 {
                if self.clinicInfoArray.count != 0 {
                    isAllow = true
                }
            } else {
                if let value = self.infoArray["DoctorList"] as? [Dictionary<String, Any>], value.count != 0 {
                    isAllow = true
                } else {
                    AlertHelper.shared.showAlert(message: "No doctors found", controller: self)
                }
            }
            if isAllow {
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                }) { (completion) in
                    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 1), at: .top, animated: true)
                    }) { (completion) in
                    }
                }
            }
        }
        
        segmentedControl.backgroundColor = UIColor.init(netHex: APP_BG_COLOR_LIGHT)
        segmentedControl.selectionIndicatorColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.init(netHex: APP_PRIMARY_COLOR), NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.init(name: APP_FONT_NAME_BOLD, size: 15) as Any]
        segmentedControl.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.black, NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14) as Any]
    }
    
    func parseClinicData() {
        
        var dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:""]
        
        if let value = self.infoArray["HospitalName"] as? String {
            dict = [IMAGE: "ic_hospital_blue",TEXT: value,BOTTOM_SPACE: "8"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["ClinicAddress"] as? String {
            dict = [IMAGE: "",TEXT: value,BOTTOM_SPACE: "8"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["ClinicContactNo"] as? String, value != "" {
            dict = [IMAGE:"ic_mobile_blue",TEXT:value,BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
        } else {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
            
        }
        
        if let lat = self.infoArray["Latitude"] as? Double,
            let long = self.infoArray["Longitude"] as? Double {
            dict = [IMAGE:"ic_map_location_blue","lat":"\(lat)","long":"\(long)",BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["ClinicTimings"] as? [Dictionary<String, Any>], value.count != 0 {
            var text = ""
            
            for i in 0..<value.count {
                if i != value.count - 1 {
                    text.append("\(value[i]["Timing"] as? String ?? "")\n")
                } else {
                    text.append(value[i]["Timing"] as? String ?? "")
                }
            }
            dict = [IMAGE:"ic_clock",TEXT:text,BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
        } else {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["AboutClinic"] as? String, value != "" {
            
            dict = [IMAGE:"ic_about_blue",TEXT:"About",BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            
            dict = [IMAGE:"",TEXT:value,BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
        } else {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
            
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["ClinicSpeciality"] as? [String], value.count != 0 {
            var text = ""
            
            for i in 0..<value.count {
                if i != value.count - 1 {
                    text.append("\(value[i])\n\n")
                } else {
                    text.append(value[i])
                }
            }
            dict = [IMAGE:"ic_specialization_blue",TEXT:"Specialization",BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            dict = [IMAGE:"",TEXT:text,BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            
        } else {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
            
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["Services"] as? [Dictionary<String, Any>], value.count != 0 {
            var text = ""
            
            for i in 0..<value.count {
                if i != value.count - 1 {
                    text.append("\(value[i]["Detail"] as? String ?? "")\n")
                } else {
                    text.append(value[i]["Detail"] as? String ?? "")
                }
            }
            dict = [IMAGE:"ic_services_blue",TEXT:"Service",BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            dict = [IMAGE:"",TEXT:text,BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            
        } else {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
            
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
        }
        
        if let value = self.infoArray["Facilities"] as? [Dictionary<String, Any>], value.count != 0 {
            var text = ""
            
            for i in 0..<value.count {
                if i != value.count - 1 {
                    text.append("\(value[i]["Detail"] as? String ?? "")\n")
                } else {
                    text.append(value[i]["Detail"] as? String ?? "")
                }
            }
            dict = [IMAGE:"ic_facilities_blue",TEXT:"Facilities",BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            dict = [IMAGE:"",TEXT:text,BOTTOM_SPACE:"8"]
            self.clinicInfoArray.append(dict)
            
        } else {
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
            
            dict = [IMAGE:"",TEXT:"",BOTTOM_SPACE:"0"]
            self.clinicInfoArray.append(dict)
        }
        
    }
    
    @objc func sloadView(lat: Double,long: Double,View: GMSMapView) {
        
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        View.camera = GMSCameraPosition(target: coord, zoom: 14, bearing: 0, viewingAngle: 0)
        
        let marker = GMSMarker()
        
        marker.appearAnimation = GMSMarkerAnimation(rawValue: 1)!
        marker.position = coord
        marker.map = View
    }
    
    @objc func infoRowIncrementor() {
        if infoRows == 2 {
            button_Title = "See less"
            infoRows = 0
        } else {
            button_Title = "See more"
            infoRows = 2
        }
        
       self.tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .automatic)
    }
    
    @objc func gotoBookAppointment(_ sender: UIButton) {
        
        let viewController = AppController.shared.getCreateAppointmentViewController()
        if let doctorList = self.infoArray["DoctorList"] as? [Dictionary<String, Any>] {
            let item = doctorList[sender.tag]
            
            viewController.DoctorId = item["ID"] as? Int ?? 0
            viewController.DoctorName = item["Name"] as? String ?? ""
        }
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoViewProfile(_ sender: UIButton) {
  
        let viewController = AppController.shared.getDoctorProfileViewController()
        if let doctorList = self.infoArray["DoctorList"] as? [Dictionary<String, Any>] {
            let item = doctorList[sender.tag]
            
            viewController.DoctorId = item["ID"] as? Int ?? 0
        }
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    @objc func addDeleteFavorite(_ sender: UIButton) {
        
        self.loadFavouriteChangeAPICall(sender.tag)
    }
    
}
