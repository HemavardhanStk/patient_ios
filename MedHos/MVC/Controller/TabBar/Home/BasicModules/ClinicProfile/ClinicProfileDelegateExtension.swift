//
//  ClinicProfileDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import FSPagerView

extension ClinicProfileViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.infoArray.count == 0 ? 0 : 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
            return 60
        }
        return screenHeight/4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            return self.getSegmentHeaderViewCell()
        }
        
        return self.pagerView
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 && self.infoArray.count != 0
        {
            if self.selectedSegmentIndex == 0
            {
                return 1
            }
            else if let value = self.infoArray["DoctorList"] as? [Dictionary<String, Any>],
                self.selectedSegmentIndex == 1 {
                return value.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1
        {
            if self.selectedSegmentIndex == 0
            {
                return getClinicProfileCell(indexPath)
            }
            else if self.selectedSegmentIndex == 1
            {
                return getClinicDoctorsTableViewCell(indexPath)
            }
        }
        
        return UITableViewCell()
    }
    
}

extension ClinicProfileViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        
        if let value = self.infoArray["ClinicPhotos"] as? [String] {
            return value.count
        }
        
        return 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: "loading")!
        
        if let value = self.infoArray["ClinicPhotos"] as? [String],
            let url = URL(string: value[index]) {    
            cell.imageView?.af_setImage(withURL: url)
        }
        
        cell.imageView?.layer.cornerRadius = 0
        cell.imageView?.layer.masksToBounds = false
        return cell
    }
}

//extension ClinicProfileViewController: ClinicDoctorsTableViewCellDelegate {
//    
//}
