//
//  ClinicProfileViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 26/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps
import FSPagerView

class ClinicProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let pagerView = FSPagerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height/12, width: 0, height: 0))
    
    var infoArray = Dictionary<String, Any>()
//    var ClinicProfileArray : ProfileDetails? = nil
    
    var clinicInfoArray = [Dictionary<String, String>]()
    
    var clinicId = "0"
    
    let BOTTOM_SPACE = "BottomSpace"
    var button_Title = "See more"
    var infoRows = 2
    var selectedSegmentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "Clinic Profile", controller: self)
        
        self.loadInfoData()
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "ClinicProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ClinicProfileTableViewCell")
        self.tableView.register(UINib(nibName: "ClinicDoctorsTableViewCell", bundle: nil), forCellReuseIdentifier: "ClinicDoctorsTableViewCell")
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoData() {
        
        self.getClinicDetailsAPICall()
        self.topadviewer()
    }
    
    func topadviewer() {
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.automaticSlidingInterval = 2.0
        self.pagerView.interitemSpacing = 30
        self.pagerView.itemSize = CGSize(width: screenWidth, height: screenHeight/4)
        self.pagerView.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
    }
    
    @objc func rightBarButtonTapped() {
        
    }
    
}
