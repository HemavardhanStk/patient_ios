//
//  ClinicDoctorsTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 25/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

//protocol ClinicDoctorsTableViewCellDelegate : class {
//    func ClinicDoctorsCellButtondidPressButton(_ Button: UIButton)
//}


class ClinicDoctorsTableViewCell: UITableViewCell {

    //var ClinicDoctorsDelegate : ClinicDoctorsTableViewCellDelegate?
    
    @IBOutlet weak var DoctorProfileImageView: UIImageView!
    @IBOutlet weak var SpecializationIconImageView: UIImageView!
    @IBOutlet weak var ExperienceIconImageView: UIImageView!
    @IBOutlet weak var LanguageIconImageView: UIImageView!
    
    @IBOutlet weak var DoctorNameLabel: UILabel!
    @IBOutlet weak var SpecializationLabel: UILabel!
    @IBOutlet weak var ExperienceLabel: UILabel!
    @IBOutlet weak var LanguageLabel: UILabel!
    @IBOutlet weak var FeeLabel: UILabel!
    
    @IBOutlet weak var ViewProfileButton: UIButton!
    @IBOutlet weak var FavouriteButton: UIButton!
    @IBOutlet weak var BookButton: UIButton!
    
    @IBOutlet weak var SpecializationLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var ExperienceLabelBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ViewProfileButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var FeeLabelBottomConstraint: NSLayoutConstraint!
    
    @IBAction func ButtonCallFunc(_ sender: UIButton) {
        
        //ClinicDoctorsDelegate?.ClinicDoctorsCellButtondidPressButton(sender)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
