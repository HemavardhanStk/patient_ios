//
//  ClinicProfileTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 25/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

class ClinicProfileTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ClinicProfileImageView: UIImageView!
    
    
    @IBOutlet weak var ClinicNameLabel: UILabel!
    @IBOutlet weak var ClinicAddressLabel: UILabel!
    @IBOutlet weak var MobileNumberLabel: UILabel!
    @IBOutlet weak var TimingsLabel: UILabel!
    @IBOutlet weak var AboutHeaderLabel: UILabel!
    @IBOutlet weak var AboutTextLabel: UILabel!
    @IBOutlet weak var SpecializationHeaderLabel: UILabel!
    @IBOutlet weak var SpecializationTextLabel: UILabel!
    @IBOutlet weak var ServiceHeaderLabel: UILabel!
    @IBOutlet weak var ServiceTextLabel: UILabel!
    @IBOutlet weak var FacilitiesHeaderLabel: UILabel!
    @IBOutlet weak var FacilitiesTextLabel: UILabel!
    
    @IBOutlet weak var ClinicIconImageView: UIImageView!
    @IBOutlet weak var MobileNumberIconImageView: UIImageView!
    @IBOutlet weak var MapIconImageView: UIImageView!
    @IBOutlet weak var TimingsIconImageView: UIImageView!
    @IBOutlet weak var AboutIconImageView: UIImageView!
    @IBOutlet weak var SpecializationIconImageView: UIImageView!
    @IBOutlet weak var ServiceIconImageView: UIImageView!
    @IBOutlet weak var FacilitiesIconImageView: UIImageView!
    
    @IBOutlet weak var SeeMoreButton: UIButton!
    @IBOutlet weak var MapView: GMSMapView!
    
    @IBOutlet weak var ClinicNameLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var ClinicProfileBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var MobileNumberLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var MapViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var TimingsLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var AboutHeaderLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var AboutTextBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var SpecializationHeaderLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var SpecializationTextLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var ServiceHeaderLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var ServiceTextLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var FacilitiesHeaderLabelBottomConstraint: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
