//
//  CalenderCollectionViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class CalenderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var SelectedDateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
