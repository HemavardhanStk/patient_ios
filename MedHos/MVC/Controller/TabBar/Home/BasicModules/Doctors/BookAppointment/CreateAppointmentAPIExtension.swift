//
//  CreateAppointmentAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension CreateAppointmentViewController {

    func getAllHospitalsAPICall() {
        
        let user = Session.shared.getUserInfo()
        var AppDate = ""
        
        if let selectedDate = self.calendarInfoArray[self.selectedDateIndex][self.NEXT_DATE] as? Date {
            let date = Helper.shared.getString(selectedDate, format: "MM/dd/yyyy")
            AppDate = date
        }
        
        let params = ["DocId":"\(self.DoctorId)",
                      "UserID":user.id,
                      "HosId": "0",
                      "AppDate":AppDate,
                      "Language":"English"]
        
        
        HttpManager.shared.loadAPICall(path: PATH_ViewBookingAllHospitals, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                self.infoArray = response
                if self.noteLabel.text == "" && self.infoArray["DrAutoBooking"] as? String == "N" {
                    self.noteLabel.text = self.infoArray["BookingAlertMessage"] as? String
                    self.noteLabelBottomConstraint.constant = 5
                }
                self.tableView.reloadData()
            } else {
                self.infoArray.removeAll()
                self.tableView.reloadData()
            }
        }) {}
        
    }
    
    
}
