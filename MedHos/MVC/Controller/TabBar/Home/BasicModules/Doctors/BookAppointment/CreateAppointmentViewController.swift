//
//  CreateAppointmentViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 15/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class CreateAppointmentViewController: UIViewController {

    @IBOutlet weak var calenderCollectionView: UICollectionView!
    @IBOutlet weak var AvailableLabel: UILabel!
    @IBOutlet weak var NotAvailableLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var noteLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String, Any>()
    //var sinfoArray : DoctorsHosptialTimings? = nil
    var DoctorId = 0
    var DoctorName = ""
    let NEXT_DATE = "Next Date"
    let DAY_STR = "Day Str"
    let DATE_STR = "Date Str"
    let MONTH_STR = "Month Str"
    let Morning = "Morning"
    let AfterNoon = "AfterNoon"
    let Evening = "Evening"
    let Night = "Night"
    let STATUS_AVAILABLE = "A"
    let STATUS_BLOCKED = "X"

    var DoctorImageUrl = ""
    var DoctorSpecialization = ""
    var HospitalName = ""
    var HospitalAddress = ""
    var HospitalNo = ""
    var TimeString = ""
    var AppointmentDate = ""
    var SessionTag: Int = 0
    var BookingAmountValue = ""
    
    let calendarIndex = 33
    var calendarInfoArray = [Dictionary<String, Any>]()
    var selectedDateIndex = 0
    let tableViewTag = 22
    let headerViewHeight:CGFloat = 80
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.DoctorName, controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.noteLabel.text = ""
        self.noteLabelBottomConstraint.constant = 0
        let notAvailable = "Not Available"
        let attributedText = NSMutableAttributedString(string: notAvailable)
        let range = (attributedText.string as NSString).range(of: notAvailable)
        attributedText.addAttributes([NSAttributedString.Key.strikethroughStyle : 1, NSAttributedString.Key.strikethroughColor : UIColor.red], range: range)
        self.NotAvailableLabel.attributedText = attributedText
        self.NotAvailableLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        self.AvailableLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.tag = self.tableViewTag
        self.tableView.register(UINib(nibName: "BookAppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "BookAppointmentTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        
        self.setupCalendarCollectionView()
        self.getAllHospitalsAPICall()
    }
   
    func gotoPreBooking() {
        
        let viewController = AppController.shared.getPreBookingViewController()
        viewController.DoctorImageUrl = self.DoctorImageUrl
        viewController.DoctorId = "\(self.DoctorId)"
        viewController.DoctorName = self.DoctorName
        viewController.DoctorSpecialization = self.DoctorSpecialization
        viewController.HospitalName = self.HospitalName
        viewController.HospitalAddress = self.HospitalAddress
        viewController.HospitalNo = self.HospitalNo
        viewController.AppointmentDate = Helper.shared.convert(date: self.AppointmentDate, from: "MM/dd/yyyy", to: "dd MMM yyyy")
        viewController.TimeString = self.TimeString
        viewController.SessionTag = self.SessionTag
        viewController.BookingAmountValue = self.BookingAmountValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
