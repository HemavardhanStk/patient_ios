//
//  BookAppointmentTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class BookAppointmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var hospitalView: UIView!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var hospitalAddressLabel: UILabel!
    @IBOutlet weak var expandLessIcon: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
