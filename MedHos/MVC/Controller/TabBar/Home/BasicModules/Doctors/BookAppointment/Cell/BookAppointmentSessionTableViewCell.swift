//
//  BookAppointmentSessionTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class BookAppointmentSessionTableViewCell: UITableViewCell {

    @IBOutlet weak var sessionStatusView: UIView!
    @IBOutlet weak var sessionStatusViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sessionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
