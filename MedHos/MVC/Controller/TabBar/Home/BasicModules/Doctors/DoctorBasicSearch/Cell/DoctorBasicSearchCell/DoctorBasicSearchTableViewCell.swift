//
//  DoctorBasicSearchTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 30/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class DoctorBasicSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var doctorView: UIView!
    
    @IBOutlet weak var DoctorNameLabel: UILabel!
    @IBOutlet weak var DoctorSpecialitiesLabel: UILabel!
    @IBOutlet weak var DoctorExperienceLabel: UILabel!
    @IBOutlet weak var DoctorFeeLabel: UILabel!
    @IBOutlet weak var DoctorLanguageLabel: UILabel!
    @IBOutlet weak var RatingLabel: UILabel!
    @IBOutlet weak var RatingPercentageLabel: UILabel!
    @IBOutlet weak var ClinicNameLabel: UILabel!
    @IBOutlet weak var ClinicAddressLabel: UILabel!
    
    @IBOutlet weak var DoctorProfileImageView: UIImageView!
    @IBOutlet weak var Button1ImageView: UIImageView!
    
    @IBOutlet weak var Button1: UIButton!
    @IBOutlet weak var BookButton: UIButton!
    
    @IBOutlet weak var RatingLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var Button1TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var Button1BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var Button1HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var LineView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
