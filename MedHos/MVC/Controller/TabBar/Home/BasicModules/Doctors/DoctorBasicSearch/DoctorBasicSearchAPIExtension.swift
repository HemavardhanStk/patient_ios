//
//  DoctorBasicSearchAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 24/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorBasicSearchViewController {
    
    func loadDoctorBasicSearchAPICall(_ isBackgroundCall: Bool = false) {
        
        self.pageNumber += 1
        
        let user = Session.shared.getUserInfo()
        
        var params = ["UserNo"                   : user.id,
                      "Language"                 : "English",
                      "Latitude"                 : self.Latitude,
                      "Longitude"                : self.Longitude,
                      "SearchCity"               : self.Location,
                      "SearchName"               : self.searchName,
                      "SearchType"               : self.searchType,
                      "SearchArea"               : "",
                      "pageNumber"               : self.pageNumber,
                      "FilterStatus"             : true,
                      "GenderFilter"             : self.genderArray,
                      "LanguageFilter"           : self.selectedLanguagesArray,
                      "FeeSelection"             : self.feesArray,
                      "ExperienceSelection"      : self.experienceArray,
                      "RatingSelection"          : [self.rating],
                      "AppointmentType"          : "Instant",
                      "CategoryFilter"           : self.categoryArray,
                      "GenderAllSelected"        : self.genderAll,
                      "LanguageAllSelected"      : self.selectedLanguagesAll,
                      "ConsultingAllSelected"    : self.searchTypeAll,
                      "ExperienceAllSelected"    : self.experienceAll,
                      "RatingAllSelected"        : false,
                      "CULatitude"               : self.Latitude,
                      "CULongitude"              : self.Longitude,
                      "NearBySearch"             : "N",
                      "SortOrder"                : self.sortOrder,
                      "City"                     : 0,
                      "Speciality"               : 0,
                      "Pincode"                  : 0] as [String : Any]
        
        if var value = params["RatingSelection"] as? [String], value[0] == "0" {
            value.removeAll()
            params["RatingSelection"] = value
        }
        
        HttpManager.shared.loadAPICall(path: PATH_BasicSearch, params: params, httpMethod: .post,controller: self, isBackgroundCall: isBackgroundCall, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response,controller: self, isBackgroundCall: isBackgroundCall) {
                self.TotalNumberOfPages = response["TotalPages"] as? Int ?? 0
                if let value = response["AvlDoctors"] as? [Dictionary<String, Any>] {
                    self.infoArray.append(contentsOf: value)
                self.tableView.reloadData()
                self.tableView.tableFooterView?.isHidden = true
                self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
               }
            }
        }) {
            self.tableView.tableFooterView?.isHidden = true
            self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        }
    }
    
    func loadBookHomeVisit(_ dict: [Dictionary<String,String>]) {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["DrClinicMobile"      : dict[0][TEXT]!,
                      "Language"            : "English",
                      "PreferredTime"       : dict[3][TEXT]!,
                      "Reason"              : dict[1][TEXT]!,
                      "UserMobileorPhone"   : user.phone,
                      "UserName"            : user.name,
                      "VisitingDate"        : dict[2][TEXT]!,
                      "DoctorNo"            : dict[4][TEXT]!,
                      "HospitalNo"          : dict[5][TEXT]!,
                      "UserNo"              : user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_HomeVisitBooking, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                let message = response["Message"] as? String ?? ""
              AlertHelper.shared.showAlert(message: message, controller: self)
            }
        }) {}   
    }
     
    func loadEmergencyCare(_ mobileNumber: String, clinicId: String, reason: String) {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["DrClinicMobile"      : mobileNumber,
                      "Language"            : "English",
                      "PreferredTime"       : "",
                      "Reason"              : reason,
                      "UserMobileorPhone"   : user.phone,
                      "UserName"            : user.name,
                      "DoctorNo"            : "0",
                      "HospitalNo"          : clinicId,
                      "UserNo"              : user.id]
       
        HttpManager.shared.loadAPICall(path: PATH_EmergencyBooking, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                let message = response["Message"] as? String ?? ""
                AlertHelper.shared.showAlert(message: message, controller: self)
            }
        }) {}
    }
}
