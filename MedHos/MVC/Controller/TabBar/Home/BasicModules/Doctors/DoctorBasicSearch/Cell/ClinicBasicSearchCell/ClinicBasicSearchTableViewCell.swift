//
//  ClinicBasicSearchTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ClinicBasicSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var clinicView: UIView!
    
    @IBOutlet weak var sponserImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var sponserImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var clinicNameLabel: UILabel!
    @IBOutlet weak var clinicAddressLabel: UILabel!
    @IBOutlet weak var clinicSpecialityLabel: UILabel!
    @IBOutlet weak var clinicAvailablityLabel: UILabel!
    @IBOutlet weak var clinicTimingsLabel: UILabel!
    
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    
    @IBOutlet weak var buttonOneHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTwoWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonOneImageView: UIImageView!
    @IBOutlet weak var buttonTwoImageView: UIImageView!
    
    @IBOutlet weak var bookButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
