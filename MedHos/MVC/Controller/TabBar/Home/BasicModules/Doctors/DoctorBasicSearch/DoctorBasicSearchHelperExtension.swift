//
//  DoctorBasicSearchHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 24/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorBasicSearchViewController {
    
    func getDoctorCell(_ indexPath: IndexPath) -> DoctorBasicSearchTableViewCell {
        
        let cell: DoctorBasicSearchTableViewCell = Bundle.main.loadNibNamed("DoctorBasicSearchTableViewCell", owner: nil, options: nil)![0] as! DoctorBasicSearchTableViewCell
        
        cell.backgroundColor = .clear
        cell.doctorView.setRoundedCorner()
        cell.DoctorProfileImageView.setRoundedCorner()
        
        cell.DoctorNameLabel.font               = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.DoctorSpecialitiesLabel.font       = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.DoctorExperienceLabel.font         = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.DoctorFeeLabel.font                = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.DoctorLanguageLabel.font           = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.RatingLabel.font                   = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.RatingPercentageLabel.font         = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.ClinicAddressLabel.font            = UIFont(name: APP_FONT_NAME_BOLD, size: 13)

        cell.DoctorNameLabel.textColor          = UIColor.black
        cell.DoctorSpecialitiesLabel.textColor  = UIColor.darkGray
        cell.DoctorExperienceLabel.textColor    = UIColor.darkGray
        cell.DoctorFeeLabel.textColor           = UIColor.darkGray
        cell.DoctorLanguageLabel.textColor      = UIColor.darkGray
        cell.RatingLabel.textColor              = UIColor.white
        cell.RatingPercentageLabel.textColor    = UIColor.black
        cell.ClinicAddressLabel.textColor       = UIColor.darkGray
        
        cell.RatingLabel.setRoundedCorner(5)
        cell.RatingLabel.backgroundColor = UIColor(netHex: APP_GREEN)
        
        cell.Button1.setRoundedCorner(5)
        cell.Button1.setTitle("Book Home Visit", for: .normal)
        cell.Button1.setTitleColor(UIColor.black, for: .normal)
        cell.Button1.layer.borderWidth = 1
        cell.Button1.layer.borderColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
        cell.Button1.titleLabel?.font =  UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        
        cell.Button1.tag = indexPath.row
        cell.Button1.addTarget(self, action: #selector(gotoBookHomeVisit), for: .touchUpInside)
        
        cell.BookButton.setPrimaryButtonStyle("Book")
        cell.BookButton.setRoundedCorner(5)
        
        cell.BookButton.tag = indexPath.row
        cell.BookButton.addTarget(self, action: #selector(gotoBookAppointment), for: .touchUpInside)
        
        var name = ""
        var specialities = ""
        var experience = ""
        var fees = ""
        var language = ""
        var ratingValue = ""
        var ratingPercentage = ""
        var clinicName = NSAttributedString()
        var address = ""
        
        let item = self.infoArray[indexPath.row]
        
        
        if let value1 = item["Name"] as? String,
            let value2 = item["Degree"] as? String {
            name = "\(value1), \(value2)"
        }
        
        specialities        = item["Speciality"] as? String ?? ""
        
        if let value = item["Expereience"] as? Int , value != 0 {
            
            if value == 1 {
                experience = "\(value) year of Experience"
            } else {
                experience = "\(value) years of Experience"
            }
        }
        
        if let value = item["Fees"] as? Int , value != 0 {
            fees = "₹ \(value)"
        }
        
        language            = item["Language"] as? String ?? ""
        
        if let value = item["DrRating"] as? Double, value != 0.0 {
            ratingValue = "  \(value) ★  "
        }
        
        if let value1 = item["DrRecPercentage"] as? Float,
            let value2 = item["DrRecommendedCount"] as? Int,
            value1 != 0.0, value2 != 0 {
            
            if value2 == 1 {
                ratingPercentage    = "\(Int(value1))% (\(value2) Vote)"
            } else {
                ratingPercentage    = "\(Int(value1))% (\(value2) Votes)"
            }
        }
        
        if let value = item["HosDetail"] as? [Dictionary<String, Any>] {
            
            var attStringOne = NSMutableAttributedString()
            var attStringTwo = NSMutableAttributedString()
            
            if let stringOne = value[0]["HospitalName"] as? String {
                
                attStringOne = NSMutableAttributedString(string: stringOne)
                attStringOne.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_BOLD, size: 15)!, range: NSMakeRange(0, stringOne.count))
                attStringOne.setColor(color: UIColor.black, forText: stringOne)
            }
            address = value[0]["AL4_Area"] as? String ?? ""
            
            if value.count > 1 {
                
                let stringTwo = " and \(value.count - 1) More Clinic(s)"
                attStringTwo = NSMutableAttributedString(string: stringTwo)
                attStringTwo.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_BOLD, size: 15)!, range: NSMakeRange(0, stringTwo.count))
                attStringTwo.setColor(color: UIColor(netHex: APP_LIGHT_BLUE_COLOR), forText: stringTwo)
                
                attStringOne.append(attStringTwo)
            }
            
            clinicName = attStringOne
            
        }
        
        if let urlString = item["Image"] as? String ,
            let url = URL(string: urlString) {
            
            cell.DoctorProfileImageView.af_setImage(withURL: url)
        }
        
        cell.DoctorNameLabel.text           = name
        cell.DoctorSpecialitiesLabel.text   = specialities
        cell.DoctorExperienceLabel.text     = experience
        cell.DoctorFeeLabel.text            = fees
        cell.DoctorLanguageLabel.text       = language
        cell.RatingLabel.text               = ratingValue
        cell.RatingPercentageLabel.text     = ratingPercentage
        cell.ClinicNameLabel.attributedText = clinicName
        cell.ClinicAddressLabel.text        = address
        
//        cell.RatingLabelHeightConstraint.constant = ratingValue == "" ? 0 : 25
        
        if ratingValue != "" || item["Image"] as? String != "" {
            cell.RatingLabelHeightConstraint.constant = 25
        } else {
            cell.RatingLabelHeightConstraint.constant = 0
        }
        
        if let value = item["HomeVisit"] as? String,
            value == "Y" {
            
            cell.Button1HeightConstraint.constant = 30
            cell.Button1ImageView.image = #imageLiteral(resourceName: "ic_hospital_blue")
            
        } else {
            
            cell.Button1HeightConstraint.constant = 0
        }
        
        return cell
    }
    
    func getClinicCell(_ indexPath: IndexPath) -> ClinicBasicSearchTableViewCell {
        
        let cell: ClinicBasicSearchTableViewCell = Bundle.main.loadNibNamed("ClinicBasicSearchTableViewCell", owner: nil, options: nil)![0] as! ClinicBasicSearchTableViewCell
        
        cell.backgroundColor = .clear
        cell.clinicView.setRoundedCorner()
        cell.logoImageView.setRoundedCorner()
        
        cell.clinicNameLabel.font          = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.clinicAddressLabel.font       = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.clinicSpecialityLabel.font    = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.clinicAvailablityLabel.font   = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.clinicTimingsLabel.font       = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        
        cell.clinicNameLabel.textColor          = UIColor.black
        cell.clinicAddressLabel.textColor       = UIColor.darkGray
        cell.clinicSpecialityLabel.textColor    = UIColor.darkGray
        cell.clinicAvailablityLabel.textColor   = UIColor(netHex: APP_GREEN)
        cell.clinicTimingsLabel.textColor       = UIColor.darkGray
        
        cell.clinicSpecialityLabel.numberOfLines = 0
        cell.clinicTimingsLabel.numberOfLines    = 0
        
        cell.buttonOne.layer.borderWidth = 1
        cell.buttonTwo.layer.borderWidth = 1
        
        cell.buttonOne.setRoundedCorner(5)
        cell.buttonTwo.setRoundedCorner(5)
        
        cell.buttonOne.layer.borderColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
        cell.buttonTwo.layer.borderColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
        
        cell.buttonOne.setTitleColor(UIColor.black, for: .normal)
        cell.buttonTwo.setTitleColor(UIColor.black, for: .normal)
        
        cell.buttonOne.titleLabel?.font =  UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.buttonTwo.titleLabel?.font =  UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        
        cell.buttonOne.tag = 1000 + indexPath.row
        cell.buttonTwo.tag = 1000 + indexPath.row
        
        cell.bookButton.setPrimaryButtonStyle("Book")
        cell.bookButton.setRoundedCorner(5)
        cell.bookButton.tag = indexPath.row
        cell.bookButton.addTarget(self, action: #selector(gotoClinicBooking), for: .touchUpInside)
        
        var name = ""
        var address = ""
        var speciality = ""
        var availablity = ""
        var timings = ""
        
        let item = self.infoArray[indexPath.row]
        
        if let urlString = item["ClinicImage"] as? String ,
            let url = URL(string: urlString) {
            
            cell.logoImageView.af_setImage(withURL: url)
        }
        
        if let value = item["displaytype"] as? String, value == "COMMONSPONSOR" {
            cell.sponserImageView.backgroundColor = UIColor.green
            cell.sponserImageViewHeightConstraint.constant = 20
        } else {
            cell.sponserImageViewHeightConstraint.constant = 0
        }
        
        name = item["ClinicName"] as? String ??  ""
        
        if let area = item["ClinicAreaName"] as? String,
            let city = item["ClinicCityName"] as? String,
            let pincode = item["ClinicPincode"] as? String {
            
            address = "\(area), \(city) - \(pincode)"
        }
        
        speciality = item["ClinicSpeciality"] as? String ?? ""
        
        if let value = item["Is24HourService"] as? String , value == "Y" {
            availablity = "24 Hours Services Available "
        }
        
        if let value = item["ClinicTimings"] as? [Dictionary<String, String>] {
            
            for i in 0..<value.count {
                if i == value.count - 1 {
                    timings.append(value[i]["Timing"] ?? "")
                } else {
                    timings.append("\(value[i]["Timing"] ?? "")\n")
                }
            }
        }
        
        cell.clinicNameLabel.text = name
        cell.clinicAddressLabel.text = address
        cell.clinicSpecialityLabel.text = speciality
        cell.clinicAvailablityLabel.text = availablity
        cell.clinicTimingsLabel.text = timings
        
        if let value1 = item["ClinicHomeVAvailable"] as? String,
            let value2 = item["EmergencyCareAvailable"] as? String,
            value1 == "Y", value2 == "Y" {
            
            cell.buttonOneHeightConstraint.constant = 30
            cell.buttonTwoWidthConstraint.constant = screenWidth/2 - 15//cell.buttonOne.frame.width
            cell.buttonOne.setTitle("Book Home Visit", for: .normal)
            cell.buttonTwo.setTitle("Emergency Care", for: .normal)
            
            cell.buttonOneImageView.image = #imageLiteral(resourceName: "ic_hospital_blue")
            cell.buttonTwoImageView.image = #imageLiteral(resourceName: "ic_notification_icon")
            
            cell.buttonOne.addTarget(self, action: #selector(getClinicBookHomeVisit(_:)), for: .touchUpInside)
            cell.buttonTwo.addTarget(self, action: #selector(getEmergencyCare), for: .touchUpInside)
            
        } else if let value = item["ClinicHomeVAvailable"] as? String,
            value == "Y" {
            cell.buttonOneHeightConstraint.constant = 30
            cell.buttonTwoWidthConstraint.constant = screenWidth/4 - 10//cell.buttonOne.frame.width/2
            
            cell.buttonOne.setTitle("Book Home Visit", for: .normal)
            cell.buttonTwo.alpha = 0
            
            cell.buttonOneImageView.image = #imageLiteral(resourceName: "ic_hospital_blue")
            cell.buttonOne.addTarget(self, action: #selector(getClinicBookHomeVisit(_:)), for: .touchUpInside)
            
        } else if let value = item["EmergencyCareAvailable"] as? String,
            value == "Y" {
            cell.buttonOneHeightConstraint.constant = 30
            cell.buttonTwoWidthConstraint.constant = screenWidth/4 - 10//cell.buttonOne.frame.width/2
            
            cell.buttonOne.setTitle("Emergency Care", for: .normal)
            cell.buttonTwo.alpha = 0
            
            cell.buttonOneImageView.image = #imageLiteral(resourceName: "ic_hospital_blue")
            cell.buttonOne.addTarget(self, action: #selector(getEmergencyCare), for: .touchUpInside)
            
        } else {
            cell.buttonOneHeightConstraint.constant = 0
        }

        return cell
    }
    
    func setRightBarButton() {
        
        if self.Location != "" {
            
            label.frame = CGRect.init(x: 20, y: 0, width: 100, height: 40)
            if self.Location.contains(" | ") {
                label.text = self.Location
            } else {
                label.text = "\(self.Location) | "
            }
            label.textColor = UIColor.white
            label.textAlignment = .right
            label.isUserInteractionEnabled = true
            label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rightBarButtonTapped)))
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(marquee), userInfo: nil, repeats: true)
            let barButton = UIBarButtonItem(customView: label)
            barButton.setBadge(text: "▼", withOffsetFromTopRight: CGPoint(x: 13, y: 11), andColor: .clear, andFilled: false)
            self.navigationItem.rightBarButtonItem = barButton
        }
    }

    @objc func rightBarButtonTapped() {
 
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "LocationPopOverViewController") as? LocationPopOverViewController {
            
            viewController.delegate = self
            viewController.currentLocation  = self.CurrentLocation
            viewController.currentLatitude  = self.CurrentLatitude
            viewController.currentLongitude = self.CurrentLongitude
            self.present(viewController, animated: true, completion: nil)
        }
        
    }

    @objc func marquee() {
        
        let str = label.text!
        let indexFirst = str.index(str.startIndex, offsetBy: 0)
        let indexSecond = str.index(str.startIndex, offsetBy: 1)
        
        label.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    }
    
    @objc func gotoClinicProfile(_ tag: Int = 0) {
        
        let viewController = AppController.shared.getClinicProfileViewController()
       
        if let value = self.infoArray[tag]["ClinicNo"] as? Int {
            viewController.clinicId = "\(value)"
        }
        
        viewController.selectedSegmentIndex = 0
    
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoClinicBooking(_ sender: UIButton) {
        
        let viewController = AppController.shared.getClinicProfileViewController()
        
        if let value = self.infoArray[sender.tag]["ClinicNo"] as? Int {
            viewController.clinicId = "\(value)"
        }
        
        viewController.selectedSegmentIndex = 1
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func gotoDoctorClinics(_ tag: Int) {
  
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DoctorClinicsViewController") as? DoctorClinicsViewController {
            
            viewController.doctorName   = self.infoArray[tag]["Name"] as? String ?? ""
            viewController.doctorId     = "\(self.infoArray[tag]["ID"] as? Int ?? 0)"
            viewController.isFav        = self.infoArray[tag]["DrFavourite"] as? String ?? ""
            viewController.infoArray    = self.infoArray[tag]["HosDetail"] as? [Dictionary<String, Any>] ?? []
            self.present(viewController, animated: true, completion: nil)
          }
    }
    
    @objc func gotoBookAppointment(_ sender: UIButton) {
        
        let viewController = AppController.shared.getCreateAppointmentViewController()
        viewController.DoctorId = self.infoArray[sender.tag]["ID"] as? Int ?? 0
        viewController.DoctorName = self.infoArray[sender.tag]["Name"] as? String ?? ""
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoBookHomeVisit(_ sender: UIButton) {
        
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MultiUsePopUpViewController") as? MultiUsePopUpViewController {
            
            viewController.delegate = self
            viewController.popUpTitle = "Book Home Visit"
            
            if sender.tag < 1000 {
                viewController.doctorId = "\(self.infoArray[sender.tag]["ID"] as? Int ?? 0)"
                viewController.contactNumber = self.infoArray[sender.tag]["HomeVisitMobile"] as? String ?? ""
            } else {
                viewController.clinicId = "\(self.infoArray[sender.tag - 1000]["ClinicNo"] as? Int ?? 0)"
                viewController.contactNumber = self.infoArray[sender.tag - 1000]["ClinicHomeVMobile"] as? String ?? ""
            }
            
            self.present(viewController, animated: true, completion: nil)
        }
        
    }
    
    @objc func getClinicBookHomeVisit(_ sender: UIButton) {
        
        let item = self.infoArray[sender.tag - 1000]
        
        let alert = UIAlertController(title: "Home Visit", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        if let value = item["ClinicHomeVMobile"] as? String {
            alert.addAction(UIAlertAction(title: "Call: \(value)", style: .default, handler: {(action:UIAlertAction) in
                if let url = URL(string: "tel://\(value)"),
                    UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
        }
            
        alert.addAction(UIAlertAction(title: "Send Message", style: .default, handler: {(action:UIAlertAction) in
            self.gotoBookHomeVisit(sender)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func getEmergencyCare(_ sender: UIButton) {
        
        let clinicId = "\(self.infoArray[sender.tag - 1000]["ClinicNo"] as? Int ?? 0)"
        let clinicMobile = self.infoArray[sender.tag - 1000]["EmergencyMobile"] as? String ?? ""
        
        let alertController = UIAlertController(title: "Emergency Care", message: "\nContact Number\n\(clinicMobile)\(self.emergencyCareAlertText)", preferredStyle: UIAlertController.Style.alert)
        
        self.emergencyCareAlertText = ""
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Reason"
        }
        
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let alertTextField = alertController.textFields![0] as UITextField
            if alertTextField.text?.count == 0 {

                self.emergencyCareAlertText = "\n\nReason can't be empty"
                self.getEmergencyCare(sender)
            } else {
                self.loadEmergencyCare(clinicMobile, clinicId: clinicId, reason: alertTextField.text ?? "")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func clearFilter() {

        self.sortOrder  = "0"
        self.categoryArray = [String]()
        self.searchTypeAll = false
        self.appointment = "Online Appointment"
        self.genderArray = [String]()
        self.genderAll = false
        self.feesArray = [String]()
        self.feesAll = false
        self.experienceArray = [String]()
        self.experienceAll = false
        self.rating = "0"
        self.selectedLanguagesArray = [String]()
        self.selectedLanguagesAll = false
    
    }

    @objc func searchDoctorWithDefaultLocation() {
        
        let defaultAddress = Session.shared.getUserDefaultAddress()
        
        self.Latitude = Double(defaultAddress.latitude) ?? 0.0
        self.Longitude = Double(defaultAddress.longitude) ?? 0.0
        self.Location = defaultAddress.areaName + " | " + defaultAddress.cityName + " | "
        
        self.setRightBarButton()
        self.infoArray.removeAll()
        self.tableView.reloadData()
        
        self.loadDoctorBasicSearchAPICall()
        
    }
    
}
