//
//  DoctorBasicSearchViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 30/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation

class DoctorBasicSearchViewController: UIViewController {
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortingButton: UIButton!
    
    let locationManager = CLLocationManager()
    let label = UILabel.init()
    
    var infoArray = [Dictionary<String,Any>]()
    
    var CurrentLocation = ""
    var CurrentLatitude = 0.0
    var CurrentLongitude = 0.0
    
    var isInitialLocationPicking = true
    var Latitude:Double = 0.00
    var Longitude:Double = 0.00
    var Location = "Retrieving location"
    
    var searchName = ""
    var searchType = ""
    var sortOrder  = "0"
    
    var categoryArray = [String]()
    var searchTypeAll = Bool()
    var appointment = String()
    var genderArray = [String]()
    var genderAll = Bool()
    var feesArray = [String]()
    var feesAll = Bool()
    var experienceArray = [String]()
    var experienceAll = Bool()
    var rating = String()
    var selectedLanguagesArray = [String]()
    var selectedLanguagesAll = Bool()
    
    var pageNumber = 0
    var TotalNumberOfPages = 0
    
    var emergencyCareAlertText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.searchName, controller: self)
        
        self.setUpColors()
        self.loadInfoData()
        self.setRightBarButton()
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager.startUpdatingLocation()
        }
        
        self.sortingButton.addTarget(self, action: #selector(gotoSort), for: .touchUpInside)
        self.filterButton.addTarget(self, action: #selector(gotoFilter), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BasicSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "BasicSearchTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(searchDoctorWithDefaultLocation), name: Notification.Name.Task.ReloadDoctorBasicSearch, object: nil)
    }
    
    func setUpColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }

    func loadInfoData() {
        self.loadDoctorBasicSearchAPICall()
    }
    
    @objc func gotoSort() {
        
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MultiUsePopUpViewController") as? MultiUsePopUpViewController {
            
            viewController.delegate = self
            viewController.popUpTitle = "Sort"
            viewController.sortOrder = self.sortOrder
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    @objc func gotoFilter() {
        
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DoctorSearchFilterViewController") as? DoctorSearchFilterViewController {
            
            viewController.delegate                 = self
            viewController.SearchType               = self.categoryArray
            viewController.SearchTypeAll            = self.searchTypeAll
            viewController.Appointment              = self.appointment
            viewController.GenderArray              = self.genderArray
            viewController.GenderAll                = self.genderAll
            viewController.FeesArray                = self.feesArray
            viewController.FeesAll                  = self.feesAll
            viewController.ExperienceArray          = self.experienceArray
            viewController.ExperienceAll            = self.experienceAll
            viewController.Rating                   = Double(Int(self.rating) ?? 0)
            viewController.SelectedLanguagesArray   = self.selectedLanguagesArray
            viewController.SelectedLanguagesAll     = self.selectedLanguagesAll
            
            self.present(viewController, animated: true, completion: nil)

        }
    }
    
}
