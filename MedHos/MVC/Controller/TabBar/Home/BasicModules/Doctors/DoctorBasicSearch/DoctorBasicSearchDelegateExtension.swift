//
//  DoctorBasicSearchDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 24/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation

extension DoctorBasicSearchViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let value = self.infoArray[indexPath.row]["ResultType"] as? String {
            
            if value == "doctor" {
                
                return self.getDoctorCell(indexPath)
                
            } else if value == "clinic" {
                
                return self.getClinicCell(indexPath)
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.infoArray.count != 0 && indexPath.row == self.infoArray.count - 1 && self.TotalNumberOfPages != self.pageNumber {
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
            self.loadDoctorBasicSearchAPICall(self.infoArray.count != 0)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let value = self.infoArray[indexPath.row]["ResultType"] as? String {
            
            if value == "doctor" {
                self.gotoDoctorClinics(indexPath.row)
            } else if value == "clinic" {
                self.gotoClinicProfile(indexPath.row)
            }
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }

}

extension DoctorBasicSearchViewController: LocationPopOverDelegate {
    
    
    func didCitySelected(_ Name: String, lat: Double, long: Double) {
        
        self.isInitialLocationPicking = false
        self.pageNumber = 0
        self.TotalNumberOfPages = 0
        self.Location = Name
        self.Latitude = lat
        self.Longitude = long
        
        self.setRightBarButton()
        
        self.infoArray.removeAll()
        self.tableView.reloadData()
        
        self.clearFilter()
        self.loadDoctorBasicSearchAPICall()
    }

}

extension DoctorBasicSearchViewController: MultiUsePopOverDelegate {
    
    func didSubmitTapped(_ DataArray: [Dictionary<String, String>], from: String) {
        
        if from == "Sort" {
            
            self.sortOrder = DataArray[0]["Value"] ?? ""
            
            self.infoArray.removeAll()
            self.tableView.reloadData()
            
            self.pageNumber = 0
            self.loadDoctorBasicSearchAPICall()
            
        } else if from == "Book Home Visit" {
            self.loadBookHomeVisit(DataArray)
        }
        
    }
}

extension DoctorBasicSearchViewController: DoctorSearchFilterDelegate {
    
    func ApplyTapped(_ SearchType           : [String],
                     SearchTypeAll          : Bool,
                     Appointment            : String,
                     GenderArray            : [String],
                     GenderAll              : Bool,
                     FeesArray              : [String],
                     FeesAll                : Bool,
                     ExperienceArray        : [String],
                     ExperienceAll          : Bool,
                     Rating                 : Double,
                     SelectedLanguagesArray : [String],
                     SelectedLanguagesAll   : Bool) {
        
        self.categoryArray          = SearchType
        self.searchTypeAll          = SearchTypeAll
        self.appointment            = Appointment
        self.genderArray            = GenderArray
        self.genderAll              = GenderAll
        self.feesArray              = FeesArray
        self.feesAll                = FeesAll
        self.experienceArray        = ExperienceArray
        self.experienceAll          = ExperienceAll
        self.rating                 = "\(Int(Rating))"
        self.selectedLanguagesArray = SelectedLanguagesArray
        self.selectedLanguagesAll   = SelectedLanguagesAll
        
        self.infoArray.removeAll()
        self.tableView.reloadData()
        
        self.pageNumber = 0
        self.loadDoctorBasicSearchAPICall()
    }
    
}

extension DoctorBasicSearchViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.isInitialLocationPicking {
            
            let userLocation :CLLocation = locations[0] as CLLocation
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
                
                if (error != nil){
                    print("error in reverseGeocode")
                }
                
                let placemark = placemarks! as [CLPlacemark]
                
                if placemark.count > 0 {
                    
                    let placemark = placemarks![0]
                    
                    self.CurrentLatitude = userLocation.coordinate.latitude
                    self.CurrentLongitude = userLocation.coordinate.longitude
                    self.Latitude = userLocation.coordinate.latitude
                    self.Longitude = userLocation.coordinate.longitude
                    self.Location = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    self.CurrentLocation = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    
                    self.setRightBarButton()
                }
            }
            self.isInitialLocationPicking = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
}

