//
//  DoctorCategoriesHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorCategoriesViewController {
    
    func getLabelCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        
        if let value = self.infoArray["SpecialitySearchList"] as? [Dictionary<String,Any>],
            indexPath.section == 0 {
            
            cell.itemTitleLabel.textColor = UIColor.lightGray
            
            cell.itemImageViewWidthConstraint.constant = 15
            cell.itemFrontImageViewHeightContraint.constant = 0
            
            cell.itemImageView.image = UIImage(named: "arrow")
            
            if self.IsSearching {
                
                let searchItem = self.searchResultArray[indexPath.row]
                cell.itemTitleLabel.text = searchItem["SearchDisplay"] as? String
                
            } else {
                
                let specialitySearchList = value[indexPath.row]
                cell.itemTitleLabel.text = specialitySearchList["SearchDisplay"] as? String
            }
            
        } else if let value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>],
            let value1 = value[indexPath.section - 1]["SpecList"] as? [Dictionary<String, Any>] {
            
                if indexPath.row == 0 {
                    cell.itemTitleLabel.textColor = UIColor.black
                    
                    cell.itemFrontImageViewHeightContraint.constant = 0
                    cell.itemImageViewWidthConstraint.constant = 15
                    
                    let item = value[indexPath.section - 1]
                    
                    if self.tableView.numberOfRows(inSection: indexPath.section) > 1 {
                        cell.itemImageView.image = UIImage(named: "ic_dropdown")
                    } else {
                        cell.itemImageView.image = UIImage(named: "arrow")
                    }
                    
                    cell.itemTitleLabel.text = item["SpecCategoryName"] as? String
                } else {
                    
                    cell.itemTitleLabel.textColor = UIColor.black
                    
                    cell.itemImageViewWidthConstraint.constant = 0
                    cell.itemFrontImageViewHeightContraint.constant = 20
                    
                    let item = value1[indexPath.row - 1]
                    
                    cell.itemTitleLabel.text = item["Speciality"] as? String
                    
                }
        }
        return cell
    }
    
    @objc func marquee() {
        
        let str = label.text!
        let indexFirst = str.index(str.startIndex, offsetBy: 0)
        let indexSecond = str.index(str.startIndex, offsetBy: 1)
        
        label.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    }
    
    func setRightBarButton() {
        if self.Location != "" {
            label.frame = CGRect.init(x: 20, y: 0, width: 100, height: 40)
            if self.Location.contains(" | ") {
                label.text = self.Location
            } else {
                label.text = "\(self.Location) | "
            }
            label.textColor = UIColor.white
            label.textAlignment = .right
            label.isUserInteractionEnabled = true
            label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rightBarButtonTapped)))
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(marquee), userInfo: nil, repeats: true)
            let barButton = UIBarButtonItem(customView: label)
            barButton.setBadge(text: "▼", withOffsetFromTopRight: CGPoint(x: 13, y: 11), andColor: .clear, andFilled: false)
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    
    @objc func setDefaultLocation() {
        
        let defaultAddress = Session.shared.getUserDefaultAddress()
        
        self.Latitude = Double(defaultAddress.latitude) ?? 0.0
        self.Longitude = Double(defaultAddress.longitude) ?? 0.0
        self.Location = defaultAddress.areaName + " | " + defaultAddress.cityName + " | "
        
        self.setRightBarButton()
        
    }
}
