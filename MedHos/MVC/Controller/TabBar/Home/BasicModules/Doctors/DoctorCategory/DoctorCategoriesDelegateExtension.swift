//
//  DoctorCategoriesDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

extension DoctorCategoriesViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
//        if DoctorFullArray != nil
//        {
//            return DoctorFullArray!.CategorizedList.count + 1
//        }
        if let value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>] {
            return value.count + 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let DoctorSpecialiaztionView = UIView(frame: CGRect.zero)
        let SpecialLabel = UILabel()
        SpecialLabel.text = "Specialization"
        SpecialLabel.textColor = #colorLiteral(red: 0, green: 0.4085357051, blue: 0.8441912375, alpha: 1)
        SpecialLabel.frame.origin.y = DoctorSpecialiaztionView.frame.minY + 5
        SpecialLabel.frame.origin.x = DoctorSpecialiaztionView.frame.minX + 5
        SpecialLabel.frame.size = CGSize(width: 150, height: 20)
        DoctorSpecialiaztionView.backgroundColor = UIColor.clear
        DoctorSpecialiaztionView.addSubview(SpecialLabel)
        return DoctorSpecialiaztionView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 30
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if section == 0
        {
            if IsSearching {
                return self.searchResultArray.count
            } else if let value = self.infoArray["SpecialitySearchList"] as? [Dictionary<String,Any>] {
                return value.count
            }
            
            return 0
        }
        else
        {
            if let value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>], value[section-1][STATUS] as? String == OPEN {
                if let value1 = value[section-1]["SpecList"] as? [Dictionary<String, Any>] {
                    return value1.count + 1
                }
            }
            
            
            
//            print(self.tableView.numberOfRows(inSection: 0))
//            if self.tableView.numberOfRows(inSection: section) == 1
//            {
//                if let value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>],
//                    let value1 = value[section - 1]["SpecList"] as? [Dictionary<String, Any>] {
//                    return value1.count + 1
//                }
//                //return (DoctorFullArray?.CategorizedList[section - 1].SpecList?.count)! + 1
//            }
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if DoctorFullArray!.SpecialitySearchList != nil
//        {
//            if indexPath.section == 0
//            {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "LabelTableViewCell", for: indexPath) as! LabelTableViewCell
//                if IsSearching
//                {
//                    cell.itemTitleLabel.text = SearchListArray?[indexPath.row].SearchDisplay
//                }
//                else
//                {
//                    cell.itemTitleLabel.text = DoctorFullArray!.SpecialitySearchList![indexPath.row].SearchDisplay
//                }
//                cell.itemImageView.image = UIImage(named: "arrow")
//                cell.itemImageViewWidthConstraint.constant = 15
//                cell.itemTitleLabel.textColor = UIColor.lightGray
//                return cell
//            }
//        }
//        if indexPath.row == 0
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "LabelTableViewCell", for: indexPath) as! LabelTableViewCell
//            cell.itemTitleLabel.text = DoctorFullArray?.CategorizedList[indexPath.section - 1].SpecCategoryName
//            cell.itemTitleLabel.textColor = UIColor.black
//            cell.itemFrontImageViewHeightContraint.constant = 0
//            if DoctorFullArray?.CategorizedList[indexPath.section - 1].opened == true
//            {
//                cell.itemImageView.image = UIImage(named: "ic_dropdown")
//            }
//            else
//            {
//                cell.itemImageView.image = UIImage(named: "arrow")
//            }
//            cell.itemImageViewWidthConstraint.constant = 15
//            cell.itemImageView.alpha = 1
//            return cell
//        }
//        else
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "LabelTableViewCell", for: indexPath) as! LabelTableViewCell
//            cell.itemTitleLabel.text = DoctorFullArray?.CategorizedList[indexPath.section - 1].SpecList![indexPath.row - 1].Speciality
//            cell.itemImageView.alpha = 0
//            cell.itemTitleLabel.textColor = UIColor.black
//            cell.itemFrontImageViewHeightContraint.constant = 20
//            return cell
//        }
        return self.getLabelCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0 && indexPath.row == 0
        {
            var needToRefreshSection:Int? = nil
            
            if var value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>] {
                for i in 0..<value.count {
                    if i == indexPath.section - 1 {
                        if value[i][STATUS] as? String == OPEN {
                           value[i][STATUS] = CLOSE
                        } else {
                           value[i][STATUS] = OPEN
                        }
                    } else {
                        if value[i][STATUS] as? String == OPEN {
                            needToRefreshSection = i + 1
                        }
                        value[i][STATUS] = CLOSE
                    }
                }
                self.infoArray["CategorizedList"] = value
            }
            
            if needToRefreshSection != nil {
                let index:IndexSet = [indexPath.section,needToRefreshSection!]
                self.tableView.reloadSections((index), with: .automatic)
            } else {
                self.tableView.reloadSections(IndexSet([indexPath.section]), with: .automatic)
            }
            
        } else if indexPath.section == 0 {
            
            if self.IsSearching {
                let searchItem = self.searchResultArray[indexPath.row]
                self.searchName = searchItem["DrpText"] as? String ?? ""
                self.searchType = searchItem["Type"] as? String ?? ""
                
            } else {
                
                if let value = self.infoArray["SpecialitySearchList"] as? [Dictionary<String,Any>] {
                    self.searchName = value[indexPath.row]["SearchName"] as? String ?? ""
                    self.searchType = value[indexPath.row]["SearchType"] as? String ?? ""
                }
            }
            self.gotoDoctorBasicSearch()
        } else {
            if let value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>],
                let value1 = value[indexPath.section - 1]["SpecList"] as? [Dictionary<String, Any>] {
                
                self.searchName = value1[indexPath.row - 1]["Speciality"] as? String ?? ""
                self.searchType = "Speciality"
            }
            self.gotoDoctorBasicSearch()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

extension DoctorCategoriesViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.Booler
        {
            let userLocation :CLLocation = locations[0] as CLLocation
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
                if (error != nil){
                    print("error in reverseGeocode")
                }
                let placemark = placemarks! as [CLPlacemark]
                if placemark.count>0{
                    let placemark = placemarks![0]
                    
                    self.CurrentLatitude = userLocation.coordinate.latitude
                    self.CurrentLongitude = userLocation.coordinate.longitude
                    self.Latitude = userLocation.coordinate.latitude
                    self.Longitude = userLocation.coordinate.longitude
                    self.Location = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    self.CurrentLocation = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    
                    self.setRightBarButton()
                }
            }
            self.Booler = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
   
}

extension DoctorCategoriesViewController: LocationPopOverDelegate {
    
    func didCitySelected(_ Name: String, lat: Double, long: Double) {
        
        self.Booler = false
        self.IsSearching = false
        self.Location = Name
        self.Latitude = lat
        self.Longitude = long
        self.setRightBarButton()
        self.tableView.reloadSections(([0]), with: .automatic)
    }
}

extension DoctorCategoriesViewController : UISearchBarDelegate {
   
    // MARK: - SearchBar Delegate Methods
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchText.count <= 2 {
            
            HttpManager.shared.cancelAllRequests()
            self.IsSearching = false
            self.tableView.reloadData()
            
        } else if let searchBarText = searchBar.text,
            searchBarText.count > 2 {
            
            let newString = searchBarText.trimmingCharacters(in: CharacterSet.whitespaces)
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector:#selector(SearchBySpecialization), object: self.SearchText)
            
            // Don't replace SearchText until after the cancelPreviousPerformRequestWithTarget call
            self.SearchText = newString
            perform(#selector(SearchBySpecialization), with: newString, afterDelay: 1)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
        
        self.IsSearching = false
        self.tableView.reloadData()
    }
    
}
