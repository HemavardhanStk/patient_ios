//
//  DoctorCategoriesAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorCategoriesViewController {
    
    func loadDoctorCategoriesAPICall() {
        
        let user = Session.shared.getUserInfo()
        let params = ["UserID":user.id,
                      "DocCategory":"1",
                      "Language":"English"]
        
        HttpManager.shared.loadAPICall(path: PATH_ShowSpeciality, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                self.infoArray = response
                
                if var value = self.infoArray["CategorizedList"] as? [Dictionary<String,Any>] {
                    for i in 0..<value.count {
                        value[i][STATUS] = CLOSE
                    }
                    self.infoArray["CategorizedList"] = value
                }
                
                self.tableView.reloadData()
            }
        }) {}
    }

    @objc func SearchBySpecialization() {
        
          let params = ["City": self.Location,
                        "Keyword": self.searchBar.text ?? "",
                        "Latitude": "\(self.Latitude)",
                        "Longitude": "\(self.Longitude)",
                        "Language": "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_SearchBySpecialization, params: params, httpMethod: .post, controller: self,isBackgroundCall: true, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self,isBackgroundCall: true) {
                if let value = response["SearchList"] as? [Dictionary<String, Any>] {
                    self.IsSearching = true
                    self.searchResultArray = value
                }
                self.tableView.reloadSections(IndexSet([0]), with: .automatic)
            }
        }) {}
    }
    
}

