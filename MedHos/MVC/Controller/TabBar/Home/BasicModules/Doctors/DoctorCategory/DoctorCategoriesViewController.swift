//
//  DoctorCategoriesViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 14/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GooglePlaces

class DoctorCategoriesViewController: UIViewController  {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String,Any>()
    var searchResultArray = [Dictionary<String, Any>]()
  
    let locationManager = CLLocationManager()
    let label = UILabel.init()
    var CurrentLatitude = 0.0
    var CurrentLongitude = 0.0
    var CurrentLocation = ""
    var Latitude:Double = 0.00
    var Longitude:Double = 0.00
    var Location = "Retrieving location"
    var SearchText = ""
    
    var searchName = ""
    var searchType = ""
    
    var Booler = true
    var IsSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
   
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitlewithRightBarButton("Find Doctors", image: "", text: "", controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
        
        self.searchBar.delegate = self
        self.searchBar.setSearchBarStyle()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.separatorStyle = .none
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 70
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 70
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setDefaultLocation), name: Notification.Name.Task.ReloadDoctorCategories, object: nil)
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        self.loadDoctorCategoriesAPICall()
    }

    @objc func rightBarButtonTapped() {
        
//        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CitiesPopOverViewController") as? CitiesPopOverViewController {
//            viewController.delegate = self
//            viewController.CurrentLocation = self.CurrentLocation
//            viewController.CurrentLatitude = self.CurrentLatitude
//            viewController.CurrentLongitude = self.CurrentLongitude
//            self.present(viewController, animated: true, completion: nil)
//            }
        
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "LocationPopOverViewController") as? LocationPopOverViewController {
            
            viewController.delegate = self
            viewController.currentLocation  = self.CurrentLocation
            viewController.currentLatitude  = self.CurrentLatitude
            viewController.currentLongitude = self.CurrentLongitude
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func gotoDoctorBasicSearch() {
        
        let viewController = AppController.shared.getDoctorBasicSearchViewController()
        viewController.CurrentLatitude = self.CurrentLatitude
        viewController.CurrentLongitude = self.CurrentLongitude
        viewController.CurrentLocation = self.CurrentLocation
        viewController.Latitude = self.Latitude
        viewController.Longitude = self.Longitude
        viewController.Location = self.Location
        viewController.searchName = self.searchName
        viewController.searchType = self.searchType
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}


