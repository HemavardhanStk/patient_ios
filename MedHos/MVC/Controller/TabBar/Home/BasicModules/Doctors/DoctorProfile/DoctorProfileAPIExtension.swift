//
//  DoctorProfileAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorProfileViewController {
    
    func loadDoctorProfileAPICall() {
        
        let user = Session.shared.getUserInfo()
        let params = ["UserId" : user.id,
                      "DocId" : self.DoctorId,
                      "Language": "English"] as [String : Any]
        
        HttpManager.shared.loadAPICall(path: PATH_GetDrProfile, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                
                self.infoArray = response["DrDetailsFull"] as? Dictionary<String,Any> ?? [:]
                self.bookButton.alpha = 1
                self.loadProfileData()
                self.setRightBarButtonImage()
                self.tableView.reloadData()
            }
        }) {}
        
    }
    
    func loadReviewInfoAPIcall() {
        let params = ["ReferenceID" : self.DoctorId,
                      "CategoryID": 1,
                      "Language": "English"] as [String : Any]
        
        HttpManager.shared.loadAPICall(path: PATH_UserViewIndividualsRating, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self,isBackgroundCall: true) {
                self.ratingArray = response
            }
        }) {}
    }
    
    func loadFavouriteChangeAPICall() {
        
        let user = Session.shared.getUserInfo()
        let isFav = self.infoArray["FavouriteDr"] as? String == "Y"
        
        let doctorId = self.DoctorId
        let updateCommand = isFav ? "remove":"add"
        
        let params = ["UserNo": user.id,
                      "DoctorNo": "\(doctorId)",
                      "UpdateCommand": updateCommand,
                      "Language": "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_AddDeleteDrFavourites, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                self.infoArray["FavouriteDr"] = isFav ? "N" : "Y"
                self.setRightBarButtonImage()
            }
        }) {}
    }
    
}
