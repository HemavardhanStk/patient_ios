//
//  OverallRatingTableViewCell.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Balaji G. All rights reserved.
//

import UIKit

class OverallRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var overallRatingLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingPercentageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
