//
//  ClinicDetailedTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 12/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

class ClinicDetailedTableViewCell: UITableViewCell {

    @IBOutlet weak var ClinicNameLabel: UILabel!
    @IBOutlet weak var ClininAddressLabel: UILabel!
    
    @IBOutlet weak var MobileImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var MobileNumberLabel: UILabel!
    
    @IBOutlet weak var ClinicImageView: UIView!
    @IBOutlet weak var ClinicImagesCollectionView: UICollectionView!
    @IBOutlet weak var ClinicImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var MobileImageViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var TimingsLabel: UILabel!
    @IBOutlet weak var MapView: GMSMapView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
