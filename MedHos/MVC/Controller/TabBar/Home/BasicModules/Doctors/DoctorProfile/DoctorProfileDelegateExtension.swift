//
//  DoctorProfileDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var count = 0
        
        if let hospitals = self.infoArray["Hospital"] as? [Dictionary<String, Any>], self.selectedSegmentIndex == 0 {
            count = hospitals.count + 2
        }
        
        if self.selectedSegmentIndex == 1 {
            count = self.ProfileArray.count + 2
        }
        
        if let Ratings = self.ratingArray["Rating"] as? [Dictionary<String, Any>], self.selectedSegmentIndex == 2 {
            count = Ratings.count + 3
        } else if self.selectedSegmentIndex == 2 {
            AlertHelper.shared.showAlert(message: "No Ratings Available", controller: self)
            count = 2
        }
        return self.infoArray.count == 0 ? 0 : count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.selectedSegmentIndex == 1 && section > 1 {
            if ProfileArray[section - 2].Opened {
                return ProfileArray[section - 2].SectionData.count + 1
            }
        } else if self.selectedSegmentIndex == 2 && section > 2 {
            if let Ratings = self.ratingArray["Rating"] as? [Dictionary<String, Any>] {
                return Ratings[section - 3]["Reply"] as? String != nil ? 2:1
            }
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return self.getProfileCell()
        } else if indexPath.section == 1 {
            return self.getSegmentHeaderViewCell()
        } else {
            switch self.selectedSegmentIndex {
                
            case 0 :
                return self.getClinicCell(indexPath)
            case 1 :
                return self.getLabelCell(indexPath)
            case 2 :
                if indexPath.section == 2 {
                    return self.getOverallRatingCell()
                } else {
                    return self.getRatingCell(indexPath)
                }
            default:
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return 60
        } else if self.selectedSegmentIndex == 2 && indexPath.section == 2 {
            return 80
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0
        {
            self.segmentView.alpha = 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) { 
        if indexPath.section == 0
        {
            self.segmentView.alpha = 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedSegmentIndex == 1
        {
            if indexPath.section > 1 && indexPath.row == 0
            {
                if self.ProfileArray[indexPath.section - 2].Opened == true
                {
                    self.ProfileArray[indexPath.section - 2].Opened = false
                }
                else
                {
                    self.ProfileArray[indexPath.section - 2].Opened = true
                }
            }
            
            let indexset = IndexSet.init(integer: indexPath.section)
            self.tableView.reloadSections(indexset, with: .none)
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension DoctorProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        
        if let hospitals = self.infoArray["Hospital"] as? [Dictionary<String, Any>], self.selectedSegmentIndex == 0 {
            if let value = hospitals[collectionView.tag]["HosImage"] as? [String] {
                count = value.count
            }
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionID", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        if let hospitals = self.infoArray["Hospital"] as? [Dictionary<String, Any>], self.selectedSegmentIndex == 0 {
            if let value = hospitals[collectionView.tag]["HosImage"] as? [String] {
                if let url = URL(string: value[indexPath.row]) {
                    cell.imageView.af_setImage(withURL: url)
                }
            }
        }
        cell.imageView.contentMode = .scaleAspectFill
        cell.imageView.layer.cornerRadius = 8
        cell.imageView.layer.masksToBounds = true
        cell.imageView.viewInFullScreen()
        return cell
    }
    
    
}
