//
//  DoctorProfileHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 05/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

extension DoctorProfileViewController {
    
    // MARK: - Profile Cell
    
    func getProfileCell() -> ProfileViewTableViewCell  {
        
        let cell: ProfileViewTableViewCell = Bundle.main.loadNibNamed("ProfileViewTableViewCell", owner: nil, options: nil)![0] as! ProfileViewTableViewCell
        
        var fee = ""
        var experience = ""
        
        if let value = self.infoArray["Experience"] as? Int, value != 0 {
            experience = "\(value) Year(s) of Experience"
        }
        
        if let value = self.infoArray["Fees"] as? Int, value != 0 {
            fee = "\(value)"
        }
        
        if let urlString = self.infoArray["DrImage"] as? String,
            let url = URL(string: urlString) {
            
            cell.DoctorProfileImageView.af_setImage(withURL: url)
        }
        
        cell.DoctorNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.DoctorDegreeLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.DoctorExperienceLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.DoctorFeeLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.DoctorNameLabel.text = self.infoArray["Name"] as? String
        cell.DoctorDegreeLabel.text = self.infoArray["Degree"] as? String
        cell.DoctorExperienceLabel.text = experience
        cell.DoctorFeeLabel.text = fee
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    // MARK: - Clinic Cell
    
    func getClinicCell(_ indexPath: IndexPath) -> ClinicDetailedTableViewCell  {
        
        let cell: ClinicDetailedTableViewCell = Bundle.main.loadNibNamed("ClinicDetailedTableViewCell", owner: nil, options: nil)![0] as! ClinicDetailedTableViewCell
        
        let tag = indexPath.section - 2
        
        
        var clinicName = ""
        var clinicAddress = ""
        var mobileNumber = ""
        var timings = ""
        
        if let hospital = self.infoArray["Hospital"] as? [Dictionary<String, Any>] {
            
            if let value = hospital[tag]["HospitalName"] as? String {
                clinicName = value
            }
            
            if let clinicAddress1 = hospital[tag]["AddressOne"] as? String,
                let clinicAddress2 = hospital[tag]["AddressTwo"] as? String,
                let clinicAddress3 = hospital[tag]["AddressThree"] as? String,
                let clinicAddress4 = hospital[tag]["AddressFour"] as? String {
                
                clinicAddress = clinicAddress1 + "\n" + clinicAddress2 + "\n" + clinicAddress3 + clinicAddress4
            }
            
            if let value = hospital[tag]["Phone1"] as? String {
                mobileNumber = value
            }
            
            if let hospitals = self.infoArray["Hospital"] as? [Dictionary<String, Any>], self.selectedSegmentIndex == 0 {
                if let value = hospitals[tag]["HosImage"] as? [String] {
                    cell.ClinicImageViewHeightConstraint.constant = value.count != 0 ? 80 : 0
                }
            }
            
            if let timingsArray = hospital[tag]["ClinicTimings"] as? [Dictionary<String, Any>] {
                
                if timingsArray.count != 0 {
                    
                    for i in 0...timingsArray.count - 1 {
                        
                        if i != timingsArray.count - 1 {
                            timings.append("\(timingsArray[i]["Timing"] as? String ?? "")\n")
                        } else {
                            timings.append(timingsArray[i]["Timing"] as? String ?? "")
                        }
                    }
                }
            }
            
            self.sloadView(lat: hospital[tag]["lat"] as? Double ?? 0.0, long: hospital[tag]["lang"] as? Double ?? 0.0, View: cell.MapView)
        }
        
        cell.ClinicNameLabel.text = clinicName
        cell.ClininAddressLabel.text = clinicAddress
        cell.MobileNumberLabel.text = mobileNumber
        cell.TimingsLabel.text = timings
        
        cell.MobileImageViewHeightConstraint.constant = mobileNumber == "" ? 0 : 15
        cell.MobileImageViewBottomConstraint.constant = mobileNumber == "" ? 0 : 5
        //cell.ClinicImageViewHeightConstraint.constant = 0
        cell.ClinicImagesCollectionView.delegate = self
        cell.ClinicImagesCollectionView.dataSource = self
        cell.ClinicImagesCollectionView.tag = tag
        cell.ClinicImagesCollectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        
        cell.MapView.layer.cornerRadius = 5
        cell.MapView.layer.masksToBounds = true
        cell.MapView.isUserInteractionEnabled = false
        
        return cell
    }
    
    // MARK: - Rating Cell
    
    func getRatingCell(_ indexPath: IndexPath) -> RatingTableViewCell  {
        
        //let user = Session.shared.getUserInfo()
        let Ratings = self.ratingArray["Rating"] as! [Dictionary<String, Any>]
        let tag = indexPath.section - 3
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingTableViewCell", for: indexPath) as! RatingTableViewCell
            
            if let value = Ratings[tag]["Gender"] {
                Helper.shared.setGenderImage(cell.LeftImageView, gender: "\(value)")
            }
            
            cell.LeftImageView.layer.cornerRadius = 15
            cell.LeftImageView.layer.masksToBounds = true
            cell.LeftImageViewWidthConstraint.constant = 30
            cell.LeftImageViewTrailingConstraint.constant = 8
            cell.RightImageViewWidthConstraint.constant = 0
            cell.RightImageViewLeadingConstraint.constant = 0
            
            cell.NameLabel.text = Ratings[tag]["Name"] as? String
            cell.DateLabel.text = Ratings[tag]["CommentedDate"] as? String
            cell.DateLabel.alpha = 1
            
            cell.NameLabel.textColor = UIColor.black
            cell.CommentLabel.textColor = UIColor.black
            
            cell.RatingViewHeightConstraint.constant = 20
            cell.RatingViewBottomConstraint.constant = 8
            cell.RatingView.layer.masksToBounds = true
            cell.RatingView.isUserInteractionEnabled = false
            
            if Ratings[tag]["Recommendation"] as? String == "Y" {
                cell.RecommendedImageView.image = UIImage(named: "ic_thumbs_up")
            } else {
                cell.RecommendedImageView.image = UIImage(named: "ic_thumbs_down")
            }
            cell.RecommendedImageView.alpha = 1
            if let value = Ratings[tag]["OverallRate"] as? Double {
                cell.RatingView.rating = value
            }
            cell.CommentLabel.text = Ratings[tag]["Comments"] as? String
            cell.CommetView.layer.cornerRadius = 15
            cell.CommetView.layer.backgroundColor = UIColor.white.cgColor
            cell.backgroundColor = UIColor.clear
            cell.CommetView.layer.masksToBounds = true
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingTableViewCell1", for: indexPath) as! RatingTableViewCell
            
            if let urlString = self.infoArray["DrImage"] as? String,
                let url = URL(string: urlString) {
                cell.RightImageView.af_setImage(withURL: url)
            }
            cell.RightImageView.layer.cornerRadius = 15
            cell.RightImageView.layer.masksToBounds = true
            cell.RightImageViewWidthConstraint.constant = 30
            cell.RightImageViewLeadingConstraint.constant = 8
            cell.LeftImageViewWidthConstraint.constant = 0
            cell.LeftImageViewTrailingConstraint.constant = 0
            
            cell.NameLabel.text = self.infoArray["Name"] as? String
            cell.DateLabel.alpha = 0
            cell.CommentLabel.text = Ratings[tag]["Reply"] as? String
            
            cell.NameLabel.textColor = UIColor.black
            cell.CommentLabel.textColor = UIColor.black
            
            cell.RatingViewHeightConstraint.constant = 0
            cell.RatingViewBottomConstraint.constant = 0
            cell.RatingView.layer.masksToBounds = true
            cell.RecommendedImageView.alpha = 0
            
            cell.CommetView.layer.cornerRadius = 15
            
            cell.RatingView.isUserInteractionEnabled = false
            cell.CommetView.layer.backgroundColor = UIColor(netHex: APP_REVIEW_DOCTOR_BG_COLOR).cgColor
            cell.backgroundColor = UIColor.clear
            cell.CommetView.layer.masksToBounds = true
            return cell
        }
    }
    
    // MARK: - Label Cell
    
    func getLabelCell(_ indexPath: IndexPath) -> LabelTableViewCell  {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        
        let tag = indexPath.section - 2
        
        if indexPath.row == 0 {
            
            self.tableView.allowsSelection = true
            
            cell.itemTitleLabel.text = self.ProfileArray[tag].SectionName
            cell.itemTitleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
            
            cell.itemFrontImageView.image = UIImage(named: ProfileArray[tag].SectionImage)
            
            cell.itemImageViewWidthConstraint.constant = 15
            cell.itemFrontImageViewHeightContraint.constant = 25
            cell.itemFrontImageViewTrailingConstraint.constant = 8
            
            if self.ProfileArray[tag].Opened == true {
                cell.itemImageView.image = #imageLiteral(resourceName: "ic_dropdown")
            } else {
                cell.itemImageView.image = #imageLiteral(resourceName: "ic_dropdown").rotate(radians: .pi * 1.5)
            }
            
        } else {
            
            cell.itemFrontImageViewTrailingConstraint.constant = 25
            cell.itemFrontImageViewHeightContraint.constant = 0
            cell.itemTitleLabel.text = ProfileArray[tag].SectionData[indexPath.row - 1]
            cell.itemTitleLabel.textColor = UIColor.black
            cell.itemImageViewWidthConstraint.constant = 0
        }
        return cell
    }
    
    // MARK: - OverallRating Cell
    
    func getOverallRatingCell() -> OverallRatingTableViewCell {
        
        let cell: OverallRatingTableViewCell = Bundle.main.loadNibNamed("OverallRatingTableViewCell", owner: nil, options: nil)![0] as! OverallRatingTableViewCell
        //cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.overallRatingLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.ratingPercentageLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.ratingPercentageLabel.textColor = UIColor.darkGray
        
        let item = self.ratingArray
        
        cell.overallRatingLabel.text = "Overall Reviews"
        if let value1 = item["RecommendedPercentage"],
            let value2 = item["TotalRecommended"] {
            cell.ratingPercentageLabel.text = "\(value1)% (\(value2) Votes)"
        }
     
        if let value = item["OverallRating"] as? Double {
            cell.ratingView.rating = value
        }
        
        return cell
    }
    
    // MARK: - Header Segment Cell
    
    func getSegmentHeaderViewCell() -> SegmentedControlTableViewCell {
        
        let cell: SegmentedControlTableViewCell = Bundle.main.loadNibNamed("SegmentedControlTableViewCell", owner: nil, options: nil)![0] as! SegmentedControlTableViewCell
        cell.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        
        self.setupSegmentControl(cell.segmentedControl)
        
        return cell
    }
    
    // MARK: - Parse Profile Data
    
    func loadProfileData() {
        
        if let value = self.infoArray["Education"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Education",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_education_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Specialities"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Specialization",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_specialization_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Service"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Service",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_service_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Comment"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Experience",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_experience_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Professional"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Membership",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_membership_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Achivement"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Awards & Recognitions",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_achievements_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Language"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Language",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_language_blue",Opened: false ))
        }
        
        if let value = self.infoArray["Tips"] as? String, value != "" {
            self.ProfileArray.append(ProfileStruct(SectionName: "Health tips",SectionData: value.components(separatedBy: "~"), SectionImage: "ic_heart_unlike",Opened: false ))
        }
    }
    
    // MARK: - Segment Listener
    
    func setupSegmentControl(_ segmentedControl: MRSegmentedControl) {
        
        segmentedControl.sectionTitles = ["Clinic", "Profile","Reviews"]
        segmentedControl.selectedSegmentIndex = self.selectedSegmentIndex
        
        segmentedControl.indexChangeBlock = { (index) in
            
            self.selectedSegmentIndex = index
            
            switch index {
            case 0:
                
                break
                
            case 1:
                
                break
                
            case 2:
                
                break
                
            default:
                break
            }
            
            self.tableView.reloadData()
            self.segmentView.addSubview(self.getSegmentHeaderViewCell())
            var isAllow = false
            if self.selectedSegmentIndex == 0 {
                if self.infoArray.count != 0 {
                    isAllow = true
                }
            } else if self.selectedSegmentIndex == 1 {
                if self.ProfileArray.count != 0 {
                    isAllow = true
                }
            } else if self.selectedSegmentIndex == 2 {
                if self.ratingArray.count != 0 {
                    isAllow = true
                }
            }
            if isAllow {
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                }) { (completion) in
                    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 1), at: .top, animated: true)
                    }) { (completion) in
                    }
                }
            } //else {
            //                self.loadDoctorProfileAPICall()
            //            }
        }
        
        segmentedControl.backgroundColor = UIColor.init(netHex: APP_BG_COLOR_LIGHT)
        segmentedControl.selectionIndicatorColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.init(netHex: APP_PRIMARY_COLOR), NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.init(name: APP_FONT_NAME_BOLD, size: 15) as Any]
        segmentedControl.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.black, NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14) as Any]
    }
    
    @objc func sloadView(lat:Double,long:Double,View:GMSMapView) {
        
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        View.camera = GMSCameraPosition(target: coord, zoom: 14, bearing: 0, viewingAngle: 0)
        
        let marker = GMSMarker()
        
        marker.appearAnimation = GMSMarkerAnimation(rawValue: 1)!
        marker.position = coord
        marker.map = View
    }
    
    func setRightBarButtonImage() {
        
        var imageTitle = ""
        
        if self.infoArray["FavouriteDr"] as? String == "Y" {
            imageTitle = "ic_heart_like"
        } else {
            imageTitle = "ic_heart_like_white"
        }
        Navigation.shared.setTitlewithRightBarButton(TITLE_DOCTOR_PROFILE, image: imageTitle, text: "", controller: self)
    }
}

