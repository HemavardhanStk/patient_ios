//
//  DoctorProfileViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 09/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

struct ProfileStruct
{
    var SectionName: String
    var SectionData: [String]
    var SectionImage: String
    var Opened:Bool = false
}

class DoctorProfileViewController: UIViewController {
    
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bookButton: UIButton!
    
    var infoArray = Dictionary<String, Any>()
    var ProfileArray = [ProfileStruct]()
    var ratingArray = Dictionary<String, Any>()
    var imagesArray = Dictionary<String,Any>()
    var DoctorId = Int()
    var selectedSegmentIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
        
    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle( title: TITLE_PROFILE, controller: self)
        self.setupColors()
        self.loadInfoData()
        
        self.segmentView.addSubview(self.getSegmentHeaderViewCell())
        self.segmentView.alpha = 0
        
        self.bookButton.addTarget(self, action: #selector(gotoBookAppointmentCall), for: .touchUpInside)
        self.bookButton.alpha = 0
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ProfileViewTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileViewTableViewCell")
        self.tableView.register(UINib(nibName: "ClinicDetailedTableViewCell", bundle: nil), forCellReuseIdentifier: "ClinicDetailedTableViewCell")
        self.tableView.register(UINib(nibName: "RatingTableViewCell", bundle: nil), forCellReuseIdentifier: "RatingTableViewCell")
        self.tableView.register(UINib(nibName: "RatingTableViewCell", bundle: nil), forCellReuseIdentifier: "RatingTableViewCell1")
        self.tableView.register(UINib(nibName: "CustomLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomLabelTableViewCell")
        self.tableView.register(UINib(nibName: "OverallRatingTableViewCell", bundle: nil), forCellReuseIdentifier: "OverallRatingTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 100
        self.tableView.showsHorizontalScrollIndicator = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        
        self.loadDoctorProfileAPICall()
        self.loadReviewInfoAPIcall()
    }
    
    @objc func rightBarButtonTapped() {
        self.loadFavouriteChangeAPICall()
    }
    
    @objc func gotoBookAppointmentCall() {
        let viewController = AppController.shared.getCreateAppointmentViewController()
        viewController.DoctorId = DoctorId
        viewController.DoctorName = self.infoArray["Name"] as? String ?? ""
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
}
