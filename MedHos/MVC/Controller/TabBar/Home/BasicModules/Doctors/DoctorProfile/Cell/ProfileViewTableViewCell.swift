//
//  ProfileViewTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 09/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ProfileViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var DoctorProfileImageView: UIImageView!
    @IBOutlet weak var DoctorNameLabel: UILabel!
    @IBOutlet weak var DoctorDegreeLabel: UILabel!
    @IBOutlet weak var DoctorExperienceLabel: UILabel!
    @IBOutlet weak var DoctorFeeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
