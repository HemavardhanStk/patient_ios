//
//  RatingTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 13/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var LeftImageView: UIImageView!
    @IBOutlet weak var LeftImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var LeftImageViewTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var RightImageView: UIImageView!
    @IBOutlet weak var RightImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var RightImageViewLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var CommetView: UIView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    
    @IBOutlet weak var RatingView: CosmosView!
    @IBOutlet weak var RatingViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var RatingViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var RecommendedImageView: UIImageView!
    
    @IBOutlet weak var CommentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
