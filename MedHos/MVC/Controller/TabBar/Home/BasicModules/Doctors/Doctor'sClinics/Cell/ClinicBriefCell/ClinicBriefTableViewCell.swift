//
//  ClinicBriefTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ClinicBriefTableViewCell: UITableViewCell {

    @IBOutlet weak var clinicView: UIView!
    
    @IBOutlet weak var clinicNameLabel: UILabel!
    @IBOutlet weak var clinicAddressLabel: UILabel!
    @IBOutlet weak var clinicDistanceLabel: UILabel!
    @IBOutlet weak var timingsTitleLabel: UILabel!
    @IBOutlet weak var clinicTimingsLabel: UILabel!
    
    @IBOutlet weak var distanceImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
