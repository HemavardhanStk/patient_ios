//
//  DoctorClinicsAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 26/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorClinicsViewController {
    
    @objc func loadFavouriteChangeAPICall() {
        
        let user = Session.shared.getUserInfo()
        let isFavourite     = self.isFav == "Y"
        let doctorId        = self.doctorId
        let updateCommand   = isFavourite ? "remove":"add"
        
        let params = ["UserNo"          : user.id,
                      "DoctorNo"        : "\(doctorId)",
                      "UpdateCommand"   : updateCommand,
                      "Language"        : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_AddDeleteDrFavourites, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                self.isFav = isFavourite ? "N" : "Y"
                self.tableView.reloadData()
            }
        }) {}
    }
    
}
