//
//  DoctorClinicsViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class DoctorClinicsViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String,Any>]()
    var doctorName = String()
    var doctorId = String()
    var isFav = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Navigation.shared.hideNavigation(self)
        self.modalPresentationStyle = .overCurrentContext
    }
    
    func classAndWidgetsInitialise() {
        
        self.doctorNameLabel.text = self.doctorName
        self.closeButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        
        self.setUpColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BasicSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "BasicSearchTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.showsVerticalScrollIndicator = false
        
        self.setTableInset()
    }
    
    func setUpColors() {
        self.tableView.backgroundColor = .clear
    }
    
    func setTableInset() {
        
        var insetValue = screenHeight - 190
        
        for i in 0..<self.infoArray.count {
            
            insetValue -= 82
            
            if let value = self.infoArray[i]["DistanceKMFromUser"] as? String, value != "" {
                insetValue -= 17
            }
            
            if let value = self.infoArray[i]["ClinicTimings"] as? [Dictionary<String, String>] {
                
                for _ in 0..<value.count {
                    insetValue -= 17
                }
            }
            
        }
        
        insetValue = insetValue < 0 ? 0 : insetValue
        
        self.tableView.contentInset = UIEdgeInsets(top: insetValue, left: 0, bottom: 0, right: 0)
    }
    
    @objc func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
}
