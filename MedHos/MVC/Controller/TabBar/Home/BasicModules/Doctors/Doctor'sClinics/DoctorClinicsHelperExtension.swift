//
//  DoctorClinicsHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorClinicsViewController {
    
    func getHeaderTableViewCell() -> DoctorClinicsHeaderTableViewCell {
        
        let cell: DoctorClinicsHeaderTableViewCell = Bundle.main.loadNibNamed("DoctorClinicsHeaderTableViewCell", owner: nil, options: nil)![0] as! DoctorClinicsHeaderTableViewCell
        
        cell.titleLabel.text = self.doctorName
        cell.dismissButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        
        return cell
    }
    
    func getClinicBriefCell(_ indexPath: IndexPath) -> ClinicBriefTableViewCell {
        
        let cell: ClinicBriefTableViewCell = Bundle.main.loadNibNamed("ClinicBriefTableViewCell", owner: nil, options: nil)![0] as! ClinicBriefTableViewCell
        
        
        cell.clinicNameLabel.font       = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.clinicAddressLabel.font    = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.clinicDistanceLabel.font   = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.timingsTitleLabel.font     = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.clinicTimingsLabel.font    = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.clinicNameLabel.textColor      = UIColor.black
        cell.clinicAddressLabel.textColor   = UIColor.darkGray
        cell.clinicDistanceLabel.textColor  = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.timingsTitleLabel.textColor    = UIColor.black
        cell.clinicTimingsLabel.textColor   = UIColor.darkGray
        
        cell.clinicAddressLabel.numberOfLines = 0
        cell.clinicTimingsLabel.numberOfLines = 0
        
        var timings = ""
        
        var item = self.infoArray[indexPath.row - 1]
        
        if let value = item["ClinicTimings"] as? [Dictionary<String, String>] {
            
            for i in 0..<value.count {
                if i == value.count - 1 {
                    timings.append(value[i]["Timing"] ?? "")
                } else {
                    timings.append("\(value[i]["Timing"] ?? "")\n")
                }
            }
        }
        
        cell.clinicNameLabel.text       = item["HospitalName"] as? String ?? ""
        cell.clinicAddressLabel.text    = item["AL4_Area"] as? String ?? ""
        cell.clinicDistanceLabel.text   = item["DistanceKMFromUser"] as? String ?? ""
        cell.timingsTitleLabel.text     = "Timings"
        cell.clinicTimingsLabel.text    = timings
        
        if item["DistanceKMFromUser"] as? String != "" {
            cell.distanceImageView.image = #imageLiteral(resourceName: "ic_achievements_blue")
        }
        
        return cell
    }
    
    func getDoctorViewBookCell() -> DoctorViewBookTableViewCell {
        
        let cell: DoctorViewBookTableViewCell = Bundle.main.loadNibNamed("DoctorViewBookTableViewCell", owner: nil, options: nil)![0] as! DoctorViewBookTableViewCell
        
        let favText = self.isFav == "Y" ? "Unfavourite" : "Favourite"
        
        cell.viewProfileButton.setTitle("View Profile", for: .normal)
        cell.bookAppointmentButton.setTitle("Book Appointment", for: .normal)
        cell.favouriteButton.setTitle(favText, for: .normal)
        
        cell.viewProfileButton.setTitleColor(UIColor.black, for: .normal)
        cell.bookAppointmentButton.setTitleColor(UIColor.black, for: .normal)
        cell.favouriteButton.setTitleColor(UIColor.black, for: .normal)
        
        cell.viewProfileButton.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.bookAppointmentButton.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.favouriteButton.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.viewProfileButton.addTarget(self, action: #selector(gotoViewDoctorProfile), for: .touchUpInside)
        cell.bookAppointmentButton.addTarget(self, action: #selector(gotoBookAppointment), for: .touchUpInside)
        cell.favouriteButton.addTarget(self, action: #selector(loadFavouriteChangeAPICall), for: .touchUpInside)
        
        cell.viewProfileImageView.image         = #imageLiteral(resourceName: "ic_specialization_blue")
        cell.bookAppointmentImageView.image     = #imageLiteral(resourceName: "ic_rate_us_icon")
        cell.favouriteImageView.image           = self.isFav == "Y" ?  #imageLiteral(resourceName: "ic_heart_like") : #imageLiteral(resourceName: "ic_heart_unlike")
        
        switch UIDevice().type {

        case .iPhoneSE, .iPhone5, .iPhone5S :
            cell.bookAppointmentButton.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 12)
            cell.bookAppointmentButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 7, right: 0)
            
        default:
            break
        }
        
        return cell
    }
    
    @objc func gotoViewDoctorProfile() {
        
        if let vc = self.presentingViewController?.children.last as? DoctorBasicSearchViewController {
            
            let viewController = AppController.shared.getDoctorProfileViewController()
            viewController.DoctorId = Int(self.doctorId) ?? 0
            self.dismissViewController()
            vc.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func gotoBookAppointment() {
        
        if let vc = self.presentingViewController?.children.last as? DoctorBasicSearchViewController {
            
            let viewController = AppController.shared.getCreateAppointmentViewController()
            viewController.DoctorId = Int(self.doctorId) ?? 0
            viewController.DoctorName = self.doctorName
            self.dismissViewController()
            vc.navigationController!.pushViewController(viewController, animated: true)
        }
    }
    
}
