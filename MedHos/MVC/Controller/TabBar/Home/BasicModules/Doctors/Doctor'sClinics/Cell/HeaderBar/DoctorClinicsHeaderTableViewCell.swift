//
//  DoctorClinicsHeaderTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 27/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class DoctorClinicsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dismissButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
