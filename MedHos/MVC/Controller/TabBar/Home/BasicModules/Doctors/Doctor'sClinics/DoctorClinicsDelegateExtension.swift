//
//  DoctorClinicsDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorClinicsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.infoArray.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 0:
            return 50
            
        case self.infoArray.count + 1 :
            return 80
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getHeaderTableViewCell()
            
        case self.infoArray.count + 1 :
            return self.getDoctorViewBookCell()
            
        default:
            return getClinicBriefCell(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row > 0 && indexPath.row != self.infoArray.count + 1 {
            print(cell.bounds.height)
        }
    }
}

extension DoctorClinicsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y <= 0 {
            self.headerView.alpha = 0
        } else {
            self.headerView.alpha = 1
        }
    }
    
}
