//
//  DoctorViewBookTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class DoctorViewBookTableViewCell: UITableViewCell {

    @IBOutlet weak var doctorView: UIView!
    
    @IBOutlet weak var viewProfileButton: UIButton!
    @IBOutlet weak var bookAppointmentButton: UIButton!
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var viewProfileImageView: UIImageView!
    @IBOutlet weak var bookAppointmentImageView: UIImageView!
    @IBOutlet weak var favouriteImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
