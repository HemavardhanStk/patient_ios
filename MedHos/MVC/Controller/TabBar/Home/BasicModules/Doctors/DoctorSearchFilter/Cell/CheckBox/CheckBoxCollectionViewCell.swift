//
//  CheckBoxCollectionViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 30/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class CheckBoxCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var CheckBoxImageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
