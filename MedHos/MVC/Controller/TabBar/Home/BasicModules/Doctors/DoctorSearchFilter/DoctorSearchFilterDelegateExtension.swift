//
//  DoctorSearchFilterDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorSearchFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.filtersList.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 5 {
            return CGFloat(76)
        }
        return self.getSessionCellHeight(tableView.tag, subIndex: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 5 {
            let RatingViewCell = UITableViewCell()
            let textLabel = UILabel()
            textLabel.text = "Ratings"
            textLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
            textLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
            textLabel.frame = CGRect(x: 5, y: 15, width: 120, height: 21)
            RatingViewCell.addSubview(textLabel)
            let RatingView = CosmosView()
            RatingView.filledColor = UIColor(netHex: APP_PRIMARY_COLOR)
            RatingView.filledBorderWidth = 0
            RatingView.emptyBorderColor = UIColor.darkGray
            RatingView.emptyBorderWidth = 1
            RatingView.fillMode = 1
            RatingView.rating = Rating
            RatingView.settings.updateOnTouch = true
            RatingView.didFinishTouchingCosmos = { rating in
                self.Rating = rating
            }
            RatingView.totalStars = 5
            RatingView.starSize = 30
            RatingView.frame = CGRect(x: 5, y: 46, width: 180, height: 30)
            RatingView.isUserInteractionEnabled = true
            RatingViewCell.isUserInteractionEnabled = true
            RatingViewCell.addSubview(RatingView)
            
            return RatingViewCell
        }
        
        return self.getSessionViewCell(indexPath, tag: tableView.tag)
    }
}

extension DoctorSearchFilterViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 6 {
            return LanguagesArray.count
        }
        return self.filterSubLists[collectionView.tag].count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var WidthDivider = 0
        
        if collectionView.tag == 0 || collectionView.tag == 3 || collectionView.tag == 4
        {
            WidthDivider = 3
        }
        else if collectionView.tag == 1
        {
            WidthDivider = 2
        }
        else if collectionView.tag == 2 || collectionView.tag == 6
        {
            WidthDivider = 4
        }
        
        return CGSize(width: collectionView.bounds.width / CGFloat(WidthDivider), height: 27)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.getCheckBoxCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            if indexPath.row == 0 {
                self.SearchType.removeAll()
                self.SearchTypeAll = !SearchTypeAll
            } else {
                self.SearchTypeAll = false
                if let index = SearchType.firstIndex(of: filterSubLists[0][indexPath.row]) {
                    self.SearchType.remove(at: index)
                } else {
                    self.SearchType.append(filterSubLists[0][indexPath.row])
                }
            }
        } else if collectionView.tag == 1 {
            self.Appointment = self.filterSubLists[collectionView.tag][indexPath.row]
        } else if collectionView.tag == 2 {
            if indexPath.row == 0 {
                self.GenderArray.removeAll()
                self.GenderAll = !self.GenderAll
            } else {
                self.GenderAll = false
                if let index = self.GenderArray.firstIndex(of: filterSubLists[2][indexPath.row]) {
                    self.GenderArray.remove(at: index)
                } else {
                    self.GenderArray.append(filterSubLists[2][indexPath.row])
                }
            }
        } else if collectionView.tag == 3 {
            if indexPath.row == 0 {
                self.FeesArray.removeAll()
                self.FeesAll = !self.FeesAll
            } else {
                self.FeesAll = false
                if let index = self.FeesArray.firstIndex(of: filterSubLists[3][indexPath.row]) {
                    self.FeesArray.remove(at: index)
                } else {
                    self.FeesArray.append(filterSubLists[3][indexPath.row])
                }
            }
        } else if collectionView.tag == 4 {
            if indexPath.row == 0 {
                self.ExperienceArray.removeAll()
                self.ExperienceAll = !self.ExperienceAll
            } else {
                self.ExperienceAll = false
                if let index = self.ExperienceArray.firstIndex(of: filterSubLists[4][indexPath.row]) {
                    self.ExperienceArray.remove(at: index)
                } else {
                    self.ExperienceArray.append(filterSubLists[4][indexPath.row])
                }
            }
        } else if collectionView.tag == 6 {
            if indexPath.row == 0 {
                self.SelectedLanguagesArray.removeAll()
                self.SelectedLanguagesAll = !self.SelectedLanguagesAll
            } else {
                self.SelectedLanguagesAll = false
                if let index = self.SelectedLanguagesArray.firstIndex(of: LanguagesArray[indexPath.row]) {
                    self.SelectedLanguagesArray.remove(at: index)
                } else {
                    self.SelectedLanguagesArray.append(LanguagesArray[indexPath.row])
                }
            }
        }
        
        collectionView.reloadSections([0])
        
    }
    
}
