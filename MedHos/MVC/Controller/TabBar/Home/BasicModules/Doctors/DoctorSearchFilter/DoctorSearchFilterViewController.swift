//
//  DoctorSearchFilterViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 30/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol DoctorSearchFilterDelegate {
    
    func ApplyTapped(_ SearchType: [String],
                     SearchTypeAll: Bool,
                     Appointment: String,
                     GenderArray: [String],
                     GenderAll: Bool,
                     FeesArray:[String],
                     FeesAll: Bool,
                     ExperienceArray: [String],
                     ExperienceAll: Bool,
                     Rating: Double,
                     SelectedLanguagesArray: [String],
                     SelectedLanguagesAll: Bool)
    
}

class DoctorSearchFilterViewController: UIViewController {
    
    @IBOutlet weak var PopUpCloseButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ClearButton: UIButton!
    @IBOutlet weak var ApplyButton: UIButton!
    
    var delegate: DoctorSearchFilterDelegate?
    
    var infoArray = [Dictionary<String, Any>]()
    var filtersList = ["Search Type","Appointment Type","Gender","Consultation Fees","Exeprience (In Years)"]
    
    var filterSubLists = [["All","Doctors","Clinic/Hospital"],
                          ["Online Appointment","Home Visit","Emergency Care","24 Hours Services"],
                          ["All","Male","Female","Others"],
                          ["All","₹ 0-100","₹ 101-200","₹ 201-300","₹ 301-400","₹ 401+"],
                          ["All","0 - 5","6 - 10","11 - 15","16 - 20","21+"]]
    
    var SearchType = [String]()
    var SearchTypeAll = false
    var Appointment = "Online Appointment"
    var GenderArray = [String]()
    var GenderAll = false
    var FeesArray = [String]()
    var FeesAll = false
    var ExperienceArray = [String]()
    var ExperienceAll = false
    var Rating : Double = 0
    var SelectedLanguagesArray = [String]()
    var SelectedLanguagesAll = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        self.setUpColors()
        self.loadInfoData()
        
        self.PopUpCloseButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        self.ApplyButton.addTarget(self, action: #selector(applyCall), for: .touchUpInside)
        self.ClearButton.addTarget(self, action: #selector(clearCall), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BookAppointmentSessionTableViewCell", bundle: nil), forCellReuseIdentifier: "BookAppointmentSessionTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
    }
    
    func setUpColors() {
        
        self.tableView.backgroundColor = UIColor.white
    }
    
    func loadInfoData() {
        
        if self.SearchTypeAll {
            self.SearchType.removeAll()
        } else {
            for i in 0..<self.SearchType.count {
                for j in 0..<self.filterSubLists[0].count {
                    if self.SearchType[i] == String(self.filterSubLists[0][j].prefix(1)) {
                        self.SearchType.insert(self.filterSubLists[0][j], at: i)
                        self.SearchType.remove(at: i + 1)
                    }
                }
            }
        }
        
        switch self.Appointment {
        case "instant" :
            self.Appointment = self.filterSubLists[1][0]
        case "homevisit" :
            self.Appointment = self.filterSubLists[1][1]
        case "emergency" :
            self.Appointment = self.filterSubLists[1][2]
        case "24hours" :
            self.Appointment = self.filterSubLists[1][3]
        default:
            self.Appointment = self.filterSubLists[1][0]
        }
        
        if self.GenderAll {
            
            self.GenderArray.removeAll()
            
        } else if self.GenderArray.count != 0 {
            for i in 0..<self.GenderArray.count {
                
                self.GenderArray.insert(Helper.shared.getGenderValue(GenderArray[i]), at: i)
                self.GenderArray.remove(at: i + 1)
            }
        }
        
        if self.FeesAll {
            
            self.FeesArray.removeAll()
            
        } else if self.FeesArray.count != 0 {
            
            for i in 0..<self.FeesArray.count {
                for j in 0..<self.filterSubLists[3].count {
                    if  self.filterSubLists[3][j].range(of: self.FeesArray[i]) != nil {
                        self.FeesArray.insert(self.filterSubLists[3][j], at: i)
                        self.FeesArray.remove(at: i + 1)
                    }
                 }
              }
           }
        
        if self.ExperienceAll {
            self.ExperienceArray.removeAll()
        } else if self.ExperienceArray.count != 0 {
            
            for i in 0..<self.ExperienceArray.count {
                for j in 0..<self.filterSubLists[4].count {
                    let trimmedString = self.filterSubLists[4][j].replacingOccurrences(of: " ", with: "")
                    if  trimmedString.contains( self.ExperienceArray[i]) {
                        self.ExperienceArray.insert(self.filterSubLists[4][j], at: i)
                        self.ExperienceArray.remove(at: i + 1)
                    }
                }
            }
        }
        
        if self.SelectedLanguagesAll {
            self.SelectedLanguagesArray.removeAll()
        }
    }

    @objc func applyCall() {
        
        if self.SearchType.count != 0 {
            
            for i in 0..<self.SearchType.count
            {
                if self.SearchType[i] == self.filterSubLists[0][1] {
                    self.SearchType.remove(at: i)
                    self.SearchType.insert("D", at: i)
                    
                } else if SearchType[i] == self.filterSubLists[0][2] {
                    self.SearchType.remove(at: i)
                    self.SearchType.insert("C", at: i)
                }
            }
        }
        else if SearchTypeAll {
            self.SearchType = ["D","C"]
        }
        
        switch self.Appointment {
        case self.filterSubLists[1][0] :
            self.Appointment = "instant"
        case self.filterSubLists[1][1] :
            self.Appointment = "homevisit"
        case self.filterSubLists[1][2] :
            self.Appointment = "emergency"
        case self.filterSubLists[1][3] :
            self.Appointment = "24hours"
        default:
            self.Appointment = "instant"
        }
        
        if self.GenderArray.count != 0 {
            for i in 0..<self.GenderArray.count {
                
                self.GenderArray.insert(Helper.shared.getGenderTag(self.GenderArray[i]), at: i)
                self.GenderArray.remove(at: i + 1)
            }
        } else if self.GenderAll {
            self.GenderArray = GENDER_TAG_TYPE_ARRAY
        }
        
        if FeesArray.count != 0 {
            for i in 0...FeesArray.count - 1 {
                
                FeesArray[i] = FeesArray[i].replacingOccurrences(of: "₹ ", with: "")
                FeesArray[i] = FeesArray[i].replacingOccurrences(of: "+", with: "")
            }
        } else if FeesAll {
            FeesArray = filterSubLists[3]
            FeesArray.removeFirst()
            for i in 0...FeesArray.count - 1 {
                FeesArray[i] = FeesArray[i].replacingOccurrences(of: "₹ ", with: "")
                FeesArray[i] = FeesArray[i].replacingOccurrences(of: "+", with: "")
            }
            
        }
        
        if ExperienceArray.count != 0 {
            for i in 0...ExperienceArray.count - 1 {
                ExperienceArray[i] = ExperienceArray[i].replacingOccurrences(of: " ", with: "")
                ExperienceArray[i] = ExperienceArray[i].replacingOccurrences(of: "+", with: "")
            }
        } else if ExperienceAll {
            ExperienceArray = filterSubLists[4]
            ExperienceArray.removeFirst()
            for i in 0...ExperienceArray.count - 1 {
                ExperienceArray[i] = ExperienceArray[i].replacingOccurrences(of: " ", with: "")
                ExperienceArray[i] = ExperienceArray[i].replacingOccurrences(of: "+", with: "")
            }
        }
        
        if self.SelectedLanguagesAll {
            SelectedLanguagesArray = LanguagesArray
            SelectedLanguagesArray.removeFirst()
        }
        
        self.delegate?.ApplyTapped(self.SearchType,
                                     SearchTypeAll: self.SearchTypeAll,
                                     Appointment: self.Appointment,
                                     GenderArray: self.GenderArray,
                                     GenderAll: self.GenderAll,
                                     FeesArray: self.FeesArray,
                                     FeesAll: self.FeesAll,
                                     ExperienceArray: self.ExperienceArray,
                                     ExperienceAll: self.ExperienceAll,
                                     Rating: self.Rating,
                                     SelectedLanguagesArray: self.SelectedLanguagesArray,
                                     SelectedLanguagesAll: self.SelectedLanguagesAll)
        
        self.dismiss(animated: true, completion: nil)
    }
   
    @objc func clearCall() {
        
        self.SearchType = [String]()
        self.SearchTypeAll = false
        self.Appointment = "Online Appointment"
        self.GenderArray = [String]()
        self.GenderAll = false
        self.FeesArray = [String]()
        self.FeesAll = false
        self.ExperienceArray = [String]()
        self.ExperienceAll = false
        self.Rating = 0
        self.SelectedLanguagesArray = [String]()
        self.SelectedLanguagesAll = false
        
        self.delegate?.ApplyTapped(self.SearchType,
                                   SearchTypeAll: self.SearchTypeAll,
                                   Appointment: self.Appointment,
                                   GenderArray: self.GenderArray,
                                   GenderAll: self.GenderAll,
                                   FeesArray: self.FeesArray,
                                   FeesAll: self.FeesAll,
                                   ExperienceArray: self.ExperienceArray,
                                   ExperienceAll: self.ExperienceAll,
                                   Rating: self.Rating,
                                   SelectedLanguagesArray: self.SelectedLanguagesArray,
                                   SelectedLanguagesAll: self.SelectedLanguagesAll)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
   
}
