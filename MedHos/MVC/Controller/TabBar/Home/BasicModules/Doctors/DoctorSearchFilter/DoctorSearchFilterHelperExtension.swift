//
//  DoctorSearchFilterHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 25/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DoctorSearchFilterViewController {
    
    func getSessionViewCell(_ indexPath: IndexPath, tag: Int) -> BookAppointmentSessionTableViewCell {
        
        let cell: BookAppointmentSessionTableViewCell = Bundle.main.loadNibNamed("BookAppointmentSessionTableViewCell", owner: nil, options: nil)![0] as! BookAppointmentSessionTableViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.sessionLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.sessionLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.sessionStatusView.alpha = 0
        cell.sessionStatusViewLeadingConstraint.constant = 0
        
        if indexPath.row == 6 {
            cell.sessionLabel.text = "Languages"
        } else {
            cell.sessionLabel.text = filtersList[indexPath.row]
        }
        
        cell.collectionView.tag = indexPath.row
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        cell.collectionView.register(UINib(nibName: "CheckBoxCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CheckBoxCollectionViewCell")
        cell.collectionView.backgroundColor = UIColor.clear
        cell.collectionView.isScrollEnabled = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.invalidateLayout()
        cell.collectionView.collectionViewLayout = layout
        
        return cell
    }
    
    func getCheckBoxCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> CheckBoxCollectionViewCell {
        
        let cell : CheckBoxCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CheckBoxCollectionViewCell", for: indexPath) as! CheckBoxCollectionViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.textLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 13)
        cell.textLabel.textColor = UIColor.darkGray
        
        if collectionView.tag == 6 {
            cell.textLabel.text = LanguagesArray[indexPath.row]
        } else {
            cell.textLabel.text = self.filterSubLists[collectionView.tag][indexPath.row]
        }
        
        if collectionView.tag == 0 {
            
            if indexPath.row == 0 {
                if self.SearchTypeAll {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            } else {
                if self.SearchType.contains(self.filterSubLists[0][indexPath.row]) {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            }
        } else if collectionView.tag == 1 {
            if self.Appointment == self.filterSubLists[1][indexPath.row] {
                cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
            } else {
                cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
            }
        } else if collectionView.tag == 2 {
            
            if indexPath.row == 0 {
                if self.GenderAll {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            } else {
                if self.GenderArray.contains(self.filterSubLists[2][indexPath.row]) {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            }
        } else if collectionView.tag == 3 {
            if indexPath.row == 0 {
                if self.FeesAll {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            } else {
                if self.FeesArray.contains(self.filterSubLists[3][indexPath.row]) {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            }
        } else if collectionView.tag == 4 {
            
            if indexPath.row == 0 {
                if self.ExperienceAll {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            } else {
                if self.ExperienceArray.contains(self.filterSubLists[4][indexPath.row]) {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            }
        } else if collectionView.tag == 6 {
            
            if indexPath.row == 0 {
                if self.SelectedLanguagesAll {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            }
            else
            {
                if self.SelectedLanguagesArray.contains(LanguagesArray[indexPath.row]) {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
                } else {
                    cell.CheckBoxImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
                }
            }
        }
        
        return cell
    }
    
    func getSessionCellHeight(_ index: Int, subIndex: Int) -> CGFloat {
        
        var CheckBoxArray = [String]()
        var height: CGFloat = 40
        var WidthDivider = 0
        
        if subIndex == 0 || subIndex == 3 || subIndex == 4 {
            WidthDivider = 3
        } else if subIndex == 1 {
            WidthDivider = 2
        } else if subIndex == 2 || subIndex == 6 {
            WidthDivider = 4
        }
        
        if subIndex == 6 {
            CheckBoxArray = LanguagesArray
        } else {
            CheckBoxArray = self.filterSubLists[subIndex]
        }
        
        if CheckBoxArray.count != 0 {
            let lineCount = CheckBoxArray.count / WidthDivider
            let remainingCount = CheckBoxArray.count % WidthDivider
            let toatlLines = lineCount + (remainingCount > 0 ? 1 : 0)
            height = height + CGFloat(toatlLines * 27)
        } else {
            height = 0
        }
        
        return height
    }
    
    
}
