//
//  PreBookingViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 18/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class PreBookingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, String>]()
    var DoctorName = ""
    var DoctorId = ""
    var DoctorImageUrl = ""
    var DoctorSpecialization = ""
    var HospitalName = ""
    var HospitalAddress = ""
    var HospitalNo = ""
    var TimeString = ""
    var AppointmentDate = ""
    var SessionTag: Int = 0
    var BookingAmountValue = ""
    var FamilyMemberUserId = ""
    var AppointmentNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: TITLE_PRE_BOOKING, controller: self)
        
        self.loadInfoArray()
        self.setupColors()
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoArray() {
        
        let user = Session.shared.getUserInfo()
        
       var dict = [IMAGE: "", PLACEHOLDER : "Appointment Date & Time", TEXT : "\(self.AppointmentDate), \(self.TimeString)"]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Mobile Number", TEXT : user.phone]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Appointment For", TEXT : user.name]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Gender", TEXT : Helper.shared.getGenderValue(user.gender)]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Note", TEXT : "Note: Appointment Booking is Free. Consulting Fees are payable at clinic \n By booking this appointment you agree to the terms and conditions"]
        self.infoArray.append(dict)
        
    }
    
    @objc func popViewControllerToTabBar() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: TabBarViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @objc func popViewController() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

    
}


