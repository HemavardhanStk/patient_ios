//
//  PreBookingHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PreBookingViewController {
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        if let imageName = self.infoArray[indexPath.row][IMAGE], imageName.count != 0 {
            cell.defaultTextIcon.image = UIImage.init(named: imageName)
        } else {
            cell.defaultTextIconWidthConstraint.constant = 0
        }
        cell.defaultTextField.placeholder = self.infoArray[indexPath.row][PLACEHOLDER]
        cell.defaultTextField.text = self.infoArray[indexPath.row][TEXT]
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row + 1
        
        switch indexPath.row {
        case 0:
            cell.defaultTextField.setImageOnRight(ImageName: "ic_calendar_icon",Size: 20)
            break
        case 2:
            cell.defaultTextField.setImageOnRight(ImageName: "edit1",Size: 20)
            break
        case 3:
            cell.defaultTextField.addPickerView(self, array: GENDER_TYPE_ARRAY)
            break
        default: break
        }
        
        if indexPath.row < 4 {
            cell.defaultTextField.returnKeyType = .done
        } else {
            cell.defaultTextField.returnKeyType = .next
        }
        
        return cell
    }
    
    func getDetailedProfileTableViewCell() -> DetailedProfileTableViewCell {
        
        let cell: DetailedProfileTableViewCell = Bundle.main.loadNibNamed("DetailedProfileTableViewCell", owner: nil, options: nil)![0] as! DetailedProfileTableViewCell
        cell.DoctorProfileImageView.af_setImage(withURL: URL(string: self.DoctorImageUrl)!)
        cell.DoctorNameLabel.text = self.DoctorName
        cell.DoctorNameLabel.textColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        cell.DoctorSpecializationLabel.text = self.DoctorSpecialization
        cell.FeeLabel.text = ""
        if self.BookingAmountValue != "0"
        {
            cell.FeeLabel.text = "₹ \(self.BookingAmountValue)"
        }
        cell.HospitalLabel.text = self.HospitalName
        cell.HospitalLabel.textColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        cell.HospitalAddressLabel.text = self.HospitalAddress
        return cell
    }
    
    func getTextViewTableViewCell(_ indexPath: IndexPath) -> TextViewTableViewCell {
        
        let cell: TextViewTableViewCell = Bundle.main.loadNibNamed("TextViewTableViewCell", owner: nil, options: nil)![0] as! TextViewTableViewCell
        
        cell.textView.tag = indexPath.row + 1
        cell.textView.backgroundColor = UIColor.white
        cell.textView.text = self.infoArray[self.infoArray.count - 1][TEXT]
        cell.textView.textColor = UIColor.black
        cell.textView.textAlignment = .center
        cell.isUserInteractionEnabled = false
        
        return cell
    }
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("SUBMIT")
        cell.defaultButton.addTarget(self, action: #selector(loadBookAppointmentAPICall), for: .touchUpInside)
        
        return cell
    }
    
    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            if textField.text?.count == 0 {
                self.updatePickerValues(senderTag: sender.tag, selectedRow: 0)
            }
            textField.resignFirstResponder()
        }
    }
    
    func updatePickerValues(senderTag: Int, selectedRow: Int) {
        if let textField = self.tableView.viewWithTag(senderTag) as? UITextField {
            
            if textField.tag == 4 {
                if GENDER_TYPE_ARRAY.count != 0 {
                    textField.text = GENDER_TYPE_ARRAY[selectedRow]
                }
            }
            self.infoArray[textField.tag - 1][TEXT] = textField.text
        }
    }
}
