//
//  PreBookingAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PreBookingViewController {
    
    @objc func loadBookAppointmentAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        var params = ["AppDate"         :self.AppointmentDate,
                      "BookingFee"      :self.BookingAmountValue,
                      "BookingReason"   :"",
                      "BookingSource"   :"iOS",
                      "Doctor"          :self.DoctorId,
                      "FamilyNewName"   :self.infoArray[2][TEXT] ?? "",
                      "Gender"          :Helper.shared.getGenderTag(self.infoArray[3][TEXT]!),
                      "Hospital"        :"\(self.HospitalNo)",
                      "Language"        :"English",
                      "ModeofPay"       :"",
                      "PaymentStatus"   :"",
                      "SMSFlag"         :"",
                      "Session"         :"\(self.SessionTag)",
                      "StringTime"      :self.TimeString ]
        
        if self.FamilyMemberUserId != "" {
            params["ParentUserNo"] = user.id
            params["UserID"] = self.FamilyMemberUserId
        } else {
            params["ParentUserNo"] = "0"
            params["UserID"] = user.id
        }
        
        HttpManager.shared.loadAPICall(path: PATH_AppoinmentBooking, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                
                if let AppointmentNoforSMS = response["AppointmentNoforSMS"] as? Int,
                    let AppointmentNo = response["AppointmentNo"] as? String {
                    
                    self.AppointmentNo = "\(AppointmentNoforSMS)"
                    
                    AlertHelper.shared.showAlertWithTwoWayHandler(message:AppointmentNo, ButtonName1: "Get SMS", ButtonName2: "Done", handler1: "loadGetSMSAPICall", handler2: "popViewControllerToTabBar", controller: self)
                }
            }
        }) {}
    }
    
    @objc func loadGetSMSAPICall() {
        
        let params = ["AppointmentNo":self.AppointmentNo,
                      "Language":"English"]
        
        HttpManager.shared.loadAPICall(path: PATH_AppointmentSMStoUser, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let message = response["Message"] as? String {
                    AlertHelper.shared.showAlertWithHandler(message: message, handler: "popViewControllerToTabBar", withCancel: false, controller: self)
                } else {
                    self.popViewControllerToTabBar()
                }
             }
        }) {}
    }
}
