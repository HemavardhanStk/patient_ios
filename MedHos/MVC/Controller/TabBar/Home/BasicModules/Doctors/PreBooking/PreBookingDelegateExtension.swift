//
//  PreBookingDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PreBookingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let HeaderView = UIView()
        HeaderView.backgroundColor = UIColor.white
        return HeaderView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let FooterView  = UIView()
        FooterView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        return FooterView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 8 : CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 11 : CGFloat.leastNonzeroMagnitude
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1:6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return indexPath.row == 4 ? 100 : 60
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            return self.getDetailedProfileTableViewCell()
        }
        
        switch indexPath.row {
            
        case 0,1,2,3 :
            return self.getTextFieldCell(indexPath)
        case 4 :
            return self.getTextViewTableViewCell(indexPath)
        case 5 :
            return self.getButtonCell()
        default:
            return UITableViewCell()
        }
        
    }
}

extension PreBookingViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.popViewController()
            return false
        } else if textField.tag == 2 {
            return false
        } else if textField.tag == 3 {
            let viewController = AppController.shared.getSingleSelectionViewController()
            viewController.pageTitle = TITLE_APPOINTMENT_FOR
            viewController.delegate = self
            let navigationViewController = UINavigationController.init(rootViewController: viewController)
            self.present(navigationViewController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
}

extension PreBookingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return GENDER_TYPE_ARRAY.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return GENDER_TYPE_ARRAY[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.updatePickerValues(senderTag: pickerView.tag, selectedRow: row)
    }
    
}

extension PreBookingViewController : SingleSelectionDelegate {
    
    func didSingleSelected(_ selectedItem: String, IDs: String, Value3: String, AppointmentDate: String, AppointmentTime: String, PatientId: String) {
        if selectedItem != ""
        {
            self.infoArray[2][TEXT] = selectedItem
            self.infoArray[3][TEXT] = Value3
            self.FamilyMemberUserId = IDs
            self.tableView.reloadRows(at: [IndexPath(row: 2, section: 1),IndexPath(row: 3, section: 1)], with: .automatic)
        }
    }
    
}
