//
//  DetailedProfileTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 19/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class DetailedProfileTableViewCell: UITableViewCell {

    
    @IBOutlet weak var DoctorProfileImageView: UIImageView!
    @IBOutlet weak var DoctorNameLabel: UILabel!
    @IBOutlet weak var DoctorSpecializationLabel: UILabel!
    @IBOutlet weak var FeeLabel: UILabel!
    
    @IBOutlet weak var HospitalLabel: UILabel!
    @IBOutlet weak var HospitalAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
