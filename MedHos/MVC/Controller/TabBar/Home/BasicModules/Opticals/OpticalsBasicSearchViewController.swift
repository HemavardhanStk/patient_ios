//
//  OpticalsBasicSearchViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 22/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class OpticalsBasicSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, BasicSearchTableViewCellDelegate, CitiesPopOverDelegate  {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()
    let label = UILabel.init()
    
    var OpticalDataArray : [OpticalData]? = []
    var CurrentLocation = ""
    var CurrentLatitude = 0.0
    var CurrentLongitude = 0.0
    var OpticalSearchByNameArray : [DisplayList]? = []
    var IsSearching = false
    var SearchText = ""
    
    var Booler = true
    var Latitude:Double = 0.00
    var Longitude:Double = 0.00
    var Location = "Retrieving location"
    var pageNumber = 0
    var TotalNumberOfPages = 0
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.Booler
        {
            let userLocation :CLLocation = locations[0] as CLLocation
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
                if (error != nil){
                    print("error in reverseGeocode")
                }
                let placemark = placemarks! as [CLPlacemark]
                if placemark.count>0{
                    let placemark = placemarks![0]
                    
                    self.CurrentLatitude = userLocation.coordinate.latitude
                    self.CurrentLongitude = userLocation.coordinate.longitude
                    self.Latitude = userLocation.coordinate.latitude
                    self.Longitude = userLocation.coordinate.longitude
                    self.Location = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    self.CurrentLocation = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    self.OpticalsBasicSearchAPICall(lat: self.Latitude,Long: self.Longitude, LocationArea: self.Location, KeyName: "")
                    
//                    Navigation.shared.setTitlewithRightBarButton(TITLE_OPTICALS, image: "ic_Done", text: self.Location, controller: self)
                    self.viewWillAppear(true)
                }
            }
            self.Booler = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if IsSearching
        {
            self.tableView.separatorStyle = .singleLine
            return 1
        }
        self.tableView.separatorStyle = .none
        return OpticalDataArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if IsSearching
        {
            return OpticalSearchByNameArray?.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 3.5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 3.5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if IsSearching
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LabelTableViewCell", for: indexPath) as! LabelTableViewCell
            
            cell.itemTitleLabel.text = OpticalSearchByNameArray?[indexPath.row].SearchDisplay
            cell.itemTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
            cell.itemImageView.image = #imageLiteral(resourceName: "arrow")
            cell.itemImageViewWidthConstraint.constant = 20
            
            return cell
        }
        return getBasicSearchTableViewCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if OpticalDataArray != nil
        {
            let lastItem = OpticalDataArray!.count - 1
            
            if indexPath.section == lastItem && pageNumber != TotalNumberOfPages
            {
                self.OpticalsBasicSearchAPICall(lat: self.Latitude,Long: self.Longitude, LocationArea: self.Location, KeyName: "")
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if !IsSearching && OpticalDataArray != nil
        {
            let viewController = AppController.shared.getMultiProfileViewController()
            viewController.Title_Name = TITLE_OPTICALS
            viewController.OpticalDetailedArray = self.OpticalDataArray![indexPath.section]
            self.navigationController!.pushViewController(viewController, animated: true)
        }
        else if IsSearching
        {
            self.IsSearching = false
            self.pageNumber = 0
            self.OpticalDataArray?.removeAll()
            self.OpticalsBasicSearchAPICall(lat: self.Latitude, Long: self.Longitude, LocationArea: self.Location, KeyName: (self.OpticalSearchByNameArray?[indexPath.row].DrpText)!)
            self.dismissKeyboard()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Navigation.shared.setTitlewithRightBarButton(TITLE_OPTICALS, image: "", text: self.Location, controller: self)
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.searchBar.delegate = self
        self.searchBar.keyboardType = .alphabet
        self.tableView.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BasicSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "BasicSearchTableViewCell")
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear

        self.setDoneOnKeyboard()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.Location != ""
        {
            label.frame = CGRect.init(x: 20, y: 0, width: 100, height: 40)
            if self.Location.contains(" | ")
            {
                label.text = self.Location
            }
            else
            {
                label.text = "\(self.Location) | "
            }
            label.textColor = UIColor.white
            label.textAlignment = .right
            label.isUserInteractionEnabled = true
            label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rightBarButtonTapped)))
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(marquee), userInfo: nil, repeats: true)
            let barButton = UIBarButtonItem(customView: label)
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    
    func OpticalsBasicSearchAPICall(lat:Double,Long:Double,LocationArea:String,KeyName:String)
    {
        let user = Session.shared.getUserInfo()
        pageNumber = pageNumber + 1
        let params = [
            "ComputerizedTest": "string",
            "UserLatitude": lat,
            "UserLongitude": Long,
            "AreaLatitude": lat,
            "AreaLongitude": Long,
            "SearchCity": LocationArea,
            "SearchName": KeyName,
            "PageSize": 10,
            "PageNumber": pageNumber,
            "StartIndex": 0,
            "FilterStatus": false,
            "SortOrder": 0,
            "Language": "English",
            "UserNo": user.id,
            "Pincode": 0] as [String:Any]
        print(params)
        APIManager.shared.makeHTTPPostRequest(path: "http://service.sensure.in/api/Opticals/OpticalsBasicSearch", body: params) { (data, response, error) in
            if error != nil
            {
                print(error!)
                return
            }
            do
            {
                let OpticalsArray = try JSONDecoder().decode(OpticalsBasicSearchStruct.self, from: data as! Data)
                if OpticalsArray.Result?.Success == 0
                {
                    self.TotalNumberOfPages = OpticalsArray.Result?.TotalPage ?? 0
                    if OpticalsArray.Result?.OpticalData?.count != 0 && OpticalsArray.Result != nil
                    {
                        for i in 0...(OpticalsArray.Result?.OpticalData?.count)! - 1
                        {
                            self.OpticalDataArray!.append(OpticalsArray.Result!.OpticalData![i])
                        }
                    }
                    else
                    {
                        if OpticalsArray.Result?.Message != nil
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                AlertHelper.shared.showAlert(message: (OpticalsArray.Result?.Message)!, controller: self)
                            }
                        }
                        
                        self.IsSearching = false
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    func OpticalsSearchByNameAPICall(lat:Double,long:Double,City:String,Keyword:String) {
        let params = [
            "KeyWord": Keyword,
            "SearchCity": City,
            "AreaLatitude": lat,
            "AreaLongitude": long,
            "Language": "English"
            ] as [String : Any]
        APIManager.shared.makeHTTPPostRequest(path: "http://service.sensure.in/api/Opticals/SearchByOpticalsName", body: params) { (data, any, error) in
            if error != nil
            {
                print(error!)
                return
            }
            do
            {
                let searchByNameResponse = try JSONDecoder().decode(LabSearchByName.self, from: data as! Data)
                
                if searchByNameResponse.Result?.Success == 0
                {
                    self.OpticalSearchByNameArray = searchByNameResponse.Result?.DisplayList
                }
                else
                {
                    AlertHelper.shared.showAlert(message: searchByNameResponse.Result?.Message ?? "Some Thing Went Wrong Try Again", controller: self)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            catch
            {
                print(error)
            }
        }
        
    }

    
    @objc func rightBarButtonTapped() {
        
        if let mvc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CitiesPopOverViewController") as? CitiesPopOverViewController {
            mvc.delegate = self
            mvc.CurrentLocation = self.CurrentLocation
            mvc.CurrentLatitude = self.CurrentLatitude
            mvc.CurrentLongitude = self.CurrentLongitude
            self.present(mvc, animated: true, completion: nil)
            
        }
//
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//
//        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
//        autocompleteController.placeFields = fields
//
//        // Specify a filter.
//        let filter = GMSAutocompleteFilter()
//        filter.country = "IN"
//        autocompleteController.autocompleteFilter = filter
//
//        // Display the autocomplete view controller.
//        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func marquee(){
        
        let str = label.text!
        let indexFirst = str.index(str.startIndex, offsetBy: 0)
        let indexSecond = str.index(str.startIndex, offsetBy: 1)
        
        label.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    }
    
    func didCitySelected(_ Name: String, lat: Double, long: Double) {
        self.Booler = false
        self.pageNumber = 0
        self.TotalNumberOfPages = 0
        self.OpticalDataArray?.removeAll()
        self.Location = Name
        self.Latitude = lat
        self.Longitude = long
        
        self.OpticalsBasicSearchAPICall(lat: self.Latitude, Long: self.Longitude, LocationArea: self.Location, KeyName: "")
        viewWillAppear(true)
        //Navigation.shared.setTitlewithRightBarButton(TITLE_OPTICALS, image: "", text: Name, controller: self)
    }
    
}
extension OpticalsBasicSearchViewController {
    func getBasicSearchTableViewCell(_ indexPath: IndexPath) -> BasicSearchTableViewCell {
        
        let cell: BasicSearchTableViewCell = Bundle.main.loadNibNamed("BasicSearchTableViewCell", owner: nil, options: nil)![0] as! BasicSearchTableViewCell
        
        cell.TextLabel1.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 13)
        cell.TextLabel2.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.TextLabel3.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.TextLabel4.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.TextLabel5.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.bottomTextLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.TextLabel1.textColor = UIColor.black
        cell.TextLabel2.textColor = UIColor.darkGray
        cell.TextLabel3.textColor = UIColor.darkGray
        cell.TextLabel4.textColor = UIColor.green
        cell.TextLabel5.textColor = UIColor.darkGray
        cell.bottomTextLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        
        cell.TextLabel1.numberOfLines = 1
        cell.TextLabel2.numberOfLines = 2
        cell.TextLabel3.numberOfLines = 2
        cell.TextLabel4.numberOfLines = 1
        cell.TextLabel5.numberOfLines = 0
        
        cell.TextLabel1.text = OpticalDataArray![indexPath.section].FIRMNAME
        
        cell.TextLabel2.text = "\(OpticalDataArray![indexPath.section].AREANAME ?? ""), \(OpticalDataArray![indexPath.section].ADDRESSTWO ?? "")"
        if OpticalDataArray![indexPath.section].PHONENO2 != ""
        {
            cell.TextLabel3.text = "\(OpticalDataArray![indexPath.section].PHONENO1 ?? ""), \(OpticalDataArray![indexPath.section].PHONENO2 ?? "")"
        }
        else
        {
            cell.TextLabel3.text = OpticalDataArray![indexPath.section].PHONENO1
        }
        
        
        if OpticalDataArray![indexPath.section].COMPUTERISEDTESTINGYORN == "Y"
        {
            cell.TextLabel4.text = "Computerized Testing Available"
        }
        else
        {
            cell.TextLabel4.text = ""
        }
        
        if OpticalDataArray![indexPath.section].TIMINGS?.count != 0
        {
            var text = ""
            for i in 0...(OpticalDataArray![indexPath.section].TIMINGS?.count)! - 1
            {
                if i != (OpticalDataArray![indexPath.section].TIMINGS?.count)! - 1
                {
                    text.append("\(OpticalDataArray![indexPath.section].TIMINGS![i])\n")
                }
                else
                {
                    text.append(OpticalDataArray![indexPath.section].TIMINGS![i])
                }
            }
            cell.TextLabel5.text = text
        }
        else
        {
            cell.TextLabel5.text = ""
        }
        
        if OpticalDataArray![indexPath.section].LOGOFILENAME != ""
        {
            cell.ProfileimageView.alpha = 1
            cell.ProfileimageView!.loadImageUsingUrlString(urlString: OpticalDataArray![indexPath.section].LOGOFILENAME!)
            cell.ProfileimageView.layer.cornerRadius = 10
            cell.ProfileimageView.layer.masksToBounds = true
        }
        else
        {
            cell.ProfileimageView.alpha = 0
        }
        
        cell.TextLabel1BottomConstraint.constant = 5
        if cell.TextLabel3.text == "" && cell.TextLabel4.text == "" && cell.TextLabel5.text == ""
        {
            cell.TextLabel2BottomConstraint.constant = 30
        }
        else
        {
            cell.TextLabel2BottomConstraint.constant = 5
        }
        
        if cell.TextLabel3.text != ""
        {
            cell.TextLabel3BottomConstraint.constant = 5
        }
        else
        {
            cell.TextLabel3BottomConstraint.constant = 0
        }
        if cell.TextLabel4.text != ""
        {
            cell.TextLabel4BottomConstraint.constant = 5
        }
        else
        {
            cell.TextLabel3BottomConstraint.constant = 0
        }
        
        
        cell.bottomTextLabel.text = OpticalDataArray![indexPath.section].TIMEAVAILABLE
        cell.BasicSearchDelegate = self
        if OpticalDataArray![indexPath.section].TESTINGCONTACTNO != ""
        {
            cell.bottomButton.alpha = 1
            cell.bottomButton.tag = indexPath.section
            cell.bottomViewHeightConstraint.constant = 40
            cell.bottomButton.setPrimaryButtonStyle("Call Now")
            cell.bottomButton.titleLabel?.font = UIFont.init(name: "Avenir-Heavy", size: 13)
        }
        else if OpticalDataArray![indexPath.section].TIMEAVAILABLE == ""
        {
            cell.bottomButton.alpha = 0
            cell.bottomViewHeightConstraint.constant = 0
        }
        else
        {
            cell.bottomButton.alpha = 0
            cell.bottomViewHeightConstraint.constant = 30
        }
        
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
        
        return cell

    }
    
    func BasicSearchCellButtondidPressButton(_ tag: Int) {
        if let url = URL(string: "tel://\(self.OpticalDataArray![tag].TESTINGCONTACTNO!)"), UIApplication.shared.canOpenURL(url) {
            
            UIApplication.shared.open(url)
        }
    }
    

    
}
//extension OpticalsBasicSearchViewController:GMSAutocompleteViewControllerDelegate
//{
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        
//        var names = place.formattedAddress?.components(separatedBy: ",")
//        if names?.count ?? 0 > 3
//        {
//            names?.reverse()
//            self.Location = " \(names![3])"// | \(names![2]) |"
//            //self.Location = "text"
//        }
//        else
//        {
//            self.Location = place.name!
//        }
//        
//        
//        self.Latitude = place.coordinate.latitude
//        self.Longitude = place.coordinate.longitude
//        self.pageNumber = 0
//        self.TotalNumberOfPages = 0
//        self.OpticalDataArray?.removeAll()
//        Navigation.shared.setTitlewithRightBarButton(TITLE_OPTICALS, image: "", text: self.Location, controller: self)
//        //self.viewWillAppear(true)
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//        self.OpticalsBasicSearchAPICall(lat: self.Latitude,Long: self.Longitude, LocationArea: self.Location, KeyName: "")
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print(error)
//    }
//    
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//    
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//    
//}
extension OpticalsBasicSearchViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) ->
        Bool{
            self.IsSearching = !(text.isEmpty)
            
            return true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 2
        {
            self.IsSearching = true
            self.OpticalsSearchByNameAPICall(lat: self.Latitude, long: self.Longitude, City: self.Location, Keyword: searchText)
        }
        else
        {
            self.IsSearching = false
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func setDoneOnKeyboard() {
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.searchBar.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        //        if self.SearchText.count < 3
        //        {
        //            self.Booler = true
        //        }
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }
    
}


