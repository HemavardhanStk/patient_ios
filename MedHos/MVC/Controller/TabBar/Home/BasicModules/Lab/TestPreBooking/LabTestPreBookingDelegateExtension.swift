//
//  LabTestPreBookingDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabTestPreBookingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            return 1
            
        case 1:
            if self.infoArray[0][TEXT] == "Doctor" || self.infoArray[0][TEXT] == "Insurance" {
                return 3
            } else {
                return 2
            }
            
        case 2:
            return 1
            
        case 3:
            return 3
            
        case 4:
            var count = Int()
            
            if self.isHomeSampleAvailable == "Y" {
                count += 1
            }
            
            if self.isReportDeliveryAvailable == "Y" {
                count += 1
            }
            
            if self.infoArray[7][TEXT] == "Yes" || self.infoArray[8][TEXT] == "Yes" {
                count += 1
            }
            
            return count
            
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
            
        case 1:
            return 8
            
        case 2,3,4:
            if self.isHomeSampleAvailable == "N" && self.isReportDeliveryAvailable == "N" && section == 4 {
                return CGFloat.leastNonzeroMagnitude
            }
            return 30
            
        default :
            return CGFloat.leastNonzeroMagnitude
        }

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
            
        case 0:
            return 100
            
        case 1:
            if indexPath.row == 1 {
                return 70
            }
            return 60
            
        case 2:
            return 100
            
        case 3:
            return 60
            
        case 4:
            return 81
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let Header = UIView(frame: CGRect.zero)
        if section > 1 {
            let HeaderLabel = UILabel()
            HeaderLabel.text = self.headers[section - 2]
            HeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
            HeaderLabel.font = HeaderLabel.font.withSize(13)
            HeaderLabel.frame.origin.x = Header.frame.minX + 5
            HeaderLabel.frame.origin.y = Header.frame.minY + 7
            HeaderLabel.frame.size = CGSize(width: 300, height: 20)
            Header.backgroundColor = UIColor.clear
            Header.layer.masksToBounds = true
            Header.addSubview(HeaderLabel)
        }
        return Header
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath {
            
        case [0,0]:
            return self.getLabProfileCell()
            
        case [1,0]:
            return self.getTextFieldCell(indexPath)
            
        case [1,1]:
            return self.getRadioButtonCell(indexPath)
            
        case[1,2]:
            return self.getTextFieldCell(indexPath)
            
        case [2,0]:
            return self.getFilesCell()
            
        case [3,0], [3,1]:
            return self.getDualTextField(indexPath)
            
        case [3,2]:
            return self.getTextFieldCell(indexPath)
            
        case [4,0]:
            return self.getPickUpChargesCell(indexPath)
            
        case [4,1]:
            if self.isHomeSampleAvailable == "Y" && self.isReportDeliveryAvailable == "Y" {
                return self.getPickUpChargesCell(indexPath)
            } else {
                return UITableViewCell()
            }
            
        case [4,2]:
            return self.getUserDefaultAddressCell()
            
        default:
            return UITableViewCell()
        }
    }
}

extension LabTestPreBookingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionID", for: indexPath as IndexPath) as! PhotoCollectionViewCell
        
        if self.prescriptionFile.count == 0 {
            cell.IconImageView.image = UIImage(named: "camera1")
            cell.deleteButton.isHidden = true
        } else {
            cell.deleteButton.isHidden = false
            cell.IconImageView.image = self.prescriptionFile[0] as? UIImage ?? #imageLiteral(resourceName: "ic_test_blue")
        }
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteFile), for: .touchUpInside)
        cell.IconImageView.setRoundedCorner()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.prescriptionFile.count == 0 {
            self.uploadFilesTapped()
        }
        
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension LabTestPreBookingViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == self.appDateTag {
            self.popViewController()
        } else if textField.tag == 2 {
            let viewController = AppController.shared.getSingleSelectionViewController()
            viewController.pageTitle = TITLE_APPOINTMENT_FOR
            viewController.delegate = self
            let navigationViewController = UINavigationController.init(rootViewController: viewController)
            self.present(navigationViewController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag != self.appDateTag {
            self.infoArray[textField.tag][TEXT] = textField.text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let newLength = text.count + string.count - range.length
            if textField.tag == 4 {
                return newLength <= 2
            }
        }
        
        return true
    }
    
}

extension LabTestPreBookingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imager = info[.originalImage] as? UIImage {
            self.prescriptionFile.append(imager)
        }
        self.tableView.reloadSections(IndexSet([2]), with: .automatic)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension LabTestPreBookingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 5 {
            return GENDER_TYPE_ARRAY.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 5 {
            return GENDER_TYPE_ARRAY[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.updatePickerValues(senderTag: pickerView.tag, selectedRow: row)
    }
}

extension LabTestPreBookingViewController: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.downloadfile(URL: myURL as NSURL)
        print("import result : \(myURL)")
    }
    
    func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
}

extension LabTestPreBookingViewController: SingleSelectionDelegate {
    func didSingleSelected(_ selectedItem: String, IDs: String, Value3: String, AppointmentDate: String, AppointmentTime: String, PatientId: String) {
        
        if selectedItem.count != 0 {
            self.infoArray[2][TEXT] = selectedItem
            self.infoArray[2][USER_ID] = IDs
            self.infoArray[4][TEXT] = ""
            self.infoArray[5][TEXT] = Value3
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 3),IndexPath(row: 1, section: 3)], with: .automatic)
        }
    }
}
