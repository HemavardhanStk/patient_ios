//
//  DualTextFieldTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class DualTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField1: ACFloatingTextfield!
    @IBOutlet weak var textField2: ACFloatingTextfield!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
