//
//  UserLocationTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 05/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class UserLocationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressTextLabel: UILabel!
    
    @IBOutlet weak var addressTitleLabelLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
