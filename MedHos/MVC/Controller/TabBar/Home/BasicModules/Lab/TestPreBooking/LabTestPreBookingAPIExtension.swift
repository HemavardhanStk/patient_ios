//
//  LabTestPreBookingAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabTestPreBookingViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        var message = STR_PLEASE_ENTER
        
        var nonMandatoryFields = [1,6]
        
        if let value = self.infoArray[1][TEXT],
            self.infoArray[0][TEXT] == self.infoArray[0][VALUE_ONE],
            value == "" {
            
            isValidationSuccess = false
            message = message + self.infoArray[1][PLACEHOLDER]!
            
        } else if let value = self.infoArray[1][TEXT],
            self.infoArray[0][TEXT] == self.infoArray[0][VALUE_TWO],
            value == "" {
            
            isValidationSuccess = false
            message = message + self.infoArray[1][SUB_PLACEHOLDER]!
        }
        
        if !isValidationSuccess {
            AlertHelper.shared.showAlert(message: message, controller: self)
            return isValidationSuccess
        }

        if self.isHomeSampleAvailable == "N" {
            nonMandatoryFields.append(7)
        }
        if self.isReportDeliveryAvailable == "N" {
            nonMandatoryFields.append(8)
        }
        
        if self.infoArray[7][TEXT] == "YES" || self.infoArray[8][TEXT] == "YES" {
            nonMandatoryFields.append(9)
        }
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: infoArray, nonMandatoryFields: nonMandatoryFields, controller: self)
        
        if let email = self.infoArray[6][TEXT],
            email != "",
            isValidationSuccess {
            
            isValidationSuccess = ValidationHelper.shared.isValidEmailID(email)
            
            if !isValidationSuccess {
                message = message + STR_VALID + self.infoArray[6][PLACEHOLDER]!
                AlertHelper.shared.showAlert(message: message, controller: self)
            }
            
        }
        
        if self.isPrescriptionMandatory {
            isValidationSuccess = self.prescriptionFile.count != 0
            
            if !isValidationSuccess {
                message = "Select Prescription to Upload"
                AlertHelper.shared.showAlert(message: message, controller: self)
            }
        }
        
        if self.infoArray[9][TEXT] == EDIT_LOCATION {
            isValidationSuccess = false
            message = "Select Location"
            AlertHelper.shared.showAlert(message: message, controller: self)
        }
        
        return isValidationSuccess
    }
    
    func loadBookTestWithOutPrescriptionAPICall() {
        
        let params = getParams()
        
        HttpManager.shared.loadAPICall(path: PATH_BookingDiagnosticLab, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let message = response["Message"] as? String,
                    let appNo = response["AppointmentNo"] as? Int {
                    
                    self.loadSMSAPICall("\(appNo)")
                    AlertHelper.shared.showAlertWithHandler(message: message, handler: "popViewControllerToTabBar", withCancel: false, controller: self)
                }
            }
        }) {}
    }
    
    func loadBookTestWithPrescriptionAPICall() {
        
        let params = getParams()
        
        HttpManager.shared.uploadImageWithParametersAnd(endUrl: PATH_DiagnosticLabbookingwithPrescription, photo: prescriptionFile as? [UIImage] ?? [], dataArray: prescriptionFile as? [Data] ?? [], parameters: params, headers: nil, jsonName: "LabAppointmentBookingData", controller: self) { (response, InvoiceResponse, responseValue) in
            
            if InvoiceResponse?.Success == 0 {
                AlertHelper.shared.showAlertWithHandler(message: InvoiceResponse!.Message ?? "Test has been Booked Successfully", handler: "popViewControllerToTabBar", withCancel: false, controller: self)
                if let appNo = responseValue["AppointmentNo"] as? Int {
                    self.loadSMSAPICall("\(appNo)")
                }
            } else {
                AlertHelper.shared.showAlert(message: InvoiceResponse?.Message ?? "Something went wrong! Try again later", controller: self)
            }
        }
        
    }
    
    func getParams() -> Dictionary<String,Any> {
        
        let user = Session.shared.getUserInfo()
        if let FamilyName = self.infoArray[2][TEXT],
            let UserId = self.infoArray[2][USER_ID],
            let MobileNumber = self.infoArray[3][TEXT],
            let Age = self.infoArray[4][TEXT],
            let Gender = self.infoArray[5][TEXT],
            let EmailID = self.infoArray[6][TEXT],
            let Address = self.infoArray[9][TEXT] {
            
            var UserDetailNo = String()
            var ParentUserNo = String()
            var referenceName = ""
            
            if UserId != user.id {
                
                UserDetailNo = UserId
                ParentUserNo = user.id
            } else {
                UserDetailNo = UserId
                ParentUserNo = "0"
            }
            
            let referredBy = self.infoArray[0][TEXT] == self.infoArray[0][VALUE_THREE] ? "user": self.infoArray[0][TEXT]!
            
            if referredBy == self.infoArray[0][VALUE_ONE] || referredBy == self.infoArray[0][VALUE_TWO] {
                referenceName = self.infoArray[1][TEXT]!
            }
            
            let HomeSampleType = self.infoArray[7][TEXT] == "YES" ? "Y":"N"
            let ReportDeliveryathome = self.infoArray[8][TEXT] == "YES" ? "Y":"N"
            
           let params = ["AgeType"                  : "",
                         "CartItemCategory"         : "",
                         "DiagnosticLabGUID"        : "",
                         "AppointmentDetails"       : self.AppointmentDetails,
                         "EmailID"                  : EmailID,
                         "FamilyName"               : FamilyName,
                         "Gender"                   : Helper.shared.getGenderTag(Gender),
                         "HomeSampleType"           : HomeSampleType,
                         "Language"                 : "English",
                         "Location"                 : Address,
                         "MobileNumber"             : MobileNumber,
                         "PatientNameforReport"     : FamilyName,
                         "ReferredBy"               : referredBy,
                         "UserReferredValue"        : referenceName,
                         "ReportDeliveryathome"     : ReportDeliveryathome,
                         "TestNos"                  : [],
                         "Age"                      : Age,
                         "DiagnosticLabNo"          : "0",
                         "ParentUserNo"             : ParentUserNo,
                         "TestorPackageNo"          : "0",
                         "UserDetailNo"             : UserDetailNo] as [String : Any]
            
            return params
        }
        return Dictionary<String,Any>()
    }
    
    func loadSMSAPICall(_ AppNo: String) {
        
        let params = ["AppointmentNo"       : AppNo,
                      "Language"            : "English"]

        HttpManager.shared.loadAPICall(path: PATH_DiagnosticLabAppointmentSMStoUser, params: params, httpMethod: .post, controller: self, completion: { (response) in
            
        }) {}
    }
}
