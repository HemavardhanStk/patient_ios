//
//  LabTestPreBookingHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import MobileCoreServices

extension LabTestPreBookingViewController {
    
    func getLabProfileCell() -> PreBookingLabProfileTableViewCell {
        
        let cell: PreBookingLabProfileTableViewCell = Bundle.main.loadNibNamed("PreBookingLabProfileTableViewCell", owner: nil, options: nil)![0] as! PreBookingLabProfileTableViewCell
        
        cell.labTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.labAddressLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.labTitleLabel.textColor = UIColor.black
        cell.labAddressLabel.textColor = UIColor.darkGray
        
        cell.labTitleLabel.text = self.labName
        cell.labAddressLabel.text = self.labAddress
        
        if let url = URL(string: self.labLogo) {
           cell.labLogoImageView.af_setImage(withURL: url)
        }
        cell.labLogoImageView.layer.cornerRadius = 35
        cell.labLogoImageView.layer.masksToBounds = true
        
        return cell
    }
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell

        cell.defaultTextIconWidthConstraint.constant = 0
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        
        switch indexPath {
        case [1,0]:
            cell.defaultTextField.setImageOnRight(ImageName: "ic_calendar_icon",Size: 20)
            cell.defaultTextField.placeholder = "Appointment Date & Time"
            cell.defaultTextField.text = "\(self.AppointmentDate), \(self.TimeString)"
            cell.defaultTextField.tag = self.appDateTag
            break
            
        case [1,2]:
            if self.infoArray[0][TEXT] == self.infoArray[0][VALUE_ONE] {
                cell.defaultTextField.placeholder = self.infoArray[1][PLACEHOLDER]
            } else if self.infoArray[0][TEXT] == self.infoArray[0][VALUE_TWO] {
                cell.defaultTextField.placeholder = self.infoArray[1][SUB_PLACEHOLDER]
            }
            cell.defaultTextField.text = self.infoArray[1][TEXT]
            cell.defaultTextField.tag = 1
            break
            
        case [3,2]:
            cell.defaultTextField.placeholder = self.infoArray[6][PLACEHOLDER]
            cell.defaultTextField.text = self.infoArray[6][TEXT]
            cell.defaultTextField.tag = 6
            break
            
        default:
            break
        }
        
        return cell
    }
    
    func getFilesCell() -> CollectionTableViewCell {
        
        let cell: CollectionTableViewCell = Bundle.main.loadNibNamed("CollectionTableViewCell", owner: nil, options: nil)![0] as! CollectionTableViewCell
        
        cell.CollectionView.delegate = self
        cell.CollectionView.dataSource = self
        cell.CollectionView.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 0)
        
        return cell
    }
    
    func getDualTextField(_ indexPath: IndexPath) -> DualTextFieldTableViewCell {
        
        let cell: DualTextFieldTableViewCell = Bundle.main.loadNibNamed("DualTextFieldTableViewCell", owner: nil, options: nil)![0] as! DualTextFieldTableViewCell
        
        var tag1 = 0
        var tag2 = 0
        
        if indexPath.row == 0 {
            tag1 = 2
            tag2 = 3
        } else if indexPath.row == 1 {
            tag1 = 4
            tag2 = 5
        }
        
        cell.textField1.setTextFieldStyle()
        cell.textField2.setTextFieldStyle()
        
        cell.textField1.delegate = self
        cell.textField2.delegate = self

        cell.textField1.placeholder = self.infoArray[tag1][PLACEHOLDER]
        cell.textField2.placeholder = self.infoArray[tag2][PLACEHOLDER]
        cell.textField1.text = self.infoArray[tag1][TEXT]
        cell.textField2.text = self.infoArray[tag2][TEXT]
        cell.textField1.tag = tag1
        cell.textField2.tag = tag2

        if indexPath.row == 0 {
            cell.textField1.setImageOnRight(ImageName: "edit1", Size: 15)
            cell.textField2.isUserInteractionEnabled = false
        } else if indexPath.row == 1 {
            cell.textField1.keyboardType = .numberPad
            cell.textField2.addPickerView(self)
        }
        
        return cell
    }
    
    func getRadioButtonCell(_ indexPath: IndexPath) -> RadioButtonTableViewCell {
        
        let cell: RadioButtonTableViewCell = Bundle.main.loadNibNamed("RadioButtonTableViewCell", owner: nil, options: nil)![0] as! RadioButtonTableViewCell
        
        cell.titleLabel.text = "Referred by"
        cell.titleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.radioButtonOne.setTitle(self.infoArray[0][VALUE_ONE], for: .normal)
        cell.radioButtonTwo.setTitle(self.infoArray[0][VALUE_TWO], for: .normal)
        cell.radioButtonThree.setTitle(self.infoArray[0][VALUE_THREE], for: .normal)
        
        cell.radioButtonOne.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.radioButtonTwo.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.radioButtonThree.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.radioButtonOne.setTitleColor(UIColor.black, for: .normal)
        cell.radioButtonTwo.setTitleColor(UIColor.black, for: .normal)
        cell.radioButtonThree.setTitleColor(UIColor.black, for: .normal)
        
        if self.infoArray[0][TEXT] == self.infoArray[0][VALUE_ONE] {
            
            cell.checkBoxImageViewOne.image = UIImage(named: "ic_check_box_selected")
            cell.checkBoxImageViewTwo.image = UIImage(named: "ic_check_box_empty")
            cell.checkBoxImageViewThree.image = UIImage(named: "ic_check_box_empty")
            
        } else if self.infoArray[0][TEXT] == self.infoArray[0][VALUE_TWO] {
            
            cell.checkBoxImageViewOne.image = UIImage(named: "ic_check_box_empty")
            cell.checkBoxImageViewTwo.image = UIImage(named: "ic_check_box_selected")
            cell.checkBoxImageViewThree.image = UIImage(named: "ic_check_box_empty")
            
        } else if self.infoArray[0][TEXT] == self.infoArray[0][VALUE_THREE] {
            
            cell.checkBoxImageViewOne.image = UIImage(named: "ic_check_box_empty")
            cell.checkBoxImageViewTwo.image = UIImage(named: "ic_check_box_empty")
            cell.checkBoxImageViewThree.image = UIImage(named: "ic_check_box_selected")
            
        }
        
        cell.radioButtonOne.tag = 0
        cell.radioButtonTwo.tag = 0
        cell.radioButtonThree.tag = 0
        cell.radioButtonOne.addTarget(self, action: #selector(radioTypeValueChanged), for: .touchUpInside)
        cell.radioButtonTwo.addTarget(self, action: #selector(radioTypeValueChanged), for: .touchUpInside)
        cell.radioButtonThree.addTarget(self, action: #selector(radioTypeValueChanged), for: .touchUpInside)
        
        return cell
    }
    
    func getPickUpChargesCell(_ indexPath: IndexPath) -> PickUpChargeTableViewCell {
        
        let cell: PickUpChargeTableViewCell = Bundle.main.loadNibNamed("PickUpChargeTableViewCell", owner: nil, options: nil)![0] as! PickUpChargeTableViewCell
        
        var tag = Int()
        
        if indexPath.row == 0 && self.isHomeSampleAvailable == "Y" {
            tag = 7
        } else {
            tag = 8
        }
            
        cell.textField.setTextFieldStyle()
        cell.textField.isUserInteractionEnabled = false
        cell.textField.setTextFieldBottomLineColor(UIColor.white, selectedLineColor: UIColor.white)
        
        cell.titlelabel.text = self.infoArray[tag][PLACEHOLDER]
        cell.titlelabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.radioButtonOne.setTitle(self.infoArray[tag][VALUE_ONE], for: .normal)
        cell.radioButtonTwo.setTitle(self.infoArray[tag][VALUE_TWO], for: .normal)
        
        cell.radioButtonOne.setTitleColor(UIColor.black, for: .normal)
        cell.radioButtonTwo.setTitleColor(UIColor.black, for: .normal)

        if self.infoArray[tag][TEXT] == self.infoArray[tag][VALUE_ONE] {
            
            cell.checkBoxImageViewOne.image = UIImage(named: "ic_check_box_selected")
            cell.checkBoxImageViewTwo.image = UIImage(named: "ic_check_box_empty")
        } else if self.infoArray[tag][TEXT] == self.infoArray[tag][VALUE_TWO] {
            
            cell.checkBoxImageViewOne.image = UIImage(named: "ic_check_box_empty")
            cell.checkBoxImageViewTwo.image = UIImage(named: "ic_check_box_selected")
        }
        
        if self.infoArray[tag]["CHARGESVALUE"] != "0" && self.infoArray[tag][TEXT] == self.infoArray[tag][VALUE_ONE] {
            
            cell.textField.placeholder = self.infoArray[tag]["CHARGESPLACEHOLDER"]
            cell.textField.text = self.infoArray[tag]["CHARGESVALUE"]
            cell.textField.alpha = 1
        } else {
            cell.textField.alpha = 0
        }
        
        cell.radioButtonOne.tag = tag
        cell.radioButtonTwo.tag = tag
        cell.radioButtonOne.addTarget(self, action: #selector(radioTypeValueChanged), for: .touchUpInside)
        cell.radioButtonTwo.addTarget(self, action: #selector(radioTypeValueChanged), for: .touchUpInside)

        return cell
    }
    
    func getUserDefaultAddressCell() -> UserLocationTableViewCell {
        
        let cell: UserLocationTableViewCell = Bundle.main.loadNibNamed("UserLocationTableViewCell", owner: nil, options: nil)![0] as! UserLocationTableViewCell
        
        cell.editButton.addTarget(self, action: #selector(editDefaultAddress), for: .touchUpInside)
        
        cell.addressTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.addressTextLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.addressTitleLabel.textColor = UIColor.black
        cell.addressTextLabel.textColor = UIColor.darkGray
        
        cell.addressTextLabel.numberOfLines = 2
        
        cell.addressTitleLabel.text = self.infoArray[9][PLACEHOLDER]
        cell.addressTextLabel.text = self.infoArray[9][TEXT]
        
        return cell
    }
    
    @objc func uploadFilesTapped() {
        
        let alert = UIAlertController(title: APP_NAME, message: "Choose Image from...", preferredStyle: .actionSheet);
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in
            
            self.pickImageFromCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Gallery", style: .default, handler: {(action:UIAlertAction) in
            
            self.pickImageFromPhotos()
        }))
        
        alert.addAction(UIAlertAction(title: "Upload PDF", style: .default, handler: {(action:UIAlertAction) in
            
            self.attachDocument()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func pickImageFromCamera() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .camera
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    func pickImageFromPhotos() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .photoLibrary
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    func attachDocument() {
        
        let types: [String] = [kUTTypePDF as String]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    
    func downloadfile(URL: NSURL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL as URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = response?.mimeType
                print("Success: \(String(describing: statusCode))")
                DispatchQueue.main.async(execute: {
                    self.prescriptionFile.append(data as Any)
                    self.tableView.reloadSections([2], with: .automatic)
                })
            } else {
                // Failure
                print("Failure: %@", error!.localizedDescription)
            }
        })
        task.resume()
    }
    
    @objc func radioTypeValueChanged(button: UIButton) {
        
        self.view.endEditing(true)
        let text = button.titleLabel!.text!
        self.infoArray[button.tag][TEXT] = text
        if button.tag > 0 {
            self.tableView.reloadSections([4], with: .automatic)
        } else {
            self.infoArray[1][TEXT] = ""
            self.tableView.reloadSections([1], with: .automatic)
        }
    }
    
    @objc func deleteFile(_ sender: UIButton) {
        
        self.prescriptionFile.remove(at: sender.tag)
        self.tableView.reloadSections([2], with: .automatic)
    }
    
    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            if textField.text?.count == 0 {
                self.updatePickerValues(senderTag: sender.tag, selectedRow: 0)
            }
            textField.resignFirstResponder()
        }
    }
    
    func updatePickerValues(senderTag: Int, selectedRow: Int) {
        if let textField = self.tableView.viewWithTag(senderTag) as? UITextField {
            if textField.tag == 5 {
                if GENDER_TYPE_ARRAY.count != 0 {
                    textField.text = GENDER_TYPE_ARRAY[selectedRow]
                }
            }
            self.infoArray[textField.tag][TEXT] = textField.text
        }
    }
    
    @objc func editDefaultAddress() {
        
        let viewController = AppController.shared.getUserLocationViewController()
//        viewController.parentName = "LabPreBooking"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func setDefaultAddress() {
        
        let defaultAddressString = Session.shared.getUserDefaultAddressString()
        
        self.infoArray[9][TEXT] = defaultAddressString
        
        self.tableView.reloadSections(([4]), with: .automatic)
        
    }
    
}
