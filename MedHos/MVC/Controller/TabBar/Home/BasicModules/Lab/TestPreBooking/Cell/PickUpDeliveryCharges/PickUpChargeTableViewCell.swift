//
//  PickUpChargeTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class PickUpChargeTableViewCell: UITableViewCell {

    @IBOutlet weak var titlelabel: UILabel!
    
    @IBOutlet weak var checkBoxImageViewOne: UIImageView!
    @IBOutlet weak var checkBoxImageViewTwo: UIImageView!
    
    @IBOutlet weak var radioButtonOne: UIButton!
    @IBOutlet weak var radioButtonTwo: UIButton!
    @IBOutlet weak var textField: ACFloatingTextfield!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
