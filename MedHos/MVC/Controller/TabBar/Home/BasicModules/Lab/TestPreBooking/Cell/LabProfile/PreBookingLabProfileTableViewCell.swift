//
//  PreBookingLabProfileTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class PreBookingLabProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var labTitleLabel: UILabel!
    @IBOutlet weak var labAddressLabel: UILabel!
    
    @IBOutlet weak var labLogoImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
