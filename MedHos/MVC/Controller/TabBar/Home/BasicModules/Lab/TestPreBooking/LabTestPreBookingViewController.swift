//
//  LabTestPreBookingViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabTestPreBookingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    
    var infoArray = [Dictionary<String, String>]()
    
    var imagePickerController: UIImagePickerController?
    
    let appDateTag = 33
    
    var headers = ["Upload Test Prescription(Optional)","Patient details", "DoorStep Serives"]
    
    let USER_ID = "USERID"
    
    var labName = ""
    var labLogo = ""
    var labAddress = ""
    
    var cartCount = ""
    
    var isHomeSampleAvailable = ""
    var homeSampleCharge = ""
    var isReportDeliveryAvailable = ""
    var reportDeliveryCharges = ""
    
    var TimeString = ""
    var AppointmentDate = ""
    var SessionTag: Int = 0
    var AppointmentDetails = ""
    
    var prescriptionFile = [Any]()
    var isPrescriptionMandatory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        if self.cartCount != "0" && self.cartCount.count != 0 {
            Navigation.shared.setTitlewithRightBarButton(TITLE_PRE_BOOKING, badge: self.cartCount, image: "qr-1", text: "", controller: self)
        } else {
            Navigation.shared.setTitle(title: TITLE_PRE_BOOKING, controller: self)
        }
        
        self.loadInfoArray()
        self.setupColors()
        
        self.headers[0] = self.isPrescriptionMandatory ? "Upload Test Prescription" : "Upload Test Prescription(Optional)"
        
        self.imagePickerController = UIImagePickerController.init()
        self.imagePickerController?.delegate = self 
        
        self.button.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(setDefaultAddress), name: Notification.Name.Task.ReloadLabPreBooking, object: nil)
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoArray() {
        
        let user = Session.shared.getUserInfo()
        let defaultAddressString = Session.shared.getUserDefaultAddressString()
        
        var dict = [IMAGE: "", PLACEHOLDER : "Referred By", VALUE_ONE : "Doctor", VALUE_TWO : "Insurance", VALUE_THREE : "Self", TEXT : ""]
        self.infoArray.append(dict)

        dict = [IMAGE: "", PLACEHOLDER : "Doctor Name", SUB_PLACEHOLDER : "Insurance Name", TEXT : ""]
        self.infoArray.append(dict)

        dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : user.name, USER_ID: user.id]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Mobile Number", TEXT : user.phone]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Age", TEXT : user.age]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Gender", TEXT : Helper.shared.getGenderValue(user.gender)]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Email Id", TEXT : user.email]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Home Sample Collection Required", VALUE_ONE : "Yes", VALUE_TWO : "No", TEXT : "", "CHARGESPLACEHOLDER":"Min Charges", "CHARGESVALUE":self.homeSampleCharge]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Report Delivery At Home Required", VALUE_ONE : "Yes", VALUE_TWO : "No", TEXT : "", "CHARGESPLACEHOLDER":"Min Charges", "CHARGESVALUE":self.reportDeliveryCharges]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Address Details", TEXT : defaultAddressString]
        self.infoArray.append(dict)
        
    }
    
    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            if prescriptionFile.count == 0 {
                self.loadBookTestWithOutPrescriptionAPICall()
            } else {
                self.loadBookTestWithPrescriptionAPICall()
            }
        }
    }

    @objc func rightBarButtonTapped() {
        
        let navigationStackCount: Int! = self.navigationController?.viewControllers.count
        guard let viewController = self.navigationController?.viewControllers[navigationStackCount-3] else { return }
        
        if ((viewController as? LabChartViewController) != nil) {
//            NotificationCenter.default.post(name: Notification.Name.Task.ReloadLabCartList, object: self, userInfo: nil)
            self.navigationController?.popToViewController(viewController, animated: true)
        } else {
            let viewController = AppController.shared.getLabChartViewController()
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func popViewControllerToTabBar() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: TabBarViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @objc func popViewController() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
}
