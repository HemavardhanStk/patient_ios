//
//  LabSlotPickerDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabSlotPickerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
//        if tableView.tag == self.tableViewTag {
//
//            if let dict = self.infoArray["DoctorHospitals"] as? [Dictionary<String, Any>] {
//                return dict.count
//            } else {
//                return 0
//            }
//        } else {
            return 1
//        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == self.tableViewTag {
            return 1
        } else {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView.tag == self.tableViewTag {
            return self.getHospitalCellHeight(indexPath.section)
        } else {
            return self.getSessionCellHeight(tableView.tag - 1, subIndex: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == self.tableViewTag {
            return self.getHospitalCell(indexPath)
        } else {
            return self.getSessionViewCell(indexPath, tag: tableView.tag)
        }
    }
}

extension LabSlotPickerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == self.calendarIndex {
            return self.calendarInfoArray.count
        } else {
            let indexTag = collectionView.tag / 100
            let sessionTag = collectionView.tag % 100
            let timeArray = self.getTimeArray(indexTag - 1, subIndex: sessionTag)
            return timeArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == self.calendarIndex {
            return CGSize(width: 60, height: collectionView.bounds.height)
        } else {
            return CGSize(width: collectionView.bounds.width / 2, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == self.calendarIndex {
            return self.getCalendarCell(collectionView, indexPath: indexPath)
        } else {
            return self.getTimeCell(collectionView, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == self.calendarIndex {
            
            let selectedIndexPath = IndexPath.init(row: self.selectedDateIndex, section: 0)
            if let cell = collectionView.cellForItem(at: selectedIndexPath) as? CalenderCollectionViewCell {
                self.setupUnSelectedDateInCalendar(cell)
            }
            
            if let cell = collectionView.cellForItem(at: indexPath) as? CalenderCollectionViewCell {
                self.selectedDateIndex = indexPath.row
                self.setupSelectedDateInCalendar(cell)
            }
            
            self.getLabTimingAPICall()
        }
        else {
            
            let indexTag = collectionView.tag / 100
            let sessionTag = collectionView.tag % 100
            
            let timeArray = self.getTimeArray(indexTag - 1, subIndex: sessionTag)
            if timeArray.count != 0 {
                let text = timeArray[indexPath.row]
                let statusArray = text.components(separatedBy: "$")
                if statusArray.count != 0 {
                    let title = statusArray[0]
                    let status = statusArray[1]
                    
                    if status == "A" {
                        
                        let (_, sessionTag) = self.getSessionTitle(sessionTag)

                        self.TimeString              = title
                        self.SessionTag              = sessionTag
                        
                        if let selectedDate = self.calendarInfoArray[self.selectedDateIndex][self.NEXT_DATE] as? Date {
                            let date = Helper.shared.getString(selectedDate, format: "MM/dd/yyyy")
                            self.AppointmentDate = date
                        }
                        
                        self.gotoPreBooking()
                    }
                }
            }
        }
    }
}
