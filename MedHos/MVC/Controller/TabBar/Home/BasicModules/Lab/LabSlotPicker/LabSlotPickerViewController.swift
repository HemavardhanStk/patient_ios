//
//  LabSlotPickerViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabSlotPickerViewController: UIViewController {

    @IBOutlet weak var calendarCollectionView: UICollectionView!
    
    @IBOutlet weak var availableLabel: UILabel!
    @IBOutlet weak var notAvailableLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var noteLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String, Any>()
    
    let NEXT_DATE = "Next Date"
    let DAY_STR = "Day Str"
    let DATE_STR = "Date Str"
    let MONTH_STR = "Month Str"
    let Morning = "Morning"
    let AfterNoon = "AfterNoon"
    let Evening = "Evening"
    let Night = "Night"
    let STATUS_AVAILABLE = "A"
    let STATUS_BLOCKED = "X"
    
    var labName = ""
    var labLogo = ""
    var labAddress = ""
    var labGUID = ""
    
    var cartCount = ""
    
    var isHomeSampleAvailable = ""
    var homeSampleCharge = ""
    var isReportDeliveryAvailable = ""
    var reportDeliveryCharges = ""
    var isPrescriptionMandatory = false
    
    var TimeString = ""
    var AppointmentDate = ""
    var SessionTag: Int = 0
    
    let calendarIndex = 33
    var calendarInfoArray = [Dictionary<String, Any>]()
    var selectedDateIndex = 0
    let tableViewTag = 22
    let headerViewHeight:CGFloat = 80
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.labName, controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.noteLabel.text = ""
        self.noteLabelBottomConstraint.constant = 0
        let notAvailable = "Not Available"
        let attributedText = NSMutableAttributedString(string: notAvailable)
        let range = (attributedText.string as NSString).range(of: notAvailable)
        attributedText.addAttributes([NSAttributedString.Key.strikethroughStyle : 1, NSAttributedString.Key.strikethroughColor : UIColor.red], range: range)
        self.notAvailableLabel.attributedText = attributedText
        self.notAvailableLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        self.availableLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.tag = self.tableViewTag
        self.tableView.register(UINib(nibName: "BookAppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "BookAppointmentTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.allowsSelection = false
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        
        self.setupCalendarCollectionView()
        self.getLabTimingAPICall()
    }
    
    func gotoPreBooking() {
        
        let date = Helper.shared.convert(date: self.AppointmentDate, from: "MM/dd/yyyy", to: "dd MMM yyyy")
        
        let viewController = AppController.shared.getLabTestPreBookingViewController()
        viewController.labLogo = self.labLogo
        viewController.labName = self.labName
        viewController.labAddress = self.labAddress
        
        viewController.cartCount = self.cartCount
        
        viewController.isHomeSampleAvailable = self.isHomeSampleAvailable
        viewController.homeSampleCharge = self.homeSampleCharge
        viewController.isReportDeliveryAvailable = self.isReportDeliveryAvailable
        viewController.reportDeliveryCharges = self.reportDeliveryCharges
        viewController.isPrescriptionMandatory = self.isPrescriptionMandatory
        viewController.AppointmentDate = date
        viewController.TimeString = self.TimeString
        viewController.SessionTag = self.SessionTag
        
        if let labNo = self.infoArray["DiagnosticLabNo"] as? Int {
            let timeString = self.TimeString.components(separatedBy: "-")
            viewController.AppointmentDetails = "\(labNo)$\(timeString[0])$\(date)$\(self.SessionTag)"
        }
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
