//
//  LabSlotPickerHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabSlotPickerViewController {
    
    func getCalendarCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> CalenderCollectionViewCell {
        
        let cell : CalenderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionViewCell", for: indexPath) as! CalenderCollectionViewCell
        
        cell.dayView.backgroundColor = UIColor.init(netHex: APP_PRIMARY_COLOR_LIGHT)
        cell.dateView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR_LIGHT)
        cell.dayLabel.textColor = UIColor.init(netHex: APP_LIGHT_GRAY_COLOR)
        cell.monthView.backgroundColor = UIColor.init(netHex: APP_PRIMARY_COLOR_LIGHT)
        cell.monthLabel.textColor = UIColor.init(netHex: APP_LIGHT_GRAY_COLOR)
        
        let item = self.calendarInfoArray[indexPath.row]
        
        if indexPath.row == self.selectedDateIndex {
            self.setupSelectedDateInCalendar(cell)
        } else {
            self.setupUnSelectedDateInCalendar(cell)
        }
        
        cell.dayLabel.text = item[self.DAY_STR] as? String
        cell.dateLabel.text = item[self.DATE_STR] as? String
        cell.monthLabel.text = item[self.MONTH_STR] as? String
        
        return cell
    }
    
    func getHospitalCell(_ indexPath: IndexPath) -> BookAppointmentTableViewCell {
        
        let cell: BookAppointmentTableViewCell = Bundle.main.loadNibNamed("BookAppointmentTableViewCell", owner: nil, options: nil)![0] as! BookAppointmentTableViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.hospitalView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR_LIGHT)
        cell.hospitalView.setRoundedCorner()
        
        cell.hospitalNameLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.hospitalAddressLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.hospitalView.backgroundColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        cell.hospitalNameLabel.textColor = UIColor.white
        cell.hospitalAddressLabel.textColor = UIColor.white
        cell.expandLessIcon.image = UIImage.init(named: "")
        
        cell.tableViewHeightConstraint.constant = self.getHospitalCellHeight(indexPath.section) - self.headerViewHeight
        self.setupSessionTableView(cell, index: indexPath.section)

        cell.hospitalNameLabel.text = self.labName
        cell.hospitalAddressLabel.text = self.labAddress

        return cell
    }
    
    func getSessionViewCell(_ indexPath: IndexPath, tag: Int) -> BookAppointmentSessionTableViewCell {
        
        let cell: BookAppointmentSessionTableViewCell = Bundle.main.loadNibNamed("BookAppointmentSessionTableViewCell", owner: nil, options: nil)![0] as! BookAppointmentSessionTableViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.sessionLabel.textColor = UIColor.darkGray
        cell.sessionLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        let (text, _) = self.getSessionTitle(indexPath.row)
        cell.sessionLabel.text = text
        
        let tagStr = "\(tag)0\(indexPath.row)"
        cell.collectionView.tag = Int(tagStr)!
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        cell.collectionView.register(UINib(nibName: "BookAppointmentTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BookAppointmentTimeCollectionViewCell")
        cell.collectionView.backgroundColor = UIColor.clear
        cell.collectionView.isScrollEnabled = false
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.invalidateLayout()
        cell.collectionView.collectionViewLayout = layout
        
        cell.clipsToBounds = true
        
        return cell
    }
    
    func getTimeCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> BookAppointmentTimeCollectionViewCell {
        
        let cell : BookAppointmentTimeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookAppointmentTimeCollectionViewCell", for: indexPath) as! BookAppointmentTimeCollectionViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.timeLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        let indexTag = collectionView.tag / 100
        let sessionTag = collectionView.tag % 100
        
        let timeArray = self.getTimeArray(indexTag - 1, subIndex: sessionTag)
        if timeArray.count != 0 {
            
            let text = timeArray[indexPath.row]
            let statusArray = text.components(separatedBy: "$")
            if statusArray.count != 0 {
                let title = statusArray[0]
                let status = statusArray[1]
                
                cell.timeLabel.backgroundColor = UIColor.white
                cell.timeLabel.text = title
                
                if status != STATUS_AVAILABLE {
                    let attributedText = NSMutableAttributedString(string: title)
                    let range = (attributedText.string as NSString).range(of: title)
                    attributedText.addAttributes([NSAttributedString.Key.strikethroughStyle : 1, NSAttributedString.Key.strikethroughColor : UIColor.red], range: range)
                    cell.timeLabel.attributedText = attributedText
                }
            }
        }
        
        return cell
    }
    
    func setupSelectedDateInCalendar(_ cell: CalenderCollectionViewCell) {
        
        cell.SelectedDateView.layer.cornerRadius = cell.SelectedDateView.bounds.width / 2
        cell.SelectedDateView.backgroundColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        cell.dayLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.dateLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.monthLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.dateLabel.textColor = UIColor.white
    }
    
    func setupUnSelectedDateInCalendar(_ cell: CalenderCollectionViewCell) {
        
        cell.SelectedDateView.backgroundColor = UIColor.clear
        cell.dayLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.dateLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.monthLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.dateLabel.textColor = UIColor.darkGray
    }
    
    func getHospitalCellHeight(_ index: Int) -> CGFloat {
        
        var height: CGFloat = self.headerViewHeight
        
        for i in 0..<4 {
            height += self.getSessionCellHeight(index, subIndex: i)
        }
        
        return height
    }
    
    func getSessionCellHeight(_ index: Int, subIndex: Int) -> CGFloat {
        
        var height: CGFloat = 40
        
        let timeArray = self.getTimeArray(index, subIndex: subIndex)
        if timeArray.count != 0 {
            let lineCount = timeArray.count / 2
            let remainingCount = timeArray.count % 2
            let toatlLines = lineCount + (remainingCount > 0 ? 1 : 0)
            height = height + CGFloat(toatlLines * 30)
        } else {
            height = 0
        }
        
        return height
    }
    
    func setupSessionTableView(_ cell: BookAppointmentTableViewCell, index: Int) {
        
        cell.tableView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR_LIGHT)
        cell.tableView.delegate = self
        cell.tableView.dataSource = self
        cell.tableView.tag = index + 1
        cell.tableView.register(UINib(nibName: "BookAppointmentSessionTableViewCell", bundle: nil), forCellReuseIdentifier: "BookAppointmentSessionTableViewCell")
        cell.tableView.tableFooterView = UIView.init()
        cell.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cell.tableView.isScrollEnabled = false
    }
    
    func loadCalendarData() {
        
        let oneDay: Double = -(60 * 60 * 24)
        let previousDate = Date().addingTimeInterval(oneDay)
        var date = previousDate
        for _ in 0..<30 {
            let (nextDate, dayStr, dateStr, monthStr) = Helper.shared.getNextDate(date, format: "EEE dd MMM")
            let dict = [self.NEXT_DATE : nextDate, self.DAY_STR : dayStr, self.DATE_STR : dateStr, self.MONTH_STR : monthStr] as [String : Any]
            self.calendarInfoArray.append(dict)
            date = nextDate
        }
        self.calendarCollectionView.reloadData()
        
    }
    
    func getSessionTitle(_ index: Int) -> (String, Int) {
        
        var text = ""
        var sessionTag: Int = 0
        switch index {
        case 0:
            text = "Morning"
            sessionTag = 1
            break
        case 1:
            text = "After Noon"
            sessionTag = 2
            break
        case 2:
            text = "Evening"
            sessionTag = 3
            break
        case 3:
            text = "Night"
            sessionTag = 4
            break
            
        default: break
        }
        
        return (text, sessionTag)
    }
    
    func getTimeArray(_ index: Int, subIndex: Int) -> [String] {
        
        if let item = self.infoArray["TimingsData"] as? Dictionary<String,Any> {
            
            var timeText = String()
            
            switch subIndex {
            case 0:
                timeText = item["Morning"] as? String ?? ""
                break
            case 1:
                timeText = item["AfterNoon"] as? String ?? ""
                break
            case 2:
                timeText = item["Evening"] as? String ?? ""
                break
            case 3:
                timeText = item["Night"] as? String ?? ""
                break
                
            default: break
            }
            
            if timeText.count != 0 {
                var timeArray = timeText.components(separatedBy: "~")
                if timeArray.count != 0 {
                    timeArray.removeLast()
                    return timeArray
                }
            }
        }
        return []
    }
    
    func setupCalendarCollectionView() {
        
        self.calendarCollectionView.tag = self.calendarIndex
        self.calendarCollectionView.delegate = self
        self.calendarCollectionView.dataSource = self
        self.calendarCollectionView.register(UINib(nibName: "CalenderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CalenderCollectionViewCell")
        self.calendarCollectionView.backgroundColor = UIColor.clear
        self.calendarCollectionView.bounces = false
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.invalidateLayout()
        self.calendarCollectionView.collectionViewLayout = layout
        self.calendarCollectionView.showsHorizontalScrollIndicator = false
        
        self.loadCalendarData()
    }

}

