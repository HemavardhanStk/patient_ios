//
//  LabSlotPickerAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabSlotPickerViewController {
    
    func getLabTimingAPICall() {
        
        var AppDate = ""
        
        if let selectedDate = self.calendarInfoArray[self.selectedDateIndex][self.NEXT_DATE] as? Date {
            let date = Helper.shared.getString(selectedDate, format: "MM/dd/yyyy")
            AppDate = date
        }
        
        let params = ["DLabFirmName"            : "",
                      "Date"                    : AppDate,
                      "LabGUID"                 : self.labGUID,
                      "Language"                : "English",
                      "TestorPackageCategory"   : "",
                      "TestorPackageNo"         : "",
                      "Day"                     : "0",
                      "DiagnosticLabNo"         : "0"]
     
        HttpManager.shared.loadAPICall(path: PATH_GetDiagnosticLabBookingTimeSlot, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any> {
                    self.infoArray = value
                    self.tableView.reloadData()
                }
            } else {
                self.infoArray.removeAll()
                self.tableView.reloadData()
            }
        }) {}
        
    }
    
}
