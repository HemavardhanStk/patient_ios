//
//  LabChartHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 16/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabChartViewController {
    
    func getCartTotalCountCell() -> LabCartTableViewCell {
        let cell: LabCartTableViewCell = Bundle.main.loadNibNamed("LabChartTableViewCell", owner: nil, options: nil)![0] as! LabCartTableViewCell
  
        cell.backgroundColor = .clear
        
        cell.actionTextLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 15)
        cell.totalAmountLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 16)
        cell.labTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 15)
        
        cell.actionTextLabel.textColor = UIColor.darkGray
        cell.totalAmountLabel.textColor = UIColor.black
        cell.labTitleLabel.textColor = UIColor.darkGray
        
        var actionText = ""
        var amountText = ""
        
        if let value = self.infoArray["TotalAmount"] as? Int, value != 0 {
            actionText = "You are about to pay"
            amountText = "₹\(value)"
        }
        
        cell.actionTextLabel.text = actionText
        cell.totalAmountLabel.text = amountText
        cell.labTitleLabel.text = self.infoArray["LabFirmName"] as? String ?? ""
        
        return cell
    }
    
    func getCartItemCell(_ indexPath: IndexPath) -> LabCartItemTableViewCell {
        let cell: LabCartItemTableViewCell = Bundle.main.loadNibNamed("LabCartItemTableViewCell", owner: nil, options: nil)![0] as! LabCartItemTableViewCell
        
        cell.itemTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.itemPriceLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 12)
        
        cell.itemTitleLabel.textColor = UIColor.black
        cell.itemPriceLabel.textColor = UIColor.darkGray
        
        cell.removeItemButton.tag = indexPath.row - 1
        
        let image = UIImage(named: "ic_delete")?.withRenderingMode(.alwaysTemplate)
        cell.removeItemButton.setImage(image, for: .normal)
        cell.removeItemButton.tintColor = UIColor(netHex: APP_LIGHT_RED_COLOR)
        cell.removeItemButton.addTarget(self, action: #selector(showRemovalAlert(_:)), for: .touchUpInside)
        
        if let value = self.infoArray["LabTestDetails"] as? Dictionary<String, Any> {
            if let value1 = value["LabTest"] as? [Dictionary<String, Any>], value1.count != 0 {
                
                let item = value1[indexPath.row - 1]
                var amount = ""
                
                if let amountvalue = item["Amount"] as? Int, amountvalue != 0 {
                    amount = "\(amountvalue)"
                }
                
                cell.itemTitleLabel.text = item["TestName"] as? String ?? ""
                cell.itemPriceLabel.text = amount
                
            } else if let value1 = value["LabPackage"] as? [Dictionary<String, Any>], value1.count != 0 {
                
                let item = value1[indexPath.row - 1]
                var amount = ""
                
                if let amountvalue = item["Amount"] as? Int, amountvalue != 0 {
                    amount = "\(amountvalue)"
                }
                
                cell.itemTitleLabel.text = item["PackageName"] as? String ?? ""
                cell.itemPriceLabel.text = amount
                
            }
        }
        
        return cell
    }
    
    @objc func showRemovalAlert(_ sender: UIButton) {

            let alertController = UIAlertController(title: "Removal Alert", message: "Are you sure to remove the item", preferredStyle: UIAlertController.Style.alert)
        
            alertController.addAction(UIAlertAction(title: "remove", style: .default, handler: {(action:UIAlertAction) in
            
                self.removePackageTestAPICall(sender)
            }))

            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        
    }
    
    
}
