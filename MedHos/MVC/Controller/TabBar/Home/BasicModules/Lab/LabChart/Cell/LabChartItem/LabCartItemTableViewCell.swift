//
//  LabChartItemTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 16/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabCartItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    
    @IBOutlet weak var removeItemButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
