//
//  LabChartViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 16/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabChartViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addMoreButton: UIButton!
    @IBOutlet weak var checkOutButton: UIButton!
    
    var infoArray = Dictionary<String, Any>()
    var cartCount = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let navigationStackCount: Int! = self.navigationController?.viewControllers.count
        
        if navigationStackCount > 2 {
            
            guard let viewController = self.navigationController?.viewControllers[navigationStackCount-1] else { return }
            
            if self.isMovingFromParent && (viewController as? LabPackageTestViewController) != nil {
                NotificationCenter.default.post(name: Notification.Name.Task.ReloadLabPackageTestList, object: self, userInfo: nil)
            }
        }
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "Cart", controller: self)
        
        self.setUpColors()
        self.loadInfoData()
        
        self.addMoreButton.alpha = 0
        self.checkOutButton.alpha = 0
        
        self.checkOutButton.addTarget(self, action: #selector(gotoLabTimeSlot), for: .touchUpInside)
        self.addMoreButton.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabCartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "LabCartItemTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 130
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
        NotificationCenter.default.addObserver(self, selector: #selector(loadInfoData), name: Notification.Name.Task.ReloadLabCartList, object: nil)
    }
    
    func setUpColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        
        self.getChartListAPICall()
    }
    
    @objc func gotoLabTimeSlot() {
        
        let viewController = AppController.shared.getLabSlotPickerViewController()
        
        viewController.labName = self.infoArray["LabFirmName"] as? String ?? ""
        viewController.labLogo = self.infoArray["LabLogoFileName"] as? String ?? ""
        viewController.labAddress = self.infoArray["LabAreaName"] as? String ?? ""
        viewController.labGUID = self.infoArray["LabGUID"] as? String ?? ""
        
        viewController.cartCount = "\(self.cartCount)"
        
        viewController.isHomeSampleAvailable = self.infoArray["IsHomeSample"] as? String ?? ""
        viewController.homeSampleCharge = "\(self.infoArray["HomeVisitMinAmount"] as? Int ?? 0)"
        viewController.isReportDeliveryAvailable = self.infoArray["ReportDeliveryAtHome"] as? String ?? ""
        viewController.reportDeliveryCharges = "\(self.infoArray["ReportdeliveryMinAmount"] as? Int ?? 0)"
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @objc func popViewController() {
        
        let navigationStackCount: Int! = self.navigationController?.viewControllers.count
        guard let viewController = self.navigationController?.viewControllers[navigationStackCount-2] else { return }
        
        if ((viewController as? LabPackageTestViewController) != nil) {
            
            self.navigationController?.popViewController(animated: true)
        } else {
            
            let viewController = AppController.shared.getLabPackageTestViewController()
            viewController.LabId = "\(self.infoArray["DiagnosticLabNo"] as? Int ?? 0)"
            viewController.LabGUID = self.infoArray["LabGUID"] as? String ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        
        
    }
}
