//
//  LabChartAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 16/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabChartViewController {
    
    func getChartListAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["Language"        : "English",
                      "MobileNumber"    : user.phone,
                      "UserId"          : user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_GetCartDataforUser, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let cartCount = value["CartCount"] as? Int {
                    self.cartCount = cartCount
                    self.infoArray = value
                    self.tableView.reloadData()
                    self.addMoreButton.alpha = 1
                    self.checkOutButton.alpha = 1
                }
            } else {
                self.infoArray = [:]
                self.tableView.reloadData()
                self.addMoreButton.alpha = 0
                self.checkOutButton.alpha = 0
            }
        }) {}
    }
    
    @objc func removePackageTestAPICall(_ sender: UIButton) {
        
        let params = self.getParams(sender)
        
        HttpManager.shared.loadAPICall(path: PATH_RemoveTestfromCart, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                self.getChartListAPICall()
            }
        }) {}
    }
    
    func getParams(_ sender: UIButton) -> Dictionary<String, Any> {
        
        let user = Session.shared.getUserInfo()
        var category = String()
        var labNo = String()
        let LabGUID = self.infoArray["LabGUID"] as? String ?? ""
        var testPackageID = ""
        
        if let value = self.infoArray["LabTestDetails"] as? Dictionary<String,Any> {
            if let value1 = value["LabTest"] as? [Dictionary<String,Any>], value1.count != 0 {
                print(sender.tag)
                print(value1[sender.tag]["TestName"] as? String ?? "")
                category = "test"
                testPackageID = "\(value1[sender.tag]["TestNo"] as? Int ?? 0)"
                labNo = "\(value1[sender.tag]["DiagnosticLabNo"] as? Int ?? 0)"
                
            } else if let value1 = value["LabPackage"] as? [Dictionary<String,Any>], value1.count != 0 {
                
                category = "package"
                testPackageID = "\(value1[sender.tag]["PackageNo"] as? Int ?? 0)"
                labNo = "\(value1[sender.tag]["DiagnosticLabNo"] as? Int ?? 0)"
            }
        }
        
        let params = ["CartItemCategory"            : category,
                      "DiagnosticLabGUID"           : LabGUID,
                      "Language"                    : "English",
                      "TestNos"                     : [],
                      "DiagnosticLabNo"             : labNo,
                      "TestorPackageNo"             : testPackageID,
                      "UserDetailNo"                : user.id] as [String : Any]
        
        return params
    }
    
}
