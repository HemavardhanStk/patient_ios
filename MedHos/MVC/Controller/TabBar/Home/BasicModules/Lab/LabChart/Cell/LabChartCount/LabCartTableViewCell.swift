//
//  LabChartTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 16/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabCartTableViewCell: UITableViewCell {

    @IBOutlet weak var actionTextLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var labTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
