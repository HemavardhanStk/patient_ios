//
//  LabChartDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 16/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabChartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let value = self.infoArray["LabTestDetails"] as? Dictionary<String, Any> {
            if let value1 = value["LabPackage"] as? [Dictionary<String, Any>], value1.count != 0 {
                return value1.count + 1
            } else if let value1 = value["LabTest"] as? [Dictionary<String, Any>], value1.count != 0 {
                return value1.count + 1
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return self.getCartTotalCountCell()
        
        default:
            return self.getCartItemCell(indexPath)
        }
        
    }
}
