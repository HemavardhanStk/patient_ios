//
//  LabPackageDetailsTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabPackageDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var packageContainerView: UIView!
    
    @IBOutlet weak var packageTitleLabel: UILabel!
    @IBOutlet weak var packageDescriptionLabel: UILabel!
    @IBOutlet weak var subLabel1: UILabel!
    @IBOutlet weak var subLabel2: UILabel!
    @IBOutlet weak var subLabel3: UILabel!
    
    @IBOutlet weak var subLabel2StrikeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
