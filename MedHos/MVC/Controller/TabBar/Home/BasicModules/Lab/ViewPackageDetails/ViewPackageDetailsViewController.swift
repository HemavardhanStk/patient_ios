//
//  ViewTestPackageViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ViewPackageDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    var infoArray = Dictionary<String, Any>()
    var testListArray = [Dictionary<String, String>]()
    
    var packageId = "0"
    var bottomViewHeight:CGFloat = 60
    
    var labGUID = ""
    var labId = ""
    
    let TESTNAMES = "TestNames"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let navigationStackCount: Int! = self.navigationController?.viewControllers.count
        guard let viewController = self.navigationController?.viewControllers[navigationStackCount-1] else { return }
        
        if self.isMovingFromParent && (viewController as? LabPackageTestViewController) != nil {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadLabPackageTestList, object: self, userInfo: nil)
        }
    }
    
    // storyBoard Controller is available in manage storyboard
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "Package details", controller: self)
        
        self.setUpColors()
        self.loadInfoData()
        self.setButton()
        
        self.button.alpha = 0
        self.button.addTarget(self, action: #selector(addRemovePackageAPICall), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabListTableViewCell", bundle: nil), forCellReuseIdentifier: "LabListTableViewCell")
        self.tableView.register(UINib(nibName: "PackageTestListTableViewCell", bundle: nil), forCellReuseIdentifier: "PackageTestListTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 130
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setUpColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func setButton(_ addType: Bool = true) {
        
        if !addType {
            self.button.setTitle("             Add to cart             ", for: .normal)
            self.button.layer.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
        } else {
            self.button.setTitle("             Remove from cart             ", for: .normal)
            self.button.layer.backgroundColor = UIColor(netHex: APP_LIGHT_RED_COLOR).cgColor
        }
        self.button.setTitleColor(UIColor.white, for: .normal)
        
        self.button.layer.cornerRadius = 5
        self.button.layer.masksToBounds = true
        self.button.alpha = self.bottomViewHeight == 0 ? 0 : 1
        
        self.bottomView.dropShadow()
        self.bottomViewHeightConstraint.constant = self.bottomViewHeight
    }
    
    func loadInfoData() {
        
        self.getPackageDetails()
    }

}
