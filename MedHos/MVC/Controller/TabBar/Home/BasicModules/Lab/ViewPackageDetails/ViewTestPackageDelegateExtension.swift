//
//  ViewTestPackageDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewPackageDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.infoArray.count == 0 ? 0:3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 2 ? self.testListArray.count + 1 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 {
           return 8
        }
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            return self.getPackageDetailsCell()
            
        case 1:
            return self.getInstructionCell()
            
        default:
            return self.getTestListCell(indexPath)
            
        }
    }
    
}



