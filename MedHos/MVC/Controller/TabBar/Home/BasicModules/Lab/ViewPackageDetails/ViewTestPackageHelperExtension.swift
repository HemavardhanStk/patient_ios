//
//  ViewTestPackageHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewPackageDetailsViewController {
    
    func getPackageDetailsCell() -> LabPackageDetailsTableViewCell {
        
        let cell : LabPackageDetailsTableViewCell = Bundle.main.loadNibNamed("LabPackageDetailsTableViewCell", owner: nil, options: nil)![0] as! LabPackageDetailsTableViewCell
        
        cell.packageTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 16)
        cell.packageDescriptionLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.subLabel1.font = UIFont(name: APP_FONT_NAME_BOLD, size: 16)
        cell.subLabel2.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.subLabel3.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.packageTitleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.packageDescriptionLabel.textColor = UIColor.darkGray
        cell.subLabel2.textColor = UIColor.darkGray
        cell.subLabel3.textColor = UIColor.white
        
        cell.packageDescriptionLabel.numberOfLines = 0
        
        cell.subLabel3.layer.backgroundColor = UIColor(netHex: APP_GREEN).cgColor
        cell.subLabel3.layer.cornerRadius = 5
        cell.subLabel3.layer.masksToBounds = true
        
        if let value = self.infoArray["LabPackage"] as? [Dictionary<String, Any>] {

            cell.packageTitleLabel.text = value[0]["PackageName"] as? String ?? ""
            cell.packageDescriptionLabel.text = value[0]["Description"] as? String ?? ""
            cell.subLabel1.text = "₹\(value[0]["NetAmount"] as? Int ?? 0)"
            
            if let value1 = value[0]["DiscountPercentage"] as? Int, value1 != 0 {
                cell.subLabel2.text = "₹\(value[0]["Amount"] as? Int ?? 0)"
                cell.subLabel3.text = "  \(value[0]["DiscountPercentage"] as? Int ?? 0) % off  "
                cell.subLabel2StrikeView.alpha = 1
            } else {
                cell.subLabel2.text = ""
                cell.subLabel3.text = ""
                cell.subLabel2StrikeView.alpha = 0
            }
        }
        
        return cell
    }
    
    func getInstructionCell() -> PackageTestListTableViewCell {
        
        let cell: PackageTestListTableViewCell = Bundle.main.loadNibNamed("PackageTestListTableViewCell", owner: nil, options: nil)![0] as! PackageTestListTableViewCell
        
        //cell.backgroundColor = .clear
        
        //cell.labBookCancelContainerView.setRoundedCorner()
        
        cell.testTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.testSubTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.testTitleLabel.textColor = UIColor.black//(netHex: APP_PRIMARY_COLOR)
        cell.testSubTitleLabel.textColor = UIColor.darkGray
        
        cell.testSubTitleLabel.numberOfLines = 0
        
        cell.testTitleLabelTopConstraint.constant = 8
        cell.testSubTitleLabelLeadingConstraint.constant = 12
        cell.testSubTitleLabelBottomConstraint.constant = 8

        cell.testTitleLabel.text = "Instruction :"
        if let value = self.infoArray["LabPackage"] as? [Dictionary<String, Any>] {
            cell.testSubTitleLabel.text = value[0]["Preparation"] as? String ?? ""
        }
        
        return cell
    }
    
    func getTestListCell(_ indexPath: IndexPath) -> PackageTestListTableViewCell {
        
        let cell: PackageTestListTableViewCell = Bundle.main.loadNibNamed("PackageTestListTableViewCell", owner: nil, options: nil)![0] as! PackageTestListTableViewCell
        
        cell.testTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.testSubTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.testTitleLabel.textColor = UIColor.black
        cell.testSubTitleLabel.textColor = UIColor.darkGray
        
        cell.testSubTitleLabel.numberOfLines = 0
        cell.testSubTitleLabelLeadingConstraint.constant = 32
        
        cell.reduceExpandButton.tag = indexPath.row
        cell.reduceExpandButton.addTarget(self, action: #selector(expandReduceTestList(_:)), for: .touchUpInside)
        
        let tag = indexPath.row
        
        if tag == 0 {
            cell.testTitleLabelTopConstraint.constant = 10
            cell.testSubTitleLabelBottomConstraint.constant = 8
            if let value = self.infoArray["LabPackage"] as? [Dictionary<String, Any>] {
                cell.testTitleLabel.text = "Tests included: \(value[0]["TestCount"] as? Int ?? 0)"
                cell.testSubTitleLabel.text = ""
            }
        } else {
            cell.testTitleLabel.text = testListArray[tag - 1][TEXT]
            cell.testSubTitleLabel.text = testListArray[tag - 1][STATUS] == SELECTED ? testListArray[tag - 1][TESTNAMES] : ""
            
            if testListArray[tag - 1][STATUS] == UN_SELECTED || testListArray[tag - 1][TESTNAMES] == "" {
                
                cell.testTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
                
                if self.testListArray[tag - 1][TESTNAMES] == "" {
                    cell.dropDownImageView.image = UIImage(named: "")
                } else {
                    cell.dropDownImageView.image = #imageLiteral(resourceName: "arrow")
                }
                
            } else {
                cell.dropDownImageView.image = #imageLiteral(resourceName: "ic_dropdown")
            }
            
        }
        
        if tag == self.testListArray.count {
            cell.testSubTitleLabelBottomConstraint.constant = 8
        }
        
        return cell
    }
    
    func parseTestList() {
        
        if let value = self.infoArray["LabPackageTestType"] as? [Dictionary<String,Any>] {
            
            for i in 0..<value.count {
                
                var titleText = String()
                var subTitleText = String()
                
                if let value1 = value[i]["TestTypeName"] as? String {
                    titleText = value1
                }
                
                if let value1 = value[i]["LabPackageTypesTest"] as? [Dictionary<String,Any>] {
                    
                 for i in 0..<value1.count {
                    if let textValue = value1[i]["TestName"] as? String {
                        if i != value1.count - 1 {
                            subTitleText.append("\(textValue)\n")
                        } else {
                            subTitleText.append(textValue)
                        }
                    }
                  }
                }
                
                let dict = [TEXT: titleText, TESTNAMES: subTitleText, STATUS: UN_SELECTED]
                self.testListArray.append(dict)
              }
            }
        }
    
    @objc func expandReduceTestList(_ sender: UIButton) {
  
        if self.testListArray[sender.tag - 1][STATUS] == SELECTED {
            self.testListArray[sender.tag - 1][STATUS] = UN_SELECTED
        } else {
            self.testListArray[sender.tag - 1][STATUS] = SELECTED
        }
        
        self.tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 2)], with: .automatic)
        
    }
        
}

