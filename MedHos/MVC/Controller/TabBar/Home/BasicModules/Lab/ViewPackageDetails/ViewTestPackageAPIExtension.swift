//
//  ViewTestPackageAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewPackageDetailsViewController {
    
    func getPackageDetails() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["PackageNo"   :   self.packageId,
                      "Language"    :   "English",
                      "UserId"      :   user.id,
                      "DiagnosticLabNo":"0"]
        HttpManager.shared.loadAPICall(path: PATH_GetPackageDetailfromID, params: params, httpMethod: .post, controller: self, completion: { (response) in
            
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                   let package = value["LabPackage"] as? [Dictionary<String, Any>],
                   let addOrRemove = package[0]["AddorRemoveButton"] as? String {
                    
                    self.setButton(addOrRemove == "Y")
                    
                    self.infoArray = value
                    self.parseTestList()
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
    @objc func addRemovePackageAPICall(_ sender: UIButton) {
        
        let (params,urlString) = self.getParams(sender)
        
        HttpManager.shared.loadAPICall(path: urlString, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
//                if let response = response["Result"] as? Dictionary<String, Any> {
                    self.setButton(urlString == "DLab/AddTesttoCart")
//                }
            }
        }) {}
    }
    
    func getParams(_ sender: UIButton) -> (Dictionary<String, Any>, String) {
        
        let user = Session.shared.getUserInfo()
        let urlString = sender.titleLabel!.text!.contains("             Add to cart             ") ? PATH_AddTesttoCart : PATH_RemoveTestfromCart
        
        var testPackageID = ""
        var testNosArray = [String]()

            if let value = self.infoArray["LabPackage"] as? [Dictionary<String,Any>] {
                
                testPackageID = "\(value[0]["PackageNo"] as? Int ?? 0)"
                
                if let value2 = value[0]["LabPackageTestList"] as? [Dictionary<String,Any>] {
                    
                    for i in 0..<value2.count {
                        if let value3 = value2[i]["TestNo"] as? Int {
                            testNosArray.append("\(value3)")
                        }
                    }
                }
            }
        
        let params = ["CartItemCategory"            : "package",
                      "DiagnosticLabGUID"           : self.labGUID,
                      "Language"                    : "English",
                      "TestNos"                     : testNosArray,
                      "DiagnosticLabNo"             : self.labId,
                      "TestorPackageNo"             : testPackageID,
                      "UserDetailNo"                : user.id] as [String : Any]
        
        return (params, urlString)
    }

    
}
