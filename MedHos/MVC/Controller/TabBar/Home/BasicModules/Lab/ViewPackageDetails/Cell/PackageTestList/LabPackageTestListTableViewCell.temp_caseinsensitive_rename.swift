//
//  LabPackageTestListTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabPackageTestListTableViewCell: UITableViewCell {

    @IBOutlet weak var labPackageContainerView: UIView!
    
    @IBOutlet weak var testTitleLabel: UILabel!
    @IBOutlet weak var testSubTitleLabel: UILabel!
    @IBOutlet weak var reduceExpandButton: UIButton!
    @IBOutlet weak var dropDownImageView: UIImageView!
    
    @IBOutlet weak var titleTestLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var testSubTitleLabelBottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
