//
//  LabBasicSearchAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabBasicSearchViewController {
    
    func getCartCountAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["UserId"          : user.id,
                      "MobileNumber"    : user.phone,
                      "Language"        : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetCartBucketResult, params: params, httpMethod: .post, controller: self, completion: { (response) in
            
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as! Dictionary<String, AnyObject>, controller: self, isBackgroundCall: true) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let cartCount = value["CartCount"] as? Int {
                    
                    if cartCount == 0 {
                        self.cartDetailsView.alpha = 0
                    } else {
                        
                        if let labName = value["LabFirmName"] as? String,
                            let testType = value["TestType"] as? String {
                            
                            self.cartDetailsLabelOne.text = labName
                            self.cartDetailsLabelTwo.text = "You have \(cartCount) \(testType) in your cart"
                        }
                        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
                        self.cartDetailsView.alpha = 1
                    }
                }
            } else {
                self.cartDetailsView.alpha = 0
            }
        }) {}
    }
    
    
    func loadLabBasicSearchAPICall(_ isBackgroundCall: Bool = false) {
        
        let user = Session.shared.getUserInfo()
        self.pageNumber += 1
        
        let params = ["FilterStatus"        : "false",
                      "SearchName"          : self.searchName,
                      "Language"            : "English",
                      "PageNumber"          : self.pageNumber,
                      "UserNo"              : user.id,
                      "StartIndex"          : 0,
                      "PageSize"            : 10,
                      "HomeDelivery"        : "",
                      "SearchCity"          : self.Location,
                      "AreaLatitude"        : self.Latitude,
                      "AreaLongitude"       : self.Longitude,
                      "UserLatitude"        : self.Latitude,
                      "UserLongitude"       : self.Longitude,
                      "SortOrder"           : 0] as [String : Any]
        
        HttpManager.shared.loadAPICall(path: PATH_DiagnosticLabBasicSearch, params: params, httpMethod: .post, controller: self,isBackgroundCall: isBackgroundCall, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self, isBackgroundCall: isBackgroundCall) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let value1 = value["LabData"] as? [Dictionary<String, Any>] {
                    
                    self.TotalNumberOfPages = value["TotalPage"] as? Int ?? 0
                    self.infoArray.append(contentsOf: value1)
                    self.tableView.reloadData()
                    self.tableView.tableFooterView?.isHidden = true
                    self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
                }
            }
        }) {}
    }
    
    @objc func loadLabSearchByNameAPICall() {
        
          let params = ["KeyWord"       : self.searchText,
                        "SearchCity"    : self.Location,
                        "AreaLatitude"  : self.Latitude,
                        "AreaLongitude" : self.Longitude,
                        "Language"      : "English"] as [String : Any]
        
        HttpManager.shared.loadAPICall(path: PATH_SearchByLabName, params: params, httpMethod: .post, controller: self, isBackgroundCall: true, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self, isBackgroundCall: true) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let value1 = value["DisplayList"] as? [Dictionary<String, Any>] {
                    self.IsSearching = true
                    self.LabSearchByNameArray = value1
                    self.tableView.reloadData()
                }
            }
        }) {}
        
    }
    
}
