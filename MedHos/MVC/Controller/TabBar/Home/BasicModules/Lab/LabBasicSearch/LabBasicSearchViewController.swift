//
//  LabBasicSearchViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation

class LabBasicSearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cartDetailsView: UIView!
    @IBOutlet weak var cartDetailsLabelOne: UILabel!
    @IBOutlet weak var cartDetailsLabelTwo: UILabel!
    @IBOutlet weak var cartDetailsViewCartButton: UIButton!
    @IBOutlet weak var cartDetailsCloseButton: UIButton!
    
    let locationManager = CLLocationManager()
    let label = UILabel.init()
    
    var CurrentLocation = ""
    var CurrentLatitude = 0.0
    var CurrentLongitude = 0.0
    
    var infoArray = [Dictionary<String, Any>]()
    var LabSearchByNameArray = [Dictionary<String, Any>]()
    var IsSearching = false
    var searchText = ""
    var searchName = ""
    
    var isInitialLocationPicking = true
    var Latitude:Double = 0.00
    var Longitude:Double = 0.00
    var Location = "Retrieving location"
    
    var pageNumber = 0
    var TotalNumberOfPages = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: TITLE_DIAGONSTIC_LAB, controller: self)
        
        self.setUpColors()
        self.setUpCartDetails()
        self.loadInfoData()
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.searchBar.delegate = self
        self.searchBar.setSearchBarStyle()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BasicSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "BasicSearchTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(searchLabWithDefaultLocation), name: Notification.Name.Task.ReloadLabBasicSearch, object: nil)
    }
    
    func setUpColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoData() {
        
        self.getCartCountAPICall()
    }
    
    func setUpCartDetails() {
        
        self.cartDetailsView.alpha = 0
        self.cartDetailsView.backgroundColor = UIColor(netHex: APP_CART_DETAILS_COLOR)
        self.cartDetailsViewCartButton.backgroundColor = UIColor(netHex: APP_GREEN)
        
        self.cartDetailsCloseButton.imageView?.setImageColor(color: UIColor(netHex: APP_PRIMARY_COLOR))
        
        self.cartDetailsLabelOne.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        self.cartDetailsLabelTwo.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        self.cartDetailsLabelOne.textColor = UIColor.darkGray
        self.cartDetailsLabelTwo.textColor = UIColor.darkGray
        
        self.cartDetailsViewCartButton.addTarget(self, action: #selector(gotoViewCart), for: .touchUpInside)
        self.cartDetailsCloseButton.addTarget(self, action: #selector(closeCartDetails), for: .touchUpInside)
        
    }
    
    @objc func gotoViewCart() {
        let viewController = AppController.shared.getLabChartViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
        self.cartDetailsView.removeFromSuperview()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    @objc func closeCartDetails() {
        
        let xPosition = -CGFloat(self.cartDetailsView.frame.size.width)
        let yPosition = self.cartDetailsView.frame.origin.y
        
        let width = self.cartDetailsView.frame.size.width
        let height = self.cartDetailsView.frame.size.height
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        UIView.animate(withDuration: 1.0, animations: {
            self.cartDetailsView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
        }) {(isFinished) in
            self.cartDetailsView.removeFromSuperview()
        }
    }
    
}
