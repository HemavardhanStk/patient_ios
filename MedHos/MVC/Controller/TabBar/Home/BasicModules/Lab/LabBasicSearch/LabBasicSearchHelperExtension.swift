//
//  LabBasicSearchHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabBasicSearchViewController {
    
    func getLabBasicSearchCell(_ indexPath: IndexPath) -> LabBasicSearchTableViewCell {
        
        let cell: LabBasicSearchTableViewCell = Bundle.main.loadNibNamed("LabBasicSearchTableViewCell", owner: nil, options: nil)![0] as! LabBasicSearchTableViewCell
        
        cell.backgroundColor = .clear
        cell.labContainerView.setRoundedCorner()
        cell.labLogoImageView.setRoundedCorner()
        cell.offerView.setRoundedCorner(5)
        cell.button1.setRoundedCorner(5)
        cell.button2.setRoundedCorner(5)
        
        cell.offerView.layer.borderColor    = UIColor(netHex: APP_GREEN_TEXT_COLOR).cgColor
        cell.offerView.layer.borderWidth    = 1
        
        cell.button1.layer.borderColor      = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
        cell.button1.layer.borderWidth      = 1
        cell.button1.setTitleColor(UIColor(netHex: APP_PRIMARY_COLOR), for: .normal)
        cell.button1.tag = indexPath.row
        cell.button1.addTarget(self, action: #selector(gotoBookTestWithPrescription), for: .touchUpInside)
        
        cell.button2.layer.backgroundColor  = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
        cell.button2.setTitleColor(UIColor.white, for: .normal)
        cell.button2.tag = indexPath.row
        
        cell.labApprovalImageView.image                 = UIImage(named: "ic_approved")
        cell.homeSampleAvailableImageView.image         = UIImage(named: "home")
        cell.reportDeliveryAvailableImageView.image     = UIImage(named: "home")
        cell.labTimingImageView.image                   = UIImage(named: "ic_clock")
        cell.availableTodayImageView.image              = UIImage(named: "ic_check_box_selected")
        cell.offerImageView.image                       = UIImage(named: "ic_offer_pointer")
        
        cell.homeSampleAvailableImageView.setImageColor(color: UIColor(netHex: APP_LIGHT_PINK_COLOR))
        cell.reportDeliveryAvailableImageView.setImageColor(color: UIColor(netHex: APP_GREEN_BLUE_MIX_COLOR))
        cell.labTimingImageView.setImageColor(color: UIColor.darkGray)
        
        cell.labNameLabel.font                  = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.labAddressLabel.font               = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.labApprovalLabel.font              = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.homeSampleAvailableLabel.font      = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.reportDeliveryAvailableLabel.font  = UIFont(name: APP_FONT_NAME_BOLD, size: 13)
        cell.labTimingsLabel.font               = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.availableTodayLabel.font           = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.offerLabel.font                    = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.labNameLabel.textColor                 = UIColor.black
        cell.labAddressLabel.textColor              = UIColor.darkGray
        cell.labApprovalLabel.textColor             = UIColor.darkGray
        cell.homeSampleAvailableLabel.textColor     = UIColor(netHex: APP_LIGHT_PINK_COLOR)
        cell.reportDeliveryAvailableLabel.textColor = UIColor(netHex: APP_GREEN_BLUE_MIX_COLOR)
        cell.labTimingsLabel.textColor              = UIColor.darkGray
        cell.availableTodayLabel.textColor          = UIColor.darkGray
        cell.offerLabel.textColor                   = UIColor(netHex: APP_GREEN_TEXT_COLOR)
        
        cell.labTimingsLabel.numberOfLines      = 0
        cell.offerLabel.numberOfLines           = 0
        
        var labName                  = ""
        var labAddress               = ""
        var labApproval              = ""
        var homeSampleAvailable      = ""
        var reportDeliveryAvailable  = ""
        var timings                  = ""
        var availableTime            = ""
        var offerString              = ""
        
        let item = self.infoArray[indexPath.row]
        
        if let value = item["LOGOFILENAME"] as? String,
            let url = URL(string: value){
            cell.labLogoImageView.af_setImage(withURL: url) 
        }
        
        labName = item["FIRMNAME"] as? String ?? ""
        
        if let area = item["AREANAME"] as? String ,
            let addressTwo = item["ADDRESSTWO"] as? String {
            
            labAddress = "\(area), \(addressTwo)"
        }
        
        labApproval = item["ACCREDITEDBY"] as? String ?? ""
        
        if let value = item["ISHOMESAMPLE"] as? Bool, value {
            homeSampleAvailable = "Home Sample Available"
        }
        
        if let value = item["REPORTDELIVERYATHOME"] as? String, value == "Y" {
            reportDeliveryAvailable = "Report Delivery Available"
        }
        
        if let value = item["TIMINGS"] as? [String], value.count != 0 {
            
            for i in 0..<value.count {
                if i == value.count - 1 {
                    timings.append(value[i])
                } else {
                    timings.append("\(value[i])\n")
                }
            }
        }

        if let value = item["BOOKTESTONLINE"] as? Bool, value {
            
            cell.button2.setTitle("Book Test", for: .normal)
            cell.button2.addTarget(self, action: #selector(gotoBookTest(_:)), for: .touchUpInside)
        } else if let value = item["PHONENO1"] as? String,
                    let value1 = item["PHONENO2"] as? String,
                    let value2 = item["HOMESAMPLECONTACTNO"] as? String,
                    value != "" || value1 != "" || value2 != "" {
            
            cell.button2.setTitle("Call Now", for: .normal)
            cell.button2.addTarget(self, action: #selector(callToLab), for: .touchUpInside)
            
        } else {
            cell.button2.alpha = 0
        }
  
        if let value = item["BOOKTESTWITHPRESCRIPTION"] as? Bool, value {
            cell.button1.setTitle("Book With Prescription", for: .normal)
        } else {
            cell.button1.alpha = 0
        }
        
        if cell.button1.alpha == 0 && cell.button2.alpha == 0 {
            cell.button2HeightConstraint.constant = 0
        }
        
        availableTime = item["TIMEAVAILABLE"] as? String ?? ""
        
        offerString = item["DISCOUNTAVAILABLECOTENT"] as? String ?? ""
        
        cell.labNameLabel.text                  = labName
        cell.labAddressLabel.text               = labAddress
        cell.labApprovalLabel.text              = labApproval
        cell.homeSampleAvailableLabel.text      = homeSampleAvailable
        cell.reportDeliveryAvailableLabel.text  = reportDeliveryAvailable
        cell.labTimingsLabel.text               = timings
        cell.availableTodayLabel.text           = availableTime
        cell.offerLabel.text                    = offerString
        
        cell.labApprovalImageView.alpha              = labApproval == "" ? 0 : 1
        cell.homeSampleAvailableImageView.alpha      = homeSampleAvailable == "" ? 0 : 1
        cell.reportDeliveryAvailableImageView.alpha  = reportDeliveryAvailable == "" ? 0 : 1
        cell.labTimingImageView.alpha                = timings == "" ? 0 : 1
        cell.availableTodayImageView.alpha           = availableTime == "" ? 0 : 1
        cell.offerImageView.alpha                    = offerString == "" ? 0 : 1
        
        let rowcount = cell.offerLabel.calculateMaxLines()
        
        cell.offerViewHeightConstraint.constant = offerString == "" ? 0 : CGFloat(rowcount*20 + 6)
        
        return cell
    }
    
    func getLabelCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        
        cell.itemTitleLabel.text = self.LabSearchByNameArray[indexPath.row]["SearchDisplay"] as? String ?? ""
        cell.itemTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.itemImageView.image = #imageLiteral(resourceName: "arrow")
        cell.itemImageViewWidthConstraint.constant = 20
        
        return cell
    }
    
    func setRightBarButton() {
        
        if self.Location != "" {
            
            label.frame = CGRect.init(x: 20, y: 0, width: 100, height: 40)
            if self.Location.contains(" | ") {
                label.text = self.Location
            } else {
                label.text = "\(self.Location) | "
            }
            label.textColor = UIColor.white
            label.textAlignment = .right
            label.isUserInteractionEnabled = true
            label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rightBarButtonTapped)))
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(marquee), userInfo: nil, repeats: true)
            let barButton = UIBarButtonItem(customView: label)
            barButton.setBadge(text: "▼", withOffsetFromTopRight: CGPoint(x: 13, y: 11), andColor: .clear, andFilled: false)
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    
    @objc func rightBarButtonTapped() {
        
//        if let mvc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CitiesPopOverViewController") as? CitiesPopOverViewController {
//            
//            mvc.delegate = self
//            mvc.CurrentLocation = self.CurrentLocation
//            mvc.CurrentLatitude = self.CurrentLatitude
//            mvc.CurrentLongitude = self.CurrentLongitude
//            self.present(mvc, animated: true, completion: nil)
//            
//        }
        
        if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "LocationPopOverViewController") as? LocationPopOverViewController {
            
            viewController.delegate = self
            viewController.currentLocation  = self.CurrentLocation
            viewController.currentLatitude  = self.CurrentLatitude
            viewController.currentLongitude = self.CurrentLongitude
            self.present(viewController, animated: true, completion: nil)
        }

    }
    
    @objc func marquee(){
        
        let str = label.text!
        let indexFirst = str.index(str.startIndex, offsetBy: 0)
        let indexSecond = str.index(str.startIndex, offsetBy: 1)
        
        label.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    }
    
    @objc func callToLab(_ sender: UIButton) {
        
        let item = self.infoArray[sender.tag]
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        if let value = item["PHONENO1"] as? String, value != "" {
            alert.addAction(UIAlertAction(title: value, style: .default, handler: {(action:UIAlertAction) in
            if let url = URL(string: "tel://\(value)"),
                UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
        }
        
        if let value = item["PHONENO2"] as? String, value != "" {
            alert.addAction(UIAlertAction(title: value, style: .default, handler: {(action:UIAlertAction) in
                if let url = URL(string: "tel://\(value)"),
                    UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
        }
        
        if let value = item["HOMESAMPLECONTACTNO"] as? String, value != "" {
            alert.addAction(UIAlertAction(title: value, style: .default, handler: {(action:UIAlertAction) in
                if let url = URL(string: "tel://\(value)"),
                    UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func gotoBookTest(_ sender: UIButton) {
        
        let viewController = AppController.shared.getLabPackageTestViewController()
        viewController.LabGUID = self.infoArray[sender.tag]["APPGUID"] as? String ?? ""
        viewController.LabId = "\(self.infoArray[sender.tag]["DIAGNOSTICLABNO"] as? Int ?? 0)"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoBookTestWithPrescription(_ sender: UIButton) {
        
        let viewController = AppController.shared.getLabSlotPickerViewController()
        viewController.labName = self.infoArray[sender.tag]["FIRMNAME"] as? String ?? ""
        
        if let value1 = self.infoArray[sender.tag]["AREANAME"] as? String,
            let value2 = self.infoArray[sender.tag]["ADDRESSTWO"] as? String {
            
            viewController.labAddress = "\(value1), \(value2)"
            
        }
        
        viewController.labLogo = self.infoArray[sender.tag]["LOGOFILENAME"] as? String ?? ""
        viewController.labGUID = self.infoArray[sender.tag]["APPGUID"] as? String ?? ""
        
        if let value = self.infoArray[sender.tag]["ISHOMESAMPLE"] as? Bool, value {
            viewController.isHomeSampleAvailable = "Y"
        } else {
            viewController.isHomeSampleAvailable = "N"
        }
        
        if let value = self.infoArray[sender.tag]["REPORTDELIVERYATHOME"] as? String, value == "Y" {
            viewController.isReportDeliveryAvailable = "Y"
        } else {
            viewController.isReportDeliveryAvailable = "N"
        }
        
        viewController.homeSampleCharge = "\(self.infoArray[sender.tag]["HOMEVISITMINAMOUNT"] as? Int ?? 0)"
        viewController.reportDeliveryCharges = "\(self.infoArray[sender.tag]["REPORTDELIVERYMINAMOUNT"] as? Int ?? 0)"
        viewController.isPrescriptionMandatory = true
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @objc func searchLabWithDefaultLocation() {
        
        let defaultAddress = Session.shared.getUserDefaultAddress()
        
        self.Latitude = Double(defaultAddress.latitude) ?? 0.0
        self.Longitude = Double(defaultAddress.longitude) ?? 0.0
        self.Location = defaultAddress.areaName + " | " + defaultAddress.cityName + " | "
        
        self.setRightBarButton()
        self.infoArray.removeAll()
        self.tableView.reloadData()
        self.searchText = ""
        self.loadLabBasicSearchAPICall()
        
    }
    
}
