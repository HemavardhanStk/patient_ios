//
//  LabBasicSearchTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabBasicSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var labContainerView: UIView!
    @IBOutlet weak var offerView: UIView!
    
    @IBOutlet weak var offerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var button2HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var labNameLabel: UILabel!
    @IBOutlet weak var labAddressLabel: UILabel!
    @IBOutlet weak var labApprovalLabel: UILabel!
    
    @IBOutlet weak var homeSampleAvailableLabel: UILabel!
    @IBOutlet weak var reportDeliveryAvailableLabel: UILabel!
    @IBOutlet weak var labTimingsLabel: UILabel!
    @IBOutlet weak var availableTodayLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    
    @IBOutlet weak var labLogoImageView: UIImageView!
    @IBOutlet weak var labApprovalImageView: UIImageView!
    @IBOutlet weak var homeSampleAvailableImageView: UIImageView!
    @IBOutlet weak var reportDeliveryAvailableImageView: UIImageView!
    @IBOutlet weak var labTimingImageView: UIImageView!
    @IBOutlet weak var availableTodayImageView: UIImageView!
    @IBOutlet weak var offerImageView: UIImageView!
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
