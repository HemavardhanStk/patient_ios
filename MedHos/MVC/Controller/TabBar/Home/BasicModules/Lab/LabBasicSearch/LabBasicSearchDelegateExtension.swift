//
//  LabBasicSearchDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation

extension LabBasicSearchViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.IsSearching {
            return self.LabSearchByNameArray.count
        } else {
            return self.infoArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.IsSearching {
            return self.getLabelCell(indexPath)
        } else {
            return self.getLabBasicSearchCell(indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.infoArray.count != 0 && indexPath.row == self.infoArray.count - 1 && self.TotalNumberOfPages != self.pageNumber {
            self.searchName = ""
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
            
            self.loadLabBasicSearchAPICall(self.infoArray.count != 0)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if !IsSearching {
            let viewController = AppController.shared.getMultiProfileViewController()
            viewController.Title_Name = TITLE_DIAGONSTIC_LAB
            viewController.TypeOfID = "Lab"
            viewController.initInfoArray = ["LabData" : self.infoArray[indexPath.row]]
            self.navigationController!.pushViewController(viewController, animated: true)
        } else {
            self.view.endEditing(true)
            self.IsSearching = false
            self.pageNumber = 0
            self.infoArray.removeAll()
            self.searchName = self.LabSearchByNameArray[indexPath.row]["DrpText"] as? String ?? ""
            self.loadLabBasicSearchAPICall()
        }
    }
    
}

extension LabBasicSearchViewController: UISearchBarDelegate {
    
    // MARK: - SearchBar Delegate Methods
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
   
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count < 2 {
            
            HttpManager.shared.cancelAllRequests()
            self.IsSearching = false
            self.tableView.reloadData()
            
        } else if let searchBarText = searchBar.text,
            searchBarText.count > 2 {
            
            let newString = searchBarText.trimmingCharacters(in: CharacterSet.whitespaces)
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector:#selector(loadLabSearchByNameAPICall), object: self.searchText)
            
            // Don't replace SearchText until after the cancelPreviousPerformRequestWithTarget call
            self.searchText = newString
            perform(#selector(loadLabSearchByNameAPICall), with: newString, afterDelay: 1)
        }

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
        
        self.IsSearching = false
        self.tableView.reloadData()
    }
    
}

extension LabBasicSearchViewController: LocationPopOverDelegate {
    
    func didCitySelected(_ Name: String, lat: Double, long: Double) {
        
        self.isInitialLocationPicking = false
        self.pageNumber = 0
        self.TotalNumberOfPages = 0
        self.infoArray.removeAll()
        self.Location = Name
        self.Latitude = lat
        self.Longitude = long
        
        self.infoArray.removeAll()
        self.tableView.reloadData()
        
        self.setRightBarButton()
        self.searchName = ""
        self.loadLabBasicSearchAPICall()
        
    }
    
}

extension LabBasicSearchViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.isInitialLocationPicking {
            
            let userLocation :CLLocation = locations[0] as CLLocation
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
                
                if (error != nil){
                    print("error in reverseGeocode")
                }
                
                let placemark = placemarks! as [CLPlacemark]
                
                if placemark.count > 0 {
                    
                    let placemark = placemarks![0]
                    
                    self.CurrentLatitude = userLocation.coordinate.latitude
                    self.CurrentLongitude = userLocation.coordinate.longitude
                    self.Latitude = userLocation.coordinate.latitude
                    self.Longitude = userLocation.coordinate.longitude
                    self.Location = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    self.CurrentLocation = " \(placemark.subLocality!) | \(placemark.locality!) |"
                    self.searchName = ""
                    self.loadLabBasicSearchAPICall()
                    
                    self.setRightBarButton()
                }
            }
            self.isInitialLocationPicking = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
}
