//
//  LabPackageTestDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabPackageTestViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.infoArray.count != 0 ? 2 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if let value1 = self.infoArray["LabTestDetails"] as? Dictionary<String,Any>,
                let value2 = value1["LabPackage"] as? [Dictionary<String,Any>],
                value2.count == 0 {
                
                return CGFloat.leastNonzeroMagnitude
            }
        }
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if let value1 = self.infoArray["LabTestDetails"] as? Dictionary<String,Any>,
                let value2 = value1["LabPackage"] as? [Dictionary<String,Any>],
                value2.count > 0 {
                return 137
            } else {
                return CGFloat.leastNonzeroMagnitude
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let Header = UIView(frame: CGRect.zero)
        let HeaderLabel = UILabel()
        
        if section == 0 {
            if let value1 = self.infoArray["LabTestDetails"] as? Dictionary<String,Any>,
                let value2 = value1["LabPackage"] as? [Dictionary<String,Any>],
                value2.count > 0 {
                HeaderLabel.text = "Packages"
            } else {
                HeaderLabel.text = ""
            }
        } else {
            HeaderLabel.text = "Tests"
        }
        HeaderLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        HeaderLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        HeaderLabel.frame.origin.x = Header.frame.minX + 10
        HeaderLabel.frame.origin.y = Header.frame.minY + 10
        HeaderLabel.frame.size = CGSize(width: 300, height: 20)
        Header.backgroundColor = UIColor.clear
        Header.addSubview(HeaderLabel)
        return Header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 0 ? 1 : self.infoTestArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            return self.getCollectionViewCell()
            
        case 1:
            return self.getTestViewCell(indexPath)
            
        default:
            return UITableViewCell()
        }
    }
        
}

extension LabPackageTestViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let value1 = self.infoArray["LabTestDetails"] as? Dictionary<String,Any> {
            if let value2 = value1["LabPackage"] as? [Dictionary<String,Any>] {
                
                return value2.count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 270, height: 137)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.getPackageViewCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = AppController.shared.getViewTestPackageViewController()
        
        viewController.labGUID = self.LabGUID
        viewController.labId = self.LabId
        
        if var value = self.infoArray["LabTestDetails"] as? Dictionary<String,Any> {
           if var value1 = value["LabPackage"] as? [Dictionary<String,Any>] {
                viewController.packageId = "\(value1[indexPath.row]["PackageNo"] as? Int ?? 0)"
            }
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension LabPackageTestViewController: UISearchBarDelegate {
    
    // MARK: - SearchBar Delegate Methods
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
        
        self.infoTestArray = self.infoTestArrayBackup
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterContentForSearchText(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
