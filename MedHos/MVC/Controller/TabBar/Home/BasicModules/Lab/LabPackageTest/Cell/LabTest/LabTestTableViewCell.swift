//
//  LabTestTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabTestTableViewCell: UITableViewCell {

    @IBOutlet weak var testContainerView: UIView!
    
    @IBOutlet weak var testNameLabel: UILabel!
    @IBOutlet weak var testDescriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var instructionButton: UIButton!
    @IBOutlet weak var addRemoveToCartButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
