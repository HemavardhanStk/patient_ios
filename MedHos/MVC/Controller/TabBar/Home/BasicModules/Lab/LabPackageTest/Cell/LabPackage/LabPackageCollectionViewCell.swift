//
//  LabPackageCollectionViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 15/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabPackageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var packageContainerView: UIView!
    
    @IBOutlet weak var packageNameLabelContainerView: UIView!
    @IBOutlet weak var packageNameLabel: UILabel!
    
    @IBOutlet weak var subLabel1: UILabel!
    @IBOutlet weak var subLabel2: UILabel!
    @IBOutlet weak var subLabel3: UILabel!
    @IBOutlet weak var testsIncludedLabel: UILabel!
    
    @IBOutlet weak var subLabel2StrikeView: UIView!
    
    @IBOutlet weak var addRemoveToCartButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
