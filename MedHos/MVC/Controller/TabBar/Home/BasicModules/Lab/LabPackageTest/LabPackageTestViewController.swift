//
//  LabPackageTestViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 15/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabPackageTestViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    
    var packageCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    var infoArray = Dictionary<String, Any>()
    
    var infoTestArray = [Dictionary<String, Any>]()
    var filteredInfoTestArray = [Dictionary<String, Any>]()
    var infoTestArrayBackup = [Dictionary<String, Any>]()
    
    var labName = ""
    var LabGUID = ""
    var LabId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.labName, controller: self)
        
        self.setUpColors()
        self.loadInfoData()
        
        self.buttonHeightConstraint.constant = 0
        self.button.addTarget(self, action: #selector(gotoCart), for: .touchUpInside)
        
        self.searchBar.delegate = self
        self.searchBar.setSearchBarStyle()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabListTableViewCell", bundle: nil), forCellReuseIdentifier: "LabListTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 130
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
        NotificationCenter.default.addObserver(self, selector: #selector(loadInfoData), name: Notification.Name.Task.ReloadLabPackageTestList, object: nil)
        
    }
    
    func setUpColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        
        self.getCartCountAPICall()
        self.getLabPackageTestsAPICall()
    }

    @objc func rightBarButtonTapped() {
        self.gotoCart()
    }
    
    @objc func gotoCart() {
        
        let navigationStackCount: Int! = self.navigationController?.viewControllers.count
        guard let viewController = self.navigationController?.viewControllers[navigationStackCount-2] else { return }
        
        if ((viewController as? LabChartViewController) != nil) {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadLabCartList, object: self, userInfo: nil)
            self.navigationController?.popViewController(animated: true)
        } else {
            let viewController = AppController.shared.getLabChartViewController()
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}
