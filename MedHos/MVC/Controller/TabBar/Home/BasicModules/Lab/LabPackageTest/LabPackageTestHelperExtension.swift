//
//  LabPackageTestHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabPackageTestViewController {
    
    
    func getCollectionViewCell() -> CollectionTableViewCell {
        
        let cell: CollectionTableViewCell = Bundle.main.loadNibNamed("CollectionTableViewCell", owner: nil, options: nil)![0] as! CollectionTableViewCell
        
        cell.backgroundColor = .clear
        cell.CollectionView.backgroundColor = .clear
        
        self.packageCollectionView = cell.CollectionView
        
        cell.CollectionView.delegate = self
        cell.CollectionView.dataSource = self
        cell.CollectionView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        cell.CollectionView.register(UINib.init(nibName: "LabPackageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LabPackageCollectionViewCell")
        
        return cell
    }
    
    func getTestViewCell(_ indexPath: IndexPath) -> LabTestTableViewCell {
        
        let cell: LabTestTableViewCell = Bundle.main.loadNibNamed("LabTestTableViewCell", owner: nil, options: nil)![0] as! LabTestTableViewCell
        
        cell.backgroundColor = .clear
        cell.testContainerView.setRoundedCorner()
        
        cell.testNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.testDescriptionLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.priceLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
  
        cell.testNameLabel.textColor = UIColor.black
        cell.testDescriptionLabel.textColor = UIColor.darkGray
        cell.priceLabel.textColor = UIColor.black
        
        cell.testDescriptionLabel.numberOfLines = 0
        
        cell.addRemoveToCartButton.layer.cornerRadius = 5
        cell.addRemoveToCartButton.layer.masksToBounds = true
        cell.addRemoveToCartButton.setTitleColor(UIColor.white, for: .normal)
        cell.addRemoveToCartButton.addTarget(self, action: #selector(addRemovePackageTestAPICall(_:)), for: .touchUpInside)
        
        cell.addRemoveToCartButton.tag = indexPath.row
        cell.instructionButton.tag = indexPath.row
        
        let image = UIImage(named: "ic_about_blue")?.withRenderingMode(.alwaysTemplate)
        cell.instructionButton.setImage(image, for: .normal)
        cell.instructionButton.tintColor = UIColor.lightGray
        cell.instructionButton.imageEdgeInsets = UIEdgeInsets(top: 2,left: 2,bottom: 2,right: 2)
        
        cell.instructionButton.addTarget(self, action: #selector(showInstructionAlert(_:)), for: .touchUpInside)
        
            let item = self.infoTestArray[indexPath.row]

            cell.testNameLabel.text = item["TestName"] as? String ?? ""
            cell.testDescriptionLabel.text = item["Description"] as? String ?? ""
            
            if let price = item["TotalAmout"] as? Int, price != 0 {
                cell.priceLabel.text = "₹\(price)"
            } else {
                cell.priceLabel.text = ""
            }
            
            if item["AddorRemoveButton"] as? String == "Y" {
                cell.addRemoveToCartButton.setTitle("  Remove from Cart  ", for: .normal)
                cell.addRemoveToCartButton.layer.backgroundColor = UIColor(netHex: APP_LIGHT_RED_COLOR).cgColor
            } else {
                cell.addRemoveToCartButton.setTitle("  Add to Cart  ", for: .normal)
                cell.addRemoveToCartButton.layer.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
            }
        
        return cell
    }
    
    func getPackageViewCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> LabPackageCollectionViewCell {
        
        let cell: LabPackageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LabPackageCollectionViewCell", for: indexPath) as! LabPackageCollectionViewCell
        
        cell.backgroundColor = .clear
        cell.packageContainerView.setRoundedCorner()
        
        cell.packageNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.testsIncludedLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.subLabel1.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 15)
        cell.subLabel2.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.subLabel3.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
 
        cell.packageNameLabel.textColor = UIColor.white
        cell.testsIncludedLabel.textColor = UIColor.darkGray
        cell.subLabel1.textColor = UIColor.black
        cell.subLabel2.textColor = UIColor.darkGray
        cell.subLabel3.textColor = UIColor.white
        
        cell.subLabel2StrikeView.backgroundColor = .darkGray
        
        cell.subLabel3.backgroundColor = UIColor(netHex: APP_GREEN)
        cell.subLabel3.layer.cornerRadius = 5
        cell.subLabel3.layer.masksToBounds = true
        
        cell.addRemoveToCartButton.layer.cornerRadius = 5
        cell.addRemoveToCartButton.layer.masksToBounds = true
        cell.addRemoveToCartButton.setTitleColor(UIColor.white, for: .normal)
        cell.addRemoveToCartButton.tag = 1000 + indexPath.row
        cell.addRemoveToCartButton.addTarget(self, action: #selector(addRemovePackageTestAPICall(_:)), for: .touchUpInside)
        
        if let value1 = self.infoArray["LabTestDetails"] as? Dictionary<String,Any>,
            let value2 = value1["LabPackage"] as? [Dictionary<String,Any>] {
            
            let item = value2[indexPath.row]
            
            cell.packageNameLabel.text = item["PackageName"] as? String ?? ""
            
            if let testsCount = item["TestCount"] as? Int, testsCount != 0 {
                
                cell.testsIncludedLabel.text = "\(testsCount) test(s) included"
            } else {
                cell.testsIncludedLabel.text = "   "
            }
            
            if let netAmount = item["NetAmount"] as? Int,
                let actualAmount = item["Amount"] as? Int,
                let discountValue = item["DiscountPercentage"] as? Int {
                
                cell.subLabel1.text = netAmount != 0 ? "₹\(netAmount)" : ""
                cell.subLabel2.text = actualAmount != 0 && actualAmount != netAmount ? "₹\(actualAmount)" : ""
                cell.subLabel3.text = discountValue != 0 ? "  \(discountValue) % off  " : ""
                
                cell.subLabel2StrikeView.alpha = actualAmount != 0 && actualAmount != netAmount ? 1 : 0
                
            }
            
            if item["AddorRemoveButton"] as? String == "Y" {
                cell.addRemoveToCartButton.setTitle("  Remove from Cart  ", for: .normal)
                cell.addRemoveToCartButton.layer.backgroundColor = UIColor(netHex: APP_LIGHT_RED_COLOR).cgColor
            } else {
                cell.addRemoveToCartButton.setTitle("  Add to Cart  ", for: .normal)
                cell.addRemoveToCartButton.layer.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
            }
        }
        
        return cell
    }
    
    @objc func showInstructionAlert(_ sender: UIButton) {

            let item = self.infoTestArray[sender.tag]
            
            let titleString = "Instruction"
            let message = item["Preparation"] as? String ?? ""
            
            let alertController = UIAlertController(title: titleString, message: message, preferredStyle: UIAlertController.Style.actionSheet)
            
            alertController.setValue(NSAttributedString(string: titleString, attributes: [NSAttributedString.Key.font : UIFont(name: APP_FONT_NAME_BOLD, size: 15)!,NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
            
            alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont(name: APP_FONT_NAME_NORMAL, size: 15)!,NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
            
            let cancelAction = UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
    }
    
    func setNavigationBarWithCartCount(_ cartCount: Int) {
        
        if cartCount == 0 {
            self.buttonHeightConstraint.constant = 0
            self.navigationItem.rightBarButtonItems?.removeAll()
            Navigation.shared.setTitle(title: self.labName, controller: self)
        } else {
            self.buttonHeightConstraint.constant = 50
            Navigation.shared.setTitlewithRightBarButton(self.labName, badge: "\(cartCount)", image: "qr-1", text: "", controller: self)
        }
    }
    
    func isFiltering() -> Bool {
        return  !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return self.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        if isFiltering() && self.infoTestArray.count == 0 {
            self.infoTestArray = self.infoTestArrayBackup
        }
        
        self.filteredInfoTestArray = self.infoTestArray.filter({( dict : Dictionary<String, Any> ) -> Bool in
            if self.searchBarIsEmpty() {
                return true
            } else {
                let testName: String = dict["TestName"] as! String
                return testName.lowercased().contains(searchText.lowercased())
            }
        })
        
        if isFiltering() {
            self.infoTestArray = self.filteredInfoTestArray
        }
        else{
            self.infoTestArray = self.infoTestArrayBackup
        }
        self.tableView.reloadData()
    }

}
