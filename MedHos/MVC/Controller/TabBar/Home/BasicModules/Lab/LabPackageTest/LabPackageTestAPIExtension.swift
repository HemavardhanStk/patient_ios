//
//  LabPackageTestAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LabPackageTestViewController {
    
    func getLabPackageTestsAPICall() {
        
        let user = Session.shared.getUserInfo()
        let params = ["LabGUID"             : self.LabGUID,
                      "Language"            :"English",
                      "DiagnosticLabNo"     :"0",
                      "UserId"              :user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_GetDiagnosticLabTestList, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let value1 = value["LabTestDetails"] as? Dictionary<String,Any>,
                    let value2 = value1["LabTest"] as? [Dictionary<String,Any>],
                   let labTitle = value["LabFirmName"] as? String {
                    
                    self.navigationItem.title = labTitle
                    self.labName = labTitle
                    
                    self.infoArray = value
                    self.infoTestArray = value2
                    self.infoTestArrayBackup = value2
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
    func getCartCountAPICall() {
        
        let (mobile, userId) = Session.shared.getUserMobileAndUserId()
        
        let params = ["UserId"          : userId,
                      "MobileNumber"    : mobile,
                      "Language"        : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetCartBucketResult, params: params, httpMethod: .post, controller: self, completion: { (response) in
            
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as! Dictionary<String, AnyObject>, controller: self, isBackgroundCall: true) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let cartCount = value["CartCount"] as? Int {
                    
                    self.setNavigationBarWithCartCount(cartCount)
                }
            } else {
                self.setNavigationBarWithCartCount(0)
            }
        }) {}
    }
    
    @objc func addRemovePackageTestAPICall(_ sender: UIButton) {
        
        let (params,urlString) = self.getParams(sender)
        
        HttpManager.shared.loadAPICall(path: urlString, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let response = response["Result"] as? Dictionary<String, Any>,
                    let cartCount = response["CartCount"] as? Int {
                    
                    self.setNavigationBarWithCartCount(cartCount)
                    
                    //MARK :- Update values
                    
                    if var value = self.infoArray["LabTestDetails"] as? Dictionary<String,Any> {
                        if sender.tag < 1000 {
                            
                            let updationValue = self.infoTestArray[sender.tag]["AddorRemoveButton"] as? String == "Y" ? "N" : "Y"
                            self.infoTestArray[sender.tag]["AddorRemoveButton"] = updationValue

                            for i in 0..<self.infoTestArrayBackup.count {
                                if self.infoTestArrayBackup[i]["TestNo"] as? Int == self.infoTestArray[sender.tag]["TestNo"] as? Int {
                                    self.infoTestArrayBackup[i]["AddorRemoveButton"] = updationValue
                                    break
                                }
                            }

                            self.tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 1)], with: .automatic)
                        } else if var value1 = value["LabPackage"] as? [Dictionary<String,Any>] {
                            
                            let updationValue = value1[sender.tag - 1000]["AddorRemoveButton"] as? String == "Y" ? "N" : "Y"
                            value1[sender.tag - 1000]["AddorRemoveButton"] = updationValue
                            value["LabPackage"] = value1
                            self.infoArray["LabTestDetails"] = value
                            self.packageCollectionView.reloadItems(at: [IndexPath(row: sender.tag - 1000, section: 0)])
                        }
                    }
                }
            }
        }) {}
    }
    
    func getParams(_ sender: UIButton) -> (Dictionary<String, Any>, String) {
        
        let user = Session.shared.getUserInfo()
        let category = sender.tag < 1000 ? "test" : "package"
        let urlString = sender.titleLabel!.text!.contains("  Add to Cart  ") ? PATH_AddTesttoCart : PATH_RemoveTestfromCart
        
        var testPackageID = ""
        var testNosArray = [String]()
        
        if let value = self.infoArray["LabTestDetails"] as? Dictionary<String,Any> {
            
            if sender.tag < 1000 {
                
                testPackageID = "\(self.infoTestArray[sender.tag]["TestNo"] as? Int ?? 0)"
                
            } else if let value1 = value["LabPackage"] as? [Dictionary<String,Any>] {
                
                testPackageID = "\(value1[sender.tag - 1000]["PackageNo"] as? Int ?? 0)"
                
                if let value2 = value1[sender.tag - 1000]["LabPackageTestList"] as? [Dictionary<String,Any>] {
                    
                    for i in 0..<value2.count {
                        if let value3 = value2[i]["TestNo"] as? Int {
                            testNosArray.append("\(value3)")
                        }
                    }
                }
            }
        }
        
        let params = ["CartItemCategory"            : category,
                      "DiagnosticLabGUID"           : self.LabGUID,
                      "Language"                    : "English",
                      "TestNos"                     : testNosArray,
                      "DiagnosticLabNo"             : self.LabId,
                      "TestorPackageNo"             : testPackageID,
                      "UserDetailNo"                : user.id] as [String : Any]
        
        return (params, urlString)
    }
    
}
