//
//  SingleSelectionHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 10/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension SingleSelectionViewController {
    
    func getItemCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        if indexPath.row < self.infoArray.count
        {
            cell.itemTitleLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 15)
            cell.itemTitleLabel.text = self.infoArray[indexPath.row][TEXT]
            
            self.setItemSelection(cell, index: indexPath.row)
        }
        return cell
    }
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        cell.defaultTextIconWidthConstraint.constant = 0
        cell.defaultTextField.placeholder = "Add New Member"
        cell.defaultTextField.keyboardType = .alphabet
        cell.defaultTextField.text = self.NewFamilyMemberName
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row
        cell.TextIconRight.alpha = 1
        cell.TextIconRight.image = #imageLiteral(resourceName: "ic_send")
        cell.DefaultTextFieldTraillingConstraint.constant = 50
        cell.defaultButton.addTarget(self, action: #selector(addFamilyMember), for: .touchUpInside)
        //cell.TextIconRight.isUserInteractionEnabled = true
        
        return cell
    }
    
    @objc func addFamilyMember() {
        
        if self.NewFamilyMemberName != "" {
            self.rightBarButtonTapped()
        } else {
            AlertHelper.shared.showAlert(message: "Name can't be empty", controller: self)
        }
    }
    
    func setItemSelection(_ cell: LabelTableViewCell, index: Int) {
        
        if self.infoArray[index][STATUS] == SELECTED {
            cell.itemImageViewWidthConstraint.constant = 20
            cell.itemImageView.image = UIImage.init(named: "ic_done")
        } else {
            cell.itemImageViewWidthConstraint.constant = 0
        }
    }
    
}
