//
//  SingleSelectionDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 10/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension SingleSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.pageTitle == TITLE_APPOINTMENT_FOR ? self.infoArray.count + 1 : self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return indexPath.row == self.infoArray.count ? 70: UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         return indexPath.row == self.infoArray.count ? self.getTextFieldCell(indexPath) : self.getItemCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var indexer = 1000
        for i in 0...self.infoArray.count - 1
        {
            if self.infoArray[i][STATUS] == SELECTED && i != indexPath.row {
                indexer = i
            }
            
            if i != indexPath.row || self.infoArray[i][STATUS] == SELECTED {
                self.infoArray[i][STATUS] = UN_SELECTED
            } else {
                self.infoArray[i][STATUS] = SELECTED
            }
        }
        
        if indexer != indexPath.row && indexer != 1000 {
            if let cell = tableView.cellForRow(at: IndexPath(row: indexer, section: 0)) as? LabelTableViewCell {
                self.setItemSelection(cell, index: indexer)
            }
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? LabelTableViewCell {
            
            self.setItemSelection(cell, index: indexPath.row)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension SingleSelectionViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.NewFamilyMemberName = textField.text!
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        for i in 0...self.infoArray.count - 1
        {
            if self.infoArray[i][STATUS] == SELECTED
            {
                self.infoArray[i][STATUS] = UN_SELECTED
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.NewFamilyMemberName = textField.text!
    }
}
