//
//  SingleSelectionViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 19/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol SingleSelectionDelegate {
    func didSingleSelected(_ selectedItem: String, IDs: String, Value3: String, AppointmentDate: String, AppointmentTime: String, PatientId: String)
}

class SingleSelectionViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var FamilyMembers = [String]()
    var infoArray = [Dictionary<String, String>]()
    var selectedItemArray = [Dictionary<String, String>]()
    var pageTitle = ""
    var NewFamilyMemberName = ""
    var delegate: SingleSelectionDelegate?
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitlewithRightBarButton(self.pageTitle, image: "", text: "DONE", controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        
        self.tableView.setRoundedCorner()

    }
    
    func loadInfoData() {
        
        if self.pageTitle == TITLE_APPOINTMENT_FOR {
            self.getFamilyListAPICall()
        } else {
            self.getPastAppointmentsAPICall()
        }
    }

    @objc func rightBarButtonTapped() {
        
        self.view.endEditing(false)
        
        self.selectedItemArray = []
        for item in self.infoArray {
            if item[STATUS] == SELECTED {
                self.selectedItemArray.append(item)
            }
        }
        
        if pageTitle == TITLE_APPOINTMENT_FOR {
            
            if self.selectedItemArray.count != 0 {
                if self.delegate != nil {
                    self.delegate?.didSingleSelected(self.selectedItemArray[0][TEXT]!, IDs: self.selectedItemArray[0]["IDs"]!, Value3: Helper.shared.getGenderValue(self.selectedItemArray[0]["Gender"]!), AppointmentDate: "", AppointmentTime: "", PatientId: "")
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                if self.delegate != nil {
                    self.delegate?.didSingleSelected(self.NewFamilyMemberName, IDs: "0", Value3: "", AppointmentDate: "", AppointmentTime: "", PatientId: "")
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        } else {
            if self.selectedItemArray.count != 0 {
                if self.delegate != nil {
                    self.delegate?.didSingleSelected(self.selectedItemArray[0]["PatientName"]!, IDs: self.selectedItemArray[0]["Appoinmentid"]!, Value3: self.selectedItemArray[0]["DoctorName"]!, AppointmentDate: self.selectedItemArray[0]["AppointmentDate"]!, AppointmentTime: self.selectedItemArray[0]["AppointmentTime"]!, PatientId: self.selectedItemArray[0]["UserId"]!)
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
