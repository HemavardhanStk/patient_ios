//
//  SingleSelectionAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 10/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension SingleSelectionViewController {
    
    func getFamilyListAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["UserId":user.id,
                      "MobileNumber":user.phone,
                      "Language":"English"]
        
        HttpManager.shared.loadAPICall(path: PATH_ListUserFamily, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["ListFamily"] as? [Dictionary<String, Any>] {
                    
                    for i in 0..<value.count {
                        let dict = [TEXT:value[i]["Name"] as? String ?? "","IDs":"\(value[i]["UserDetailId"] as? Int ?? 0)","Gender":value[i]["Gender"] as? String ?? "",STATUS: UN_SELECTED]
                        self.infoArray.append(dict)
                    }
                    self.tableView.reloadData()
                }
            }
        }) {}
    }

    func getPastAppointmentsAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["UserId":user.id,
                      "MobileNumber":user.phone,
                      "Language":"English"]

        HttpManager.shared.loadAPICall(path: PATH_GetMedicalAppointment, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["PastAppointments"] as? [Dictionary<String, Any>] {
                    
                    for i in 0..<value.count {
                        let dict = [TEXT:"Doctor Name : \(value[i]["DoctorName"] as? String ?? "") \nAppointment No: \(value[i]["ReferenceID"] as? String ?? "" ) \nAppointment Date: \(value[i]["AppointmentDate"] as? String ?? "")",
                                    STATUS: UN_SELECTED,"Appoinmentid":"\(value[i]["Appoinmentid"] as? Int ?? 0)",
                                    "DoctorName": value[i]["DoctorName"] as? String ?? "",
                                    "AppointmentDate": value[i]["AppointmentDate"] as? String ?? "",
                                    "AppointmentTime": value[i]["RequestedTime"] as? String ?? "",
                                    "PatientName": value[i]["PatientName"] as? String ?? "",
                                    "UserId": "\(value[i]["UserNo"] as? Int ?? 0)"]
                        self.infoArray.append(dict)
                    }
                    self.tableView.reloadData()
                }
            }
        }) {}
    }

}
