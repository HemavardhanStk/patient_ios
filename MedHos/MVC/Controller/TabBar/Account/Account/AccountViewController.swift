//
//  AccountViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 17/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNumberLabel: UILabel!
    @IBOutlet weak var tapRecogniserButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, Any>]()
    let SECTION = "Section"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        self.setupColors()
        self.setTopView()
        self.loadInfoData()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTabBarTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTabBarTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: -10, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
//        NotificationCenter.default.addObserver(self, selector: #selector(scrollToTop), name: Notification.Name.Task.AccountPageScrollToTop, object: nil)
    }

    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func setTopView() {
        
        self.userImageView.setRoundedCorner(20)
        
        self.userNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        self.userNumberLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        self.userNameLabel.textColor = .white
        self.userNumberLabel.textColor = .white
        
        let user = Session.shared.getUserInfo()
        
        self.userNameLabel.text = user.name
        self.userNumberLabel.text = user.phone
        
        if let url = URL(string: user.image) {
            self.userImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "login"))
        } else {
            self.userImageView.image = #imageLiteral(resourceName: "login")
        }
        
        self.tapRecogniserButton.addTarget(self, action: #selector(gotoProfile), for: .touchUpInside)
    }
    
    func loadInfoData() {
        
        self.infoArray = []
        var subinfoArray = [Dictionary<String, String>]()
        
        var dict = [IMAGE: "ic_location_icon", TITLE: "My Location"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_mobileno_icon", TITLE : "Change Mobile Number"]
        subinfoArray.append(dict)
        
        var subDict = [self.SECTION: subinfoArray, TITLE : "Change Settings"] as [String : Any]
        self.infoArray.append(subDict)
        
        subinfoArray = []
        
        dict = [IMAGE: "ic_faq_icon", TITLE : "FAQ"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_call_icon", TITLE : "Call"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_contact_icon", TITLE : "Contact Us"]
        subinfoArray.append(dict)

        dict = [IMAGE: "ic_emerency_icon", TITLE : "Emergency Number"]
        subinfoArray.append(dict)
        
        subDict = [self.SECTION: subinfoArray, TITLE : "Help"] as [String : Any]
        self.infoArray.append(subDict)
        
        subinfoArray = []
        dict = [IMAGE: "ic_notification_icon", TITLE : "Notifications"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_refer-to-friend_icon", TITLE : "Refer to Friends"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_rate_us_icon", TITLE : "Rate Us"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_sign-out_icon", TITLE : "Sign Out"]
        subinfoArray.append(dict)
        
        subDict = [self.SECTION: subinfoArray, TITLE : "General"] as [String : Any]
        self.infoArray.append(subDict)
        
        self.tableView.reloadData()
    }

    @objc func gotoProfile() {
        let viewController = AppController.shared.getPatientProfileViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
