//
//  AccountHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AccountViewController {
    
    func getItemTableViewCell(_ indexPath: IndexPath) -> LabelTabBarTableViewCell {
        
        let cell: LabelTabBarTableViewCell = Bundle.main.loadNibNamed("LabelTabBarTableViewCell", owner: nil, options: nil)![0] as! LabelTabBarTableViewCell
        
        cell.selectionStyle = .none
        
        cell.itemImageViewWidthConstraint.constant = 25
        cell.itemImageViewLeadingConstraint.constant = 12
        
        cell.itemNameLabel.textColor = .black
        cell.itemNameLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 15)
        
        cell.itemImageView.contentMode = .scaleAspectFit
        
        if let array = self.infoArray[indexPath.section][self.SECTION] as? [Dictionary<String, String>] {
            
            let item = array[indexPath.row]
            
            cell.itemNameLabel.text = item[TITLE]
            cell.itemImageView.image = UIImage(named: item[IMAGE]!)
            cell.arrowImageView.image = UIImage(named: "ic_right_arrow_blue")
            cell.itemBadgeLabel.text = ""
            cell.bottomLineView.alpha = indexPath.row == array.count - 1 ? 0 : 1
            
            if indexPath.row == 0 {
                cell.backgroundColor = .clear
                cell.labelTabBarView.backgroundColor = .clear
                cell.labelTabBarBgImageView.alpha = 1
                cell.labelTabBarBgImageView.image = UIImage(named: "ic_view_bg")
            } else if indexPath.row == array.count - 1 {
                cell.backgroundColor = .clear
                cell.labelTabBarView.backgroundColor = .clear
                cell.labelTabBarBgImageView.alpha = 1
                cell.labelTabBarBgImageView.image = UIImage(cgImage: UIImage(named: "ic_view_bg")!.cgImage!, scale: 1.0, orientation: .downMirrored)
            }
            
        }
        
        return cell
    }
    
    func navigate(_ indexPath: IndexPath) {
        
        switch indexPath.section {
            
            
        case 0:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getUserLocationViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let viewController = AppController.shared.getChangeMobileViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
            
            default: break
            }
            break
            
        case 1:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getFAQViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let supportNumber = Session.shared.getSupportMobileNumber()
                if let url = URL(string: "tel://\(supportNumber)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                break
                
            case 2:
                let viewController = AppController.shared.getContactUsViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 3:
                let viewController = AppController.shared.getEmergencyNumbersViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            default: break
            }
            break
            
        case 2:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getNotificationViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let invitationMessage = Session.shared.getInviteMessage()
                let AppInvitation = UIActivityViewController(activityItems: [invitationMessage as Any], applicationActivities: nil)
                present(AppInvitation,animated: true,completion: nil)
                break
                
            case 2:
                let appUrl = Session.shared.getAppUrl()
                guard let url = URL(string : "\(appUrl)&action=write-review") else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                break
                
            case 3:
                UserInfo.shared.showLogoutAlert(self)
                break
                
            default: break
            }
            break
            
        default: break
        }
    }

    
}
