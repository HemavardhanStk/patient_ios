//
//  AccountDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 17/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.infoArray[section][self.SECTION] as? [Dictionary<String, String>] {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let item = self.infoArray[section]
        
        let headerView = UIView(frame: CGRect.zero)
        let headerLabel = UILabel()
        headerLabel.text = item[TITLE] as? String
        headerLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        headerLabel.frame.origin.x = headerView.frame.minX + 5
        headerLabel.frame.origin.y = headerView.frame.minY + 10
        headerLabel.frame.size = CGSize(width: 300, height: 20)
        headerView.backgroundColor = UIColor.clear
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getItemTableViewCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.navigate(indexPath)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }

}
