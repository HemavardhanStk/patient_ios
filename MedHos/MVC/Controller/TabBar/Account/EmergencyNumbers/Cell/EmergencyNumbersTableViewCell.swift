//
//  EmergencyNumbersTableViewCell.swift
//  tests
//
//  Created by Hemavardhan on 02/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class EmergencyNumbersTableViewCell: UITableViewCell {

    @IBOutlet weak var ContactNameLabel: UILabel!
    @IBOutlet weak var ContactValueLabel: UILabel!
    @IBOutlet weak var CallButton: UIImageView!
    @IBOutlet weak var BottomLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
