//
//  EmergencyNumbersHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 01/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension EmergencyNumbersViewController {
    
    // MARK: - Notification Cell
    
    func getEmergencyNumbersCell(_ indexPath: IndexPath) -> EmergencyNumbersTableViewCell {
        
        let cell: EmergencyNumbersTableViewCell = Bundle.main.loadNibNamed("EmergencyNumbersTableViewCell", owner: nil, options: nil)![0] as! EmergencyNumbersTableViewCell
        
        cell.selectionStyle = .none
        
        var ContactName = ""
        var ContactValue = ""
        
        if let value = self.infoArray[indexPath.row]["ContactName"] as? String {
            ContactName = value
        }
        
        if let value = self.infoArray[indexPath.row]["ContactValue"] as? Int {
            ContactValue = "\(value)"
        }
        
        cell.ContactNameLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 14)
        cell.ContactValueLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.ContactNameLabel.textColor = UIColor.black
        cell.ContactValueLabel.textColor = UIColor.black
        
        cell.ContactNameLabel.text = ContactName
        cell.ContactValueLabel.text = ContactValue
        
        return cell
    }
    
    func selectionHandler(_ indexPath: IndexPath) {
        
        if let value = self.infoArray[indexPath.row]["ContactValue"] as? Int {
            if let url = URL(string: "tel://\(value)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
}
