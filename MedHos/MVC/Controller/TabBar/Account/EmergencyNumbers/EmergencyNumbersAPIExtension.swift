//
//  EmergencyNumbersAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension EmergencyNumbersViewController {
    
    func getEmergencyNumberAPICall() {
        let params = ["Language" : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetEmergencyDataforUser, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["ECList"] as? [Dictionary<String, Any>] {
                    self.infoArray = value
                    self.tableView.reloadData() 
                }
            }
        }) {}
    }
    
}
