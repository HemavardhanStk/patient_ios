//
//  PatientProfileViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import AlamofireImage

class PatientProfileViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var isStepperSignup = false
    var titleName = ""
    
    var datapasser = [String:Any]()
    
    var existingInfoArray = Dictionary<String, Any>()
    var infoArray = [Dictionary<String, String>]()
    
    var imagePickerController: UIImagePickerController?
    var selectedProfileImage: UIImage?
    
    let DOB_DATE_FORMAT = "dd-MMM-yyyy"
    
    let TAG_MOBILE_NUMBER = 2
    let TAG_GENDER = 4
    let TAG_ALTERNATIVE_MOBILE_NUMBER = 5
    let TAG_DOB = 6
    let TAG_MARITAL_STATUS = 7
    let TAG_BLOOD_GROUP = 8
    let TAG_LANGUAGE = 9
    
    var initialUserProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            NotificationCenter.default.post(name: Notification.Name.Task.AccountPageScrollToTop, object: self, userInfo: nil)
        }
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.titleName.count == 0 ? TITLE_PROFILE : self.titleName, controller: self)
        
        self.initialUserProfile ? self.loadInfoData() : self.loadUserDetails()
        self.setupColors()
        
        self.imagePickerController = UIImagePickerController.init()
        self.imagePickerController?.delegate = self
        
        self.tableView.register(UINib(nibName: "ProfileImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileImageTableViewCell")
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        
        self.tableView.allowsSelection = false
        self.tableView.tableFooterView = UIView.init()
        NotificationCenter.default.addObserver(self, selector: #selector(setDefaultAddress), name: Notification.Name.Task.ReloadPatientProfile, object: nil)
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        
        self.tableView.setRoundedCorner()
    }
    
    func loadInfoData() {
        
        let defaultAddressString = Session.shared.getUserDefaultAddressString()
        
        var dict = [IMAGE: "", PLACEHOLDER : "Profile Image", TEXT : ""]
        self.infoArray.append(dict)
        
        var Name = ""
        var MobileNo = ""
        var EmailId = ""
        var Gender = ""
        var AlternateMobile = ""
        var DOB = ""
        var MartialStatus = ""
        var BloodGroup = ""
        var PreferredLanguage = ""
        if self.existingInfoArray.count != 0 {
            if let text = self.existingInfoArray["Image"] as? String {
                let imageView = UIImageView.init()
                if let url = URL.init(string: "\(text)") {
                    imageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: true, completion: { (data) in
                        self.selectedProfileImage = imageView.image
                        let indexPath = IndexPath(row: 0, section: 0)
                        if let cell = self.tableView.cellForRow(at: indexPath) as? ProfileImageTableViewCell {
                            self.setProfileImageView(cell)
                        }
                    })
                }
            }
            if let text = self.existingInfoArray["FirstName"] as? String {
                Name = "\(text)"
            }
            
            if let text = self.existingInfoArray["Mobile"] as? String {
                MobileNo = "\(text)"
            }
            
            if let text = self.existingInfoArray["Email"] as? String {
                EmailId = "\(text)"
            }
            
            if let text = self.existingInfoArray["Gender"] as? String {
                Gender = Helper.shared.getGenderValue("\(text)")
            }
            
            if let text = self.existingInfoArray["AlternateMobile"] as? String {
                AlternateMobile = "\(text)"
            }
            
            if let text = self.existingInfoArray["BirthDate"] as? String {
                if "\(text)".count != 0 {
                    DOB = "\(text)"
                }
            }
            
            if let text = self.existingInfoArray["MStatus"] as? String {
                MartialStatus = Helper.shared.getMartialStatusText(text)
            }
            
            if let text = self.existingInfoArray["BloodGroup"] as? String {
                BloodGroup = "\(text)"
            }
            
            if let text = self.existingInfoArray["PreferedLanguage"] as? Int {
                PreferredLanguage = Helper.shared.getPreferredLanguageText("\(text)")
            }
            
        }
        dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : Name]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Mobile Number", TEXT : MobileNo]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Email Id", TEXT : EmailId]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Gender", TEXT : Gender]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Alternate Mobile", TEXT : AlternateMobile]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Date of Birth", TEXT : DOB]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "MartialStatus", TEXT : MartialStatus]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "BloodGroup", TEXT : BloodGroup]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Preferred Language", TEXT : PreferredLanguage]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Address Details", TEXT : defaultAddressString]
        self.infoArray.append(dict)
        
        self.tableView.reloadData()
    }

    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            self.savePersonalDetailsAPICall()
        }
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        if self.initialUserProfile {
            Session.shared.setLoginStatus(LOGIN_STATUS.HOME.rawValue)
            UserInfo.shared.showTabBarViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}




