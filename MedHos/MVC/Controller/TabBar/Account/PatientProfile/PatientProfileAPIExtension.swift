//
//  PatientProfileAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PatientProfileViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        var message = STR_PLEASE_ENTER
        
        var nonMandatory = [0, self.TAG_ALTERNATIVE_MOBILE_NUMBER,self.TAG_DOB]
        
        if self.initialUserProfile {
            nonMandatory.append(contentsOf: [2,7,10])
            nonMandatory = nonMandatory.sorted{$0 < $1}
        }
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: infoArray, nonMandatoryFields: nonMandatory, controller: self)
        
        if isValidationSuccess {
            
            if let email = self.infoArray[3][TEXT],
                email != "" ,
                !ValidationHelper.shared.isValidEmailID(email) {
                
                isValidationSuccess = false
                message = message + STR_VALID + self.infoArray[3][PLACEHOLDER]!
            }
            
            if !isValidationSuccess {
                AlertHelper.shared.showAlert(message: message, controller: self)
            }
        }

        return isValidationSuccess
    }
    
    func loadUserDetails() {
        
        let user = Session.shared.getUserInfo()
        
        let params = [ "UserId"         : user.id,
                       "MobileNumber"   : user.phone,
                       "Language"       : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetUserProfile, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["UserProfileData"] as? Dictionary<String, Any> {
                    self.existingInfoArray = value
                    self.loadInfoData()
                }
            }
        }) {}
        
    }
    
    // MARK: - Personal Details
    
    func savePersonalDetailsAPICall() {
        
        let params = self.getInputParam()
        
        HttpManager.shared.loadAPICall(path: PATH_UpdateUserDetails, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if self.isStepperSignup {
                    AlertHelper.shared.showAlert(title: APP_NAME, message: "Details have been updated", controller: self)
                } else {
                    if let message = response["Message"] as? String {
                        AlertHelper.shared.showAlertWithHandler(message: message, handler: "popViewController", withCancel: false, controller: self)
                    }
                }
            }
        }) {}
    }
    
    func getInputParam() -> Dictionary<String, Any> {
        
        let user = Session.shared.getUserInfo()
        
        var userId = user.id
        
        if self.initialUserProfile {
            userId = Session.shared.getUserMobileAndUserId().1
            self.infoArray[2][TEXT] = Session.shared.getUserMobileAndUserId().0
            self.infoArray[9][TEXT] = LANGUAGE_ARRAY[0]
        }

        if let Name = self.infoArray[1][TEXT],
            let Phone = self.infoArray[2][TEXT],
            let Email = self.infoArray[3][TEXT],
            let Gender = self.infoArray[4][TEXT],
            let AlternateMobile = self.infoArray[5][TEXT],
            let BirthDate = self.infoArray[6][TEXT],
            let MStatus = self.infoArray[7][TEXT],
            let BloodGroup = self.infoArray[8][TEXT],
            let PreferredLanguage = self.infoArray[9][TEXT] {
            
            let Input_UserProfile = ["UserNo" : userId,
                "FirstName" : Name,
                "Phone" : Phone,
                "FamMobile" : Phone,
                "Email" : Email,
                "Gender" : Helper.shared.getGenderTag(Gender),
                "FamAlternateMobile" : AlternateMobile,
                "BirthDate" : BirthDate,
                "MStatus" : Helper.shared.getMartialStatus(MStatus),
                "BloodGroup" : BloodGroup,
                "PreferredLanguage" : Helper.shared.getPreferredLanguage(PreferredLanguage),
                "Language" : "English",
                "Pincode" : 0
                ] as Dictionary<String, Any>
            
            return Input_UserProfile
        }
        
        return Dictionary<String, Any>()
    }
    
    // MARK: - Image Upload
    
    func saveImageDetailsAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        var userId = user.id
        
        if self.initialUserProfile {
            userId = Session.shared.getUserMobileAndUserId().1
        }
        
        let params = ["UserId":userId]
        
        HttpManager.shared.uploadImageWithParametersAnd(endUrl: PATH_AddUpdateUserAccount, photo: [self.selectedProfileImage!], parameters: params, headers: nil, jsonName: "UserData", controller: self) { (response, InvoiceResponse, responseValue) in
            
            if InvoiceResponse?.Success == 0 {
                AlertHelper.shared.showAlert(message: InvoiceResponse?.Message ?? "Image Uploaded Successfully", controller: self)
            } else {
                AlertHelper.shared.showAlert(title: APP_NAME, message: InvoiceResponse?.Message ?? "Something went wrong! Try again.", controller: self)
            }
        }
    }
}
