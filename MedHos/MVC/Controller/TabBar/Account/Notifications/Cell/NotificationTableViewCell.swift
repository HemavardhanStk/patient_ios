//
//  NotificationTableViewCell.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 28/02/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var notificationDateTime: UILabel!
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var rightArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
