//
//  NotificationHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension NotificationViewController {
    
    // MARK: - Notification Cell
    
    func getNotificationCell(_ indexPath: IndexPath) -> NotificationTableViewCell {
        
        let cell: NotificationTableViewCell = Bundle.main.loadNibNamed("NotificationTableViewCell", owner: nil, options: nil)![0] as! NotificationTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.notificationView.backgroundColor = UIColor.white
        cell.notificationView.setRoundedCorner()
        cell.notificationTitle.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.notificationMessage.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.notificationDateTime.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.notificationTitle.textColor = UIColor.black
        cell.notificationMessage.textColor = UIColor.darkGray
        cell.notificationDateTime.textColor = UIColor.lightGray
        
        let item = self.infoArray[indexPath.row]
        if let value = item["NotificationDate"] as? String {
            cell.notificationDateTime.text = value
        }
        
        if let value = item["TitleName"] as? String {
            cell.notificationTitle.text = value
        }
        
        if let value = item["NotificationMessage"] as? String {
            cell.notificationMessage.text = value
        }
        
        cell.contentImageView.layer.masksToBounds = true
        cell.contentImageView.layer.cornerRadius = cell.contentImageView.frame.size.height / 2
        
        if let imageUrl = item["ImageURL"] as? String,
            imageUrl.count != 0 {
            cell.contentImageView.loadImageFromUrl(imageUrl, contentMode: .scaleAspectFill, imageTransition: UIImageView.ImageTransition.flipFromLeft(1.0))
        } else {
            cell.contentImageView.image = UIImage.init(named: "AppIcon")
        }
        
        if let value = item["RedirectType"] as? String,
            value.count != 0 {
            cell.rightArrow.isHidden = false
            cell.rightArrow.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
            
            //For Image Type
            // load image in background to show quick in fullscreen
            if value.caseInsensitiveCompare("Image") == .orderedSame {
                if let valueURL = item["RedirectValue"] as? String{
                    let imgVw = UIImageView()
                    imgVw.af_setImage(withURL: URL(string: valueURL)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: true, completion: { (data) in
                    })
                }
            }
        }
        return cell
    }
    
    func notificationCellSelected(_ indexPath: IndexPath) -> Void {
        let item = self.infoArray[indexPath.row]
        if let value = item["RedirectType"] as? String,
            value.count != 0 {
            if value.caseInsensitiveCompare("URL") == .orderedSame {
                var valueURL = item["RedirectURL"] as? String
                valueURL = "https://" + valueURL!
                if let requestUrl = NSURL(string: valueURL!) {
                    UIApplication.shared.open(requestUrl as URL, options: [:]) { (success) in
                    }
                }
            }
            else if value.caseInsensitiveCompare("Video") == .orderedSame {
                if let valueURL = item["RedirectURL"] as? String{
                    let videoID: String = extractYoutubeID(valueURL)!
                    let linkToApp = URL(string: "youtube://watch?v=\(videoID)")
                    let linkToWeb = URL(string: "https://www.youtube.com/watch?v=\(videoID)")
                    if let anApp = linkToApp {
                        if UIApplication.shared.canOpenURL(anApp) {
                            // Can open the youtube app URL so launch the youTube app with this URL
                            if let anApp = linkToApp {
                                UIApplication.shared.open(anApp)
                            }
                        } else {
                            // Can't open the youtube app URL so launch Safari instead
                            if let aWeb = linkToWeb {
                                UIApplication.shared.open(aWeb)
                            }
                        }
                    }
                }
            }
            else{
                self.imageTapped(indexPath)
            }
        }
    }
    
    @objc func imageTapped(_ indexPath: IndexPath) {
        let item = self.infoArray[indexPath.row]
        let newImageView = UIImageView()
        if let valueURL = item["RedirectValue"] as? String{
            newImageView.af_setImage(withURL: URL(string: valueURL)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: true, completion: { (data) in
                
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
                self.navigationController?.isNavigationBarHidden = true
                self.tabBarController?.tabBar.isHidden = true
            })
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    
    func extractYoutubeID(_ youtubeURL: String?) -> String? {
        let regexString: String = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        let regex = try? NSRegularExpression(pattern: regexString, options: .caseInsensitive)
        let rangeOfFirstMatch = regex?.rangeOfFirstMatch(in: youtubeURL ?? "", options: [], range: NSRange(location: 0, length: youtubeURL?.count ?? 0))
        if !NSEqualRanges(rangeOfFirstMatch!, NSRange(location: NSNotFound, length: 0)) {
            var substringForFirstMatch: String? = nil
            if let aMatch = rangeOfFirstMatch {
                substringForFirstMatch = (youtubeURL as NSString?)?.substring(with: aMatch)
            }
            return substringForFirstMatch
        }
        return nil
    }
    
}
