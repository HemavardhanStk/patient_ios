//
//  NotificationViewController.swift
//  tests
//
//  Created by Hemavardhan on 12/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    
    var dataDict = [String:Any]()
    var NotificationImagesDict = [Int:Data]()
    var infoArray = [Dictionary<String, Any>()]
    var titleName = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            NotificationCenter.default.post(name: Notification.Name.Task.AccountPageScrollToTop, object: self, userInfo: nil)
        }
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.titleName.count == 0 ? TITLE_NOTIFICATION : self.titleName, controller: self)
        
        self.loadInfoData()
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
        
    }
    
    func loadInfoData() {
        
        self.getNotificationsAPICall()
    }
    
}
