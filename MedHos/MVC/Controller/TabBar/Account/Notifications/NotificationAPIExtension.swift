//
//  NotificationAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension NotificationViewController {
    
    func getNotificationsAPICall()
    {
        
        let user = Session.shared.getUserInfo()
        
        let params = [ "ReferenceID": user.id,
                        "CategoryID": "0",
                        "ProfileID": "1",
                        "Language": "English"]
        
        self.infoArray = []
        
        HttpManager.shared.loadAPICall(path: PATH_NotificationCheck, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["NotifyList"] as? [Dictionary<String, Any>] {
                    self.infoArray = value
                    self.tableView.reloadData()
                }
            }
        }) {}
        
        
    }
}
