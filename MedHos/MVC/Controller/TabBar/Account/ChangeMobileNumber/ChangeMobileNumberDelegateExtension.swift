//
//  ChangeMobileNumberDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

// MARK: - TableView Delegate

extension ChangeMobileNumberViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.infoArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == self.infoArray.count {
            return 80
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.infoArray.count {
            return self.getButtonCell()
        } else {
            return self.getTextFieldCell(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TextField Delegate

extension ChangeMobileNumberViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.infoArray[textField.tag - 1][TEXT] = textField.text
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let newLength = text.count + string.count - range.length
            return newLength <= MOBILE_LENGTH
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextResponder = self.tableView.viewWithTag(textField.tag + 1)
        if nextResponder is UITextField {
            nextResponder?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}


