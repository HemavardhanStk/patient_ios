//
//  ChangeMobileNumberHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ChangeMobileNumberViewController {
    
    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        if let imageName = self.infoArray[indexPath.row][IMAGE], imageName.count != 0 {
            cell.defaultTextIcon.image = UIImage.init(named: imageName)
        } else {
            cell.defaultTextIconWidthConstraint.constant = 0
        }
        cell.defaultTextField.placeholder = self.infoArray[indexPath.row][PLACEHOLDER]
        cell.defaultTextField.text = self.infoArray[indexPath.row][TEXT]
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row + 1
        cell.defaultTextField.keyboardType = .numberPad
        
        return cell
    }
    
    // MARK: - Button Cell
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("SUBMIT")
        cell.defaultButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        return cell
    }
}


