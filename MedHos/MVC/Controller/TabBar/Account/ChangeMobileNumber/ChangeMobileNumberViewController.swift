//
//  ChangeMobileNumberViewController.swift
//  tests
//
//  Created by Hemavardhan on 02/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
var ChangedNumber = String()
class ChangeMobileNumberViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        Navigation.shared.setTitle(title: TITLE_CHANGE_MOBILE, controller: self)
    }
    
    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        self.setupColors()
        self.loadInfoData()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        
    }
    
    func loadInfoData() {
        
        var dict = [IMAGE: "ic_mobile", PLACEHOLDER : "Current Mobile Number", TEXT : ""]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "ic_mobile", PLACEHOLDER : "New Mobile Number", TEXT : ""]
        self.infoArray.append(dict)
    }
    
    // MARK: - Handlers
    
    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            self.changeMobileAPICall()
        }
    }
    
}
