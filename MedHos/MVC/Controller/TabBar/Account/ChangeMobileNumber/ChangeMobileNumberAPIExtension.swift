//
//  ChangeMobileNumberAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ChangeMobileNumberViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        var message = STR_PLEASE_ENTER
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: infoArray, controller: self)
        
        if isValidationSuccess {
            
            if !ValidationHelper.shared.isValidMobileNumber(self.infoArray[0][TEXT]!) {
                isValidationSuccess = false
                message = message + STR_VALID + self.infoArray[0][PLACEHOLDER]!
            } else if !ValidationHelper.shared.isValidMobileNumber(self.infoArray[1][TEXT]!) {
                isValidationSuccess = false
                message = message + STR_VALID + self.infoArray[1][PLACEHOLDER]!
            }
            
            if !isValidationSuccess {
                AlertHelper.shared.showAlert(message: message, controller: self)
            }
        }
        
        return isValidationSuccess
    }
    
    func changeMobileAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["Refno" : user.id,
                      "OldMobileNo" : self.infoArray[0][TEXT]!,
                      "NewMobileNo" : self.infoArray[1][TEXT]!] as Dictionary<String, String>
        
        HttpManager.shared.loadAPICall(path: PATH_ChangeMobileNo, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                
                let viewController = AppController.shared.getOTPVerificationViewController()
                viewController.refId = user.id
                viewController.mobileNumber = self.infoArray[1][TEXT]!
                viewController.otpType = OTP_TYPE.CHANGE_MOBILE.rawValue
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }) {}
    }
    
}
