//
//  UserLocationHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 02/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

extension UserLocationViewController {
    
    func getMapViewCell() -> MapViewTableViewCell {
        
        let cell: MapViewTableViewCell = Bundle.main.loadNibNamed("MapViewTableViewCell", owner: nil, options: nil)![0] as! MapViewTableViewCell
        
        cell.mapView.delegate = self
        cell.mapView.tag = 35
        cell.mapView.isMyLocationEnabled = true
        cell.mapView.settings.myLocationButton = true
        
        if let value1 = self.infoArray[0][self.LATITUDE] ,
            let value2 = self.infoArray[0][self.LONGITUDE] ,
            let latitude = Double(value1),
            let longitude = Double(value2) {
            
            let currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
            
            cell.mapView.camera = GMSCameraPosition(target: currentLocation, zoom: 17, bearing: 0, viewingAngle: 0)
        }
        
        return cell
    }
    
    func getAddressCell() -> AddressTableViewCell {
        
        let cell: AddressTableViewCell = Bundle.main.loadNibNamed("AddressTableViewCell", owner: nil, options: nil)![0] as! AddressTableViewCell
        
        cell.locationTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.addressTextLabel.font   = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.locationTitleLabel.textColor = UIColor.black
        cell.addressTextLabel.textColor   = UIColor.black
        
        cell.addressTextLabel.numberOfLines = 0
        cell.searchImageView.image = #imageLiteral(resourceName: "search-icon")
        
        cell.searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        
        cell.locationTitleLabel.text = self.infoArray[1][PLACEHOLDER]
        cell.addressTextLabel.text = self.infoArray[1][TEXT]
        
        return cell
    }
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        cell.defaultTextIconWidthConstraint.constant = 0
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row
        cell.defaultTextField.placeholder = self.infoArray[indexPath.row][PLACEHOLDER]
        cell.defaultTextField.text = self.infoArray[indexPath.row][TEXT]
        
        return cell
    }
    
    @objc func searchButtonTapped() {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.country = "IN"
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
        
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            var addressStrings = lines[0].components(separatedBy: ",")
            addressStrings.reverse()
            
            self.infoArray[1][TEXT] = lines.joined()
            
            self.infoArray[4][self.AREA] = addressStrings[3]
            self.infoArray[4][self.CITY] = addressStrings[2]
            self.infoArray[4][self.PINCODE] = address.postalCode
            self.infoArray[4][self.STATE] = address.administrativeArea
            self.infoArray[4][self.COUNTRY] = address.country
            
            self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
            
        }
    }
    
}
