//
//  UserLocationViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 02/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import CoreLocation

class UserLocationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    
    var infoArray = [Dictionary<String, String>]()
    
    let locationManager = CLLocationManager()
    var isInitialLocationPicking = false
    
    let LATITUDE = "lat"
    let LONGITUDE = "long"
    
    let AREA = "area"
    let CITY = "city"
    let PINCODE = "pincode"
    let STATE = "state"
    let COUNTRY = "country"
    
    // we are unable to track the parent controller name , so we are using below var to store parent class name
//    var parentName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "Location", controller: self)
        
        self.setUpColors()
        self.loadInfoData()
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.submitButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.alwaysBounceVertical = false
    }
    
    func setUpColors() {
        self.tableView.backgroundColor = UIColor.white
    }
    
    func loadInfoData() {
        
        let defaultAddress = Session.shared.getUserDefaultAddress()
        
        if defaultAddress.latitude.count == 0 && defaultAddress.longitude.count == 0 {
            self.isInitialLocationPicking = true
        }
        
        var dict = [LATITUDE: defaultAddress.latitude, LONGITUDE: defaultAddress.longitude]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER: "Location", TEXT: ""]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER: "House No/Flat No.", TEXT: defaultAddress.buildingNo]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER: "Street name / LandMark", TEXT: defaultAddress.streetName]
        self.infoArray.append(dict)
        
        // store area, city, state and country in the below position
        
        dict = [AREA: "", CITY: "", PINCODE: "", STATE: "", COUNTRY: ""]
        self.infoArray.append(dict)
        
    }
    
    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            self.saveUserLocation()
        }
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        let navigationStackCount: Int! = self.navigationController?.viewControllers.count
        guard let viewController = self.navigationController?.viewControllers[navigationStackCount-2] else { return }
        
        if (viewController as? DoctorCategoriesViewController) != nil {//self.parentName == "DoctorCategories" {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadDoctorCategories, object: self, userInfo: nil)
        } else if (viewController as? DoctorBasicSearchViewController) != nil {//self.parentName == "DoctorBasicSearch" {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadDoctorBasicSearch, object: self, userInfo: nil)
        } else if (viewController as? LabBasicSearchViewController) != nil {//self.parentName == "LabBasicSearch" {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadLabBasicSearch, object: self, userInfo: nil)
        } else if (viewController as? LabTestPreBookingViewController) != nil {//self.parentName == "LabPreBooking" {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadLabPreBooking, object: self, userInfo: nil)
        } else if (viewController as? PatientProfileViewController) != nil {//self.parentName == "PatientProfile" {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadPatientProfile, object: self, userInfo: nil)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
