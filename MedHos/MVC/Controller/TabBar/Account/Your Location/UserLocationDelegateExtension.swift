//
//  UserLocationDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 02/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

extension UserLocationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        
        case 0:
            return 250
            
        case 2,3:
            return 60
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return self.getMapViewCell()
            
        case 1:
            return self.getAddressCell()
            
        case 2,3:
            return self.getTextFieldCell(indexPath)
            
        default:
            return UITableViewCell()
        }
    }
    
}

extension UserLocationViewController: UITextFieldDelegate {
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.infoArray[textField.tag][TEXT] = textField.text
    }
    
}

extension UserLocationViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        let dict = [self.LATITUDE: "\(position.target.latitude)", self.LONGITUDE: "\(position.target.longitude)"]
        self.infoArray[0] = dict

        self.reverseGeocodeCoordinate(position.target)
    }
    
}

extension UserLocationViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.isInitialLocationPicking {
            let userLocation :CLLocation = locations[0] as CLLocation
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
                if (error != nil){
                    print("error in reverseGeocode")
                }
                
                let dict = [self.LATITUDE: "\(userLocation.coordinate.latitude)", self.LONGITUDE: "\(userLocation.coordinate.longitude)"]
                self.infoArray[0] = dict
                
                if let mapView = self.tableView.viewWithTag(35) as? GMSMapView {
                    mapView.camera = GMSCameraPosition(target: userLocation.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
                }
                
                self.reverseGeocodeCoordinate(userLocation.coordinate)
            }
            self.isInitialLocationPicking = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }

}

extension UserLocationViewController : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if let mapView = self.tableView.viewWithTag(35) as? GMSMapView {
            mapView.camera = GMSCameraPosition(target: place.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
        }
        
        let dict = [self.LATITUDE: "\(place.coordinate.latitude)", self.LONGITUDE: "\(place.coordinate.longitude)"]
        self.infoArray[0] = dict
        
        self.infoArray[2][TEXT] = ""
        self.infoArray[3][TEXT] = ""
        
        self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0),IndexPath(row: 3, section: 0)], with: .automatic)
        
        self.reverseGeocodeCoordinate(place.coordinate)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {

            self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}



