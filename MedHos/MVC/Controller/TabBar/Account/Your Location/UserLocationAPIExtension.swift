//
//  UserLocationAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 02/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UserLocationViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: self.infoArray, nonMandatoryFields: [4], controller: self)
        
        return isValidationSuccess
    }
    
    func saveUserLocation() {
        
        let user = Session.shared.getUserInfo()
        
         if let lat      = self.infoArray[0][self.LATITUDE],
            let long     = self.infoArray[0][self.LONGITUDE],
            let buildNo  = self.infoArray[2][TEXT],
            let street   = self.infoArray[3][TEXT],
            let area     = self.infoArray[4][self.AREA],
            let city     = self.infoArray[4][self.CITY],
            let pincode  = self.infoArray[4][self.PINCODE],
            let state    = self.infoArray[4][self.STATE],
            let country  = self.infoArray[4][self.COUNTRY] {
            
          let params = ["Longitude"       : long,
                        "UserId"          : user.id,
                        "AreaName"        : area,
                        "AreaNo"          : "0",
                        "Language"        : "English",
                        "StateName"       : state,
                        "CityName"        : city,
                        "Latitude"        : lat,
                        "Pincode"         : pincode,
                        "BuildingName"    : buildNo,
                        "StreetName"      : street,
                        "CountryName"     : country]
                
            HttpManager.shared.loadAPICall(path: PATH_UserDefaultLocationUpdate, params: params, httpMethod: .post, controller: self, completion: { (response) in
                if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                    
                    let defaultAddress = DefaultAddress(latitude    : lat,
                                                        longitude   : long,
                                                        buildingNo  : buildNo,
                                                        streetName  : street,
                                                        areaName    : area,
                                                        cityName    : city,
                                                        pincode     : pincode,
                                                        stateName   : state,
                                                        countryName : country)
                    
                    let defaultAddressString = defaultAddress.buildingNo + "," + defaultAddress.streetName + "," + defaultAddress.areaName + "," + defaultAddress.cityName + "-" + defaultAddress.pincode + "," + defaultAddress.stateName + "," + defaultAddress.countryName
                    
                    Session.shared.setUserDefaultAddress(defaultAddress)
                    Session.shared.setUserDefaultAddressString(defaultAddressString)
                    
                    if let value = response["Result"] as? Dictionary<String, Any>,
                        let message = value["Message"] as? String {
                        
                        AlertHelper.shared.showAlertWithHandler(message: message, handler: "popViewController", withCancel: false, controller: self)
                    }
                }
            }) {}
        }
    }
    
}
