//
//  ContactUsAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ContactUsViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        var message = STR_PLEASE_ENTER
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: infoArray, controller: self)
        
        if isValidationSuccess {
            
            if !ValidationHelper.shared.isValidMobileNumber(self.infoArray[1][TEXT]!) {
                isValidationSuccess = false
                message = message + STR_VALID + self.infoArray[1][PLACEHOLDER]!
            } else if !ValidationHelper.shared.isValidEmailID(self.infoArray[2][TEXT]!) {
                isValidationSuccess = false
                message = message + STR_VALID + self.infoArray[2][PLACEHOLDER]!
            }
            
            if !isValidationSuccess {
                AlertHelper.shared.showAlert(message: message, controller: self)
            }
        }
        
        return isValidationSuccess
    }
    
    func contactEmailAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["ReferenceID" : user.id,
                      "Name" : self.infoArray[0][TEXT]!,
                      "MobileNumber" : self.infoArray[1][TEXT]!,
                      "emailid" : self.infoArray[2][TEXT]!,
                      "SupportInterest" : self.infoArray[3][TEXT]!,
                      "Message" : self.infoArray[4][TEXT]!,
                      "CategoryNo" : 0,
                      "Language":"English"
            ] as [String : Any]
        
        
        var image = [UIImage]()
        
        if let value = self.imagePicked {
            image.append(value)
        }
        
        HttpManager.shared.uploadImageWithParametersAnd(endUrl: PATH_UserAddSupport, photo: image, parameters: params, headers: nil, jsonName: "UserSupportData", controller: self) { (response, InvoiceResponse, responseValue) in
            
            if InvoiceResponse?.Success == 0 {
                AlertHelper.shared.showAlertWithHandler(message: InvoiceResponse?.Message ?? "Mail sent Successfully", handler: "performSegueToReturnBack", withCancel: false, controller: self)
            } else {
                AlertHelper.shared.showAlert(title: APP_NAME, message: InvoiceResponse?.Message ?? "SomeThing went wrong!. Try Again", controller: self)
            }
        }
    }
    
    @objc func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
