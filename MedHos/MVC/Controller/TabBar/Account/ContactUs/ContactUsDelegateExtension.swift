//
//  ContactUsDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit


// MARK: - TableView Delegate

extension ContactUsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.infoArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == self.infoArray.count - 1 || indexPath.row == self.infoArray.count - 2{
            return 120
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.infoArray.count {
            return self.getButtonCell()
        } else if indexPath.row == self.infoArray.count - 2 {
            return self.getMessageCell(indexPath)
        } else if indexPath.row == self.infoArray.count - 1 {
            return self.getImageButtonCell()
        } else {
            return self.getTextFieldCell(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TextField Delegate

extension ContactUsViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.infoArray[textField.tag - 1][TEXT] = textField.text
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let newLength = text.count + string.count - range.length
            if textField.tag == 2 {
                return newLength <= 10
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextResponder = self.tableView.viewWithTag(textField.tag + 1)
        if nextResponder is UITextField {
            nextResponder?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}

// MARK: - TextView Delegate

extension ContactUsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            self.setupTextViewText("", textView: textView)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.infoArray[textView.tag - 1][PLACEHOLDER]
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text != self.infoArray[textView.tag - 1][PLACEHOLDER] {
            self.infoArray[textView.tag - 1][TEXT] = textView.text
        }
    }
}

// MARK: - UIImagePickerController Delegate

extension ContactUsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imager = info[.originalImage] as? UIImage
        {
            self.addImage(imager)
            self.tableView.reloadData()
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}
