//
//  ContactUsHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ContactUsViewController {
    
    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        if let imageName = self.infoArray[indexPath.row][IMAGE], imageName.count != 0 {
            cell.defaultTextIcon.image = UIImage.init(named: imageName)
        } else {
            cell.defaultTextIconWidthConstraint.constant = 0
        }
        cell.defaultTextField.placeholder = self.infoArray[indexPath.row][PLACEHOLDER]
        cell.defaultTextField.text = self.infoArray[indexPath.row][TEXT]
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row + 1
        cell.defaultTextField.returnKeyType = .next
        
        switch indexPath.row {
        case 1:
            cell.defaultTextField.keyboardType = .numberPad
            break
        case 2:
            cell.defaultTextField.keyboardType = .emailAddress
            break
            
        default: break
        }
        
        return cell
    }
    
    // MARK: - TextView Cell
    
    func getMessageCell(_ indexPath: IndexPath) -> TextViewTableViewCell {
        
        let cell: TextViewTableViewCell = Bundle.main.loadNibNamed("TextViewTableViewCell", owner: nil, options: nil)![0] as! TextViewTableViewCell
        
        self.setupTextViewPlaceholder(cell.textView)
        cell.textView.delegate = self
        cell.textView.tag = indexPath.row + 1
        cell.textView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        cell.textView.setRoundedCorner()
        
        if self.infoArray[cell.textView.tag - 1][TEXT] == "" {
            cell.textView.text = self.infoArray[cell.textView.tag - 1][PLACEHOLDER]
            cell.textView.textColor = UIColor.lightGray
        }
        else {
            cell.textView.text = self.infoArray[cell.textView.tag - 1][TEXT]
            cell.textView.textColor = UIColor.black
        }
        
        return cell
    }
    
    // MARK: - Button Cell
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("SUBMIT")
        cell.defaultButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        return cell
    }
    
    func getImageButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![1] as! ButtonTableViewCell
        cell.imageButton.addTarget(self, action: #selector(imageSelectionButtonTapped), for: .touchUpInside)
        
        if (self.imagePicked != nil) {
            cell.imageButton.setImage(self.imagePicked, for: .normal)
        }
        else
        {
            cell.imageButton.setImage(#imageLiteral(resourceName: "ic_add_image"), for: .normal)
        }
        
        return cell
    }
}

extension ContactUsViewController {
    // MARK: - Image Selection
    
    @objc func imageSelectionButtonTapped() {
        
        let alert = UIAlertController(title: APP_NAME, message: "Choose Image from...", preferredStyle: .actionSheet);
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in
            
            self.pickImageFromCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Gallery", style: .default, handler: {(action:UIAlertAction) in
            
            self.pickImageFromPhotos()
        }))
        
        if self.imagePicked != nil {
            alert.addAction(UIAlertAction(title: "Remove Photo", style: .default, handler: {(action:UIAlertAction) in
                
                self.imagePicked = nil
                self.tableView.reloadData()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func pickImageFromCamera() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .camera
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    func pickImageFromPhotos() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .photoLibrary
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
}
