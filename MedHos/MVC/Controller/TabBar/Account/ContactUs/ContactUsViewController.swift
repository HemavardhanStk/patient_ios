//
//  ContactUssViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 05/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, String>]()
    let messageIndex: Int = 4
    var titleName = ""
    var imagePicked: UIImage? = nil
    
    
    var imagePickerController: UIImagePickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            NotificationCenter.default.post(name: Notification.Name.Task.AccountPageScrollToTop, object: self, userInfo: nil)
        }
    }

    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: TITLE_CONTACT_US, controller: self)
        self.setupColors()
        self.loadInfoData()
        
        self.imagePickerController = UIImagePickerController.init()
        self.imagePickerController?.delegate = self
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        self.tableView.tableFooterView = UIView.init()
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        
        self.tableView.setRoundedCorner()
    }
    
    func setupTextViewPlaceholder(_ textView: UITextView) {
        
        textView.text = self.infoArray[self.messageIndex][PLACEHOLDER] as! String
        textView.textColor = UIColor.lightGray
    }
    
    func setupTextViewText(_ text: String, textView: UITextView) {
        
        textView.text = text
        textView.textColor = UIColor.black
    }
    
    func loadInfoData() {
        
        let user = Session.shared.getUserInfo()
        
        var dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : user.name]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Mobile Number", TEXT : user.phone]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Email Id", TEXT : user.email]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Title", TEXT : self.titleName]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Message", TEXT : ""]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "", TEXT : "ImageUpload"]
        self.infoArray.append(dict)
    }
    
    func addImage(_ image: UIImage) {
        self.imagePicked = image
        self.tableView.reloadData()
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            self.contactEmailAPICall()
        }
    }
    
}
