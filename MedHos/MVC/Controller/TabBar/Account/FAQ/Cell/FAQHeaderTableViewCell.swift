//
//  FAQHeaderTableViewCell.swift
//  tests
//
//  Created by Hemavardhan on 02/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class FAQHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var QuestionLabel: UILabel!
    @IBOutlet weak var StateSpecifierImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
