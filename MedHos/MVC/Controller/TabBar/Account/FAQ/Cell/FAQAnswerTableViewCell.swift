//
//  FAQAnswerTableViewCell.swift
//  tests
//
//  Created by Hemavardhan on 02/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class FAQAnswerTableViewCell: UITableViewCell {

    @IBOutlet weak var AnswerLabel: UILabel!
    @IBOutlet weak var LabelLeadingConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
