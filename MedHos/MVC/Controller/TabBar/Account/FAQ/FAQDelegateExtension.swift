//
//  FAQDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

// MARK: - TableView Delegate

extension FAQViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return self.getFAQQuestionCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        var item = self.infoArray[indexPath.section]
        if item[STATUS] == OPEN {
            item[STATUS] = CLOSE
        } else {
            item[STATUS] = OPEN
        }
        self.infoArray[indexPath.section] = item
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: UITableView.RowAnimation.automatic)
    }
}
