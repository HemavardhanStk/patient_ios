//
//  FAQHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension FAQViewController {
    
    // MARK: - FAQ Cell
    
    func getFAQQuestionCell(_ indexPath: IndexPath) -> FAQTableViewCell {
        
        let cell: FAQTableViewCell = Bundle.main.loadNibNamed("FAQTableViewCell", owner: nil, options: nil)![0] as! FAQTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.faqView.backgroundColor = UIColor.white
        cell.answerView.backgroundColor = UIColor.clear
        cell.faqView.setRoundedCorner()
        
        cell.questionLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.answerLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.answerLabel.textColor = UIColor.darkGray
        
        let item = self.infoArray[indexPath.section]
        cell.questionLabel.text = item[KEY_QUESTION]
        
        if item[STATUS] == OPEN {
            cell.questionView.backgroundColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
            cell.questionLabel.textColor = UIColor.white
            cell.answerLabel.text = item[KEY_ANSWER]
            cell.StatusImageView.image = UIImage.init(named: "ic_minus_white")
        } else {
            cell.questionView.backgroundColor = UIColor.clear
            cell.questionLabel.textColor = UIColor.black
            cell.answerLabel.text = ""
            cell.StatusImageView.image = UIImage.init(named: "ic_plus_blue")
        }
        
        return cell
    }
}
