//
//  FAQAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension FAQViewController {
    
    
    func loadFAQAPICall() {
        
        let params = ["FaqCategory" : "0",
                      "Language" : "English"] as Dictionary<String, String>
        
        HttpManager.shared.loadAPICall(path: PATH_GetFAQ, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let faqList = response["FaqList"] as? [Dictionary<String, AnyObject>] {
                    for i in 0..<faqList.count {
                        let item = faqList[i]
                        if let dict = [KEY_QUESTION : item[KEY_QUESTION] as? String,
                                       KEY_ANSWER : item[KEY_ANSWER] as? String,
                                       STATUS : CLOSE] as? Dictionary<String, String> {
                            self.infoArray.append(dict)
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
}
