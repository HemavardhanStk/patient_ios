//
//  LabelTabBarTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 02/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabelTabBarTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTabBarView: UIView!
    
    @IBOutlet weak var labelTabBarBgImageView: UIImageView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var itemImageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemImageViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemBadgeLabel: UILabel!
    
    @IBOutlet weak var bottomLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
