//
//  ExploreMedHosTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 02/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ExploreMedHosTableViewCell: UITableViewCell {

    @IBOutlet weak var exploreView: UIView!
    
    @IBOutlet weak var itemOneImageView: UIImageView!
    @IBOutlet weak var itemTwoImageView: UIImageView!
    
    @IBOutlet weak var itemOneNameLabel: UILabel!
    @IBOutlet weak var itemOneSubLabel: UILabel!
    @IBOutlet weak var itemOneButton: UIButton!
    
    @IBOutlet weak var itemTwoNameLabel: UILabel!
    @IBOutlet weak var itemTwoSubLabel: UILabel!
    @IBOutlet weak var itemTwoButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
