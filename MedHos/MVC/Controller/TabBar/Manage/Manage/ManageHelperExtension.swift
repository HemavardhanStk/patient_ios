//
//  ManageHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ManageViewController {
    
    func getItemTableViewCell(_ indexPath: IndexPath) -> LabelTabBarTableViewCell {
        
        let cell: LabelTabBarTableViewCell = Bundle.main.loadNibNamed("LabelTabBarTableViewCell", owner: nil, options: nil)![0] as! LabelTabBarTableViewCell
        
        cell.selectionStyle = .none
        
        cell.itemNameLabel.textColor = .black
        cell.itemBadgeLabel.textColor = UIColor.white
        
        cell.itemNameLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 15)
        cell.itemBadgeLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.itemImageView.contentMode = .scaleAspectFit
        
        if let array = self.infoArray[indexPath.section][self.SECTION] as? [Dictionary<String, String>] {
            
            let item = array[indexPath.row]
            
            cell.itemNameLabel.text = item[TITLE]
            cell.itemImageView.image = UIImage(named: item[IMAGE]!)
            cell.arrowImageView.image = UIImage(named: "ic_right_arrow_blue")
            cell.bottomLineView.alpha = indexPath.row == array.count - 1 ? 0 : 1
            
            if indexPath.row == 0 {
                cell.backgroundColor = .clear
                cell.labelTabBarView.backgroundColor = .clear
                cell.labelTabBarBgImageView.alpha = 1
                cell.labelTabBarBgImageView.image = UIImage(named: "ic_view_bg")
            } else if indexPath.row == array.count - 1 {
                cell.backgroundColor = .clear
                cell.labelTabBarView.backgroundColor = .clear
                cell.labelTabBarBgImageView.alpha = 1
                cell.labelTabBarBgImageView.image = UIImage(cgImage: UIImage(named: "ic_view_bg")!.cgImage!, scale: 1.0, orientation: .downMirrored)
            }
            
            cell.itemBadgeLabel.text = ""
            
            if indexPath == [0,0] {
                let upcomingCount = Session.shared.getUpcomingCount()
                if upcomingCount.count != 0 && upcomingCount != "0" {
                    cell.itemBadgeLabel.text = "\(upcomingCount)"
                    cell.itemBadgeLabel.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
                    cell.itemBadgeLabel.setRoundedCorner(11)
                }
            } else if indexPath == [0,2] {
                let dueCount = Session.shared.getDueCount()
                if dueCount.count != 0 && dueCount != "0" {
                    cell.itemBadgeLabel.text = "\(dueCount)"
                    cell.itemBadgeLabel.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
                    cell.itemBadgeLabel.setRoundedCorner(11)
                }
            }
        }
        
        return cell
    }
    
    func navigate(_ indexPath: IndexPath) {
        
        switch indexPath.section {
            
    
        case 0:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getUpcomingAppointmentViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let viewController = AppController.shared.getPastAppointmentViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 2:
                let viewController = AppController.shared.getDueAppointmentViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 3:
                let viewController = AppController.shared.getPendingFeedBackViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            default: break
            }
            break

        case 1:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getBookedTestListViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let viewController = AppController.shared.getCompletedTestListViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 2:
                let viewController = AppController.shared.getCancelledTestListViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            default: break
            }
            break
            
        case 2:
            
            switch indexPath.row {
            case 0:
                let viewController = AppController.shared.getFamilyMembersViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 1:
                let viewController = AppController.shared.getHealthRecordViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            case 2:
                let viewController = AppController.shared.getHealthReminderViewController()
                self.navigationController?.pushViewController(viewController, animated: true)
                break
                
            default: break
            }
            break

        default: break
        }
    }

    
}
