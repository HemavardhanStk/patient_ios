//
//  ManageViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 16/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ManageViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userImageButton: UIButton!
    @IBOutlet weak var findDoctorButton: UIButton!
    @IBOutlet weak var qrScannerButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, Any>]()
    let SECTION = "Section"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
    }
    
    override func viewDidAppear(_ animated: Bool) {

        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 2, section: 0)], with: UITableView.RowAnimation.automatic)
    }
    
    func classAndWidgetsInitialise() {
        
        self.setupColors()
        self.setTopView()
        self.loadInfoData()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTabBarTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTabBarTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: -10, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }

    func setTopView() {
        
        let user = Session.shared.getUserInfo()
        
        if let url = URL(string: user.image) {
            self.userImageButton.af_setImage(for: .normal, url: url, placeholderImage: #imageLiteral(resourceName: "login"))
        } else {
            self.userImageButton.setImage(#imageLiteral(resourceName: "login"), for: .normal)
        }
        
        self.userImageButton.layer.borderWidth = 2
        self.userImageButton.layer.borderColor = UIColor.white.cgColor
        
        self.userImageButton.setRoundedCorner(20)
        self.findDoctorButton.setRoundedCorner(2)
        self.qrScannerButton.imageView?.setImageColor(color: UIColor.black)
        
        self.userImageButton.addTarget(self, action: #selector(gotoProfile), for: .touchUpInside)
        self.findDoctorButton.addTarget(self, action: #selector(gotoDoctors), for: .touchUpInside)
        self.qrScannerButton.addTarget(self, action: #selector(gotoQrSacanner), for: .touchUpInside)
    }
    
    func loadInfoData() {
        
        self.infoArray = []
        var subinfoArray = [Dictionary<String, String>]()
        
        var dict = [IMAGE: "ic_upcoming_icon", TITLE: "Upcoming Appointment"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_pastappointment_icon", TITLE : "Past Appointments"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_due_icon", TITLE : "Due Appointments"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_feedback_icon", TITLE : "My Feedback"]
        subinfoArray.append(dict)

        var subDict = [self.SECTION: subinfoArray, TITLE : "Appointments"] as [String : Any]
        self.infoArray.append(subDict)
        
        subinfoArray = []
        
        dict = [IMAGE: "ic_healthrecord_icon", TITLE : "Booked Tests"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_reminder_icon", TITLE : "Completed Tests"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_reminder_icon", TITLE : "Cancel Tests"]
        subinfoArray.append(dict)

        subDict = [self.SECTION: subinfoArray, TITLE : "Diagnostic Lab Tests"] as [String : Any]
        self.infoArray.append(subDict)
        
        subinfoArray = []
        dict = [IMAGE: "ic_family_icon", TITLE : "Family Members"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_healthrecord_icon", TITLE : "Health Records"]
        subinfoArray.append(dict)
        
        dict = [IMAGE: "ic_reminder_icon", TITLE : "Health Reminders"]
        subinfoArray.append(dict)

        subDict = [self.SECTION: subinfoArray, TITLE : "Manage"] as [String : Any]
        self.infoArray.append(subDict)
       
        self.tableView.reloadData()
    }
    
    @objc func gotoDoctors() {
        let viewController = AppController.shared.getDoctorsViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoQrSacanner() {
        let viewController = AppController.shared.getQRScannerViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoProfile() {
        let viewController = AppController.shared.getPatientProfileViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
