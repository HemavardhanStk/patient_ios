//
//  FamilyMembersAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension FamilyMembersViewController {
    
    func getFamilyMembersAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = [ "UserId": user.id,
                        "MobileNumber": user.phone,
                        "Language": "English"] as Dictionary<String, String>
        
        HttpManager.shared.loadAPICall(path: PATH_ListUserFamily, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["ListFamily"] as? [Dictionary<String, Any>] {
                    self.infoArray = value
                    self.infoArrayBackup = value
                    self.tableView.reloadData()
                }
            }
        }) {}
        
        
    }
    
}
