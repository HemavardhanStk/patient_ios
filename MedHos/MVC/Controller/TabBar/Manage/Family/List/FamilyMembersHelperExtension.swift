//
//  FamilyMembersHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension FamilyMembersViewController {
    
    // MARK: - Feedback Cell
    
    func getFamilyListCell(_ indexPath: IndexPath) -> FamilyListTableViewCell {
        
        let cell: FamilyListTableViewCell = Bundle.main.loadNibNamed("FamilyListTableViewCell", owner: nil, options: nil)![0] as! FamilyListTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.familyView.backgroundColor = UIColor.white
        cell.familyView.setRoundedCorner()
        cell.FamilyMembername.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 14)
        cell.FamilyRelationship.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.FamilyMemberMobileNo.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.FamilyMembername.textColor = UIColor.black
        cell.FamilyRelationship.textColor = UIColor.darkGray
        cell.FamilyMemberMobileNo.textColor = UIColor.darkGray
        
        let item = self.infoArray[indexPath.row]
        
        if let imageUrl = item["Image"] as? String,
            imageUrl.count != 0 {
            cell.FamilyMemberImage.loadImageFromUrl(imageUrl, contentMode: .scaleAspectFill, imageTransition: UIImageView.ImageTransition.flipFromLeft(1.0))
        }
        
        cell.FamilyMembername.text = item["Name"] as? String ?? ""
        cell.FamilyRelationship.text = item["Relation"] as? String ?? ""
        cell.FamilyMemberMobileNo.text = item["PhoneNo"] as? String ?? ""
        
        return cell
    }
    
    func isFiltering() -> Bool {
        return  !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return self.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        if isFiltering() && self.infoArray.count == 0 {
            self.infoArray = self.infoArrayBackup
        }
        
        self.filteredInfoArray = self.infoArray.filter({( dict : Dictionary<String, Any> ) -> Bool in
            if self.searchBarIsEmpty() {
                return true
            } else {
                let Name: String = dict["Name"] as! String
                let Relation: String = dict["Relation"] as! String
                let PhoneNo: String = dict["PhoneNo"] as! String
                return Name.lowercased().contains(searchText.lowercased()) || Relation.lowercased().contains(searchText.lowercased()) || PhoneNo.lowercased().contains(searchText.lowercased())
            }
        })
        
        if isFiltering() {
            self.infoArray = self.filteredInfoArray
        }
        else{
            self.infoArray = self.infoArrayBackup
        }
        self.tableView.reloadData()
    }
    
}
