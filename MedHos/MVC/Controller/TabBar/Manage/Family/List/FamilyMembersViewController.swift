//
//  FamilyMembersViewController.swift
//  tests
//
//  Created by Hemavardhan on 13/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class FamilyMembersViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    var infoArray = [Dictionary<String, Any>]()
    var filteredInfoArray = [Dictionary<String, Any>]()
    var infoArrayBackup = [Dictionary<String, Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitlewithRightBarButton("Family", image: "ic_plus_white", text: "", controller: self)
        
        self.loadInfoData()
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "FamilyListTableViewCell", bundle: nil), forCellReuseIdentifier: "FamilyListTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadInfoData), name: Notification.Name.Task.ReloadFamilyList, object: nil)
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
        
        self.searchBar.setSearchBarStyle()
    }
    
    @objc func loadInfoData() {
        self.getFamilyMembersAPICall()
    }

    @objc func rightBarButtonTapped() {
        let viewController = AppController.shared.getAddFamilyMemberViewController()
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    func gotoEditFamily(_ indexPath:IndexPath) {
        
        let viewController = AppController.shared.getAddFamilyMemberViewController()
        viewController.isStepperSignup = false
        viewController.titleName = "Edit Family"
        viewController.existingInfoArray = self.infoArray[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}
