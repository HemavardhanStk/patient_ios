//
//  FamilyListTableViewCell.swift
//  tests
//
//  Created by Hemavardhan on 13/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class FamilyListTableViewCell: UITableViewCell {

    @IBOutlet weak var familyView: UIView!
    
    @IBOutlet weak var FamilyMemberImage: CustomImageView!
    @IBOutlet weak var FamilyMembername: UILabel!
    @IBOutlet weak var FamilyRelationship: UILabel!
    @IBOutlet weak var FamilyMemberMobileNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FamilyMemberImage.layer.cornerRadius = 20
        FamilyMemberImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
