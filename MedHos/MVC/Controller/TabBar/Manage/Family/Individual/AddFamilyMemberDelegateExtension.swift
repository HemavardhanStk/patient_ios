//
//  AddFamilyMemberDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddFamilyMemberViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.infoArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 130
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            return self.getProfileImageCell()
        } else if indexPath.row == self.infoArray.count {
            return self.getButtonCell()
        } else {
            return self.getTextFieldCell(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension AddFamilyMemberViewController : UITextFieldDelegate
{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.infoArray[textField.tag][TEXT] = textField.text
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let newLength = text.count + string.count - range.length
            if textField.tag == self.TAG_ALTERNATIVE_MOBILE_NUMBER || textField.tag == self.TAG_MOBILE_NUMBER {
                return newLength <= MOBILE_LENGTH
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextResponder = self.tableView.viewWithTag(textField.tag + 1)
        if nextResponder is UITextField {
            nextResponder?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}

// MARK: - UIImagePickerController Delegate

extension AddFamilyMemberViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editimager = info[.editedImage] as? UIImage
        {
            self.selectedProfileImage = editimager
        }
        else if let imager = info[.originalImage] as? UIImage
        {
            self.selectedProfileImage = imager
        }
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = self.tableView.cellForRow(at: indexPath) as? ProfileImageTableViewCell {
            self.setProfileImageView(cell)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension AddFamilyMemberViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == self.TAG_GENDER {
            return GENDER_TYPE_ARRAY.count
        } else if pickerView.tag == self.TAG_MARITAL_STATUS {
            return MARITAL_STATUS_ARRAY.count
        } else if pickerView.tag == self.TAG_BLOOD_GROUP {
            return BLOOD_GROUP_ARRAY.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == self.TAG_GENDER {
            return GENDER_TYPE_ARRAY[row]
        } else if pickerView.tag == self.TAG_MARITAL_STATUS {
            return MARITAL_STATUS_ARRAY[row]
        } else if pickerView.tag == self.TAG_BLOOD_GROUP {
            return BLOOD_GROUP_ARRAY[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.updatePickerValues(senderTag: pickerView.tag, selectedRow: row)
    }
}
