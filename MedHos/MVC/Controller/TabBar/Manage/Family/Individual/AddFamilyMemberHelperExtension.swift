//
//  AddFamilyMemberHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddFamilyMemberViewController {
    
    // MARK: - Profile Image Cell
    
    func getProfileImageCell() -> ProfileImageTableViewCell {
        
        let cell: ProfileImageTableViewCell = Bundle.main.loadNibNamed("ProfileImageTableViewCell", owner: nil, options: nil)![0] as! ProfileImageTableViewCell
        cell.backgroundColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        
        cell.ProfileImageView.contentMode = .scaleAspectFill
        cell.ProfileImageView.layer.cornerRadius = cell.ProfileImageView.frame.size.height / 2
        cell.ProfileImageView.clipsToBounds = true
        self.setProfileImageView(cell)
        
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(profileImageTapped))
        cell.ProfileImageView.addGestureRecognizer(gesture)
        cell.ProfileImageView.isUserInteractionEnabled = true
        
        return cell
    }
    
    func setProfileImageView(_ cell: ProfileImageTableViewCell) {
        
        if self.selectedProfileImage == nil {
            cell.ProfileImageView.image = UIImage.init(named: "login")
        } else {
            cell.ProfileImageView.image = self.selectedProfileImage
        }
    }
    
    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        if let imageName = self.infoArray[indexPath.row][IMAGE], imageName.count != 0 {
            cell.defaultTextIcon.image = UIImage.init(named: imageName)
        } else {
            cell.defaultTextIconWidthConstraint.constant = 0
        }
        cell.defaultTextField.placeholder = self.infoArray[indexPath.row][PLACEHOLDER]
        cell.defaultTextField.text = self.infoArray[indexPath.row][TEXT]
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row
        
        switch indexPath.row {
        case self.TAG_GENDER:
            cell.defaultTextField.addPickerView(self, array: GENDER_TYPE_ARRAY)
            break
        case self.TAG_DOB :
            let datePicker = cell.defaultTextField.addDatePickerView(self)
            datePicker.maximumDate = Date()
            break
        case self.TAG_MARITAL_STATUS:
            cell.defaultTextField.addPickerView(self, array: MARITAL_STATUS_ARRAY)
            break
        case self.TAG_BLOOD_GROUP:
            cell.defaultTextField.addPickerView(self, array: BLOOD_GROUP_ARRAY)
            break
        case self.TAG_MOBILE_NUMBER:
            //cell.defaultTextField.isUserInteractionEnabled = false
            cell.defaultTextField.isEnabled = true
            break
        case self.TAG_ALTERNATIVE_MOBILE_NUMBER:
            cell.defaultTextField.keyboardType = .numberPad
        default: break
        }
        
        if indexPath.row == self.infoArray.count - 1 {
            cell.defaultTextField.returnKeyType = .done
        } else {
            cell.defaultTextField.returnKeyType = .next
        }
        
        return cell
    }
    
    // MARK: - Button Cell
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("SUBMIT")
        cell.defaultButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - Local Listeners
    
    @objc func profileImageTapped() {
        
        let alert = UIAlertController(title: APP_NAME, message: "Choose Image from...", preferredStyle: .actionSheet);
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in
            
            self.pickImageFromCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Gallery", style: .default, handler: {(action:UIAlertAction) in
            
            self.pickImageFromPhotos()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func pickImageFromCamera() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .camera
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    func pickImageFromPhotos() {
        
        self.imagePickerController?.allowsEditing = false
        self.imagePickerController?.sourceType = .photoLibrary
        if let imageController = self.imagePickerController {
            self.present(imageController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Picker Listeners
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            textField.text = Helper.shared.getString(sender.date, format: DOB_DATE_FORMAT)
            self.infoArray[textField.tag][TEXT] = textField.text
        }
    }
    
    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            if textField.text?.count == 0 {
                self.updatePickerValues(senderTag: sender.tag, selectedRow: 0)
            }
            textField.resignFirstResponder()
        }
    }
    
    func updatePickerValues(senderTag: Int, selectedRow: Int) {
        if let textField = self.tableView.viewWithTag(senderTag) as? UITextField {
            if textField.tag == self.TAG_DOB {
                textField.text = Helper.shared.getString(Date(), format: DOB_DATE_FORMAT)
            } else if textField.tag == self.TAG_GENDER {
                if GENDER_TYPE_ARRAY.count != 0 {
                    textField.text = GENDER_TYPE_ARRAY[selectedRow]
                }
            } else if textField.tag == self.TAG_MARITAL_STATUS {
                if MARITAL_STATUS_ARRAY.count != 0 {
                    textField.text = MARITAL_STATUS_ARRAY[selectedRow]
                }
            } else if textField.tag == self.TAG_BLOOD_GROUP {
                if BLOOD_GROUP_ARRAY.count != 0 {
                    textField.text = BLOOD_GROUP_ARRAY[selectedRow]
                }
            }
            self.infoArray[textField.tag][TEXT] = textField.text
        }
    }

    
}
