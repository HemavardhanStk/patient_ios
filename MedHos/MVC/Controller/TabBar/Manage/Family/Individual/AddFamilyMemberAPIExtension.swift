//
//  AddFamilyMemberAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension AddFamilyMemberViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        //var message = STR_PLEASE_ENTER
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: infoArray, nonMandatoryFields: [0, 2, 3, 4, 7, 8], controller: self)
        
        return isValidationSuccess
    }
    
    // MARK: - Personal Details
    
    func savePersonalDetailsAPICall() {
        
        let params = self.getInputParam()
        
        var image = [UIImage]()
        
        if let value = self.selectedProfileImage {
            image.append(value)
        }
        
        HttpManager.shared.uploadImageWithParametersAnd(endUrl: PATH_UserFamilyReg, photo: image, parameters: params, headers: nil, jsonName: "UserFamilyData", controller: self) { (response, InvoiceResponse, responseValue) in
            
            if InvoiceResponse?.Success == 0 {
                AlertHelper.shared.showAlertWithHandler(message: (InvoiceResponse?.Message)!, handler: "popViewController", withCancel: false, controller: self)
            } else {
                AlertHelper.shared.showAlert(title: APP_NAME, message: (InvoiceResponse?.Message)!, controller: self)
            }
        }
    }
    
    func getInputParam() -> Dictionary<String, Any> {
        
        let user = Session.shared.getUserInfo()
        
        if let Name = self.infoArray[1][TEXT],
            let Phone = self.infoArray[2][TEXT],
            let AlternateMobile = self.infoArray[3][TEXT],
            let Email = self.infoArray[4][TEXT],
            let Relation = self.infoArray[5][TEXT],
            let Gender = self.infoArray[6][TEXT],
            let BirthDate = self.infoArray[7][TEXT],
            let MStatus = self.infoArray[8][TEXT],
            let BloodGroup = self.infoArray[9][TEXT] {
            
            let Input_UserProfile = ["ParentUserNo" : user.id,
                "Name" : Name,
                "MobNo" : Phone,
                "Email" : Email,
                "Relation" : Relation,
                "Gender" : Helper.shared.getGenderTag(Gender),
                "AlternateMobileNo" : AlternateMobile,
                "dateofbirth" : BirthDate,
                "MartialStatus" : Helper.shared.getMartialStatus(MStatus),
                "BloodGroup" : BloodGroup,
                "Language" : "English",
                "Pincode" : "",
                "Pass":"",
                "UserNo" : "\(self.existingInfoArray["UserDetailId"] as? Int ?? 0)",
                ] as Dictionary<String, Any>
            
            return Input_UserProfile
        }
        
        return Dictionary<String, Any>()
    }
}

