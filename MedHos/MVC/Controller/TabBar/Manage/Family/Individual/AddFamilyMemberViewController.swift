//
//  AddFamilyMemberViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import AlamofireImage

class AddFamilyMemberViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var isStepperSignup = false
    var titleName = ""
    
    var datapasser = [String:Any]()
    
    var existingInfoArray = Dictionary<String, Any>()
    var infoArray = [Dictionary<String, String>]()
    
    var imagePickerController : UIImagePickerController?
    var selectedProfileImage: UIImage?
    
    let DOB_DATE_FORMAT = "dd-MMM-yyyy"
    
    let TAG_MOBILE_NUMBER = 2
    let TAG_ALTERNATIVE_MOBILE_NUMBER = 3
    let TAG_GENDER = 6
    let TAG_DOB = 7
    let TAG_MARITAL_STATUS = 8
    let TAG_BLOOD_GROUP = 9
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: self.titleName.count == 0 ? TITLE_ADDFAMILY : self.titleName, controller: self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.tableView.register(UINib(nibName: "ProfileImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileImageTableViewCell")
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        
        self.imagePickerController = UIImagePickerController.init()
        self.imagePickerController?.delegate = self
        
        self.tableView.allowsSelection = false
        self.tableView.tableFooterView = UIView.init()
        
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        
        self.tableView.setRoundedCorner()
    }
    
    func loadInfoData() {
        
        var dict = [IMAGE: "", PLACEHOLDER : "Profile Image", TEXT : ""]
        self.infoArray.append(dict)
        
        var Name = ""
        var MobileNo = ""
        var AlternateMobile = ""
        var EmailId = ""
        var Relation = ""
        var Gender = ""
        var DOB = ""
        var MartialStatus = ""
        var BloodGroup = ""
    
        if self.existingInfoArray.count != 0 {
            if let text = self.existingInfoArray["Image"] {
                let imageView = UIImageView.init()
                if let url = URL.init(string: "\(text)") {
                    imageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.3), runImageTransitionIfCached: true, completion: { (data) in
                        self.selectedProfileImage = imageView.image
                        let indexPath = IndexPath(row: 0, section: 0)
                        if let cell = self.tableView.cellForRow(at: indexPath) as? ProfileImageTableViewCell {
                            self.setProfileImageView(cell)
                        }
                    })
                }
            }
            if let text = self.existingInfoArray["Name"] as? String {
                Name = "\(text)"
            }
            
            if let text = self.existingInfoArray["PhoneNo"] as? String {
                MobileNo = "\(text)"
            }
            
            if let text = self.existingInfoArray["AlternateMobileNo"] as? String {
                AlternateMobile = "\(text)"
            }
            
            if let text = self.existingInfoArray["Email"] as? String {
                EmailId = "\(text)"
            }
            
            if let text = self.existingInfoArray["Relation"] as? String {
                Relation = "\(text)"
            }
            
            if let text = self.existingInfoArray["Gender"] as? String {
                Gender = Helper.shared.getGenderValue("\(text)")
            }
            
            if let text = self.existingInfoArray["DOB"] as? String {
                if "\(text)".count != 0 {
                    DOB = "\(text)"
                }
            }
            
            if let text = self.existingInfoArray["MartialStatus"] as? String {
                MartialStatus = Helper.shared.getMartialStatusText(text)
            }
            
            if let text = self.existingInfoArray["BloodGroup"] {
                BloodGroup = "\(text)"
            }
            
        }
        dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : Name]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Mobile Number", TEXT : MobileNo]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Alternate Mobile", TEXT : AlternateMobile]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Email Id", TEXT : EmailId]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Relation", TEXT : Relation]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Gender", TEXT : Gender]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Date of Birth", TEXT : DOB]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "MartialStatus", TEXT : MartialStatus]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "BloodGroup", TEXT : BloodGroup]
        self.infoArray.append(dict)
        
    }
    
    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            self.savePersonalDetailsAPICall()
        }
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        NotificationCenter.default.post(name: Notification.Name.Task.ReloadFamilyList, object: self, userInfo: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
