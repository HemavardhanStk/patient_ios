//
//  ReviewTableViewCell.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 12/03/19.
//  Copyright © 2019 Hemavardhan O. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var appDateLabel: UILabel!
    @IBOutlet weak var appNumber: UILabel!
    @IBOutlet weak var rightArrow: UIImageView!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
