//
//  CompletedFeedbackAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension CompletedFeedBackViewController {
    
    func getCompletedFeedbacksAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["UserId" : user.id,
            "MobileNumber" : user.phone] as Dictionary<String, String>
        
        HttpManager.shared.loadAPICall(path: PATH_UserGetAllRating, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let Reviews = response["GetUserRating"] as? [Dictionary<String, AnyObject>] {
                    self.infoArray = Reviews
                    self.infoArrayBackup = Reviews
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
}
