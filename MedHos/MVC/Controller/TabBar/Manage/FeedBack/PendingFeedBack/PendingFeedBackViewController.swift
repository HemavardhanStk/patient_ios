//
//  PendingFeedBackViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 29/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class PendingFeedBackViewController: UIViewController {
    

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, Any>]()
    var filteredInfoArray = [Dictionary<String, Any>]()
    var infoArrayBackup = [Dictionary<String, Any>]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
        
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitlewithRightBarButton(TITLE_PENDING_FEEDBACk, image: "ic_thumbs_white", text: "", controller: self)
        
        self.loadInfoData()
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        NotificationCenter.default.addObserver(self, selector: #selector(loadInfoData), name: Notification.Name.Task.ReloadPendingFeedback, object: nil)
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
        
        self.searchBar.setSearchBarStyle()
    }
    
    @objc func loadInfoData() {
        self.getPendingFeedbacksAPICall()
    }

    @objc func rightBarButtonTapped() {
        let viewController = AppController.shared.getCompletedFeedBackViewController()
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
}
