//
//  PendingFeedbackHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PendingFeedBackViewController {
    
    // MARK: - Feedback Cell
    
    func getFeedbackCell(_ indexPath: IndexPath) -> ReviewTableViewCell {
        
        let cell: ReviewTableViewCell = Bundle.main.loadNibNamed("ReviewTableViewCell", owner: nil, options: nil)![0] as! ReviewTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.feedbackView.backgroundColor = UIColor.white
        cell.feedbackView.setRoundedCorner()
        cell.doctorNameLabel.font = UIFont.init(name: APP_FONT_NAME_BOLD, size: 15)
        cell.appDateLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.appNumber.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 12)
        
        cell.doctorNameLabel.textColor = UIColor.black
        cell.appDateLabel.textColor = UIColor.darkGray
        cell.appNumber.textColor = UIColor.lightGray
        
        let item = self.infoArray[indexPath.row]
        if let value = item["DoctorName"] as? String {
            cell.doctorNameLabel.text = value
        }
        
        if let value = item["AppointmentStatus"] as? String,
            let date = item["AppointmentDate"] as? String {
            if value == "Cancelled" {
                cell.appDateLabel.text = "\(date) - Cancelled"
            } else {
                cell.appDateLabel.text = date
            }
        }

        if let value = item["AppointmentNo"] as? Int {
            cell.appNumber.text = "Appointment No: \(value)"
        }

        cell.contentImageView.layer.masksToBounds = true
        cell.contentImageView.layer.cornerRadius = cell.contentImageView.frame.size.height / 2

        if let imageUrl = item["DoctorImage"] as? String,
            imageUrl.count != 0 {
            cell.contentImageView.loadImageFromUrl(imageUrl, contentMode: .scaleAspectFill, imageTransition: UIImageView.ImageTransition.flipFromLeft(1.0))
        }
        
        cell.rightArrow.image = UIImage(named: "ic_reply_reviews")
        cell.rightArrow.isHidden = false
        
        return cell
    }
    
    func isFiltering() -> Bool {
        return  !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return self.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        if isFiltering() && self.infoArray.count == 0 {
            self.infoArray = self.infoArrayBackup
        }
        
        self.filteredInfoArray = self.infoArray.filter({( dict : Dictionary<String, Any> ) -> Bool in
            if self.searchBarIsEmpty() {
                return true
            } else {
                let DoctorName: String = dict["DoctorName"] as! String
                let AppointmentStatus: String = dict["AppointmentStatus"] as! String
                let AppointmentDate: String = dict["AppointmentDate"] as! String
                return DoctorName.lowercased().contains(searchText.lowercased()) || AppointmentStatus.lowercased().contains(searchText.lowercased()) || AppointmentDate.lowercased().contains(searchText.lowercased())
            }
        })
        
        if isFiltering() {
            self.infoArray = self.filteredInfoArray
        }
        else{
            self.infoArray = self.infoArrayBackup
        }
        self.tableView.reloadData()
    }
    
}
