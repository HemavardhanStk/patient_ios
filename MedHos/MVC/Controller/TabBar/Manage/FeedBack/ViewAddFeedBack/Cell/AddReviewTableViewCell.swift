//
//  AddReviewTableViewCell.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 12/03/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class AddReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ratingTitleLabel: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    
    @IBOutlet weak var recommendationLabel: UILabel!
    @IBOutlet weak var recommendationIcon: UIImageView!
    @IBOutlet weak var recommendationIconTwo: UIImageView!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var submitButtonHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
