//
//  ViewAddFeedbackHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewAddFeedbackViewController {
    
    func getVisitDetailsCell() -> ViewReportTableViewCell {
        
        let cell: ViewReportTableViewCell = Bundle.main.loadNibNamed("ViewReportTableViewCell", owner: nil, options: nil)![0] as! ViewReportTableViewCell

        let doctorImageTag = self.actionType == "Add" ? "DoctorImage" : "DrImage"
        if let value = self.infoArray[doctorImageTag] as? String,
            let url = URL(string: value) {
            cell.PatientProfileImageView.af_setImage(withURL: url)
        }
        
        cell.PatientProfileImageView.layer.masksToBounds = true
        cell.PatientProfileImageView.layer.cornerRadius = cell.PatientProfileImageView.bounds.height / 2
        
        let doctorNameTag = self.actionType == "Add" ? "DoctorName" : "Name"
        
        var appNo = "Appointment No :"
        var appDate = "Appointment Date :"
        var appTime = "Appointment Time :"
        
        switch UIDevice().type {
            
        case .iPhone5, .iPhone5S:
            appNo = "App No:"
            appDate = "App Date:"
            appTime = "App Time:"
        default:break
        }
        
        cell.PatientNameTextLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.PatientNameTextLabel.text = self.infoArray[doctorNameTag] as? String ?? ""
        
        self.setupVistLabelStyle(cell.dateKeyLabel, text: appNo)
        self.setupVistLabelStyle(cell.appNoKeyLabel, text: appDate)
        self.setupVistLabelStyle(cell.ageKeyLabel, text: appTime)
        self.setupVistLabelStyle(cell.nextVisitKeyLabel, text: "Status :")
        self.setupVistLabelStyle(cell.observationKeyLabel, text: "")
        self.setupVistLabelStyle(cell.treatmentKeyLabel, text: "")
        
        self.setupVistLabelStyle(cell.dateTextLabel, text: "\(self.infoArray["AppointmentNo"] as? Int ?? 0)", color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.appNoTextLabel, text: self.infoArray["AppointmentDate"] as? String ?? "", color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.ageTextLabel, text: self.infoArray["AppointmentTime"] as? String ?? "", color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.nextVisitTextLabel, text: self.infoArray["AppointmentStatus"] as? String ?? "", color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.observationTextLabel, text: "", color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.treatmentTextLabel, text: "", color: UIColor.darkGray)
        
        cell.observationExtendButton.alpha = 0
        cell.treatmentExtendButton.alpha = 0
        
        cell.treatmentTextLabelBottomConstraint.constant = 0
        
        return cell
    }

    // MARK: - Rating Cell
    
    func getIndividualRatingCell(_ indexPath: IndexPath) -> AddReviewTableViewCell {
        
        let cell: AddReviewTableViewCell = Bundle.main.loadNibNamed("AddReviewTableViewCell", owner: nil, options: nil)![0] as! AddReviewTableViewCell
        
        cell.layer.masksToBounds = true
        cell.ratingTitleLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        
        var key = ""
        var text = ""
        if indexPath.row == 1 {
            key = "Cleanliness"
            text = "Clinic Cleaness"
        } else if indexPath.row == 2 {
            key = "StaffCourteous"
            text = "Courteous Staff"
        } else if indexPath.row == 3 {
            key = "DiseaseDiagnosis"
            text = "Disease Diagnosis"
        } else if indexPath.row == 4 {
            key = "Politeness"
            text = "Doctor Politeness"
        } else if indexPath.row == 5 {
            key = "DiseaseExplained"
            text = "Disease Explained"
        }
        
        self.setupRatingView(cell, key: key, text: text)
        self.setRatingListener(cell.ratingBar, key: key)
        
        return cell
    }
    
    func getOverallRatingCell(_ indexPath: IndexPath) -> AddReviewTableViewCell {
        
        let cell: AddReviewTableViewCell = Bundle.main.loadNibNamed("AddReviewTableViewCell", owner: nil, options: nil)![1] as! AddReviewTableViewCell
        
        cell.ratingTitleLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 16)
        
        let key = "OverallRate"
        let text = "Overall Rating"
        
        self.setupRatingView(cell, key: key, text: text)
        self.setupReplySection(cell)
        self.setRatingListener(cell.ratingBar, key: key)
        
        return cell
    }
    
    func setupVistLabelStyle(_ label: UILabel, text: String, color: UIColor = UIColor.black) {
        
        label.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        label.textColor = color
        label.text = text
    }
    
    func setupRatingView(_ cell: AddReviewTableViewCell, key: String, text: String) {
        
        cell.ratingTitleLabel.text = text
        cell.ratingBar.minTouchRating = 0.5
        cell.ratingBar.settings.fillMode = .half
        cell.ratingBar.isUserInteractionEnabled = self.actionType == "Add"
        
        if let value = self.infoArray[key] as? Double {
            cell.ratingBar.rating = value
        } else {
            cell.ratingBar.rating = 0.0
        }
    }
    
    func setupReplySection(_ cell: AddReviewTableViewCell) {
        
        cell.recommendationLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.commentsTextView.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.recommendationLabel.textColor = UIColor.darkGray
        
        self.setupTextViewPlaceholder(cell.commentsTextView)
        cell.commentsTextView.delegate = self
        cell.commentsTextView.tag = 1
        cell.commentsTextView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR_LIGHT)
        cell.commentsTextView.setRoundedCorner()
        
        cell.recommendationLabel.text = "Recommendation"
        
        if let value = self.infoArray["Recommendation"],
            self.actionType == "View" {
            
            if "\(value)" == "Y" {
                cell.recommendationIcon.image = UIImage.init(named: "ic_thumbs_liked_green")
            } else {
                cell.recommendationIcon.image = UIImage.init(named: "ic_thumbs_unliked_red")
            }
        } else {
            
            cell.recommendationIcon.isUserInteractionEnabled = true
            cell.recommendationIconTwo.isUserInteractionEnabled = true
            
            cell.recommendationIcon.tag = 34
            cell.recommendationIconTwo.tag = 35
            
            cell.recommendationIcon.image = UIImage(named: "ic_thumbs_like_gray")
            cell.recommendationIconTwo.image = UIImage(named: "ic_thumbs_unlike_gray")
            
            let likeTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleLike))
            let unLikeTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleUnlike))
            cell.recommendationIcon.addGestureRecognizer(likeTapGesture)
            cell.recommendationIconTwo.addGestureRecognizer(unLikeTapGesture)
            
        }
        
        if self.actionType == "View" {
            cell.commentsTextView.isEditable = false
            cell.submitButton.isHidden = true
        }
        
        if let value = self.infoArray["Comments"] as? String,
            value != "" {
            self.setupTextViewText(value, textView: cell.commentsTextView)
        } else if self.actionType == "View" {
            cell.commentsTextView.alpha = 0
        }
        
        cell.submitButton.setPrimaryButtonStyle("SUBMIT")
        cell.submitButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        cell.submitButtonHeightConstraint.constant = self.actionType == "Add" ? 40 : 0
    }
    
    func setupTextViewPlaceholder(_ textView: UITextView) {
        
        textView.text = self.feedbackPlaceholder
        textView.textColor = UIColor.lightGray
    }
    
    func setupTextViewText(_ text: String, textView: UITextView) {
        
        textView.text = text
        textView.textColor = UIColor.black
    }

    func setRatingListener(_ ratingBar: CosmosView, key: String) {
        
        ratingBar.didFinishTouchingCosmos = { rating in
            self.infoArray.updateValue(rating, forKey: key)
        }
    }
    
    @objc func handleLike() {
        
        self.infoArray["recommend"] = "Y"
        
        if let likeImageView = self.tableView.viewWithTag(34) as? UIImageView,
           let unlikeImageView = self.tableView.viewWithTag(35) as? UIImageView {
            
            likeImageView.image = UIImage(named: "ic_thumbs_liked_green")
            unlikeImageView.image = UIImage(named: "ic_thumbs_unlike_gray")
        }
    }
    
    @objc func handleUnlike() {
        
        self.infoArray["recommend"] = "N"
        
        if let likeImageView = self.tableView.viewWithTag(34) as? UIImageView,
           let unlikeImageView = self.tableView.viewWithTag(35) as? UIImageView {
            
            likeImageView.image = UIImage(named: "ic_thumbs_like_gray")
            unlikeImageView.image = UIImage(named: "ic_thumbs_unliked_red")
        }
    }
    
}
