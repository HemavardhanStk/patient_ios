//
//  ViewAddFeedbackDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewAddFeedbackViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var key = ""
        if indexPath.row == 1 {
            key = "Cleanliness"
        } else if indexPath.row == 2 {
            key = "StaffCourteous"
        } else if indexPath.row == 3 {
            key = "DiseaseDiagnosis"
        } else if indexPath.row == 4 {
            key = "Politeness"
        } else if indexPath.row == 5 {
            key = "DiseaseExplained"
        }

        if let value = self.infoArray[key] as? Double, value == 0.0 {
            return CGFloat.leastNonzeroMagnitude
        }

        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getVisitDetailsCell()
            
        case 1...5 :
            return self.getIndividualRatingCell(indexPath)
            
        case 6 :
            return self.getOverallRatingCell(indexPath)
            
        default:
            return UITableViewCell()
        }
    }
    
}

extension ViewAddFeedbackViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            self.setupTextViewText("", textView: textView)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.feedbackPlaceholder
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text != self.feedbackPlaceholder {
            self.infoArray["Comments"] = textView.text
        }
    }
}
