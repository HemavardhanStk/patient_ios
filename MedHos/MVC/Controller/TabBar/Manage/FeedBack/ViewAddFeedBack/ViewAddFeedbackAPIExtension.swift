//
//  ViewAddFeedbackAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewAddFeedbackViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        
        if self.infoArray["OverallRate"] as? Double == 0.0 || self.infoArray["OverallRate"] == nil {
            isValidationSuccess = false
            AlertHelper.shared.showAlert(message: "Select Overall Rating", controller: self)
        }
        
        if self.infoArray["recommend"] as? String == "" || self.infoArray["recommend"] == nil {
            isValidationSuccess = false
            AlertHelper.shared.showAlert(message: "Select Recommendation", controller: self)
        }
        
        return isValidationSuccess
    }
    
    func loadSubmitRatingsAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["AppointmentNo"       : self.infoArray["AppointmentNo"],
                      "CategoryNo"          : 1,
                      "Cleanliness"         : self.infoArray["Cleanliness"] as? Double ?? 0.0,
                      "Comnt"               : self.infoArray["Comments"],
                      "DiseaseDiagnosis"    : self.infoArray["DiseaseDiagnosis"] as? Double ?? 0.0,
                      "DiseaseExplained"    : self.infoArray["DiseaseExplained"] as? Double ?? 0.0,
                      "Language"            : "English",
                      "OverAllRating"       : self.infoArray["OverallRate"],
                      "Politeness"          : self.infoArray["Politeness"] as? Double ?? 0.0,
                      "Recommend"           : self.infoArray["recommend"],
                      "StaffCourteous"      : self.infoArray["StaffCourteous"] as? Double ?? 0.0,
                      "refid"               : self.infoArray["DoctorNo"],
                      "userno"              : user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_AddRating, params: params as Dictionary<String, Any>, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let message = response["Message"] as? String {
                    AlertHelper.shared.showAlertWithHandler(message: message, handler: "popViewController", withCancel: false, controller: self)
                }
            }
        }) {}
    }
    
}

