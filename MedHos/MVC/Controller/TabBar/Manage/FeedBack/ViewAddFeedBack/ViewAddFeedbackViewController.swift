//
//  ViewAddFeedbackViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 08/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ViewAddFeedbackViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String, Any>()
    var actionType = ""
    var isFromViewAppointment = false
    var feedbackPlaceholder = "Comments"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "Feedback", controller: self)
        
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }

    @objc func submitButtonTapped() {
        
        if self.isValidationSuccess() {
            self.loadSubmitRatingsAPICall()
        }
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        if self.isFromViewAppointment {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: TabBarViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        } else {
            NotificationCenter.default.post(name: Notification.Name.Task.ReloadPendingFeedback, object: self, userInfo: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
