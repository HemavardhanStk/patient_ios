//
//  ViewAppointmentHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

extension ViewAppointmentViewController {
    
    @objc func CancelCall() {
        
        let alertController = UIAlertController(title: "Cancel\nAppointment", message: self.alertText, preferredStyle: UIAlertController.Style.alert)
        
        self.alertText = ""
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Reason for cancel"
        }
        
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let alertTextField = alertController.textFields![0] as UITextField
            if alertTextField.text?.count == 0 {
                
                self.alertText = "\nReason can't be empty"
                self.CancelCall()
            } else {
                self.loadCancelAppointmentAPICall(alertTextField.text ?? "", RescheduleAppointment: "N")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)

    }
    
    @objc func RescheduleCall() {
        
        let alertController = UIAlertController(title: "Re-Schedule\nAppointment", message: self.alertText, preferredStyle: UIAlertController.Style.alert)
        
        self.alertText = ""
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Reason for Re-Schedule"
        }
        
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let alertTextField = alertController.textFields![0] as UITextField
            if alertTextField.text?.count == 0 {
                
                self.alertText = "\nReason can't be empty"
                self.RescheduleCall()
            } else {
                self.loadCancelAppointmentAPICall(alertTextField.text ?? "", RescheduleAppointment: "Y")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)

    }
    
    @objc func DontRemindCall() {
        
        let alertController = UIAlertController(title: "Don't Remind", message: self.alertText, preferredStyle: UIAlertController.Style.alert)
        
        self.alertText = ""
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Reason"
        }
        
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let alertTextField = alertController.textFields![0] as UITextField
            if alertTextField.text?.count == 0 {
                
                self.alertText = "\nReason can't be empty"
                self.DontRemindCall()
            } else {
                self.loadDontRemindAPICall(alertTextField.text ?? "")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)

    }
    
    @objc func BookAppointment() {
        let viewController = AppController.shared.getCreateAppointmentViewController()
        viewController.DoctorId = self.infoArray["DoctorId"] as? Int ?? 0
        viewController.DoctorName = self.infoArray["DoctorName"] as? String ?? ""
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoHealthRecord() {
        
        let viewController = AppController.shared.getAddHealthRecordViewController()
        viewController.appNo = "\(self.infoArray["Appoinmentid"] as? Int ?? 0)"
//        viewController.recordId = "\(self.infoArray["MedRecordId"] as? Int ?? 0)"
//        viewController.DocumentTitle = self.infoArray["DocumentName"] as? String ?? ""
//        viewController.recordTitle = self.infoArray["DocumentName"] as? String ?? ""
//        viewController.typeOfRecord = self.infoArray["DocumentType"] as? String ?? ""
//        viewController.documentDate = self.infoArray["DocumentDate"] as? String ?? ""
        viewController.ConsultingDate = self.infoArray["AppointmentDate"] as? String ?? ""
        viewController.ConsultingTime = self.infoArray["RequestedTime"] as? String ?? ""
        viewController.doctorName = self.infoArray["DoctorName"] as? String ?? ""
        viewController.patientName = self.infoArray["PatientName"] as? String ?? ""
        viewController.patientId = "\(self.infoArray["UserNo"] as? Int ?? 0)"
        viewController.mapYN = "Y"
        viewController.canChangeMapping = false
//        if let value = self.infoArray["filename"] as? [Dictionary<String,Any>] {
//
//            for i in 0..<value.count {
//                if let value1 = value[i]["FileLocation"] as? String {
//                    viewController.existingFilesUrlArray.append(value1)
//                }
//            }
//        }
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    @objc func sloadView(lat:Double,long:Double) {
        
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.MapView.camera = GMSCameraPosition(target: coord, zoom: 14, bearing: 0, viewingAngle: 0)
        
        let marker = GMSMarker()
        
        marker.appearAnimation = GMSMarkerAnimation(rawValue: 1)!
        marker.position = coord
        marker.map = self.MapView
    }
    
    @objc func ViewProfileFunction() {
        let viewController = AppController.shared.getDoctorProfileViewController()
        viewController.DoctorId = self.infoArray["DoctorId"] as? Int ?? 0
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    func loadInfoData() {
        
        self.MapView.layer.cornerRadius = 5
        self.MapView.layer.masksToBounds = true
        self.MapView.isUserInteractionEnabled = false
        
        if let value = self.infoArray["DrImage"] as? String {
            let url = URL.init(string: value)
            self.DoctorImageView.af_setImage(withURL: url!)
        }
        
        self.DoctorNameLabel.text = self.infoArray["DoctorName"] as? String ?? ""
        self.PatientNameLabel.text = self.infoArray["PatientName"] as? String ?? ""
        self.DoctorSpecializtionLabel.text = self.infoArray["Speciality"] as? String ?? ""
        self.ClinicNameLabel.text = self.infoArray["ClinicName"] as? String ?? ""
        self.ClinicAddressLabel.text = self.infoArray["Address"] as? String ?? ""
        
        if let value = self.infoArray["ClinicPhotos"] as? [String], value.count != 0  {
            
            self.ImageUrlsArray = value
            
            let cell = Bundle.main.loadNibNamed("CollectionTableViewCell", owner: self, options: nil)?[0] as! CollectionTableViewCell
            
            cell.CollectionView.delegate = self
            cell.CollectionView.dataSource = self
            cell.frame.size.width = ClinicImageView.frame.width
            cell.frame.size.height = 81
            self.ClinicImageView.addSubview(cell)
            
        } else {
            self.ClinicImageViewHeight.constant = 0
            self.ClinicImageViewTrailing.constant = 0
        }
        
        if let value = self.infoArray["ClinicMobile"] as? String,value != "" {
            self.MobileNumberLabel.text = value
        } else {
            self.MobileImageHeight.constant = 0
        }
        
        if let lat = self.infoArray["Lattitude"] as? Double,
            let long = self.infoArray["Longitude"] as? Double {
            self.sloadView(lat: lat, long: long)
        }
        
        self.Button1.titleLabel?.numberOfLines = 2
        self.Button1.titleLabel?.lineBreakMode = .byWordWrapping
        self.Button1.titleLabel?.textAlignment = .center
        
        self.Button2.titleLabel?.numberOfLines = 2
        self.Button2.titleLabel?.lineBreakMode = .byWordWrapping
        self.Button2.titleLabel?.textAlignment = .center
        
        self.Button3.titleLabel?.numberOfLines = 2
        self.Button3.titleLabel?.lineBreakMode = .byWordWrapping
        self.Button3.titleLabel?.textAlignment = .center
        
        
        if inheritedFrom == TITLE_UPCOMING {
            
            switch UIDevice().type
            {
                case .iPhone5, .iPhone5S :
                    self.DateLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 10)
                    break
                
                default:break
            }
            
            self.Button1Wdith.constant = screenWidth/2 - 1.5
            self.Button2Wdith.constant = screenWidth/2 - 1.5
            self.Button3.alpha = 0

            self.TokenLabel.text = self.infoArray["ReferenceID"] as? String ?? ""
            
            if let value1 = self.infoArray["AppointmentDate"] as? String,
                let value2 = self.infoArray["RequestedTime"] as? String {
                self.DateLabel.text = "\(value1) at \(value2)"
            }
            
            
            let str = NSMutableAttributedString(string: "Cancel\nAppointment")
            str.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 6))
            str.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(7, 7))
            str.setColor(color: UIColor.white, forText: "Cancel\nAppointment")
            self.Button1.setAttributedTitle(str, for: .normal)
            self.Button1.addTarget(self, action: #selector(CancelCall), for: .touchUpInside)
            
            let str1 = NSMutableAttributedString(string: "Reschedule\nAppointment")
            str1.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 10))
            str1.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(11, 11))
            str1.setColor(color: UIColor.white, forText: "Reschedule\nAppointment")
            self.Button2.setAttributedTitle(str1, for: .normal)
            self.Button2.addTarget(self, action: #selector(RescheduleCall), for: .touchUpInside)
            self.Button2.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)

            
            
        } else if inheritedFrom == TITLE_PAST {
            
            self.TokenImageWidth.constant = 0
            self.TokenImageTrailingConstant.constant = 0
            self.ClockImageWidth.constant = 0
            
            if let value = self.infoArray["RatingFlag"] as? String, value == "N" {
                self.Button1Wdith.constant = screenWidth/3 - 1.33
                self.Button2Wdith.constant = screenWidth/3 - 1.33
                self.Button3Wdith.constant = screenWidth/3 - 1.33
            } else {
                self.Button1.alpha = 0
                
                self.Button1Wdith.constant = 0
                self.Button2Wdith.constant = screenWidth/2 - 2
                self.Button3Wdith.constant = screenWidth/2 - 2
            }
            
            
            self.TokenLabel.text = "Appointment Date:"
            
            if let value1 = self.infoArray["AppointmentDate"] as? String,
                let value2 = self.infoArray["RequestedTime"] as? String {
                self.DateLabel.text = "\(value1) at \(value2)"
            }
            
            let str = NSMutableAttributedString(string: "Give\nFeedback")
            str.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 4))
            str.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(5, 8))
            str.setColor(color: UIColor.white, forText: "Give\nFeedback")
            self.Button1.setAttributedTitle(str, for: .normal)
            self.Button1.addTarget(self, action: #selector(GiveFeedBack), for: .touchUpInside)
            self.Button1.backgroundColor = UIColor.lightGray
            
            
            let str1 = NSMutableAttributedString(string: "Book\nAgain")
            str1.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 4))
            str1.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(5, 5))
            str1.setColor(color: UIColor.white, forText: "Book\nAgain")
            self.Button2.setAttributedTitle(str1, for: .normal)
            self.Button2.addTarget(self, action: #selector(BookAppointment), for: .touchUpInside)
            self.Button2.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
            
            let str2 = NSMutableAttributedString(string: "Upload\nRecord")
            str2.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 6))
            str2.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(7, 5))
            str2.setColor(color: UIColor.white, forText: "Upload\nRecord")
            self.Button3.setAttributedTitle(str2, for: .normal)
            self.Button3.addTarget(self, action: #selector(gotoHealthRecord), for: .touchUpInside)
            self.Button3.backgroundColor = UIColor(netHex: APP_GREEN)
        } else if inheritedFrom == TITLE_DUE {
            
            self.TokenImageWidth.constant = 0
            self.TokenImageTrailingConstant.constant = 0
            self.ClockImageWidth.constant = 0
            self.AppointmentStatusLabelHeight.constant = 0
            
            self.Button1Wdith.constant = screenWidth/2 - 1.5
            self.Button2Wdith.constant = screenWidth/2 - 1.5
            self.Button3.alpha = 0
            
            self.Button1.backgroundColor = UIColor.lightGray
            self.Button2.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
            
            
            self.TokenLabel.text = "Next Due Date:"
            
            if let value = self.infoArray["NextVisitDate"] as? String {
                self.DateLabel.text = value
            }
            
            let str = NSMutableAttributedString(string: "Don't\nRemind")
            str.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 6))
            str.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(6, 6))
            str.setColor(color: UIColor.white, forText: "Don't\nRemind")
            self.Button1.setAttributedTitle(str, for: .normal)
            self.Button1.addTarget(self, action: #selector(DontRemindCall), for: .touchUpInside)
            
            
            let str1 = NSMutableAttributedString(string: "Book\nAppointment")
            str1.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, 4))
            str1.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(5, 5))
            str1.setColor(color: UIColor.white, forText: "Book\nAppointment")
            self.Button2.setAttributedTitle(str1, for: .normal)
            self.Button2.addTarget(self, action: #selector(BookAppointment), for: .touchUpInside)
            
        }
    }
    
    @objc func showPresTestAlert() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Prescription", style: .default, handler: {(action:UIAlertAction) in
            if self.infoArray["DrugStatus"] as? String == "Y" {
                let viewController = AppController.shared.getViewPrescriptionTestReportViewController()
                viewController.reportType = "PatDrug"
                viewController.appNo = "\(self.infoArray["Appoinmentid"] as? Int ?? 0)"
                self.navigationController!.pushViewController(viewController, animated: true)
            } else {
                AlertHelper.shared.showAlert(message: "Prescription not available", controller: self)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Test Report", style: .default, handler: {(action:UIAlertAction) in
            if self.infoArray["TestStatus"] as? String == "Y" {
                let viewController = AppController.shared.getViewPrescriptionTestReportViewController()
                viewController.reportType = "PatTest"
                viewController.appNo = "\(self.infoArray["Appoinmentid"] as? Int ?? 0)"
                self.navigationController!.pushViewController(viewController, animated: true)
            } else {
                AlertHelper.shared.showAlert(message: "Test Report not available", controller: self)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func GiveFeedBack() {
        
        let infoArray = ["DoctorNo" : self.infoArray["DoctorId"] as? Int ?? 0,
                            "AppointmentNo" : self.infoArray["Appoinmentid"] as? Int ?? 0,
                            "DoctorName" : self.infoArray["DoctorName"] as? String ?? "",
                            "DoctorImage" : self.infoArray["DrImage"] as? String ?? "",
                            "AppointmentDate" : self.infoArray["AppointmentDate"] as? String ?? "",
                            "AppointmentTime" : self.infoArray["RequestedTime"] as? String ?? "",
                            "AppointmentStatus" : self.infoArray["Status"] as? String ?? ""] as? [String : Any]
        
        let viewController = AppController.shared.getViewAddFeedbackViewController()
        viewController.infoArray = infoArray ?? [:]
        viewController.actionType = "Add"
        viewController.isFromViewAppointment = true
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
}
