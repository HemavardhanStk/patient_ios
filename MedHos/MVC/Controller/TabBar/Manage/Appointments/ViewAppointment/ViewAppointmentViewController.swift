//
//  ViewAppointmentViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 08/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewAppointmentViewController: UIViewController {
    
    //@IBOutlet weak var tableView:UITableView!
    
    @IBOutlet weak var TokenImageWidth: NSLayoutConstraint!
    @IBOutlet weak var TokenImageTrailingConstant: NSLayoutConstraint!
    @IBOutlet weak var ClockImageWidth: NSLayoutConstraint!
    
    // Top labels ..Need to be used as per page requirement
    @IBOutlet weak var TokenLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    
    @IBOutlet weak var PatientNameLabel: UILabel!
    
    @IBOutlet weak var AppointmentStatusLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var StatusLabel: UILabel!
    
    @IBOutlet weak var DoctorImageView: UIImageView!
    @IBOutlet weak var ViewProfile: UIButton!
    @IBOutlet weak var DoctorNameLabel: UILabel!
    @IBOutlet weak var DoctorSpecializtionLabel: UILabel!
    
    @IBOutlet weak var ClinicNameLabel: UILabel!
    @IBOutlet weak var ClinicAddressLabel: UILabel!
    
    @IBOutlet weak var MobileImageHeight: NSLayoutConstraint!
    @IBOutlet weak var MobileNumberLabel: UILabel!
    
    @IBOutlet weak var ClinicImageView: UIView!
    @IBOutlet weak var ClinicImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ClinicImageViewTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet weak var MapViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var Button1: UIButton!
    @IBOutlet weak var Button2: UIButton!
    @IBOutlet weak var Button3: UIButton!
    @IBOutlet weak var Button1Wdith: NSLayoutConstraint!
    @IBOutlet weak var Button2Wdith: NSLayoutConstraint!
    @IBOutlet weak var Button3Wdith: NSLayoutConstraint!
    
    var infoArray = Dictionary<String, Any>()
    
    var inheritedFrom = String()
    var ImageUrlsArray = [String]()
//    var Actiontype = 0
    var alertText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
    }

    func classAndWidgetsInitialise() {
        
        if self.inheritedFrom == TITLE_PAST {
            Navigation.shared.setTitlewithRightBarButton(self.inheritedFrom, image: "dot", text: "", controller: self)
        } else {
            Navigation.shared.setTitle(title: self.inheritedFrom, controller: self)
        }
        
        self.loadInfoData()
        
        self.ViewProfile.addTarget(self, action: #selector(ViewProfileFunction), for: .touchUpInside)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewProfileFunction))
        self.DoctorImageView.isUserInteractionEnabled = true
        self.DoctorImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func rightBarButtonTapped() {
        self.showPresTestAlert()
    }
    
    // MARK: - Handlers
    
    @objc func popViewController() {
        
        NotificationCenter.default.post(name: Notification.Name.Task.ReloadUpcomingAppointments, object: self, userInfo: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
