//
//  ViewAppointmentAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewAppointmentViewController {
    
    func loadDontRemindAPICall(_ Remarks:String) {
        
        let params = ["AppointmentNo": "\(self.infoArray["Appoinmentid"] ?? 0 )",
                      "DocID": "\(self.infoArray["DoctorId"] ?? 0 )",
                      "Remarks": Remarks,
                      "Language": "English",
                      "RescheduleAppointment": ""]

        HttpManager.shared.loadAPICall(path: PATH_DueReminderUpdate, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: TabBarViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }) {}
        
    }
    
    func loadCancelAppointmentAPICall(_ Remarks:String, RescheduleAppointment: String) {

        let params = ["AppointmentNo": "\(self.infoArray["Appoinmentid"] ?? 0)",
                      "DocID": "\(self.infoArray["Doctorid"] ?? 0)",
                      "Remarks": Remarks,
                      "Language": "English",
                      "RescheduleAppointment": RescheduleAppointment]
        
        HttpManager.shared.loadAPICall(path: PATH_CancelBooking, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {

                if RescheduleAppointment == "Y" {
                    self.BookAppointment()
                } else {
                    AlertHelper.shared.showAlertWithHandler(message: response["Message"] as? String ?? "Appointment has been cancelled successfully", handler: "popViewController", withCancel: false, controller: self)
                }
            }
        }) {}

        
    }
    
}
