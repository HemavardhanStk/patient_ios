//
//  DueAppointmentsHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension DueAppointmentViewController {
    
    func getAppointmentsCell(_ indexPath:IndexPath) -> AppointmentTableViewCell {
        
        let cell: AppointmentTableViewCell = Bundle.main.loadNibNamed("AppointmentTableViewCell", owner: nil, options: nil)![0] as! AppointmentTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.appointmentView.backgroundColor = UIColor.white
        cell.appointmentView.setRoundedCorner()
        cell.TokenImageWidth.constant = 0
        cell.ClockImageWidth.constant = 0
        cell.ClockImageTrailing.constant = 0
        
        cell.AppointmentDateLabel.adjustsFontSizeToFitWidth = true
        cell.AppointmentStatusLabel.textColor = UIColor(netHex: APP_GREEN)
        
        let item = self.infoArray[indexPath.row]
        
        cell.AppDateLabelName.text = "Next Due Date:"
        
        if let NextVisitDate = item["NextVisitDate"] as? String {
            cell.AppointmentDateLabel.text = NextVisitDate
        }
        
        if let PatientName = item["PatientName"] as? String {
            cell.BookedForLabel.text = "Booked for \(PatientName)"
        }
        
        if let imageUrl = item["DrImage"] as? String,
            imageUrl.count != 0 {
            cell.DoctorProfileImageView.loadImageFromUrl(imageUrl, contentMode: .scaleAspectFill, imageTransition: UIImageView.ImageTransition.flipFromLeft(1.0))
        }
        
        if let DoctorName = item["DoctorName"] as? String {
            cell.DoctorNameLabel.text = DoctorName
        }
        
        if let Speciality = item["Speciality"] as? String {
            cell.DoctorSpecializationLabel.text = Speciality
            
        }
        
        cell.AppointmentStatusLabel.text = ""
        
        return cell
    }
    
}

