//
//  PastAppointmentsHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PastAppointmentViewController {
    
    func getAppointmentsCell(_ indexPath:IndexPath) -> AppointmentTableViewCell {
        
        let cell: AppointmentTableViewCell = Bundle.main.loadNibNamed("AppointmentTableViewCell", owner: nil, options: nil)![0] as! AppointmentTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.appointmentView.backgroundColor = UIColor.white
        cell.appointmentView.setRoundedCorner()
        cell.TokenImageWidth.constant = 0
        cell.ClockImageWidth.constant = 0
        cell.ClockImageTrailing.constant = 0
//
//        cell.AppointmentDateLabel.adjustsFontSizeToFitWidth = true
        
        let item = self.infoArray[indexPath.row]
      
        if let AppointmentDate = item["AppointmentDate"],
            let RequestedTime = item["RequestedTime"] {
            cell.AppointmentDateLabel.text = "\(AppointmentDate) at \(RequestedTime)"
        }
        
        if let PatientName = item["PatientName"] as? String {
            cell.BookedForLabel.text = "Booked for \(PatientName)"
        }
        
        if let imageUrl = item["DrImage"] as? String,
            imageUrl.count != 0 {
            cell.DoctorProfileImageView.loadImageFromUrl(imageUrl, contentMode: .scaleAspectFill, imageTransition: UIImageView.ImageTransition.flipFromLeft(1.0))
        }
        
        if let DoctorName = item["DoctorName"] as? String {
            cell.DoctorNameLabel.text = DoctorName
        }
        
        if let Speciality = item["Speciality"] as? String {
            cell.DoctorSpecializationLabel.text = Speciality
            
        }
        
        if let Status = item["Status"] as? String {
            
            cell.AppointmentStatusLabel.text = Status
            
            if Status == "Cancelled" {
                cell.AppointmentStatusLabel.textColor = UIColor(netHex: APP_LIGHT_RED_COLOR)
            } else if Status == "Completed" {
                cell.AppointmentStatusLabel.textColor = UIColor(netHex: APP_GREEN)
            } else {
                cell.AppointmentStatusLabel.text = ""
            }
            
            
        }
        
        return cell
    }
    
}
