//
//  PastAppointmentsDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension PastAppointmentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getAppointmentsCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = AppController.shared.getViewAppointmentViewController()
        //viewController.PastAppointmentArray = PastAppointmentArray?.PastAppointments[indexPath.section]
        viewController.infoArray = self.infoArray[indexPath.row]
        viewController.inheritedFrom = TITLE_PAST
        self.navigationController!.pushViewController(viewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

