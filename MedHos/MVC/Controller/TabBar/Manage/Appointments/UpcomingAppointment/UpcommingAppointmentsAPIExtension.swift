//
//  UpcommingAppointmentsAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UpcomingAppointmentViewController {
    
    func getUpcomingAppointmentsAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["UserId"        :user.id,
                      "MobileNumber"  :user.phone,
                      "Language"      : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetUpcomeAppointment, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["UpcomingAppointments"] as? [Dictionary<String, Any>] {
                    self.infoArray = value
                }
            } else {
                self.infoArray.removeAll()
            }
            self.tableView.reloadData()
        }) {}
    }
    
}
