//
//  UpcommingAppointmentsHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 31/05/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UpcomingAppointmentViewController {
    
    func getAppointmentsCell(_ indexPath:IndexPath) -> AppointmentTableViewCell {
        
        let cell: AppointmentTableViewCell = Bundle.main.loadNibNamed("AppointmentTableViewCell", owner: nil, options: nil)![0] as! AppointmentTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.appointmentView.backgroundColor = UIColor.white
        cell.appointmentView.setRoundedCorner()
        cell.TokenImageWidth.constant = 13
        cell.ClockImageWidth.constant = 13
        cell.ClockImageTrailing.constant = 5
     
        switch UIDevice().type
        {
        case .iPhone5, .iPhone5S :
            cell.AppDateLabelName.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 10)
            break
        default:break
        }
        
        cell.AppointmentDateLabel.adjustsFontSizeToFitWidth = true
        cell.AppointmentStatusLabel.textColor = UIColor(netHex: APP_GREEN)
        
        let item = self.infoArray[indexPath.row]
        
        if let value = item["ReferenceID"] as? String {
                cell.AppDateLabelName.text = value
        }
        
        if let AppointmentDate = item["AppointmentDate"],
            let RequestedTime = item["RequestedTime"] {
            cell.AppointmentDateLabel.text = "\(AppointmentDate) at \(RequestedTime)"
        }
        
        if let PatientName = item["PatientName"] as? String {
            cell.BookedForLabel.text = "Booked for \(PatientName)"
        }
        
        if let imageUrl = item["DrImage"] as? String,
            imageUrl.count != 0 {
            cell.DoctorProfileImageView.loadImageFromUrl(imageUrl, contentMode: .scaleAspectFill, imageTransition: UIImageView.ImageTransition.flipFromLeft(1.0))
        }
        
        if let DoctorName = item["DoctorName"] as? String {
             cell.DoctorNameLabel.text = DoctorName
        }
        
        if let Speciality = item["Speciality"] as? String {
            cell.DoctorSpecializationLabel.text = Speciality

        }
        
        if let Status = item["Status"] as? String {
            cell.AppointmentStatusLabel.text = Status
        }
        
        return cell
    }
    
    
}
