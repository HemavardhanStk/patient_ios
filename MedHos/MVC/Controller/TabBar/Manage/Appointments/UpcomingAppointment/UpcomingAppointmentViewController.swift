//
//  UpcomingAppointmentViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 02/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class UpcomingAppointmentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var UpcomingAppointmentArray: UpcomingAppointmentStruct? = nil
    var titleName = ""
    
    var infoArray = [Dictionary<String, Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: TITLE_UPCOMING, controller: self)
        
        self.loadInfoData()
        self.setupColors()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "AppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "AppointmentTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        NotificationCenter.default.addObserver(self, selector: #selector(loadInfoData), name: Notification.Name.Task.ReloadUpcomingAppointments, object: nil)
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    @objc func loadInfoData() {
        self.getUpcomingAppointmentsAPICall()
    }
    
}
