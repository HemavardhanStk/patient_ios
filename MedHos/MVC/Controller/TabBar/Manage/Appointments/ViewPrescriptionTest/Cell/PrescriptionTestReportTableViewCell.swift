//
//  PrescriptionTestReportTableViewCell.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 08/05/19.
//  Copyright © 2019 Balaji G. All rights reserved.
//

import UIKit

class PrescriptionTestReportTableViewCell: UITableViewCell {

    
    @IBOutlet weak var itemView: UIView!
    
    @IBOutlet weak var itemTextLabel1: UILabel!
    @IBOutlet weak var itemTextLabel2: UILabel!
    @IBOutlet weak var itemTextLabel3: UILabel!
    @IBOutlet weak var itemTextLabel4: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
