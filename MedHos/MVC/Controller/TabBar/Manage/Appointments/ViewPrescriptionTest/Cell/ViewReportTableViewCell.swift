//
//  ViewReportTableViewCell.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 08/05/19.
//  Copyright © 2019 Balaji G. All rights reserved.
//

import UIKit

class ViewReportTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ReportView: UIView!
    
    @IBOutlet weak var dateKeyLabel: UILabel!
    @IBOutlet weak var dateTextLabel: UILabel!
    @IBOutlet weak var appNoKeyLabel: UILabel!
    @IBOutlet weak var appNoTextLabel: UILabel!
    @IBOutlet weak var ageKeyLabel: UILabel!
    @IBOutlet weak var ageTextLabel: UILabel!
    @IBOutlet weak var nextVisitKeyLabel: UILabel!
    @IBOutlet weak var nextVisitTextLabel: UILabel!
    @IBOutlet weak var PatientNameTextLabel: UILabel!
    @IBOutlet weak var observationKeyLabel: UILabel!
    @IBOutlet weak var observationTextLabel: UILabel!
    @IBOutlet weak var treatmentKeyLabel: UILabel!
    @IBOutlet weak var treatmentTextLabel: UILabel!
    
    @IBOutlet weak var observationExtendButton: UIButton!
    @IBOutlet weak var treatmentExtendButton: UIButton!
    
    @IBOutlet weak var PatientProfileImageView: UIImageView!
    
    @IBOutlet weak var observationTextLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var treatmentTextLabelBottomConstraint: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
