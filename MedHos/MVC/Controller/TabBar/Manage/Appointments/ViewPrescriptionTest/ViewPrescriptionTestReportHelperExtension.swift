//
//  ViewPrescriptionTestReportHelperExtension.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 08/05/19.
//  Copyright © 2019 Balaji G. All rights reserved.
//

import UIKit

extension ViewPrescriptionTestReportViewController {
    
    func getReportCell(_ indexPath: IndexPath) -> PrescriptionTestReportTableViewCell {
        
        let cell: PrescriptionTestReportTableViewCell = Bundle.main.loadNibNamed("PrescriptionTestReportTableViewCell", owner: nil, options: nil)![0] as! PrescriptionTestReportTableViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.itemView.backgroundColor = UIColor.white
        cell.itemView.setRoundedCorner()
        
        cell.itemTextLabel1.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.itemTextLabel2.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.itemTextLabel3.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.itemTextLabel4.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.itemTextLabel1.textColor = UIColor.black
        cell.itemTextLabel2.textColor = UIColor.black
        cell.itemTextLabel3.textColor = UIColor.black
        cell.itemTextLabel4.textColor = UIColor.black
        
        let dict = self.infoDataArray[indexPath.row - 1]
        
        var item1text = ""
        var item2text = ""
        var item3text = ""
        var item4text = ""
        
        if self.reportType == "PatDrug" {
            
            item1text = "\(dict["DrugType"] ?? "") : \((dict["DrugName"] ?? ""))"
            
            if dict["Benefit"] as? Int == 1 {
                
                item2text = "AF | Dosage-\(dict["Dosage"] ?? "0") | M-\(dict["Morning"] ?? "0") | A-\(dict["Noon"] ?? "0") | N-\(dict["Night"] ?? "0")"
            } else if dict["Benefit"] as? Int == 2 {
                
                item2text = "BF | Dosage-\(dict["Dosage"] ?? "0") | M-\(dict["Morning"] ?? "0") | A-\(dict["Noon"] ?? "0") | N-\(dict["Night"] ?? "0")"
            }
            
            item3text = "Start Date: \(dict["SDate"] ?? "")"
            
            item4text = "No of Days: \(dict["Days"] ?? "")"
            
        } else {
            
            item1text = "Test Name : \(dict["TestName"] ?? "")"
            item2text = "\(dict["BenefitData"] ?? "") | \(dict["SessionData"] ?? "")"
       
            if dict["Instruction"] as? String != nil {
                item3text = "Instruction : \(dict["Instruction"] ?? "")"
            } else {
                item3text = ""
            }
            
            cell.itemTextLabel3.numberOfLines = 0
        }
        
        cell.itemTextLabel1.text = item1text
        cell.itemTextLabel2.text = item2text
        cell.itemTextLabel3.text = item3text
        cell.itemTextLabel4.text = item4text
        
        return cell
    }
    
    func getVisitDetailsCell(_ indexPath: IndexPath) -> ViewReportTableViewCell {
        
        let cell: ViewReportTableViewCell = Bundle.main.loadNibNamed("ViewReportTableViewCell", owner: nil, options: nil)![0] as! ViewReportTableViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.ReportView.backgroundColor = UIColor.white
        cell.ReportView.setRoundedCorner()
        
        var profileImage = ""
        if gender == "M" {
            profileImage = "ic_male"
        } else if gender == "F" {
            profileImage = "ic_female"
        } else {
            profileImage = "ic_user_manual"
        }
        
        cell.PatientProfileImageView.image = UIImage.init(named: profileImage)
        cell.PatientProfileImageView.contentMode = .scaleAspectFill
        cell.PatientProfileImageView.layer.masksToBounds = true
        cell.PatientProfileImageView.layer.cornerRadius = cell.PatientProfileImageView.bounds.height / 2
        
        cell.PatientNameTextLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.PatientNameTextLabel.text = self.infoArray[0][TEXT]

        self.setupVistLabelStyle(cell.dateKeyLabel, text: self.infoArray[1][PLACEHOLDER]!)
        self.setupVistLabelStyle(cell.appNoKeyLabel, text: self.infoArray[2][PLACEHOLDER]!)
        self.setupVistLabelStyle(cell.ageKeyLabel, text: self.infoArray[3][PLACEHOLDER]!)
        self.setupVistLabelStyle(cell.nextVisitKeyLabel, text: self.infoArray[4][PLACEHOLDER]!)
        self.setupVistLabelStyle(cell.observationKeyLabel, text: self.infoArray[5][PLACEHOLDER]!)
        self.setupVistLabelStyle(cell.treatmentKeyLabel, text: self.infoArray[6][PLACEHOLDER]!)
        
        self.setupVistLabelStyle(cell.dateTextLabel, text: self.infoArray[1][TEXT]!, color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.appNoTextLabel, text: self.infoArray[2][TEXT]!, color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.ageTextLabel, text: self.infoArray[3][TEXT]!, color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.nextVisitTextLabel, text: self.infoArray[4][TEXT]!, color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.observationTextLabel, text: self.infoArray[5][TEXT]!, color: UIColor.darkGray)
        self.setupVistLabelStyle(cell.treatmentTextLabel, text: self.infoArray[6][TEXT]!, color: UIColor.darkGray)
        
        cell.observationTextLabel.numberOfLines = self.ObservationRowsCount
        cell.treatmentTextLabel.numberOfLines = self.TreatmentRowsCount
        
        cell.observationExtendButton.setTitle(self.Observation_Button_Title, for: .normal)
        cell.observationExtendButton.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.observationExtendButton.setTitleColor(UIColor(netHex: APP_PRIMARY_COLOR), for: .normal)
        cell.observationExtendButton.addTarget(self, action: #selector(ObservationRowIncrementor), for: .touchUpInside)
        
        let rowcount = cell.observationTextLabel.calculateMaxLines()
        if rowcount > 2
        {
            cell.observationExtendButton.alpha = 1
        } else {
            
            cell.observationExtendButton.alpha = 0
        }
        
        cell.treatmentExtendButton.setTitle(self.Treatment_Button_Title, for: .normal)
        cell.treatmentExtendButton.titleLabel?.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 12)
        cell.treatmentExtendButton.setTitleColor(UIColor(netHex: APP_PRIMARY_COLOR), for: .normal)
        cell.treatmentExtendButton.addTarget(self, action: #selector(TreatmentRowIncrementor), for: .touchUpInside)
        
        let rowcount2 = cell.treatmentTextLabel.calculateMaxLines()
        if rowcount2 > 2
        {
            cell.treatmentExtendButton.alpha = 1
        } else {
            
            cell.treatmentExtendButton.alpha = 0
        }
        
        return cell
    }
    
    func setupVistLabelStyle(_ label: UILabel, text: String, color: UIColor = UIColor.black) {
        
        label.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        label.textColor = color
        label.text = text
    }
    
    @objc func ObservationRowIncrementor()
    {
        print("ob")
        if self.ObservationRowsCount == 2
        {
            self.Observation_Button_Title = "See less"
            self.ObservationRowsCount = 0
        }
        else
        {
            self.Observation_Button_Title = "See more"
            self.ObservationRowsCount = 2
        }
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }
    }
    
    @objc func TreatmentRowIncrementor()
    {
        print("tm")
        if self.TreatmentRowsCount == 2
        {
            self.Treatment_Button_Title = "See less"
            self.TreatmentRowsCount = 0
        }
        else
        {
            self.Treatment_Button_Title = "See more"
            self.TreatmentRowsCount = 2
        }
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }
    }
    
}
