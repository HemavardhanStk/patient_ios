//
//  ViewPrescriptionTestReportAPIExtension.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 08/05/19.
//  Copyright © 2019 Balaji G. All rights reserved.
//

import UIKit

extension ViewPrescriptionTestReportViewController {
    
    func loadReportAPICall() {
        
        let params = ["isrequestfromuser" : "Y",
                      "AppointmentNo"     : self.appNo,
                      "Language"          : "English"] as Dictionary<String, String>
        
        HttpManager.shared.loadAPICall(path: PATH_GetPatientPrescription, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                self.infoDataArray = []
                if let list = response[self.reportType] as? [Dictionary<String, Any>] {
                    self.infoDataArray = list
                }
                if let value = response["PatPresc"] as? Dictionary<String, Any> {
                    
                    self.date = value["AppointmentDate"] as? String ?? ""
                    self.age = value["Age"] as? String ?? ""
                    self.gender = value["Gender"] as? String ?? ""
                    self.nextVisitDate = value["NextVisit"] as? String ?? ""
                    self.observation = value["Observation"] as? String ?? ""
                    self.treatment = value["Treatement"] as? String ?? ""
                }
                self.loadInfoData()
                self.tableView.reloadData()
            }
        }) {}
    }
 }
