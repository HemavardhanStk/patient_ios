//
//  ViewPrescriptionTestReportViewController.swift
//  MedHos Doctor
//
//  Created by Hemavardhan on 08/05/19.
//  Copyright © 2019 Balaji G. All rights reserved.
//

import UIKit

class ViewPrescriptionTestReportViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, String>]()
    var infoDataArray = [Dictionary<String, Any>]()
    
    var name = ""
    var date = ""
    var appNo = ""
    var age = ""
    var gender = ""
    var nextVisitDate = ""
    var observation = ""
    var treatment = ""
    var reportType = ""
    
    var ObservationRowsCount = 2
    var Observation_Button_Title = "See more"
    
    var TreatmentRowsCount = 2
    var Treatment_Button_Title = "See more"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        if self.reportType == "PatDrug" {
            Navigation.shared.setTitle(title: "Prescription", controller: self)
        } else {
            Navigation.shared.setTitle(title: "Test Details", controller: self)
        }
        
        self.setupColors()
        
        self.loadReportAPICall()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "PatientHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "PatientHistoryTableViewCell")
        self.tableView.register(UINib(nibName: "ViewReportTableViewCell", bundle: nil), forCellReuseIdentifier: "ViewReportTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
        
        self.tableView.setRoundedCorner()
    }
    
    func loadInfoData() {
        
        var age = "-"
        var nextVisitDate = "-"
        var observation = "-"
        var treatment = "-"
        
        if self.age != "" {
            age = self.age
        }
        
        if self.nextVisitDate != "" {
            
            nextVisitDate = Helper.shared.convert(date: self.nextVisitDate, from: "MM/dd/yyyy HH:mm:ss a", to: "d MMM yyyy")
        }
        
        if self.observation != "" {
            observation = self.observation
        }
        
        if self.treatment != "" {
            treatment = self.treatment
        }
        
        var dict = [PLACEHOLDER:"",TEXT:""]
        
        dict = [PLACEHOLDER:"Name",TEXT: self.name]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER:"Date",TEXT: self.date]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER: "App No",TEXT: self.appNo]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER: "Age",TEXT: age]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER:"Next Visit Date",TEXT: nextVisitDate]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER:"Observation :",TEXT: observation]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER:"Treatment :",TEXT: treatment]
        self.infoArray.append(dict)
        
    }
    
}
