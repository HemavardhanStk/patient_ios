//
//  AppointmentTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 02/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var appointmentView: UIView!
    @IBOutlet weak var TokenImageWidth: NSLayoutConstraint!
    @IBOutlet weak var AppDateLabelName: UILabel!
    @IBOutlet weak var ClockImageWidth: NSLayoutConstraint!
    @IBOutlet weak var ClockImageTrailing: NSLayoutConstraint!
    @IBOutlet weak var AppointmentDateLabel: UILabel!
    @IBOutlet weak var BookedForLabel: UILabel!
    @IBOutlet weak var DoctorProfileImageView: CustomImageView!
    @IBOutlet weak var DoctorNameLabel: UILabel!
    @IBOutlet weak var DoctorSpecializationLabel: UILabel!
    @IBOutlet weak var AppointmentStatusLabel: UILabel!
    //@IBOutlet weak var AppointmentStatusLabelHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
