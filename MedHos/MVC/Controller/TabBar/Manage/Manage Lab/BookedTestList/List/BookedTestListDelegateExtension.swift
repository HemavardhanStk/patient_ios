//
//  BookedTestListDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension BookedTestListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getBookedTestListCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = AppController.shared.getViewBookedTestViewController()
        viewController.DiagnosticLabAppGuid = self.infoArray[indexPath.row]["DiagnosticLabAppGuid"] as? String ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}
