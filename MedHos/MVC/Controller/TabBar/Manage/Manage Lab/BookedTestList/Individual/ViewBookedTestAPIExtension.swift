//
//  ViewBookedTestAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 13/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewBookedTestViewController {
    
    func getBookedLabAppDetails() {
        
        let params = ["DiagnosticLabAppGuid"    : self.DiagnosticLabAppGuid,
                      "Language"                : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_ViewLabTestDetailsPatient, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let value2 = value["LabTestData"] as? Dictionary<String,Any> {
                    self.button.alpha = 1
                    self.infoArray = value2
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
    func loadCancelTestAPICall(_ reason: String) {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["CancelledReason" :    reason,
                      "Language"        :    "English",
                      "LabTestID"       :    "\(self.infoArray["LabTestID"] as? Int ?? 0)",
                      "UserNo"          :    user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_CancelLabTest, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any> {
                    AlertHelper.shared.showAlertWithHandler(message: value["Message"] as? String ?? "Succefull", handler: "popViewController", withCancel: false, controller: self)
                }
            }
        }) {}
    }
    
}
