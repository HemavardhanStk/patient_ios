//
//  ViewBookedTestDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 13/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewBookedTestViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.infoArray.count != 0 ? 5:0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 1:
            if let value = self.infoArray["TestRequestUploadFileName"] as? String, value == "" {
                return CGFloat.leastNonzeroMagnitude
            }
            
        case 2:
            if let value = self.infoArray["TestList"] as? [Dictionary<String,Any>], value.count == 0 {
                return CGFloat.leastNonzeroMagnitude
            }
            
        case 3:
            if let value = self.infoArray["CancelledTestList"] as? [Dictionary<String,Any>], value.count == 0 {
                return CGFloat.leastNonzeroMagnitude
            }
            
        default:break
            
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getlabAppointmentStatusCell()
            
        case 1:
            if let value = self.infoArray["TestRequestUploadFileName"] as? String, value != "" {
                return self.getTestPrescriptionCell()
            }
            
        case 2:
            if let value = self.infoArray["TestList"] as? [Dictionary<String,Any>], value.count != 0 {
                return self.getLabAppTestListCell(BOOKED_LIST)
            }

        case 3:
            if let value = self.infoArray["CancelledTestList"] as? [Dictionary<String,Any>], value.count != 0 {
                return self.getLabAppTestListCell(CANCELLED_LIST)
            }
            
        case 4:
            return self.getLabDetailsCell()
            
        default:break
            
        }
        return UITableViewCell()
    }
    
}

extension ViewBookedTestViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView.tag == self.testCollectionViewTag {
            return 1
        } else if collectionView.tag == self.labCollectionViewTag {
            
            if let imagesDict = self.infoArray["LabImages"] as? [Dictionary<String,Any>] {
                return imagesDict.count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 79, height: 79)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionID", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        var urlStrings = [String]()
        
        if collectionView.tag == self.testCollectionViewTag {
             urlStrings.append(self.infoArray["TestRequestUploadFileName"] as? String ?? "")
        } else if collectionView.tag == self.labCollectionViewTag {
            if let imagesDict = self.infoArray["LabImages"] as? [Dictionary<String,Any>] {
                for i in 0..<imagesDict.count {
                    urlStrings.append(imagesDict[i]["ImageUrl"] as? String ?? "")
                }
            }
        }
        if urlStrings[indexPath.row] != "" {
         
            let url = URL(string: urlStrings[indexPath.row])!
            cell.imageView.cm_setImage(from: url, contentMode: .scaleAspectFill,placeholderImage: "loading" ) // we are using this method because alamofire is not handling the urls provided by this service
        }
        
        cell.imageView.viewInFullScreen()
        cell.imageView.setRoundedCorner()
        
        return cell
    }
    
}
