//
//  ViewCancelledTestHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

extension ViewCancelledTestViewController {
    
    func getlabAppointmentStatusCell() -> LabAppointmentDetailsTableViewCell {
        
        let cell: LabAppointmentDetailsTableViewCell = Bundle.main.loadNibNamed("LabAppointmentDetailsTableViewCell", owner: nil, options: nil)![0] as! LabAppointmentDetailsTableViewCell
        
        cell.backgroundColor = .clear
        cell.labContainerView.setRoundedCorner()
        
        cell.appointmentLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.dateTimeLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.PatientNameLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.appStatusTitleLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        cell.appStatusLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.tokenNoImageViewWidthConstraint.constant = 0
        cell.tokenNoTrailingConstraint.constant = 0
        cell.clockImageView.alpha = 0
        
        cell.appointmentLabel.text = "Appointment Date :"
        cell.dateTimeLabel.text = "\(self.infoArray["AppDateTime"] as? String ?? "") / \(self.infoArray["AppointmentTime"] as? String ?? "")"
        cell.PatientNameLabel.text = "Booked for \(self.infoArray["PatientName"] as? String ?? "")"
        cell.appStatusTitleLabel.text = "Appointment Status"
        cell.appStatusLabel.text = self.infoArray["Status"] as? String ?? ""
        
        return cell
    }
    
    func getTestPrescriptionCell() -> TestPrescriptionTableViewCell {
        
        let cell: TestPrescriptionTableViewCell = Bundle.main.loadNibNamed("TestPrescriptionTableViewCell", owner: nil, options: nil)![0] as! TestPrescriptionTableViewCell
        
        cell.backgroundColor = .clear
        
        cell.testPrescriptionContainerView.setRoundedCorner()
        
        cell.titleLabel.text = "Test Prescription"
        cell.titleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.titleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        
        cell.testPrescriptionCollectionView.delegate = self
        cell.testPrescriptionCollectionView.dataSource = self
        cell.testPrescriptionCollectionView.tag = self.testCollectionViewTag
        cell.testPrescriptionCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        
        return cell
        
    }
    
    func getLabAppTestListCell() -> LabBookCancelListTableViewCell {
        
        let cell: LabBookCancelListTableViewCell = Bundle.main.loadNibNamed("LabBookCancelListTableViewCell", owner: nil, options: nil)![0] as! LabBookCancelListTableViewCell
        
        cell.backgroundColor = .clear
        
        cell.labBookCancelContainerView.setRoundedCorner()
        
        cell.titleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.subLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.titleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.subLabel.textColor = UIColor.darkGray
        
        cell.subLabel.numberOfLines = 0
        
        var titleString = String()
        var subListString = String()
        
        if let value = self.infoArray["PackageYorN"] as? String,
            let value1 = self.infoArray["PackageName"] as? String,
            value == "Y" {
            
            titleString = "Package Opted :"
            subListString = value1
            
            cell.viewDetailsButtonWidthConstraint.constant = 90
            cell.viewDetailsButton.addTarget(self, action: #selector(gotoViewPackagePage), for: .touchUpInside)
            
        } else if let value = self.infoArray["TestList"] as? [Dictionary<String,Any>] {
            
            titleString = "Test Opted :"
            
            for i in 0..<value.count {
                if i == value.count - 1 {
                    if let stringValue = value[i]["TestName"] as? String {
                        subListString.append(stringValue)
                    }
                } else {
                    if let stringValue = value[i]["TestName"] as? String {
                        subListString.append("\(stringValue)\n")
                    }
                }
            }
        }

        cell.titleLabel.text = titleString
        cell.subLabel.text = subListString
        
        return cell
    }
    
    func getLabDetailsCell() -> LabAddressTableViewCell {
        
        let cell: LabAddressTableViewCell = Bundle.main.loadNibNamed("LabAddressTableViewCell", owner: nil, options: nil)![0] as! LabAddressTableViewCell
        
        cell.backgroundColor = .clear
        cell.labContainerView.setRoundedCorner()
        
        cell.labAddressLabel.numberOfLines = 0
        
        cell.labNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.labAddressLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.mapTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        
        cell.labNameLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.labAddressLabel.textColor = UIColor.darkGray
        cell.mapTitleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        
        cell.labNameLabel.text = self.infoArray["LabName"] as? String ?? ""
        cell.labAddressLabel.text = self.infoArray["LabAddress"] as? String ?? ""
        cell.mapTitleLabel.text = "Map"
        
        cell.labImageView.delegate = self
        cell.labImageView.dataSource = self
        cell.labImageView.tag = self.labCollectionViewTag
        cell.labImageView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        
        if let imagesDict = self.infoArray["LabImages"] as? [Dictionary<String,Any>] {
            cell.labImageViewHeightConstraint.constant = imagesDict.count == 0 ? 0 : 80
        }
        
        self.sloadView(lat: self.infoArray["Latitude"] as? Double ?? 0.0, long: self.infoArray["Longitude"] as? Double ?? 0.0, View: cell.mapView)
        
        return cell
    }
    
    @objc func sloadView(lat:Double,long:Double,View:GMSMapView) {
        
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        View.camera = GMSCameraPosition(target: coord, zoom: 14, bearing: 0, viewingAngle: 0)
        
        let marker = GMSMarker()
        
        marker.appearAnimation = GMSMarkerAnimation(rawValue: 1)!
        marker.position = coord
        marker.map = View
    }
    
    @objc func gotoViewPackagePage() {
        let viewController = AppController.shared.getViewTestPackageViewController()
        viewController.packageId = "\(self.infoArray["PackageNo"] as? Int ?? 0)"
        viewController.bottomViewHeight = 0
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
