//
//  CancelledTestListAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension CancelledTestListViewController {
    
    func getCancelledTestListAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["Gender"        : user.gender,
                      "Language"      : "English",
                      "MobileNumber"  : user.phone,
                      "UserName"      : user.name,
                      "UserNo"        : user.id]
        
        HttpManager.shared.loadAPICall(path: PATH_GetLabPatientCancelledList, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let value2 = value["CancelledList"] as? [Dictionary<String,Any>]{
                    self.infoArray = value2
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
}

