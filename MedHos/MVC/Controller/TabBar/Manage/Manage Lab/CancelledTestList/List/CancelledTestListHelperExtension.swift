//
//  CancelledTestListHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension CancelledTestListViewController {
    
    // MARK: - Lab List Cell
    
    func getCancelledTestListCell(_ indexPath: IndexPath) -> LabListTableViewCell {
        
        let cell: LabListTableViewCell = Bundle.main.loadNibNamed("LabListTableViewCell", owner: nil, options: nil)![0] as! LabListTableViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        cell.labDetailsView.backgroundColor = UIColor.white
        cell.labDetailsView.setRoundedCorner()
        
        cell.appointmentNoLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.dateTimeLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.patientNameLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.labNameLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        cell.labAddressLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.appointmentNoLabel.textColor = UIColor.black
        cell.dateTimeLabel.textColor = UIColor.black
        cell.patientNameLabel.textColor = UIColor.darkGray
        cell.labNameLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.labAddressLabel.textColor = UIColor.darkGray
        
        let item = self.infoArray[indexPath.row]
        
        cell.appointmentNoLabel.text = "\(item["LabTestID"] as? Int ?? 0)"
        cell.dateTimeLabel.text = "\(item["AppDateTime"] as? String ?? "") / \(item["AppointmentTime"] as? String ?? "")"

        if let value1 = item["TestPackageName"] as? String,
            let value2 = item["PatientName"] as? String {
            
            var attStringOne = NSMutableAttributedString()
            var attStringTwo = NSMutableAttributedString()
        
            attStringOne = NSMutableAttributedString(string: value1)
            attStringOne.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_BOLD, size: 15)!, range: NSMakeRange(0, value1.count))
            attStringOne.setColor(color: UIColor.black, forText: value1)
        
            let string = " Booked For \(value2)"
            attStringTwo = NSMutableAttributedString(string: string)
            attStringTwo.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_NORMAL, size: 14)!, range: NSMakeRange(0, string.count))
            attStringTwo.setColor(color: UIColor.darkGray, forText: string)
            
            attStringOne.append(attStringTwo)
            
            cell.patientNameLabel.attributedText = attStringOne
        }
        
        cell.labNameLabel.text = item["LabName"] as? String ?? ""
        cell.labAddressLabel.text = "\(item["AreaName"] as? String ?? ""), \(item["CityName"] as? String ?? "") "
        
        return cell
    }
    
    
}

