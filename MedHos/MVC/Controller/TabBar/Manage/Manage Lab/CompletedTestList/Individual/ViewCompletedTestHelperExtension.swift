//
//  ViewCompletedTestHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewCompletedTestViewController {
    
    func getLabProfileAppointmentDateCell() -> PreBookingLabProfileTableViewCell {
        
        let cell: PreBookingLabProfileTableViewCell = Bundle.main.loadNibNamed("PreBookingLabProfileTableViewCell", owner: nil, options: nil)![0] as! PreBookingLabProfileTableViewCell
        
        cell.labTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        cell.labAddressLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.labTitleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        cell.labAddressLabel.textColor = UIColor.black
        
        cell.labTitleLabel.text = self.infoArray["LabName"] as? String ?? ""
        cell.labAddressLabel.text = "Appointment Date : \(self.infoArray["AppDateTime"] as? String ?? "")"
        
        if let urlString = self.infoArray["LabLogoImage"] as? String,
            let url = URL(string: urlString) {
            cell.labLogoImageView.af_setImage(withURL: url)
        }
        cell.labLogoImageView.layer.cornerRadius = 35
        cell.labLogoImageView.layer.masksToBounds = true
        
        return cell
    }
    
    func getLabAppTestListCell() -> LabBookCancelListTableViewCell {
        
        let cell: LabBookCancelListTableViewCell = Bundle.main.loadNibNamed("LabBookCancelListTableViewCell", owner: nil, options: nil)![0] as! LabBookCancelListTableViewCell
        
        cell.backgroundColor = .clear
        
        cell.labBookCancelContainerView.setRoundedCorner()
        
        cell.titleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.subLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        cell.titleLabel.textColor = UIColor.black
        cell.subLabel.textColor = UIColor.darkGray
        
        cell.subLabel.numberOfLines = 0
        
        var subListString = String()
        
        if let value = self.infoArray["CancelledTestList"] as? [Dictionary<String,Any>] {
            for i in 0..<value.count {
                if i == value.count - 1 {
                    if let stringValue = value[i]["TestName"] as? String {
                        subListString.append(stringValue)
                    }
                } else {
                    if let stringValue = value[i]["TestName"] as? String {
                        subListString.append("\(stringValue)\n")
                    }
                }
            }
        }
        
        cell.titleLabel.text = "Cancelled Tests :"
        cell.subLabel.text = subListString
        
        return cell
    }
    
    func getTestPrescriptionCell() -> TestPrescriptionTableViewCell {
        
        let cell: TestPrescriptionTableViewCell = Bundle.main.loadNibNamed("TestPrescriptionTableViewCell", owner: nil, options: nil)![0] as! TestPrescriptionTableViewCell
        
        cell.backgroundColor = .clear
        
        cell.testPrescriptionContainerView.setRoundedCorner()
        
        cell.titleLabel.text = "Test Prescription"
        cell.titleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 14)
        cell.titleLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        
        cell.testPrescriptionCollectionView.delegate = self 
        cell.testPrescriptionCollectionView.dataSource = self
        cell.testPrescriptionCollectionView.tag = self.prescriptionCollectionViewTag
        cell.testPrescriptionCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        
        return cell
        
    }
    
    func getTestReportCell(_ indexPath: IndexPath) -> TestPrescriptionTableViewCell {
        
        let cell: TestPrescriptionTableViewCell = Bundle.main.loadNibNamed("TestPrescriptionTableViewCell", owner: nil, options: nil)![0] as! TestPrescriptionTableViewCell

        cell.backgroundColor = .clear
        
        cell.testPrescriptionContainerView.setRoundedCorner()
        
        cell.titleLabel.numberOfLines = 0
        
        if let value = self.infoArray["TestReportDocuments"] as? [Dictionary<String, Any>],
            let value1 = value[indexPath.row - 3]["ReportName"] as? String,
            let value2 = value[indexPath.row - 3]["TestList"] as? [String] {
            
            var testString = String()
            
            for i in 0..<value2.count {
                if i == value2.count - 1 {
                    testString.append(value2[i])
                } else {
                    testString.append("\(value2[i]),")
                }
            }
            
                let attrs1 = [NSAttributedString.Key.font : UIFont(name: APP_FONT_NAME_BOLD, size: 15)!, NSAttributedString.Key.foregroundColor : UIColor.black]
                let attrs2 = [NSAttributedString.Key.font : UIFont(name: APP_FONT_NAME_NORMAL, size: 14)!, NSAttributedString.Key.foregroundColor : UIColor.darkGray]
            
                let attributedString1 = NSMutableAttributedString(string: "\(value1)\n", attributes: attrs1)
                let attributedString2 = NSMutableAttributedString(string: testString, attributes: attrs2)
            
                attributedString1.append(attributedString2)
            
                cell.titleLabel.attributedText = attributedString1
            }
        
        cell.testPrescriptionCollectionView.delegate = self
        cell.testPrescriptionCollectionView.dataSource = self
        cell.testPrescriptionCollectionView.tag = indexPath.row - 3
        cell.testPrescriptionCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionID")
        
        return cell
    }
    
}
