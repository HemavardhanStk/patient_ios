//
//  ViewCompletedTestViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class ViewCompletedTestViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = Dictionary<String, Any>()
    
    var prescriptionCollectionViewTag = 23
    
    var DiagnosticLabAppGuid = ""
    
    let BOOKED_LIST = "Bookedlist"
    let CANCELLED_LIST = "CancelledList"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.setTitle(title: "Completed Test", controller: self)
        
        self.setUpColors()
        self.loadInfoData()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabListTableViewCell", bundle: nil), forCellReuseIdentifier: "LabListTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.estimatedRowHeight = 130
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.allowsSelection = false
    }
    
    func setUpColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.clear
    }
    
    func loadInfoData() {
        
        self.getCompletedLabAppDetails()
    }

}
