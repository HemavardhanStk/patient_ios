//
//  ViewCompletedTestDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewCompletedTestViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let value = self.infoArray["TestReportDocuments"] as? [Dictionary<String, Any>] {
            
            return value.count + 3
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 0:
            return 100
            
        case 1:
            if let value = self.infoArray["CancelledTestList"] as? [Dictionary<String,Any>], value.count == 0 {
                return CGFloat.leastNonzeroMagnitude
            }
            
        case 2:
            if let value = self.infoArray["TestRequestUploadFileName"] as? String, value == "" {
                return CGFloat.leastNonzeroMagnitude
            }
       
        default:
            break
            
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.getLabProfileAppointmentDateCell()
            
        case 1:
            return self.getLabAppTestListCell()
            
        case 2:
            return self.getTestPrescriptionCell()
            
        default:
            return self.getTestReportCell(indexPath)
            
        }
    }
    
}

extension ViewCompletedTestViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == self.prescriptionCollectionViewTag {
            return 1
        } else  {

            if let value = self.infoArray["TestReportDocuments"] as? [Dictionary<String, Any>],
            let value2 = value[collectionView.tag]["ReportDocuments"] as? [Dictionary<String, Any>] {
                return value2.count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 79, height: 79)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionID", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        if collectionView.tag == self.prescriptionCollectionViewTag {
            if let urlString = self.infoArray["TestRequestUploadFileName"] as? String,
                let url = URL(string:  urlString) {
                cell.imageView.cm_setImage(from: url, contentMode: .scaleAspectFill,placeholderImage: "loading" )
            }
        } else {
            
            if let value = self.infoArray["TestReportDocuments"] as? [Dictionary<String, Any>],
                let value2 = value[collectionView.tag]["ReportDocuments"] as? [Dictionary<String, Any>] {
                
                if value2[indexPath.row]["FileType"] as? String == "Image",
                    let urlString = value2[indexPath.row]["ImageUrl"] as? String,
                    let url = URL(string: urlString) {
                    cell.imageView.cm_setImage(from: url, contentMode: .scaleAspectFill,placeholderImage: "loading" )
                    cell.imageView.viewInFullScreen()
                } else if value2[indexPath.row]["FileType"] as? String == "PDF" {
                    cell.imageView.image = UIImage(named: "ic_pdf")
                }
            }
        }
        cell.imageView.setRoundedCorner()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag != self.prescriptionCollectionViewTag {
            
            if let value = self.infoArray["TestReportDocuments"] as? [Dictionary<String, Any>],
                let value2 = value[collectionView.tag]["ReportDocuments"] as? [Dictionary<String, Any>] {
                
//                if value2[indexPath.row]["FileType"] as? String == "Image",
//                    let urlString = value2[indexPath.row]["ImageUrl"] as? String,
//                    let url = URL(string: urlString) {
//
//                } else
                    if value2[indexPath.row]["FileType"] as? String == "PDF",
                    let urlString = value2[indexPath.row]["ImageUrl"] as? String {
                    
                    let viewController = WebViewController()
                    viewController.urlString = "https://docs.google.com/gview?embedded=true&url=\(urlString)"
                    viewController.pageTitle = "Test Report"
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                }
            }
            
        }
        
    }
    
}
