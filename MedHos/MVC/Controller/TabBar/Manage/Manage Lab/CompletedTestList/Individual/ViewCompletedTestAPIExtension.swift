//
//  ViewCompletedTestAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 20/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension ViewCompletedTestViewController {
 
    func getCompletedLabAppDetails() {
        
        let params = ["DiagnosticLabAppGuid"    : self.DiagnosticLabAppGuid,
                      "Language"                : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_ViewLabTestDetailsPatient, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response["Result"] as? Dictionary<String, AnyObject> ?? [:], controller: self) {
                if let value = response["Result"] as? Dictionary<String, Any>,
                    let value2 = value["LabTestData"] as? Dictionary<String,Any> {
                    
                    self.infoArray = value2
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
    
}
