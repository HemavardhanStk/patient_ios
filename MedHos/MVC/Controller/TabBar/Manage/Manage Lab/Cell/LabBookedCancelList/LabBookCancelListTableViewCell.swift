//
//  LabBookCancelListTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 14/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabBookCancelListTableViewCell: UITableViewCell {

    @IBOutlet weak var labBookCancelContainerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var viewDetailsButtonWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
