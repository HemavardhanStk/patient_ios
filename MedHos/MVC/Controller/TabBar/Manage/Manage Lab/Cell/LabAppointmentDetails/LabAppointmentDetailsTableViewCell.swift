//
//  LabAppointmentDetailsTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 09/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class LabAppointmentDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var labContainerView: UIView!
    
    @IBOutlet weak var tokenNoImageView: UIImageView!
    @IBOutlet weak var tokenNoImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tokenNoTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var clockImageView: UIImageView!
    
    @IBOutlet weak var appointmentLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var PatientNameLabel: UILabel!
    @IBOutlet weak var appStatusTitleLabel: UILabel!
    @IBOutlet weak var appStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
