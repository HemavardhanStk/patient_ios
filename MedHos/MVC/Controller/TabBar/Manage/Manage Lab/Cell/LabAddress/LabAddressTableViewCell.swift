//
//  LabAddressTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 09/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GoogleMaps

class LabAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var labContainerView: UIView!
    
    @IBOutlet weak var labNameLabel: UILabel!
    @IBOutlet weak var labAddressLabel: UILabel!
    @IBOutlet weak var labImageView: UICollectionView!
    @IBOutlet weak var labImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapTitleLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
