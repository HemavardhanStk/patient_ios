//
//  TestPrescriptionTableViewCell.swift
//  MedHos
//
//  Created by Hemavardhan on 09/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class TestPrescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var testPrescriptionContainerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var testPrescriptionCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
