//
//  TextFieldPopUpViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 29/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol TextFieldPopOverDelegate {
    
    func didSubmitTapped(_ DataArray: [Dictionary<String, String>])
}

class TextFieldPopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var PopUpView: UIView!
    @IBOutlet weak var PopUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var PopUpTitleLabel: UILabel!
    @IBOutlet weak var PopUpCloseButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: TextFieldPopOverDelegate?
    
    var infoArray = [Dictionary<String, String>]()
    var messageIndex: Int = 0
    var PopUpTitle = ""
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if PopUpTitle == "Sort" {
            return self.infoArray.count + 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if PopUpTitle == "Sort" {
            if indexPath.row == self.infoArray.count {
                return getButtonCell()
            }
            return getItemCell(indexPath)
        }
        if indexPath.row == 0
        {
            return getMessageCell(indexPath)
        }
        else if indexPath.row == 1
        {
            return getButtonCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if PopUpTitle == "Sort" {
            if indexPath.row == self.infoArray.count {
                return 60
            }
            return 51
        }
        
        if indexPath.row == 0 {
            return 120
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if PopUpTitle == "Sort" {
            
//            var IndexOfRow : Int? = nil
            
            if self.infoArray[indexPath.row]["Selected"] == "N" {
                
                for i in 0..<self.infoArray.count {
                    if self.infoArray[i]["Selected"] == "Y" && i != indexPath.row {
                        self.infoArray[i]["Selected"] = "N"
//                        IndexOfRow = i
                    }
                    
                    if i == indexPath.row {
                        self.infoArray[i]["Selected"] = "Y"
                    }
                }
            } else {
                self.infoArray[indexPath.row]["Selected"] = "N"
            }
            
            self.tableView.reloadSections(IndexSet([0]), with: .automatic)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        PopUpView.setRoundedCorner()
        PopUpTitleLabel.text = PopUpTitle
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "TextViewTableViewCell", bundle: nil), forCellReuseIdentifier: "TextViewTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.separatorStyle = .none
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = UIColor.white
        self.tableView.showsVerticalScrollIndicator = false
        
        self.PopUpCloseButton.addTarget(self, action: #selector(dismisser), for: .touchUpInside)
        if PopUpTitle == "Sort" {
            loadSortData()
            self.PopUpViewHeightConstraint.constant = 380
        }
        else {
            loadInfoData()
            self.PopUpViewHeightConstraint.constant = 250
        }
        
        // Do any additional setup after loading the view.
    }
    
    func loadInfoData() {
  
        var dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : ""]
        
        dict = [IMAGE: "", PLACEHOLDER : "Reason for Cancellation", TEXT : ""]
        self.infoArray.append(dict)
        
    }
    
    func loadSortData() {
        
        var dict = [TEXT : "", "Selected" : "", "Value":""]
        
        dict = [TEXT : "By Doctor / Clinic Name", "Selected" : "N", "Value":"1"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Consulting Fee - Low - High", "Selected" : "N", "Value":"2"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Consulting Fee - High - Low", "Selected" : "N", "Value":"3"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Distance KM", "Selected" : "N", "Value":"0"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Ratings - High - Low", "Selected" : "N", "Value":"4"]
        self.infoArray.append(dict)

    }
    
    func getItemCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
            cell.itemImageViewWidthConstraint.constant = 8
            cell.itemTitleLabel.text = self.infoArray[indexPath.row][TEXT]
            cell.itemTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        
            cell.itemImageViewWidthConstraint.constant = 20
        
            if self.infoArray[indexPath.row]["Selected"] == "N" {
                cell.itemImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
            } else {
                cell.itemImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
            }
            return cell
    }
    
    // MARK: - Button Cell
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("SUBMIT")
        cell.defaultButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - TextView Cell
    
    func getMessageCell(_ indexPath: IndexPath) -> TextViewTableViewCell {
        
        let cell: TextViewTableViewCell = Bundle.main.loadNibNamed("TextViewTableViewCell", owner: nil, options: nil)![0] as! TextViewTableViewCell
        
        self.setupTextViewPlaceholder(cell.textView)
        cell.textView.delegate = self
        cell.textView.tag = indexPath.row
        cell.textView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        cell.textView.setRoundedCorner()
        
        if self.infoArray[cell.textView.tag][TEXT] == "" {
            cell.textView.text = self.infoArray[cell.textView.tag][PLACEHOLDER]
            cell.textView.textColor = UIColor.lightGray
        } else {
            cell.textView.text = self.infoArray[cell.textView.tag][TEXT]
            cell.textView.textColor = UIColor.black
        }
        
        return cell
    }

    @objc func submitButtonTapped() {
        
        if PopUpTitle == "Sort" {
            
            if delegate != nil {
                
                var value = ""
                
                for i in 0..<self.infoArray.count {
                    if self.infoArray[i]["Selected"] == "Y" {
                        value = self.infoArray[i]["Value"]!
                    }
                }
                
                let dict = ["Value": value]
                
                self.delegate?.didSubmitTapped([dict])
            }
        }
        
        
        if delegate != nil {
            self.delegate?.didSubmitTapped(self.infoArray)
            self.dismisser()
        }
    }

    @objc func dismisser()
    {
        dismiss(animated: true, completion: nil)
    }
    
    
}
// MARK: - TextView Delegate

extension TextFieldPopUpViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            self.setupTextViewText("", textView: textView)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.infoArray[textView.tag][PLACEHOLDER]
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text != self.infoArray[textView.tag][PLACEHOLDER] {
            self.infoArray[textView.tag][TEXT] = textView.text
        }
    }
    
    func setupTextViewPlaceholder(_ textView: UITextView) {
        
        textView.text = self.infoArray[self.messageIndex][PLACEHOLDER]// as! String
        textView.textColor = UIColor.lightGray
    }
    
    func setupTextViewText(_ text: String, textView: UITextView) {
        
        textView.text = text
        textView.textColor = UIColor.black
    }

    
    
}
