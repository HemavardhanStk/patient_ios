//
//  AdPopOverViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 26/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AdPopOverViewController: UIViewController {

    @IBOutlet weak var adView: UIView!
    @IBOutlet weak var adImageView: UIImageView!
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var buttonThree: UIButton!
    
    var infoArray = Dictionary<String, Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        self.adView.setRoundedCorner(15)
        self.buttonTwo.setRoundedCorner(20)
        
        if let title = self.infoArray["PopupCloseButtonText"] as? String {
            self.buttonOne.setTitle(title, for: .normal)
        }
        
        if let title = self.infoArray["PopupActionButtonText"] as? String {
            self.buttonTwo.setTitle(title, for: .normal)
        }
        
        if let title = self.infoArray["TandCButtonText"] as? String {
            self.buttonThree.setTitle(title, for: .normal)
        }
        
        if let imageUrlString = self.infoArray["PopupURL"] as? String,
            let url = URL(string: imageUrlString) {
            self.adImageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "loading"))
        }
        
        self.buttonOne.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        self.buttonTwo.addTarget(self, action: #selector(gotoDoctorCategories), for: .touchUpInside)
        self.buttonThree.addTarget(self, action: #selector(gotoTermsAndConditions), for: .touchUpInside)
    }

    @objc func gotoTermsAndConditions() {
        
        if let presentingController = self.presentingViewController?.children.first as? HomeViewController,
            let urlString = self.infoArray["TandCButtonRedirectionURL"] as? String,
            let title = self.infoArray["TandCButtonText"] as? String {
            
            let viewController = WebViewController()
            viewController.urlString = urlString
            viewController.pageTitle = title
            
            self.dismiss(animated: true) {
                presentingController.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    @objc func gotoDoctorCategories() {
        
        if let presentingController = self.presentingViewController?.children.first as? HomeViewController {
            let viewController = AppController.shared.getDoctorsViewController()
            self.dismiss(animated: true) {
                presentingController.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    @objc func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
