//
//  CitiesPopOverViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 24/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GooglePlaces

protocol CitiesPopOverDelegate {
    
    func didCitySelected(_ Name: String, lat: Double, long: Double)
}

class CitiesPopOverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var PopUpView: UIView!
    @IBOutlet weak var CloseButton: UIButton!
    @IBOutlet weak var CurrentLocationLabel: UILabel!
    @IBOutlet weak var SearchButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: CitiesPopOverDelegate?
    
    var CurrentLocation = ""
    var CurrentLatitude = 0.0
    var CurrentLongitude = 0.0
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CitiesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getItemCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        var indexer = 1000
//        for i in 0...CitiesList.count - 1
//        {
//            if CitiesList[i][STATUS] == SELECTED && i != indexPath.row
//            {
//                indexer = i
//            }
//            if i != indexPath.row || CitiesList[i][STATUS] == SELECTED
//            {
//                CitiesList[i][STATUS] = UN_SELECTED
//            }
//            else
//            {
//                CitiesList[i][STATUS] = SELECTED
//            }
//        }
//
//        if indexer != indexPath.row && indexer != 1000
//        {
//            if let cell = tableView.cellForRow(at: IndexPath(row: indexer, section: 0)) as? LabelTableViewCell {
//                self.setItemSelection(cell, index: indexer)
//            }
//        }
//
//        if let cell = tableView.cellForRow(at: indexPath) as? LabelTableViewCell {
//
//            self.setItemSelection(cell, index: indexPath.row)
//        }
        
        if self.delegate != nil {

            self.delegate?.didCitySelected(CitiesList[indexPath.row]["CityName"]!,lat: 0.0,long: 0.0)
            self.dismiss(animated: true, completion: nil)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = UIColor.white
        
        self.SearchButton.layer.borderWidth = 0.8
        self.SearchButton.layer.borderColor = UIColor.darkGray.cgColor
        self.CurrentLocationLabel.text = self.CurrentLocation
        self.CurrentLocationLabel.isUserInteractionEnabled = true
        self.CurrentLocationLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CurrentLocationCall)))
        
        self.tableView.setRoundedCorner()
        if CitiesList.count == 0
        {
            TopCities()
        }
        SearchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        CloseButton.addTarget(self, action: #selector(dismisser), for: .touchUpInside)
    }
    
    func TopCities()
    {
        let user = Session.shared.getUserInfo()
        let params = [
            "UserID": user.id,
            "Language": "English"
            ] as [String : Any]
        APIManager.shared.makeHTTPPostRequest(path: "http://service.sensure.in/api/User/GetBookingFeeforCity", body: params) { (data, response, error) in
            if error != nil
            {
                print(error!)
                return
            }
            do
            {
                let citiesData = try JSONDecoder().decode(TopCitiesStruct.self, from: data as! Data)
                
                if citiesData.Success == 0
                {
                    for i in 0...citiesData.City.count - 1
                    {
                        let Dict = ["CityName":citiesData.City[i].CityName]
                        CitiesList.append(Dict as! [String : String])
                    }
                    DispatchQueue.main.async {
//                        LoadingIndicator.shared.hide()
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.dismiss(animated: true, completion: nil)
                    AlertHelper.shared.showAlert(message: citiesData.Message, controller: self)
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    func getItemCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        
        cell.itemTitleLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 15)
        cell.itemTitleLabel.text = CitiesList[indexPath.row]["CityName"]
        
        //self.setItemSelection(cell, index: indexPath.row)
        
        return cell
    }

    @objc func searchButtonTapped() {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.country = "IN"
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
        DispatchQueue.main.async {
            self.PopUpView.alpha = 0
        }
    }
    
    @objc func dismisser()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func CurrentLocationCall()
    {
        if self.delegate != nil {
            self.delegate?.didCitySelected(self.CurrentLocation,lat: self.CurrentLatitude,long: self.CurrentLongitude)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
}
extension CitiesPopOverViewController:GMSAutocompleteViewControllerDelegate
{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        var name = ""
        var latitude = 0.0
        var longitude = 0.0
        
        var names = place.formattedAddress?.components(separatedBy: ",")
        if names?.count ?? 0 > 3
        {
            names?.reverse()
            name = " \(names![3]) | \(names![2]) |"
            latitude = place.coordinate.latitude
            longitude = place.coordinate.longitude
        }
        else
        {
            name = place.name!
            for i in 0...CitiesList.count - 1
            {
                if CitiesList[i]["CityName"] != name
                {
                    latitude = place.coordinate.latitude
                    longitude = place.coordinate.longitude
                }
            }
        }
        
        if self.delegate != nil {
            
            self.delegate?.didCitySelected(name,lat: latitude,long: longitude)
            self.dismiss(animated: true, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: {
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

