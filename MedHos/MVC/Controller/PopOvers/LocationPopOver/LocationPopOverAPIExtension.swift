//
//  LocationPopOverAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LocationPopOverViewController {
    
    func getCitiesListAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["UserID": user.id,
                      "Language": "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_GetBookingFeeforCity, params: params, httpMethod: .post, controller: self,isBackgroundCall: true, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self, isBackgroundCall: true) {
                if let value = response["City"] as? [Dictionary<String, Any>] {
                    
                    for i in 0..<value.count {
                        if let city = value[i]["CityName"] as? String {
                            let dict = ["CityName": city]
                            CitiesList.append(dict)
                        }
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }) {}
    }
}
