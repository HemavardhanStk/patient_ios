//
//  LocationPopOverHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GooglePlaces

extension LocationPopOverViewController {
    
    func getItemCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        
        cell.itemTitleLabel.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 15)
        cell.itemTitleLabel.text = CitiesList[indexPath.row]["CityName"]
        
        return cell
    }
    
    @objc func currentLocationCall() {
        if self.delegate != nil {
            self.delegate?.didCitySelected(self.currentLocation,lat: self.currentLatitude,long: self.currentLongitude)
            self.dismiss(animated: true, completion: nil)
        }
    }

    @objc func defaultLocationCall() {
        if self.delegate != nil {
            self.delegate?.didCitySelected(self.defaultLocation,lat: Double(self.defaultLatitude) ?? 0.0,long: Double(self.defaultLongitude) ?? 0.0)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func editDefaultLocationCall() {
        
        if let vc = self.presentingViewController?.children.last as? DoctorCategoriesViewController {
            
            let viewController = AppController.shared.getUserLocationViewController()
//            viewController.parentName = "DoctorCategories"
            self.dismissController()
            vc.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if let vc = self.presentingViewController?.children.last as? DoctorBasicSearchViewController {
            
            let viewController = AppController.shared.getUserLocationViewController()
//            viewController.parentName = "DoctorBasicSearch"
            self.dismissController()
            vc.navigationController?.pushViewController(viewController, animated: true)
        }
        
        if let vc = self.presentingViewController?.children.last as? LabBasicSearchViewController {
            
            let viewController = AppController.shared.getUserLocationViewController()
//            viewController.parentName = "LabBasicSearch"
            self.dismissController()
            vc.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func searchButtonTapped() {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.country = "IN"
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
        
        DispatchQueue.main.async {
            self.popOverView.alpha = 0
        }
    }
    
}
