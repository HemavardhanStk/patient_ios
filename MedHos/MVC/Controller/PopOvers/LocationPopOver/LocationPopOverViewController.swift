//
//  LocationPopOverViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 04/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

var CitiesList = [Dictionary<String,String>]()

protocol LocationPopOverDelegate {
    
    func didCitySelected(_ Name: String, lat: Double, long: Double)
}

class LocationPopOverViewController: UIViewController {

    @IBOutlet weak var popOverView: UIView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var defaultLocationButton: UIButton!
    @IBOutlet weak var editDefaultLocationButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var defaultLocationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: LocationPopOverDelegate?
    
    var currentLocation = ""
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    
    var defaultLocation = ""
    var defaultLatitude = ""
    var defaultLongitude = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    func classAndWidgetsInitialise() {
        
        Navigation.shared.hideNavigation(self)
        
        self.loadInfoData()
        self.popOverView.setRoundedCorner()
        
        self.defaultLocationLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        self.defaultLocationLabel.text = String(self.defaultLocation.dropLast(3))
        
        self.searchButton.layer.borderWidth = 0.8
        self.searchButton.layer.borderColor = UIColor.darkGray.cgColor
        
        self.dismissButton.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        self.currentLocationButton.addTarget(self, action: #selector(currentLocationCall), for: .touchUpInside)
        self.editDefaultLocationButton.addTarget(self, action: #selector(editDefaultLocationCall), for: .touchUpInside)
        self.searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = UIColor.white
        
    }
    
    func loadInfoData() {
        
        let defaultAddress = Session.shared.getUserDefaultAddress()
        
        if defaultAddress.areaName.count != 0 {
            self.defaultLocation = defaultAddress.areaName + " | " + defaultAddress.cityName + " | "
            self.defaultLatitude = defaultAddress.latitude
            self.defaultLongitude = defaultAddress.longitude
            
            self.defaultLocationButton.addTarget(self, action: #selector(defaultLocationCall), for: .touchUpInside)
        } else {
            self.defaultLocation = EDIT_LOCATION + "   "
            
            self.defaultLocationButton.addTarget(self, action: #selector(editDefaultLocationCall), for: .touchUpInside)
        }
        
        if CitiesList.count == 0 {
            self.getCitiesListAPICall()
        }
    }
    
    @objc func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
}
