//
//  LocationPopOverDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import GooglePlaces

extension LocationPopOverViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CitiesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getItemCell(indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.delegate != nil {
            
            self.delegate?.didCitySelected(CitiesList[indexPath.row]["CityName"]!,lat: 0.0,long: 0.0)
            self.dismiss(animated: true, completion: nil)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension LocationPopOverViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        var name = ""
        var latitude = 0.0
        var longitude = 0.0
        
        var names = place.formattedAddress?.components(separatedBy: ",")
        if names?.count ?? 0 > 3
        {
            names?.reverse()
            name = " \(names![3]) | \(names![2]) |"
            latitude = place.coordinate.latitude
            longitude = place.coordinate.longitude
        }
        else
        {
            name = place.name!
            for i in 0...CitiesList.count - 1
            {
                if CitiesList[i]["CityName"] != name
                {
                    latitude = place.coordinate.latitude
                    longitude = place.coordinate.longitude
                }
            }
        }
        
        if self.delegate != nil {
            
            self.delegate?.didCitySelected(name,lat: latitude,long: longitude)
            self.dismiss(animated: true, completion: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
