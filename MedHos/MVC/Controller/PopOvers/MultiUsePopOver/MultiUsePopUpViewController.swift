//
//  MultiUsePopUpViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 29/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

protocol MultiUsePopOverDelegate {
    
    func didSubmitTapped(_ DataArray: [Dictionary<String, String>], from: String)
}

class MultiUsePopUpViewController: UIViewController {
    
    @IBOutlet weak var PopUpView: UIView!
    @IBOutlet weak var PopUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var PopUpTitleLabel: UILabel!
    @IBOutlet weak var PopUpCloseButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: MultiUsePopOverDelegate?
    
    var infoArray = [Dictionary<String, String>]()
    var messageIndex: Int = 0
    var popUpTitle = ""
    var contactNumber = ""
    var doctorId = "0"
    var clinicId = "0"
    var sortOrder = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.PopUpView.setRoundedCorner()
        self.PopUpTitleLabel.text = popUpTitle
        
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "TextViewTableViewCell", bundle: nil), forCellReuseIdentifier: "TextViewTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.tableView.separatorStyle = .none
        self.tableView.tableFooterView = UIView.init()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = UIColor.white
        self.tableView.showsVerticalScrollIndicator = false
        
        self.PopUpCloseButton.addTarget(self, action: #selector(dismisser), for: .touchUpInside)
        if self.popUpTitle == "Sort" {
            
            self.loadSortData()
            self.PopUpViewHeightConstraint.constant = 380
            
        } else if self.popUpTitle == "Book Home Visit" {
            
            self.loadBookHomeVisitData()
            self.PopUpViewHeightConstraint.constant = 370
            self.tableView.allowsSelection = false
            
        } else {
            self.loadInfoData()
            self.PopUpViewHeightConstraint.constant = 250
        }
        
    }
    
    func loadInfoData() {
  
        var dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : ""]
        
        dict = [IMAGE: "", PLACEHOLDER : "Reason for Cancellation", TEXT : ""]
        self.infoArray.append(dict)
        
    }
    
    func loadSortData() {
        
        var dict = [TEXT : "", "Selected" : "", "Value":""]
        
        dict = [TEXT : "By Doctor / Clinic Name", "Selected" : "N", "Value":"1"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Consulting Fee - Low - High", "Selected" : "N", "Value":"2"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Consulting Fee - High - Low", "Selected" : "N", "Value":"3"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Distance KM", "Selected" : "N", "Value":"0"]
        self.infoArray.append(dict)

        dict = [TEXT : "By Ratings - High - Low", "Selected" : "N", "Value":"4"]
        self.infoArray.append(dict)

        for i in 0..<self.infoArray.count {
            
            if self.sortOrder == self.infoArray[i]["Value"] {
                self.infoArray[i]["Selected"] = "Y"
            }
            
        }
        
    }

    func loadBookHomeVisitData() {
        
        var dict = [IMAGE: "", PLACEHOLDER : "Name", TEXT : ""]
        
        dict = [IMAGE: "", PLACEHOLDER : "Doctor Mobile No", TEXT : self.contactNumber]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Reason", TEXT : ""]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Home Visit Date", TEXT : ""]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "Preferred Time", TEXT : ""]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "DoctorId", TEXT : self.doctorId]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "", PLACEHOLDER : "ClinicId", TEXT : self.clinicId]
        self.infoArray.append(dict)
        
    }
    
    @objc func submitButtonTapped() {
        
        if self.popUpTitle == "Sort" {
            
            if delegate != nil {
                
                var value = ""
                
                for i in 0..<self.infoArray.count {
                    if self.infoArray[i]["Selected"] == "Y" {
                        value = self.infoArray[i]["Value"]!
                    }
                }
                
                let dict = ["Value": value]
                
                self.delegate?.didSubmitTapped([dict], from: self.popUpTitle)
                self.dismisser()
            }
            
        } else if self.popUpTitle == "Book Home Visit" {
            
            if self.isValidationSuccess() {
                
                if delegate != nil {
                    
                    self.delegate?.didSubmitTapped(self.infoArray, from: self.popUpTitle)
                    self.dismisser()
                }
            }
            
        } else if self.popUpTitle == "" {
            
            if delegate != nil {
                self.delegate?.didSubmitTapped(self.infoArray, from: self.popUpTitle)
                self.dismisser()
            }
        }
    }

    @objc func dismisser() {
        dismiss(animated: true, completion: nil)
    }
    
}

