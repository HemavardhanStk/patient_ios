//
//  MultiUsePopUpDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 26/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension MultiUsePopUpViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.popUpTitle == "Sort" {
            
            return self.infoArray.count + 1
            
        } else if self.popUpTitle == "Book Home Visit" {
            
            return self.infoArray.count - 1
            
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.popUpTitle == "Sort" {
            return indexPath.row == self.infoArray.count ? self.getButtonCell() : self.getItemCell(indexPath)
        }
        
        if self.popUpTitle == "Book Home Visit" {
            return indexPath.row == self.infoArray.count - 2 ? self.getButtonCell() : self.getTextFieldCell(indexPath)
        }
        
        if indexPath.row == 0 {
            return self.getMessageCell(indexPath)
        } else if indexPath.row == 1 {
            return self.getButtonCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.popUpTitle == "Sort" {
            
            if indexPath.row == self.infoArray.count {
                return 60
            }
            return 51
        }
        
        if self.popUpTitle == "Book Home Visit" {
            return 60
        }
        
        if indexPath.row == 0 {
            return 120
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.popUpTitle == "Sort" {
            
            //            var IndexOfRow : Int? = nil
            
            if self.infoArray[indexPath.row]["Selected"] == "N" {
                
                for i in 0..<self.infoArray.count {
                    if self.infoArray[i]["Selected"] == "Y" && i != indexPath.row {
                        self.infoArray[i]["Selected"] = "N"
                        //                        IndexOfRow = i
                    }
                    
                    if i == indexPath.row {
                        self.infoArray[i]["Selected"] = "Y"
                    }
                }
            } else {
                self.infoArray[indexPath.row]["Selected"] = "N"
            }
            
            self.tableView.reloadSections(IndexSet([0]), with: .automatic)
        }
        
    }
    
}

extension MultiUsePopUpViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.infoArray[textField.tag - 1][TEXT] = textField.text
    }
    
}

// MARK: - TextView Delegate

extension MultiUsePopUpViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            self.setupTextViewText("", textView: textView)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.infoArray[textView.tag][PLACEHOLDER]
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text != self.infoArray[textView.tag][PLACEHOLDER] {
            self.infoArray[textView.tag][TEXT] = textView.text
        }
    }
    
}
