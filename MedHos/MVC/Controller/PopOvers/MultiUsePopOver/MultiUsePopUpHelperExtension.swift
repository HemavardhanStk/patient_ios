//
//  MultiUsePopUpHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 26/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension MultiUsePopUpViewController {
    
    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> TextFieldTableViewCell {
        
        let cell: TextFieldTableViewCell = Bundle.main.loadNibNamed("TextFieldTableViewCell", owner: nil, options: nil)![0] as! TextFieldTableViewCell
        
        if let imageName = self.infoArray[indexPath.row][IMAGE], imageName.count != 0 {
            cell.defaultTextIcon.image = UIImage.init(named: imageName)
        } else {
            cell.defaultTextIconWidthConstraint.constant = 0
        }
        cell.defaultTextField.placeholder = self.infoArray[indexPath.row][PLACEHOLDER]
        cell.defaultTextField.text = self.infoArray[indexPath.row][TEXT]
        cell.defaultTextField.setTextFieldStyle()
        cell.defaultTextField.delegate = self
        cell.defaultTextField.tag = indexPath.row + 1
        
        switch indexPath.row {
        case 0 :
            cell.defaultTextField.isEnabled = false
            
        case 2:
            let datePicker = cell.defaultTextField.addDatePickerView(self)
            datePicker.minimumDate = Date()
            
        case 3 :
            let datePicker = cell.defaultTextField.addDatePickerView(self)
            datePicker.datePickerMode = .time
            datePicker.minimumDate = Date()
            break
       
        default: break
        }
        
        if indexPath.row == self.infoArray.count - 1 {
            cell.defaultTextField.returnKeyType = .done
        } else {
            cell.defaultTextField.returnKeyType = .next
        }
        
        return cell
    }
    
    func getItemCell(_ indexPath: IndexPath) -> LabelTableViewCell {
        
        let cell: LabelTableViewCell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
        cell.itemImageViewWidthConstraint.constant = 8
        cell.itemTitleLabel.text = self.infoArray[indexPath.row][TEXT]
        cell.itemTitleLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        
        cell.itemImageViewWidthConstraint.constant = 20
        
        if self.infoArray[indexPath.row]["Selected"] == "N" {
            cell.itemImageView.image = #imageLiteral(resourceName: "ic_check_box_empty")
        } else {
            cell.itemImageView.image = #imageLiteral(resourceName: "ic_check_box_selected")
        }
        return cell
    }
    
    // MARK: - Button Cell
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("SUBMIT")
        cell.defaultButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - TextView Cell
    
    func getMessageCell(_ indexPath: IndexPath) -> TextViewTableViewCell {
        
        let cell: TextViewTableViewCell = Bundle.main.loadNibNamed("TextViewTableViewCell", owner: nil, options: nil)![0] as! TextViewTableViewCell
        
        self.setupTextViewPlaceholder(cell.textView)
        cell.textView.delegate = self
        cell.textView.tag = indexPath.row
        cell.textView.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        cell.textView.setRoundedCorner()
        
        if self.infoArray[cell.textView.tag][TEXT] == "" {
            cell.textView.text = self.infoArray[cell.textView.tag][PLACEHOLDER]
            cell.textView.textColor = UIColor.lightGray
        } else {
            cell.textView.text = self.infoArray[cell.textView.tag][TEXT]
            cell.textView.textColor = UIColor.black
        }
        
        return cell
    }
    
    func setupTextViewPlaceholder(_ textView: UITextView) {
        
        textView.text = self.infoArray[self.messageIndex][PLACEHOLDER]// as! String
        textView.textColor = UIColor.lightGray
    }
    
    func setupTextViewText(_ text: String, textView: UITextView) {
        
        textView.text = text
        textView.textColor = UIColor.black
    }
    
    // MARK: - Picker Listeners
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            
            if sender.tag == 3 {
                textField.text = Helper.shared.getString(sender.date, format: "dd-MMM-yyyy")
            } else if sender.tag == 4 {
                textField.text = Helper.shared.getString(sender.date, format: APP_DISPLAY_TIME_FORMAT)
            }
            
            self.infoArray[sender.tag - 1][TEXT] = textField.text
        }
    }
    
    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        if let textField = self.tableView.viewWithTag(sender.tag) as? UITextField {
            if textField.text?.count == 0 {
                self.updatePickerValues(senderTag: sender.tag, selectedRow: 0)
            }
            textField.resignFirstResponder()
        }
    }
    
    func updatePickerValues(senderTag: Int, selectedRow: Int) {
        
        if let textField = self.tableView.viewWithTag(senderTag) as? UITextField {
            
            if senderTag == 3 {
                
                let date = Helper.shared.getString(Date(), format: "dd-MMM-yyyy")
                textField.text = date
                
            } else if senderTag == 4 {
                
                let time = Helper.shared.getString(Date(), format: APP_DISPLAY_TIME_FORMAT)
                textField.text = time
            }
            
            self.infoArray[senderTag - 1][TEXT] = textField.text
        }
    }
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        var isValidationSuccess = true
        
        isValidationSuccess = ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: self.infoArray, controller: self)
        
        return isValidationSuccess
    }
    
}
