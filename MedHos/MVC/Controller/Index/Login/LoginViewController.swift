//
//  LoginViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class LoginViewController: UIViewController {

    @IBOutlet weak var contactUsButton: UIButton!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginLogoImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userMobileNumberTextField: ACFloatingTextfield!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var termsLabel: UILabel!
    
    let privacy = "Privacy Policy"
    let terms = "Terms & Conditions"
    var supportMobileNumber = ""
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Navigation.shared.hideNavigation(self)
    }
    
    func classAndWidgetsInitialise() {
        
        self.setupColors()
        self.loadSupportDetailsAPICall()
        
        self.contactUsButton.addTarget(self, action: #selector(showSupportAlert), for: .touchUpInside)
        
        self.loginView.layer.cornerRadius = 15
        self.loginView.layer.masksToBounds = true
        
        if #available(iOS 11.0, *) {
            self.loginView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
            print("not compact")
        }
        
        self.submitButton.setPrimaryButtonStyle("SUBMIT")
        self.submitButton.addTarget(self, action: #selector(loginAPICall), for: .touchUpInside)
        self.submitButton.layer.cornerRadius = 25
        self.submitButton.isUserInteractionEnabled = false
        self.submitButton.layer.backgroundColor = UIColor.lightGray.cgColor
        
        self.userMobileNumberTextField.placeholder = "Mobile Number"
        self.userMobileNumberTextField.setTextFieldStyle()
        self.userMobileNumberTextField.delegate = self
        self.userMobileNumberTextField.keyboardType = .numberPad
        
        let PolicyFullText = "\(self.privacy) & \(self.terms)"
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: PolicyFullText)
        let Privacyrange: NSRange = NSRange.init(location: 0, length: 14)
        let TermsRange: NSRange = NSRange.init(location: 17, length: 18)
        attributedString.setColor(color: UIColor(netHex: APP_PRIMARY_COLOR), forText: self.privacy)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: Privacyrange)
        attributedString.setColor(color: UIColor(netHex: APP_PRIMARY_COLOR), forText: self.terms)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: TermsRange)
        self.termsLabel.attributedText = attributedString
        self.termsLabel.isUserInteractionEnabled = true
        self.termsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel)))
        self.termsLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupColors() {
        self.view.backgroundColor = UIColor(netHex: APP_BG_COLOR)
    }
    
    // MARK: - Handlers
    
    func gotoWebView(_ urlString: String, pageTitle: String) {
        
        let viewController = WebViewController()
        viewController.urlString = urlString
        viewController.pageTitle = pageTitle
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}
