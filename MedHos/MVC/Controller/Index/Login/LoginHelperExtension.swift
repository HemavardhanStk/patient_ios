//
//  LoginHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LoginViewController {
    
    @objc func showSupportAlert() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Call Us", style: .default, handler: {(action:UIAlertAction) in
            self.callSupport()
        }))
        
        alert.addAction(UIAlertAction(title: "Email Us", style: .default, handler: {(action:UIAlertAction) in
            let viewController = AppController.shared.getContactUsViewController()
            self.navigationController?.pushViewController(viewController, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "FAQ", style: .default, handler: {(action:UIAlertAction) in
            let viewController = AppController.shared.getFAQViewController()
            self.navigationController?.pushViewController(viewController, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func callSupport() {
        if let url = URL.init(string: "tel://\(self.supportMobileNumber)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        
        guard let text = self.termsLabel.attributedText?.string else {
            return
        }
        
        if let range = text.range(of: NSLocalizedString("Terms & Conditions", comment: "terms")),
            recognizer.didTapAttributedTextInLabel(label: self.termsLabel, inRange: NSRange(range, in: text)) {
            self.gotoWebView(TERMS_AND_CONDITION_URL, pageTitle: TITLE_TERMS_AND_CONDITIONS)
        } else if let range = text.range(of: NSLocalizedString("Privacy Policy", comment: "privacy")),
            recognizer.didTapAttributedTextInLabel(label: self.termsLabel, inRange: NSRange(range, in: text)) {
            self.gotoWebView(PRIVACY_POLICY_URL, pageTitle: TITLE_PRIVACY_POLICY)
        }
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            self.loginLogoImageViewHeightConstraint.constant = self.loginLogoImageViewHeightConstraint.constant - keyboardHeight
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            self.loginLogoImageViewHeightConstraint.constant = self.loginLogoImageViewHeightConstraint.constant + keyboardHeight
        }
    }
}
