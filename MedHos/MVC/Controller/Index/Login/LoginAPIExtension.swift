//
//  LoginAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension LoginViewController {
    
    func loadSupportDetailsAPICall() {
        
        let params = [ "CategoryID" : 0 ]
        
        HttpManager.shared.loadAPICall(path: PATH_SupportDetails, params: params, httpMethod: .post, controller: self, isBackgroundCall: false, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["SupportContactNo"] as? String {
                    self.supportMobileNumber = value
                }
            }
        }) {}
    }
    
    @objc func loginAPICall() {
        
        self.view.endEditing(true)
        
        let params = ["MobileNumber" : self.mobileNumber,
                      "Language"     : "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_User_RegisterAndLogin, params: params, httpMethod: .post, controller: self, isBackgroundCall: false, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                
                let viewController = AppController.shared.getOTPVerificationViewController()
                viewController.mobileNumber = self.mobileNumber
                viewController.otpType = OTP_TYPE.LOGIN.rawValue
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }) {}
    }
    
}
