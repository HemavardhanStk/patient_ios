//
//  LoginDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

// MARK: - TextField Delegate

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.mobileNumber = textField.text ?? ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let newLength = text.count + string.count - range.length
 
            if newLength >= MOBILE_LENGTH {
                self.submitButton.isUserInteractionEnabled = true
                self.submitButton.layer.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR).cgColor
            } else {
                self.submitButton.isUserInteractionEnabled = false
                self.submitButton.layer.backgroundColor = UIColor.lightGray.cgColor
            }
            
            return newLength <= MOBILE_LENGTH
        }
        
        return true
    }
}
