//
//  AppTourContentViewController.swift
//  tests
//
//  Created by Hemavardhan on 14/02/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AppTourContentViewController: UIViewController{

    @IBOutlet weak var pageIcon: UIImageView!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var pageDescriptionLabel: UILabel!
    @IBOutlet weak var LabelToDescriptionheight: NSLayoutConstraint!
    @IBOutlet weak var pageIconTopHeight: NSLayoutConstraint!
    @IBOutlet weak var pageIconBottomAlign: NSLayoutConstraint!
    
    var pageIndex: Int?
    var infoDict: Dictionary<String, String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        switch UIDevice().type {
            
        case .iPhoneSE,.iPhone5,.iPhone5S:
            
            LabelToDescriptionheight.constant = 10
            pageIconBottomAlign.constant = 100
            
        case .iPhone6, .iPhone6plus, .iPhone6S,
             .iPhone6Splus, .iPhone7, .iPhone7plus,
             .iPhone8, .iPhone8plus:
            
            LabelToDescriptionheight.constant = 10
            pageIconBottomAlign.constant = 130
            
        case .iPhoneX, .iPhoneXR, .iPhoneXS, .iPhoneXSMax:
            
            LabelToDescriptionheight.constant = 20
            pageIconBottomAlign.constant = 130
            pageIconTopHeight.constant = 100
            
        case .iPadPro9_7, .iPadPro10_5, .iPadPro12_9,
             .iPadPro2_12_9, .iPadAir, .iPadAir2,
             .iPad5, .iPad6 :
            
            pageIconBottomAlign.constant = 300
            LabelToDescriptionheight.constant = 30
            pageTitleLabel.font = UIFont.init(name: "Avenir-Heavy", size: 40)
            pageDescriptionLabel.font = UIFont.init(name: "Avenir-Book", size: 35)
            
        default:break
        }
        
        
        self.classAndWidgetsInitialise()
    }
    
    func classAndWidgetsInitialise() {
        
        if let image = self.infoDict?[IMAGE] {
            self.pageIcon.image = UIImage.init(named: image)
        }
        self.pageTitleLabel.text = self.infoDict?[TITLE]
        self.pageDescriptionLabel.text = self.infoDict?[DESCRIPTION]
        self.pageIcon.alpha = 1.0
        self.pageTitleLabel.alpha = 1.0
        self.pageDescriptionLabel.alpha = 1.0
        self.pageIcon.transform = CGAffineTransform.init(scaleX: 1, y: 1)
        
    }
    
}
