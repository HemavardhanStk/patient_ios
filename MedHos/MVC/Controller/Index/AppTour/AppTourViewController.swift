//
//  AppTourViewController.swift
//  tests
//
//  Created by Hemavardhan on 14/02/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class AppTourViewController: UIViewController {

    @IBOutlet weak var SkipButton: UIButton!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var infoArray = [Dictionary<String, String>]()
    var index = 0
    
    var pageViewController: UIPageViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.classAndWidgetsInitialise()
    }
    func classAndWidgetsInitialise() {
        
        
        self.loadAppTourData()
        
        self.pageViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "PageViewController") as? UIPageViewController
        self.pageViewController.dataSource = self as UIPageViewControllerDataSource
        self.pageViewController.delegate = self as UIPageViewControllerDelegate
        let pageContentViewController = self.viewControllerAtIndex(index: index)
        self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        
        let height = UIScreen.main.bounds.height - 50
        
        switch UIDevice().type {
            
        case .iPhoneSE,.iPhone5,.iPhone5S:
            
            self.pageViewController.view.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: height)
        case .iPhone6, .iPhone6plus, .iPhone6S,
             .iPhone6Splus, .iPhone7, .iPhone7plus,
             .iPhone8, .iPhone8plus, .iPhoneX,
             .iPhoneXR, .iPhoneXS, .iPhoneXSMax :
            
            self.pageViewController.view.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: height - 50)
            
        case .iPadPro9_7, .iPadPro10_5, .iPadPro12_9,
             .iPadPro2_12_9, .iPadAir, .iPadAir2,
             .iPad5, .iPad6 :
            
            self.pageViewController.view.frame = CGRect.init(x: 0, y: 100, width: self.view.frame.width, height: height - 100)
            
        default:break
        }
        
        self.addChild(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParent: self)
        
        self.pageControl.numberOfPages = self.infoArray.count
        self.updatePageControl()
        
        self.NextButton.setPrimaryButtonStyle("Next")
        self.SkipButton.setPrimaryButtonStyle("Skip")
        self.NextButton.addTarget(self, action: #selector(startButtonTapped), for: .touchUpInside)
        self.SkipButton.addTarget(self, action: #selector(proceedSignin), for: .touchUpInside)
    }
    
    func loadAppTourData() {
        
        var dict = [IMAGE: "ic_screen1", TITLE : "Welcome to MedHos", DESCRIPTION : "Find and Book Doctor Appointments online at anytime from any place"]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "ic_screen2", TITLE : "Find Doctors", DESCRIPTION : "Find Doctors based on speciality and location. View their profile, available timeslots and reviews"]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "ic_screen3", TITLE : "Book Appointments", DESCRIPTION : "Book your Doctor appointment with ease and receive an SMS confirmation"]
        self.infoArray.append(dict)
        
        dict = [IMAGE: "ic_screen4", TITLE : "Get Facilitation", DESCRIPTION : "Get expert consultation and backup your health records online. Get reminders and follow-up SMS"]
        self.infoArray.append(dict)
    }
    
    func updatePageControl() {
        if self.index == self.infoArray.count - 1 {
            self.NextButton.setTitle("Start", for: .normal)
            self.SkipButton.alpha = 0
        } else {
            self.NextButton.setTitle("Next", for: .normal)
            self.SkipButton.alpha = 1
        }
        self.pageControl.currentPage = self.index
    }
    
    func viewControllerAtIndex(index : Int) -> UIViewController? {
        
        if self.infoArray.count == 0 || index >= self.infoArray.count {
            return nil
        }
        let pageContentViewController = AppController.shared.getAppTourContentViewController()
        pageContentViewController.infoDict = self.infoArray[index]
        pageContentViewController.pageIndex = index
        
        return pageContentViewController
    }
    
    func getChildViewControllerIndex(_ viewController: UIViewController) -> Int {
        
        if let index: Int = (viewController as? AppTourContentViewController)?.pageIndex {
            return index
        }
        return 0
    }
    
    @objc func startButtonTapped() {
        
        if self.index < self.infoArray.count - 1 {
            self.index += 1
            self.updatePageControl()
            let pageContentViewController = self.viewControllerAtIndex(index: self.index)
            self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        } else {
            self.proceedSignin()
        }
    }
    
    @objc func proceedSignin() {
        UserInfo.shared.showSigninViewController()
    }
}
extension AppTourViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        
        return self.infoArray.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        
        return 0
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index: Int = self.getChildViewControllerIndex(viewController)
        if index <= 0 {
            index = 0
            return nil
        }
        index = index - 1
        
        return self.viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            let viewController = pageViewController.viewControllers?.last as? AppTourContentViewController
            self.index = (viewController?.pageIndex)!
            self.updatePageControl()
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index: Int = self.getChildViewControllerIndex(viewController)
        index = index + 1
        if index >= self.infoArray.count {
            index = self.infoArray.count - 1
            return nil
        }
        
        return self.viewControllerAtIndex(index: index)
    }
}
