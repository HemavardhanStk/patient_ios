//
//  RootViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 06/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    @IBOutlet weak var retryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.hideNavigation(self)
        
        self.loadInfoData()
        
        self.retryButton.addTarget(self, action: #selector(loadInfoData), for: .touchUpInside)
    }

    @objc func loadInfoData() {
        
        if HttpManager.shared.connectedToNetwork() {
            self.retryButton.alpha = 0
            self.getCurrentVersion()
        } else {
            self.retryButton.alpha = 1
            AlertHelper.shared.showAlert(message: "No Internet Connection. Turn On Your Mobile Data or Wifi and Try again!", controller: self)
        }
        
    }
    
    func gotoRootView() {
        
        let (viewController, isAllowRootView) = AppController.shared.getRootViewController()
        if isAllowRootView {
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
}
