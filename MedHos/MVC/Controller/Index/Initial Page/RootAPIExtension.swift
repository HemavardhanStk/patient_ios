//
//  RootAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension RootViewController {
    
    func getCurrentVersion() {
        
        let params = [ "TypeNo" : 2 ]
        let CurrentVersion = Session.shared.getVersion()
        
        HttpManager.shared.loadAPICall(path: PATH_CheckVersionControl, params: params, httpMethod: .post, controller: self, isBackgroundCall: false, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let Control = response["Control"] as? Dictionary<String,Any> {
                    if let lastVersion = Control["LastVersion"] as? Double, lastVersion > Double(CurrentVersion) ?? 0.0 {
                        let mandatory = Control["Mandatory"] as? String ?? "N"
                        let appUrl = Control["DownloadURL"] as? String ?? ""
                        Session.shared.setAppUrl(appUrl)
                        self.showNonMandatoryAlert(mandatory)
                    } else {
                        self.gotoRootView()
                    }
                } else {
                    self.gotoRootView()
                }
            }
        }) {}
    }
    
}
