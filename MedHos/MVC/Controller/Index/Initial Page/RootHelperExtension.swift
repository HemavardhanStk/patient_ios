//
//  RootHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension RootViewController {
    
    func showNonMandatoryAlert(_ mandatory:String) {
        
        let message = "Update the MedHos and enjoy with new features"
        
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: UIAlertController.Style.alert);
        
        if mandatory == "N" {
            alert.addAction(UIAlertAction(title: "Later", style: .default, handler: {(action:UIAlertAction) in
                self.gotoRootView()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Update", style: .default, handler: {(action:UIAlertAction) in
            let appUrl = Session.shared.getAppUrl() 
            guard let url = URL(string : appUrl) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
