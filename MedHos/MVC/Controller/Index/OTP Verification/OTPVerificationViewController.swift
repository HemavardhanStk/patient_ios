//
//  OTPVerificationViewController.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

enum OTP_TYPE: String {
    case LOGIN
    case CHANGE_MOBILE
}


class OTPVerificationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var infoArray = [Dictionary<String, String>]()
    var otpArray = [Dictionary<String, String>]()
    var otp: String = ""
    
    var otpType = ""
    var refId = ""
    var mobileNumber = ""
    
    let TAG_RESEND: Int = 100
    let RESEND = "Resend"
    let otpLength = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.classAndWidgetsInitialise()
        // Do any additional setup after loading the view.
    }

    // MARK: - Initialise Methods
    
    func classAndWidgetsInitialise() {
        
        Navigation.shared.hideNavigation(self)
        
        self.setupColors()
        self.loadInfoData()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "OTPVerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "OTPVerificationTableViewCell")
        self.tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtonTableViewCell")
        self.tableView.tableFooterView = UIView.init()
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
    }
    
    func setupColors() {
        
        self.view.backgroundColor = UIColor.init(netHex: APP_BG_COLOR)
        self.tableView.backgroundColor = UIColor.white
        
        self.tableView.setRoundedCorner()
    }
    
    func loadInfoData() {
        
        var dict = [IMAGE: "ic_facebook", PLACEHOLDER : "OTP", TEXT : ""]
        self.infoArray.append(dict)
        
        dict = [PLACEHOLDER : "OTP", TEXT : ""]
        self.otpArray.append(dict)
        self.otpArray.append(dict)
        self.otpArray.append(dict)
        self.otpArray.append(dict)
        self.otpArray.append(dict)
        self.otpArray.append(dict)
    }
    
    func resendTapped() {
        
        self.resendOtpAPICall()
    }
    
    @objc func doLogout() {
        
        UserInfo.shared.logoutUser(self)
    }
    
    @objc func verifyOtpButtonTapped() {
        
        if self.isValidationSuccess() {
            if self.otpType == OTP_TYPE.LOGIN.rawValue {
                self.verifyUserAPICall()
            } else if self.otpType == OTP_TYPE.CHANGE_MOBILE.rawValue {
                self.verifyUserMobileOTPAPICall()
            }
        }
    }
    
    @objc func popViewController() {
        
        self.navigationController?.popViewController(animated: true)
    }
}
