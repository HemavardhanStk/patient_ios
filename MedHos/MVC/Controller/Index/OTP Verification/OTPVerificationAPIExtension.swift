//
//  OTPVerificationAPIExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension OTPVerificationViewController {
    
    func isValidationSuccess() -> Bool {
        
        self.view.endEditing(true)
        return ValidationHelper.shared.isNilCheckValidationSuccess(infoArray: self.otpArray, controller: self)
    }
    
    func resendOtpAPICall() {
        
        let params = ["MobileNumber" : self.mobileNumber,
                      "Language"     : "English"]
        
        HttpManager.shared.loadAPICall(path: "User/ResendOTPLoginRegister", params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let message = response["Message"] as? String {
                    AlertHelper.shared.showAlert(message: message, controller: self)
                }
            }
        }) {}
    }
    
    // MARK: - Login Verification API Call
    
    func verifyUserAPICall() {
        
        let params = ["UserOTP" : self.otp,
                      "MobileNumber" : self.mobileNumber,
                      "MobileDeviceID" : "\(UIDevice.current.identifierForVendor!)",
                      "Source":"iOS",
                      "Language": "English"]
        
        HttpManager.shared.loadAPICall(path: PATH_VerifyUserLoginOTP, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let value = response["userDetails"] as? Dictionary<String, Any> {
                    
                    let mobileNumber = value["UserPhone"] as? String ?? ""
                    let userId = "\(value["User_IDs"] as? Int ?? 0)"
                    let userName = value["UserName"] as? String ?? ""
                    
                    Session.shared.setUserMobileAndUserId(mobile: mobileNumber, userId: userId)
                    
                    if userName.count != 0 {
                        Session.shared.setLoginStatus(LOGIN_STATUS.HOME.rawValue)
                        UserInfo.shared.showTabBarViewController()
                    } else {
                        Session.shared.setLoginStatus(LOGIN_STATUS.PERSONAL_DETAILS.rawValue)
                        
                        let viewController = AppController.shared.getPatientProfileViewController()
                        viewController.initialUserProfile = true
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }
            }
        }) {}
    }

    // MARK: - Change Password Verification API Call
    
    func verifyUserMobileOTPAPICall() {
        
        let user = Session.shared.getUserInfo()
        
        let params = ["otpFromUser" : self.otp,
                      "UserId" : user.id,
                      "Language" : "English"] as Dictionary<String, String>
        
        HttpManager.shared.loadAPICall(path: PATH_GetOTP, params: params, httpMethod: .post, controller: self, completion: { (response) in
            if ValidationHelper.shared.isAPIValidationSuccess(response, controller: self) {
                if let message = response["Message"] as? String {
                    AlertHelper.shared.showAlertWithHandler(message: message, handler: "doLogout", withCancel: false, controller: self)
                }
            }
        }) {}
    }
}
