//
//  OTPVerificationHelperExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension OTPVerificationViewController {
    
    // MARK: - Header Cell
    
    func getHeaderCell() -> OTPVerificationTableViewCell {
        
        let cell: OTPVerificationTableViewCell = Bundle.main.loadNibNamed("OTPVerificationTableViewCell", owner: nil, options: nil)![0] as! OTPVerificationTableViewCell
        
        cell.contentTitle.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 16)
        cell.contentDescription.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.userMobileNumberLabel.textColor = UIColor(netHex: APP_PRIMARY_COLOR)
        
        cell.userMobileNumberLabel.text = self.mobileNumber
        cell.editUserMobileButton.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - Resend otp Cell
    
    func getResendOtpCell() -> LabelTableViewCell {
        
        let cell = Bundle.main.loadNibNamed("LabelTableViewCell", owner: nil, options: nil)![0] as! LabelTableViewCell
 
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Didn't receive OTP? Resend")
        attributedString.setColor(color: UIColor.black, forText: "Didn't receive OTP? ")
        attributedString.setColor(color: UIColor(netHex: APP_PRIMARY_COLOR), forText: "Resend")
        
        cell.itemTitleLabel.attributedText = attributedString
        
        cell.itemTitleLabel.textAlignment = .right
        
        cell.itemTitleLabel.isUserInteractionEnabled = true
        cell.itemTitleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel)))
        cell.itemTitleLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 14)
        
        cell.itemTitleLabel.tag = 56
        
        return cell
    }

    // MARK: - Textfield Cell
    
    func getTextFieldCell(_ indexPath: IndexPath) -> OTPVerificationTableViewCell {
        
        let cell: OTPVerificationTableViewCell = Bundle.main.loadNibNamed("OTPVerificationTableViewCell", owner: nil, options: nil)![1] as! OTPVerificationTableViewCell
        
        cell.otpTextField.keyboardType = .numberPad
        cell.otpTextField.delegate = self
        cell.otpTextField.text = self.otp
        cell.otpListenerButton.addTarget(self, action: #selector(otpListenerButtonTapped), for: .touchUpInside)
        
        self.setOtpLabel(cell.otpFieldOne, tag: 1)
        self.setOtpLabel(cell.otpFieldTwo, tag: 2)
        self.setOtpLabel(cell.otpFieldThree, tag: 3)
        self.setOtpLabel(cell.otpFieldFour, tag: 4)
        self.setOtpLabel(cell.otpFieldFive, tag: 5)
        self.setOtpLabel(cell.otpFieldSix, tag: 6)
        
        return cell
    }
    
    func setOtpLabel(_ label: UILabel, tag: Int) {
        
        label.tag = tag
        label.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 20)
        let text = self.otpArray[tag - 1][TEXT]
        if text?.count != 0 {
            label.text = text
        } else {
            self.setEmptyValueForOTP(label)
        }
    }
    
    func setEmptyValueForOTP(_ label: UILabel) {
        let attributedText = NSAttributedString.init(string: ".", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 100), NSAttributedString.Key.baselineOffset: label.frame.height / 3])
        label.attributedText = attributedText
    }
    
    // MARK: - Button Cell
    
    func getButtonCell() -> ButtonTableViewCell {
        
        let cell: ButtonTableViewCell = Bundle.main.loadNibNamed("ButtonTableViewCell", owner: nil, options: nil)![0] as! ButtonTableViewCell
        
        cell.defaultButton.setPrimaryButtonStyle("VERIFY")
        cell.defaultButton.addTarget(self, action: #selector(verifyOtpButtonTapped), for: .touchUpInside)
        
        return cell
    }
  
    // MARK: - OTP TextField Logic Implementation
    
    func setOtpValue() {
        
        let totalCount = 6
        
        let otpCount: Int = self.otp.count
        for i in 0..<otpCount {
            if let label = self.tableView.viewWithTag(i+1) as? UILabel {
                label.text = self.otp[i]
                self.otpArray[i][TEXT] = self.otp[i] 
            }
        }
        
        if otpCount < totalCount {
            for i in otpCount..<totalCount {
                if let label = self.tableView.viewWithTag(i+1) as? UILabel {
                    self.setEmptyValueForOTP(label)
                    self.otpArray[i][TEXT] = ""
                }
            }
        }
    }
    
    @objc func otpListenerButtonTapped() {
        
        if let textField = self.tableView.viewWithTag(10) as? UITextField {
            textField.becomeFirstResponder()
        }
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        
        if let label = self.tableView.viewWithTag(56) as? UILabel {
            
            let text =  "Didn't receive OTP? Resend  "
            
            if let range = text.range(of: NSLocalizedString("Resend", comment: "resend")),
                recognizer.didTapAttributedTextInLabel(label: label, inRange: NSRange(range, in: text)) {
                self.resendOtpAPICall()
            }
        }
    }
    
}

