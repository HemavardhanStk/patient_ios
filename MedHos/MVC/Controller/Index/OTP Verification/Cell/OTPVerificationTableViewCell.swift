//
//  OTPVerificationTableViewCell.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 20/02/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import UIKit

class OTPVerificationTableViewCell: UITableViewCell {

    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var contentTitle: UILabel!
    @IBOutlet var userMobileNumberLabel: UILabel!
    @IBOutlet var editUserMobileButton: UIButton!
    @IBOutlet weak var contentDescription: UILabel!
    
        
    @IBOutlet weak var otpFieldOne: UILabel!
    @IBOutlet weak var otpFieldTwo: UILabel!
    @IBOutlet weak var otpFieldThree: UILabel!
    @IBOutlet weak var otpFieldFour: UILabel!
    @IBOutlet weak var otpFieldFive: UILabel!
    @IBOutlet weak var otpFieldSix: UILabel!
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var otpListenerButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
