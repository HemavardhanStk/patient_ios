//
//  OTPVerificationDelegateExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 15/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

// MARK: - TableView Delegate

extension OTPVerificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.infoArray.count + 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 190
        } else {
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            return self.getHeaderCell()
            
        case 1:
            return self.getTextFieldCell(indexPath)
            
        case 2:
            return self.getResendOtpCell()
            
        default:
            return self.getButtonCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TextField Delegate

extension OTPVerificationViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if var text = textField.text {
            if string.count == 0 {
                text.removeLast()
            }
            let newLength = text.count + string.count - range.length
            
            if newLength == 6 {
                textField.resignFirstResponder()
            }
            
            if newLength <= 6 {
                self.otp = "\(text)\(string)"
                self.setOtpValue()
                return true
            } else {
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

