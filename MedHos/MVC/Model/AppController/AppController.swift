//
//  AppController.swift
//  tests
//
//  Created by Hemavardhan on 14/02/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

enum AppStoryboard: String {
    
    case Main = "Main"
    case Home = "Home"
    case Manage = "Manage"
    case Account = "Account"
    
    var instance: UIStoryboard {
        return UIStoryboard.init(name: self.rawValue, bundle: Bundle.main)
    }
}

enum LOGIN_STATUS: String {
    case APP_TOUR = "APP_TOUR"
    case LOG_IN = "LOGIN_IN"
    case PERSONAL_DETAILS = "PERSONAL_DETAILS"
    case HOME = "HOME"
}


class AppController: NSObject {
    
    static let shared : AppController = AppController()
    
    
    func getRootViewController() -> (UIViewController, Bool) {
        
        if Session.shared.getLoginStatus() == LOGIN_STATUS.LOG_IN.rawValue {
            UserInfo.shared.showSigninViewController()
            return (UIViewController.init(),false)
        } else if Session.shared.getLoginStatus() == LOGIN_STATUS.PERSONAL_DETAILS.rawValue {
            return (gotoProfileInitially(), true)
        } else if Session.shared.getLoginStatus() == LOGIN_STATUS.HOME.rawValue {
            UserInfo.shared.showTabBarViewController()
            return (UIViewController.init(),false)
        } else {
            return (self.getAppTourViewController(), true)
        }
    }
    
    func getAppTourContentViewController() -> AppTourContentViewController {
        if let viewController: AppTourContentViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "AppTourContentViewController") as? AppTourContentViewController {
            return viewController
        }
        return AppTourContentViewController()
    }
    
    func getAppTourViewController() -> AppTourViewController {
        if let viewController: AppTourViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "AppTourViewController") as? AppTourViewController {
            return viewController
        }
        return AppTourViewController()
    }
    
    func getInitialViewController() -> RootViewController {
        if let viewController: RootViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "RootViewController") as? RootViewController {
            return viewController
        }
        return RootViewController()
    }

    func getLoginViewController() -> LoginViewController {
        if let viewController: LoginViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            return viewController
        }
        return LoginViewController()
    }
    
    func getOTPVerificationViewController() -> OTPVerificationViewController {
        if let viewController: OTPVerificationViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OTPVerificationViewController") as? OTPVerificationViewController {
            return viewController
        }
        return OTPVerificationViewController()
    }
    
    func getRAMAnimatedTabBarController() -> TabBarViewController {
        if let viewController: UITabBarController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "MedHosTabBar") as? TabBarViewController {
            return viewController as! TabBarViewController
        }
        return TabBarViewController()
    }
    
    func getQRScannerViewController() -> QRScannerViewController {
        if let viewController: QRScannerViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "QRScannerViewController") as? QRScannerViewController {
            return viewController
        }
        return QRScannerViewController()
    }

    // Home Page
    
    func getHomeViewController() -> HomeViewController {
        if let viewController: HomeViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            return viewController
        }
        return HomeViewController()
    }
    
    func getDoctorsViewController() -> DoctorCategoriesViewController {
        if let viewController: DoctorCategoriesViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "DoctorCategoriesViewController") as? DoctorCategoriesViewController {
            return viewController
        }
        return DoctorCategoriesViewController()
    }
    
    func getDoctorBasicSearchViewController() -> DoctorBasicSearchViewController {
        if let viewController: DoctorBasicSearchViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "DoctorBasicSearchViewController") as? DoctorBasicSearchViewController {
            return viewController
        }
        return DoctorBasicSearchViewController()
    }
    
    func getDoctorClinicsViewController() -> DoctorClinicsViewController {
        if let viewController: DoctorClinicsViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "DoctorClinicsViewController") as? DoctorClinicsViewController {
            return viewController
        }
        return DoctorClinicsViewController()
    }
    
    func getDoctorProfileViewController() -> DoctorProfileViewController {
        if let viewController: DoctorProfileViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "DoctorProfileViewController") as? DoctorProfileViewController {
            return viewController
        }
        return DoctorProfileViewController()
    }
    
    func getCreateAppointmentViewController() -> CreateAppointmentViewController {
        if let viewController: CreateAppointmentViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "CreateAppointmentViewController") as? CreateAppointmentViewController {
            return viewController
        }
        return CreateAppointmentViewController()
    }
    
    func getPreBookingViewController() -> PreBookingViewController {
        if let viewController: PreBookingViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "PreBookingViewController") as? PreBookingViewController {
            return viewController
        }
        return PreBookingViewController()
    }
    
    func getClinicProfileViewController() -> ClinicProfileViewController {
        if let viewController: ClinicProfileViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "ClinicProfileViewController") as? ClinicProfileViewController {
            return viewController
        }
        return ClinicProfileViewController()
    }
    
    func getMyFavoriteViewController() -> MyFavoriteViewController {
        if let viewController: MyFavoriteViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "MyFavoriteViewController") as? MyFavoriteViewController {
            return viewController
        }
        return MyFavoriteViewController()
    }
    
    func getHealthReminderViewController() -> HealthReminderViewController {
        if let viewController: HealthReminderViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HealthReminderViewController") as? HealthReminderViewController {
            return viewController
        }
        return HealthReminderViewController()
    }
    
        func getViewReminderViewController() -> ViewReminderViewController {
        if let viewController: ViewReminderViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "ViewReminderViewController") as? ViewReminderViewController {
            return viewController
        }
        return ViewReminderViewController()
    }
    
    func getAddHealthReminderViewController() -> AddEditHealthReminderViewController {
        if let viewController: AddEditHealthReminderViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "AddEditHealthReminderViewController") as? AddEditHealthReminderViewController {
            return viewController
        }
        return AddEditHealthReminderViewController()
    }
    
    func getHealthRecordViewController() -> HealthRecordViewController {
        if let viewController: HealthRecordViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HealthRecordViewController") as? HealthRecordViewController {
            return viewController
        }
        return HealthRecordViewController()
    }
    
    func getEditRecordViewController() -> EditRecordViewController {
        if let viewController: EditRecordViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "EditRecordViewController") as? EditRecordViewController {
            return viewController
        }
        return EditRecordViewController()
    }
    
    func getAddHealthRecordViewController() -> AddEditHealthRecordViewController {
        if let viewController: AddEditHealthRecordViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "AddEditHealthRecordViewController") as? AddEditHealthRecordViewController {
            return viewController
        }
        return AddEditHealthRecordViewController()
    }
    
    func getSingleSelectionViewController() -> SingleSelectionViewController {
        if let viewController: SingleSelectionViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "SingleSelectionViewController") as? SingleSelectionViewController {
            return viewController
        }
        return SingleSelectionViewController()
    }
    
    func getMultiProfileViewController() -> MultiProfileViewController {
        if let viewController: MultiProfileViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "MultiProfileViewController") as? MultiProfileViewController {
            return viewController
        }
        return MultiProfileViewController()
    }
    
    func getLabBasicSearchViewController() -> LabBasicSearchViewController {
        if let viewController: LabBasicSearchViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "LabBasicSearchViewController") as? LabBasicSearchViewController {
            return viewController
        }
        return LabBasicSearchViewController()
    }
    
    func getLabPackageTestViewController() -> LabPackageTestViewController {
        if let viewController: LabPackageTestViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "LabPackageTestViewController") as? LabPackageTestViewController {
            return viewController
        }
        return LabPackageTestViewController()
    }
    
    func getLabChartViewController() -> LabChartViewController {
        if let viewController: LabChartViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "LabChartViewController") as? LabChartViewController {
            return viewController
        }
        return LabChartViewController()
    }

    func getLabSlotPickerViewController() -> LabSlotPickerViewController {
        if let viewController: LabSlotPickerViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "LabSlotPickerViewController") as? LabSlotPickerViewController {
            return viewController
        }
        return LabSlotPickerViewController()
    }
    
    func getLabTestPreBookingViewController() -> LabTestPreBookingViewController {
        if let viewController: LabTestPreBookingViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "LabTestPreBookingViewController") as? LabTestPreBookingViewController {
            return viewController
        }
        return LabTestPreBookingViewController()
    }

    
    func getPharmacyBasicSearchViewController() -> PharmacyBasicSearchViewController {
        if let viewController: PharmacyBasicSearchViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "PharmacyBasicSearchViewController") as? PharmacyBasicSearchViewController {
            return viewController
        }
        return PharmacyBasicSearchViewController()
    }
    
    func getOpticalsBasicSearchViewController() -> OpticalsBasicSearchViewController {
        if let viewController: OpticalsBasicSearchViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "OpticalsBasicSearchViewController") as? OpticalsBasicSearchViewController {
            return viewController
        }
        return OpticalsBasicSearchViewController()
    }
    
    func getCitiesPopOverViewController() -> CitiesPopOverViewController {
        if let viewController: CitiesPopOverViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "CitiesPopOverViewController") as? CitiesPopOverViewController {
            return viewController
        }
        return CitiesPopOverViewController()
    }
    
    func getDoctorSearchFilterViewController() -> DoctorSearchFilterViewController {
        if let viewController: DoctorSearchFilterViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "DoctorSearchFilterViewController") as? DoctorSearchFilterViewController {
            return viewController
        }
        return DoctorSearchFilterViewController()
    }
    
    func getTextFieldPopUpViewController() -> MultiUsePopUpViewController {
        if let viewController: MultiUsePopUpViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "MultiUsePopUpViewController") as? MultiUsePopUpViewController {
            return viewController
        }
        return MultiUsePopUpViewController()
    }
    
    // Manage Page
    func getManageViewController() -> ManageViewController {
        if let viewController: ManageViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier:  "ManageViewController") as? ManageViewController {
            return viewController
        }
        return ManageViewController()
    }
    
    func getUpcomingAppointmentViewController() -> UpcomingAppointmentViewController {
        if let viewController: UpcomingAppointmentViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "UpcomingAppointmentViewController") as? UpcomingAppointmentViewController {
            return viewController
        }
        return UpcomingAppointmentViewController()
    }
    
    func getPastAppointmentViewController() -> PastAppointmentViewController {
        if let viewController: PastAppointmentViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "PastAppointmentViewController") as? PastAppointmentViewController {
            return viewController
        }
        return PastAppointmentViewController()
    }
    
    func getDueAppointmentViewController() -> DueAppointmentViewController {
        if let viewController: DueAppointmentViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "DueAppointmentViewController") as? DueAppointmentViewController {
            return viewController
        }
        return DueAppointmentViewController()
    }
    
    func getViewAppointmentViewController() -> ViewAppointmentViewController {
        if let viewController: ViewAppointmentViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewAppointmentViewController") as? ViewAppointmentViewController {
            return viewController
        }
        return ViewAppointmentViewController()
    }
    
    func getViewPrescriptionTestReportViewController() -> ViewPrescriptionTestReportViewController {
        if let viewController: ViewPrescriptionTestReportViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewPrescriptionTestReportViewController") as? ViewPrescriptionTestReportViewController {
            return viewController
        }
        return ViewPrescriptionTestReportViewController()
    }
    
    func getBookedTestListViewController() -> BookedTestListViewController {
        if let viewController: BookedTestListViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "BookedTestListViewController") as? BookedTestListViewController {
            return viewController
        }
        return BookedTestListViewController()
    }
    
    func getViewBookedTestViewController() -> ViewBookedTestViewController {
        if let viewController: ViewBookedTestViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewBookedTestViewController") as? ViewBookedTestViewController {
            return viewController
        }
        return ViewBookedTestViewController()
    }
    
    func getCompletedTestListViewController() -> CompletedTestListViewController {
        if let viewController: CompletedTestListViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "CompletedTestListViewController") as? CompletedTestListViewController {
            return viewController
        }
        return CompletedTestListViewController()
    }

    func getViewCompletedTestViewController() -> ViewCompletedTestViewController {
        if let viewController: ViewCompletedTestViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewCompletedTestViewController") as? ViewCompletedTestViewController {
            return viewController
        }
        return ViewCompletedTestViewController()
    }
    
    func getCancelledTestListViewController() -> CancelledTestListViewController {
        if let viewController: CancelledTestListViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "CancelledTestListViewController") as? CancelledTestListViewController {
            return viewController
        }
        return CancelledTestListViewController()
    }

    func getViewCancelledTestViewController() -> ViewCancelledTestViewController {
        if let viewController: ViewCancelledTestViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewCancelledTestViewController") as? ViewCancelledTestViewController {
            return viewController
        }
        return ViewCancelledTestViewController()
    }
    
    func getViewTestPackageViewController() -> ViewPackageDetailsViewController {
        if let viewController: ViewPackageDetailsViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewPackageDetailsViewController") as? ViewPackageDetailsViewController {
            return viewController
        }
        return ViewPackageDetailsViewController()
    }
        
    func getViewAddFeedbackViewController() -> ViewAddFeedbackViewController {
        if let viewController: ViewAddFeedbackViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "ViewAddFeedbackViewController") as? ViewAddFeedbackViewController {
            return viewController
        }
        return ViewAddFeedbackViewController()
    }
    
    func getPendingFeedBackViewController() -> PendingFeedBackViewController {
        if let viewController: PendingFeedBackViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "PendingFeedBackViewController") as? PendingFeedBackViewController {
            return viewController
        }
        return PendingFeedBackViewController()
    }
    
    func getCompletedFeedBackViewController() -> CompletedFeedBackViewController {
        if let viewController: CompletedFeedBackViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "CompletedFeedBackViewController") as? CompletedFeedBackViewController {
            return viewController
        }
        return CompletedFeedBackViewController()
    }
    
    func getFamilyMembersViewController() -> FamilyMembersViewController {
        if let viewController: FamilyMembersViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "FamilyMembersViewController") as? FamilyMembersViewController {
            return viewController
        }
        return FamilyMembersViewController()
    }
  
    func getAddFamilyMemberViewController() -> AddFamilyMemberViewController {
        if let viewController: AddFamilyMemberViewController = AppStoryboard.Manage.instance.instantiateViewController(withIdentifier:  "AddFamilyMemberViewController") as? AddFamilyMemberViewController {
            return viewController
        }
        return AddFamilyMemberViewController()
    }
    
    // Account Page
    func getAccountViewController() -> AccountViewController {
        if let viewController: AccountViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController {
            return viewController
        }
        return AccountViewController()
    }
    
    func getUserLocationViewController() -> UserLocationViewController {
        if let viewController: UserLocationViewController = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "UserLocationViewController") as? UserLocationViewController {
            return viewController
        }
        return UserLocationViewController()
    }
    
    func getPatientProfileViewController() -> PatientProfileViewController {
        if let viewController: PatientProfileViewController = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PatientProfileViewController") as? PatientProfileViewController {
            return viewController
        }
        return PatientProfileViewController()
    }
    
    func getContactUsViewController() -> ContactUsViewController {
        if let viewController: ContactUsViewController = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController {
            return viewController
        }
        return ContactUsViewController()
    }
    
    func getChangeMobileViewController() -> ChangeMobileNumberViewController {
        if let viewController: ChangeMobileNumberViewController = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "ChangeMobileNumberViewController") as? ChangeMobileNumberViewController {
            return viewController
        }
        return ChangeMobileNumberViewController()
    }
    
    func getFAQViewController() -> FAQViewController {
        if let viewController: FAQViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "FAQViewController") as? FAQViewController {
            return viewController
        }
        return FAQViewController()
    }
    
    func getEmergencyNumbersViewController() -> EmergencyNumbersViewController {
        if let viewController: EmergencyNumbersViewController = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "EmergencyNumbersViewController") as? EmergencyNumbersViewController {
            return viewController
        }
        return EmergencyNumbersViewController()
    }
    
    func getNotificationViewController() -> NotificationViewController {
        if let viewController: NotificationViewController = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController {
            return viewController
        }
        return NotificationViewController()
    }
}

extension AppController {
    
    func gotoProfileInitially() -> PatientProfileViewController {
        
        Session.shared.setLoginStatus(LOGIN_STATUS.PERSONAL_DETAILS.rawValue)
        
        let viewController = self.getPatientProfileViewController()
        viewController.initialUserProfile = true
        return viewController
    }
    
}
