//
//  APIManager.swift
//  Taby
//
//  Created by Naveen Kumar on 30/12/2018.
//  Copyright © 2018 Naveen Kumar. All rights reserved.
//

import UIKit
import SystemConfiguration

typealias ServiceResponse = (Any, Int, NSError?) -> Void

class APIManager:  NSObject, URLSessionDelegate, URLSessionTaskDelegate  {
    
    static let shared: APIManager = APIManager() 
    
    let containerWindow = UIApplication.shared.windows[0]
    
    var urlSession: URLSession!
   
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    
    //MARK: GET METHOD
    func makeHTTPGetRequest(path: String, onCompletion: @escaping ServiceResponse) {
        
        guard URL(string: path) != nil else {
            return
        }
        
        let urlwithPercentEscapes = path.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: NSURL(string: urlwithPercentEscapes!)! as URL)
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        urlSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        
        
        
        let task = urlSession?.dataTask(with: (request as URLRequest) as URLRequest, completionHandler: {data, response, error -> Void in
            
            var result : Any = []
            
            if  data != nil {
                do{
                    result = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                } catch {
                    
                }
            }
            else {
                print(error?.localizedDescription ?? "")
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                print("Status code -- \(httpResponse.statusCode)")
                print("RESPONSE SUCCESS ===>> \(result as Any)")
                onCompletion(result, httpResponse.statusCode, error as NSError?)
            }
            
        })
        task?.resume()
    }
    
    //MARK: POST METHOD
    func makeHTTPPostRequest(path: String, body: [String:Any], onCompletion: @escaping ServiceResponse) {
        let _: NSError?
        
//        DispatchQueue.main.async {
//            LoadingIndicator.shared.show(self)
//        }
//        
        
        let urlwithPercentEscapes = path.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        
        let request = NSMutableURLRequest(url: NSURL(string: urlwithPercentEscapes!)! as URL)
        request.addValue("Basic c2Vuc3VyZUFuZHJvaWQ6QW5kcm9pZDEyMyM=", forHTTPHeaderField: "Authorization")

        let accessTocken = Session.shared.getAccessToken()
        
        if accessTocken != "" {
            request.addValue("\(accessTocken)", forHTTPHeaderField: "Bearer")
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: body, options: []) else {
            
            return
        }
        
        request.httpBody = httpBody
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        urlSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        
        let task = urlSession.dataTask(with: request as URLRequest, completionHandler: {data, response, err -> Void in
//            DispatchQueue.main.async {
//                LoadingIndicator.shared.hide()
//            }
            if err == nil{
                let resultstring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                if let httpResponse = response as? HTTPURLResponse {
                   print("Status code -- \(httpResponse.statusCode)")
                  print("RESPONSE SUCCESS ===>> \(resultstring as Any)")
                    
                    if let data = data {
                       // do {
                            //let json = try JSONSerialization.jsonObject(with: data, options: [])  as! [String:AnyObject]
                            //print(json)
                            
                            onCompletion(data as Data, httpResponse.statusCode, err as NSError?)
                        //}catch {
                            //print(error)
                            
                        //}
                    }
                }
            }
            else {
                if let httpResponse = response as? HTTPURLResponse {
                    onCompletion([], httpResponse.statusCode, err as NSError?)
                }
                else {
                    onCompletion([], (err! as NSError).code, err as NSError?)
                }
            }
        })
        task.resume()
    }
    
    func MakeHttpImageUploadRequest(paramName: String,url: String,dict: [String:Any], fileName: String, image: UIImage,  onCompletion: @escaping ServiceResponse)
    {
        
        let myUrl = NSURL(string: url);
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let UserData = ["UserData":dict]
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic c2Vuc3VyZUFuZHJvaWQ6QW5kcm9pZDEyMyM=", forHTTPHeaderField: "Authorization")
        
        let imageData = image.jpegData(compressionQuality: 1)
        
        if(imageData==nil)  { return; }
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        urlSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        
        request.httpBody = createBodyWithParameters(parameters: UserData, filePathKey: paramName, imageDataKey: imageData!, boundary: boundary) as Data
        
        let task = urlSession.dataTask(with: request as URLRequest, completionHandler: {data, response, error  -> Void in

            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            let resultstring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            if let httpResponse = response as? HTTPURLResponse {
                print("Status code -- \(httpResponse.statusCode)")
                print("RESPONSE SUCCESS ===>> \(resultstring as Any)")
                
                if let data = data {
                    onCompletion(data as Data, httpResponse.statusCode, error as NSError?)
                }
            }
            else
            {
                if let httpResponse = response as? HTTPURLResponse {
                    onCompletion([], httpResponse.statusCode, error as NSError?)
                }
                else {
                    onCompletion([], (error! as NSError).code, error as NSError?)
                }
          }
        }
        )
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: [String:Any]]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("{'\(Array(value.keys)[0])':\(Array(value.values)[0])}\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/*"
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        
        return body
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

}
extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
