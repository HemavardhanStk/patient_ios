//
//  Struct.swift
//  tests
//
//  Created by Hemavardhan on 23/02/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation
import UIKit


struct LoginContactStruct : Decodable
{
    var Success: Int
    var Message: String?
    var SupportContactNo: String
    var SupportContactMail: String
}

struct LoginResponse : Decodable
{
    var Message : String?
    var Success : Int?
}

struct userdata : Decodable
{
    var Message : String
    var Success : Int
    var UserProfileData : sub
}
struct BasicUserDatastruct :Decodable
{
    var User_IDs: Int
    var UserName: String
    var UserPhone: String
    var UserImage: String
    var UserGender: String
}

extension BasicUserDatastruct {
    init(BasicUserDatastruct : Dictionary<String,Any>){
        User_IDs = BasicUserDatastruct["User_IDs"] as? Int ?? 0
        UserName = BasicUserDatastruct["UserName"] as? String ?? ""
        UserPhone = BasicUserDatastruct["UserPhone"] as? String ?? ""
        UserImage = BasicUserDatastruct["UserImage"] as? String ?? ""
        UserGender = BasicUserDatastruct["UserGender"] as? String ?? ""
    }
}

struct AccessDetails : Decodable
{
    var AccessToken: String
    var SupportContactNo: String
    var InviteMessage: String
}

extension AccessDetails {
    init(AccessDetails : Dictionary<String,Any>){
        
        AccessToken = AccessDetails["AccessToken"] as? String ?? ""
        SupportContactNo = AccessDetails["SupportContactNo"] as? String ?? ""
        InviteMessage = AccessDetails["InviteMessage"] as? String ?? ""
    }
}

struct sub : Decodable {
    var FirstName : String
    var LastName : String
    var MiddleName : String
    var Phone : String
    var AnnDate : String
    var Street : String
    var Area : Int?
    var City : Int
    var State : Int
    var Country : Int
    var Pincode : String
    var Relation : String
    var UserNo : Int
    var BuildingName : String?
    var BuildingNo : String?
    var AreaName : String
    var CityName : String
    var StateName : String
    var CountryName : String
    var PincodeId : Int?
    var Image : String
    var Photo : String?
    var ParentUserNo : String
    var UserReferValue : String
    var PreferredLanguage : String?
    var IsActive : String?
    var userno : Int
    var relation : String?
    var Mobile : String
    var Email : String
    var Gender : String
    var AlternateMobile : String?
    var BirthDate : String
    var MStatus : String?
    var BloodGroup : String
    var PreferedLanguage : Int
}

struct ChangeImageStruct:Decodable
{
   var Message:String
   var Success:Int
   var ReturnImages:String
}

struct imageFile:Decodable
{
    var Message: String
    var Success: Int
    var ReturnImages: String
}

struct FAQDataArray:Decodable
{
    var Message:String?
    var Success:Int
    var FaqList:[FAQList]
}

struct FAQList:Decodable
{
    var _que :String
    var ans:String
    var opened:Bool? = false
}


struct EmergencyDataList:Decodable 
{
    var Message:String?
    var Success:Int
    var ECList:[EmergencyContactNumbers]
}

struct EmergencyContactNumbers:Decodable {
    var ContactName:String
    var ContactValue:Int
}
struct MobileNoChangeResponse:Decodable
{
    var Success: Int
    var Message: String
    var OTP: Int
}
struct OTPResponseStruct:Decodable {

    var Success: Int?
    var Message: String?
    var UserType: String?
    var userDetails: OTPResponseStruct2?
    var SupportContactNo: String?
    var InviteMessage: String?
    var AccessToken: String?
}

struct OTPResponseStruct2:Decodable {
    var User_IDs: Int
    var UserName: String?
    var UserPhone: String?
    var UserImage: String?
    var UserGUID: String?
    var UserGender: String?
    var AreaNo: Int?
    var AreaName: String?
    var Latitude: Double?
    var Longitude: Double?
    var BuildingName: String?
    var StreetName: String?
    var CityName: String?
    var Pincode: String?
    var StateName: String?
    var CountryName: String?
    var Age: String?
    var EmailID: String?
    var MobileNo: String?
}
struct ChangeMobileNumberOtpStruct : Decodable
{
    var Success: Int
    var Message: String
    var User_IDv: Int
    var Gender: String
}

// home response
struct HomePageResponse:Decodable {
    var Success: Int
    var Message: String
    var RecentResult: RecentResult
    var CityList: [CityList]?
    var SpecialityList: [SpecialityList]?
    var UserDetail: UserDetail
    var SupportContactNo: String
    var InviteMessage: String
    var AccessToken: String
    var TopBannerList: [TopBannerList]
    var UpcomingBannerList: [UpcomingBannerList]
    var HomePopup: HomePopup
}

struct RecentResult:Decodable {
    var UpcomingAppCount: Int
    var DueAppCount: Int
    var PendingFeedbackCount: Int
    var ClinicSearch: [ClinicSearch]
    var DrSearch: [DrSearch]?
    var MyDoctors: [MyDoctors]?
    var SponsorList: [SponsorList]?
}

struct ClinicSearch:Decodable {
    var ClinicName: String
    var ClinicID: Int
    var CityName: String
    var Speciality: String
    var AreaName: String
    var ClinicLogo: String
    var ClinicAddress: String
}
struct DrSearch:Decodable {
    
    var DoctorName: String
    var Speciality: String
    var DoctorImage: String
    var Rating: Int
    var Recommended: Int
    var DrFavourite: String
    var DoctorNo: Int
}

struct MyDoctors:Decodable {
    var DoctorName: String
    var Speciality: String
    var DoctorImage: String
    var Rating: Int
    var Recommended: Int
    var DrFavourite: String
    var DoctorNo: Int
}

struct SponsorList:Decodable {
    var SLogoPath: String
    var SName: String
    var SAddress: String
    var SSpecialities: String
    var STimings: [STimings]
    var SPhotos: [String]
    var AppointmentType: String
    var AppointmentCall: String
    var ClinicMobileNo: String
    var About: String
    var ClinicID: Int
}
struct STimings:Decodable {
    var Timing: String
}
struct CityList:Decodable {
    var CityID: Int
    var CityName: String
    var LanguageCityName: String
    var BookingFee: String
}
struct SpecialityList:Decodable {
    var SpecialityNo: Int
    var Speciality: String
    var LanguageSpeciality: String
    var Spe_CategoryNo: Int
    var Spe_CategoryName: String
}
struct UserDetail:Decodable
{
    var User_IDs: Int?
    var UserName: String?
    var UserPhone: String?
    var UserImage: String?
    var UserGUID: String?
    var UserGender: String?
    var AreaNo: Int?
    var AreaName: String?
    var Latitude: Double?
    var Longitude: Double?
    var BuildingName: String?
    var StreetName: String?
    var CityName: String?
    var Pincode: String?
    var StateName: String?
    var CountryName: String?
    var Age: String?
    var EmailID: String?
    var MobileNo: String?
}
struct TopBannerList:Decodable {
    var BannerContent: String
    var BannerLocation: String
    var RedirectURL: String
}
struct UpcomingBannerList:Decodable {
    var BannerContent: String
    var BannerLocation: String
    var RedirectURL: String
}
struct HomePopup:Decodable{
    var PopupURL: String
    var PopupCloseButtonText: String
    var PopupActionButtonText: String
    var PopupActionButtonActivityname: String
    var TandCButtonText: String
    var TandCButtonRedirectionURL: String
}

// Manage page

struct FamilyArrayStruct : Decodable
{
    var ListFamily: [FamilyDetailsStruct]
    var Message: String
    var Success: Int
}

struct FamilyDetailsStruct : Decodable {
    var Name: String
    var Relation: String
    var UserDetailId: Int
    var PhoneNo: String
    var Email: String?
    var DOB: String
    var Pincode: String?
    var Image: String
    var Gender: String
    var MartialStatus: String
    var BloodGroup: String
    var AlternateMobileNo: String?
}


extension FamilyDetailsStruct {
    init(FamilyDetailsStruct : Dictionary<String,Any>){
        
        Name = FamilyDetailsStruct["Name"] as? String ?? ""
        Relation = FamilyDetailsStruct["Relation"] as? String ?? ""
        UserDetailId = FamilyDetailsStruct["UserDetailId"] as? Int ?? 0
        PhoneNo = FamilyDetailsStruct["PhoneNo"] as? String ?? ""
        Email = FamilyDetailsStruct["Email"] as? String ?? ""
        DOB = FamilyDetailsStruct["DOB"] as? String ?? ""
        Pincode = FamilyDetailsStruct["Pincode"] as? String ?? ""
        Image = FamilyDetailsStruct["Image"] as? String ?? ""
        Gender = FamilyDetailsStruct["Gender"] as? String ?? ""
        MartialStatus = FamilyDetailsStruct["MartialStatus"] as? String ?? ""
        BloodGroup = FamilyDetailsStruct["BloodGroup"] as? String ?? ""
        AlternateMobileNo = FamilyDetailsStruct["AlternateMobileNo"] as? String ?? ""
    }
}

struct DoctorsInitalStruct : Decodable
{
    var DrSearchList: [DrSearchList]?
    var ClinicSearchList: [ClinicSearchList]?
    var SpecialitySearchList: [SpecialitySearchList]?
    var SplList: [SplList]?
    var CategorizedList: [CategorizedList]
    var Message: String
    var Success: Int
}
struct DrSearchList : Decodable
{
    var DrpText: String?
    var DrpValue: Int?
    var `Type`: String?
    var title: String?
    var imgsrc: String?
    var AreaNo: Int?
    var AreaName: String?
    var CityNo: Int?
    var SearchType: String?
    var SearchDisplay: String?
    var CityName: String?
    var SearchName: String?
    var SearchMatches: Int?
    var Latitude: Float?
    var Longitude: Float?
    var GroupType: String?
}

struct ClinicSearchList : Decodable{
    var DrpText: String?
    var DrpValue: Int?
    var `Type`: String?
    var title: String?
    var imgsrc: String?
    var AreaNo: Int?
    var AreaName: String?
    var CityNo: Int?
    var SearchType: String?
    var SearchDisplay: String?
    var CityName: String?
    var SearchName: String?
    var SearchMatches: Int?
    var Latitude: Float?
    var Longitude: Float?
    var GroupType: String?
}

struct SpecialitySearchList : Decodable{
    var DrpText: String?
    var DrpValue: Int?
    var `Type`: String?
    var title: String?
    var imgsrc: String?
    var AreaNo: Int?
    var AreaName: String?
    var CityNo: Int?
    var SearchType: String?
    var SearchDisplay: String?
    var CityName: String?
    var SearchName: String?
    var SearchMatches: Int?
    var Latitude: Float?
    var Longitude: Float?
    var GroupType: String?
}

struct SplList : Decodable
{
    var SpecialityNo: Int?
    var Speciality: String?
    var LanguageSpeciality: String?
    var Spe_CategoryNo: Int?
    var Spe_CategoryName: String?
}

struct CategorizedList : Decodable
{
    var opened:Bool? = false
    var SpecCategoryNo: Int?
    var SpecCategoryName: String?
    var LSCategoryName: String?
    var SpecList: [SpecList]?
}
struct SpecList : Decodable
{
    var SpecialityNo: Int?
    var Speciality: String?
    var LanguageSpeciality: String?
    var Spe_CategoryNo: Int?
    var Spe_CategoryName: String?
}

struct HealthReminderStruct : Decodable {
    var Success: Int
    var Message: String
    var ReminderList: [ReminderList]?
}

struct ReminderList : Decodable
{
    var HReminderID: Int?
    var UserID: Int?
    var ReminderType: String?
    var ReminderTitle: String?
    var Dosage: String?
    var NoofTimesperday: Int?
    var Timings: String?
    var Frequency: String?
    var FrequencyDetails: String?
    var Duration: Int?
    var StartDate: String?
    var EndDate: String?
    var Comments: String?
    var Status: String?
    var Language: String?
    var ParentUserNo: Int?
    var FamilyNewName: String?
    var Action: String?
    var DeActivate: String?
}

struct ReminderCancelResponse: Decodable
{
    var Success:Int
    var Message:String
    var HReminderID:Int
}

struct NotificationStruct : Decodable
{
    var Success : Int
    var Message : String
    var NotifyList : [NotifyList]?
    
}
struct NotifyList : Decodable
{
    var CategoryID : Int?
    var ReferenceID : Int?
    var NotificationMessage : String?
    var Datetime : String?
    var ImageURL : String?
    var Status : String?
    var TitleName : String?
    var RedirectType : String?
    var RedirectURL : String?
    var RedirectValue : String?
    var NotificationDate : String?
}

// Health Record Struct
struct HealthRecordResponseStruct : Decodable
{
    var Success : Int
    var Message : String
    var MedRecList : [MedRecList]?
}
struct MedRecList:Decodable
{
    var UserID : Int?
    var MedRecordId : Int?
    var FileNo : Int?
    var DoctorName : String?
    var ConsultingDate : String?
    var ConsultingTime : String?
    var DocumentName : String?
    var AppointmentNo : String?
    var DocumentDate : String?
    var PatientName : String?
    var filename : [filename]?
    var DocumentType : String?
    var LangDocumentType : String?
}
struct filename:Decodable
{
    var FileNo : Int?
    var FileLocation : String?
    var OriginalLocation : String?
    var DocumentType : String?
}

// My Favorite Page

struct MyFavoriteStruct : Decodable
{
    var Result : Result
}

struct Result: Decodable
{
     var  DoctorFavourites: [DoctorFavourites]
     var  MedicalFavourites: [MedicalFavourites]
     var  OpticalsFavourites: [OpticalsFavourites]
     var  LabFavourites: [LabFavourites]
     var  Success: Int
     var  Message: String
     var  ModelState: String?
}

struct DoctorFavourites: Decodable
{
     var  DoctorName: String?
     var  Speciality: String?
     var  DoctorImage: String?
     var  Rating: Float?
     var  Recommended: Int?
     var  DrFavourite: String?
     var  DoctorNo: Int
}

struct MedicalFavourites: Decodable
{
    var MEDICALNO: Int
    var FIRMNAME: String
    var MOBILENO: String?
    var LOGOFILENAME: String
    var ADDRESSONE: String
    var ADDRESSTWO: String
    var ISFAVOURITE: String
}

struct OpticalsFavourites:Decodable
{
    var OPTICALSNO: Int
    var FIRMNAME: String
    var MOBILENO: String?
    var LOGOFILENAME: String
    var ADDRESSONE: String
    var ADDRESSTWO: String
    var ISFAVOURITE: String
}

struct LabFavourites:Decodable
{
    var DIAGNOSTICLABNO: Int
    var FIRMNAME: String
    var MOBILENO: String?
    var LOGOFILENAME: String
    var ADDRESSONE: String
    var ADDRESSTWO: String
    var ISFAVOURITE: String
}

// Pending FeedBack Struct
struct pendingFeedBackStruct: Decodable
{
     var Success : Int
     var Message : String
     var Reviews : [Reviews]
}
struct Reviews:Decodable
{
     var DoctorNo : Int
     var AppointmentNo : Int
     var DoctorName : String
     var DoctorImage : String
     var AppointmentDate : String
     var AppointmentTime : String
     var AppointmentStatus : String
}

// Completed FeedBack Struct
struct CompletedFeedBackStruct: Decodable
{
    var Success :  Int
    var Message : String
    var GetUserRating : [GetUserRating]
}
struct GetUserRating:Decodable
{
    var OverallRate :  Float?
    var Cleanliness :  Float?
    var Politeness :  Float?
    var StaffCourteous :  Float?
    var DiseaseDiagnosis :  Float?
    var DiseaseExplained :  Float?
    var Name : String
    var Comments : String?
    var Recommendation : String
    var Reply : String?
    var CommentedDate : String
    var UserImagePath : String
    var RatingID :  Int
    var Gender : String
    var PendingReply : String
    var AppointmentNo :  Int
    var AppointmentDate : String
    var AppointmentTime : String
    var AppointmentStatus : String
    var DrImage : String
    var Recommend :  Int
    var TotalRecommend :  Int
}

// upcoming Appointment Struct
struct UpcomingAppointmentStruct: Decodable
{
    var Success : Int
    var Message : String
    var UpcomingAppointments : [UpcomingAppointments]?
}

struct UpcomingAppointments: Decodable
{
    var AL1_DoorNo : String
    var AL2_LandMark : String
    var AL3_Street : String
    var AL4_Area : String
    var AL5_City : String
    var AL6_Pincode : Int
    var PatientName : String
    var patientRelation : String?
    var Appoinmentid : Int
    var DoctorId : Int
    var DoctorName : String
    var AppointmentDate : String
    var RequestedTime : String
    var Speciality : String
    var ClinicId : Int
    var ClinicName : String
    var Address : String
    var CancelledDate : String?
    var Remarks : String?
    var Status : String
    var Action : String?
    var Action1 : String?
    var Lattitude : Double
    var Longitude : Double
    var ReferenceID : String
    var ReasonforVisit : String
    var CheckIn : String
    var BookingFee : String
    var BookingStatus : String
    var UserNo : Int
    var Session : Int
    var UserMobileNo : String
    var AppointmentTimeHrs : String?
    var MedicalRecordFile : String?
    var RatingFlag : String?
    var ClinicPhotos : [String]?
    var DrImage : String
    var DoctorFees : Int
    var ClinicMobile : String?
    var PrescriptionAvailable : String?
    var MRecordFiles : [MRecordFiles]?
    var Gender : String
    var Mobile : String?
    var Latitude : Double?
    var Doctorid : Int?
    var ExpectedTime : String?
}

//Past Appointment Struct
struct PastAppointmentStruct:Decodable
{
    var Success : Int
    var Message : String
    var PastAppointments : [PastAppointments]
}

struct PastAppointments:Decodable
{
    var AL1_DoorNo : String?
    var AL2_LandMark : String?
    var AL3_Street : String?
    var AL4_Area : String?
    var AL5_City : String?
    var AL6_Pincode : Int?
    var PatientName : String
    var patientRelation : String?
    var Appoinmentid : Int
    var DoctorId : Int
    var DoctorName : String
    var AppointmentDate : String
    var RequestedTime : String
    var Speciality : String?
    var ClinicId : Int?
    var ClinicName : String?
    var Address : String?
    var CancelledDate : String?
    var Remarks : String?
    var Status : String?
    var Action : String?
    var Action1 : String?
    var Lattitude : Double?
    var Longitude : Double?
    var ReferenceID : String
    var ReasonforVisit : String?
    var CheckIn : String?
    var BookingFee : String?
    var BookingStatus : String?
    var UserNo : Int
    var Session : Int?
    var UserMobileNo : String?
    var AppointmentTimeHrs : String?
    var MedicalRecordFile : String?
    var RatingFlag : String?
    var ClinicPhotos : [String]?
    var DrImage : String?
    var DoctorFees : Int?
    var ClinicMobile : String?
    var PrescriptionAvailable : String?
    var MRecordFiles : [MRecordFiles]?
    var Gender : String?
    var Mobile : String?
    var Latitude : Double?
    var Doctorid : Int?
    var ExpectedTime : String?
}

struct MRecordFiles:Decodable
{
    var FileNo : Int
    var FileLocation : String
    var OriginalLocation : String?
    var DocumentType : String?
}

//Due Appointment Struct
struct DueAppointmentStruct: Decodable
{
    var DueAppointment : [DueAppointment]?
    var Success : Int
    var Message : String
}

struct DueAppointment:Decodable
{
    var AL1_DoorNo : String?
    var AL2_LandMark : String?
    var AL3_Street : String?
    var AL4_Area : String?
    var AL5_City : String?
    var AL6_Pincode : Int?
    var PatientName : String
    var patientRelation : String?
    var Appoinmentid : Int
    var DoctorId : Int
    var DoctorName : String
    var AppointmentDate : String
    var RequestedTime : String
    var Speciality : String?
    var ClinicId : Int
    var ClinicName : String
    var Address : String
    var CancelledDate : String?
    var Remarks : String?
    var Status : String
    var Action : String?
    var Action1 : String?
    var Lattitude : Double
    var Longitude : Double
    var ReferenceID : String
    var ReasonforVisit : String?
    var NextVisitDate : String?
    var AppointDay : String?
    var ClinicPhotos : [String]?
    var DrImage : String
    var DoctorFee : Int?
    var ClinicMobile : String?
    var Gender : String
    var UserID : Int
}

struct DoctorProfileStruct : Decodable
{
    var Success : Int
    var Message : String?
    var DrDetailsFull : DrDetailsFull?
}

struct DrDetailsFull : Decodable
{
    var ID : Int?
    var Name : String
    var Degree : String?
    var Experience : Int?
    var Fees : Int?
    var Address : String?
    var Landmark : String?
    var StateMent : String?
    var Specialities : String?
    var Language : String?
    var Recommended : Int?
    var Rating : Double?
    var DrRecPercentage : Int?
    var HomeVisit : String?
    var HomeVisitMobile : String?
    var Hospital : [Hospital]?
    var Service : String?
    var Education : String?
    var Professional : String?
    var Achivement : String?
    var Comment : String?
    var Tips : String?
    var FavouriteDr : String?
    var DrImage : String?
    var DrRegNo : String?
    var DrEmail : String?
    var DrPhone : String?
    var ProfileLink : String?
    var About : String?
    var Image : String?
    var Furl : String?
    var Turl : String?
    var Lurl : String?
    var DoctorHios : Hospital?
    var RatingReview : [RatingReview]?
}

struct Hospital:Decodable
{
    var HospitalName : String?
    var AddressOne : String?
    var AddressTwo : String?
    var AddressThree : String?
    var AddressFour : String?
    var Phone1 : String?
    var Email : String?
    var lat : Double?
    var lang : Double?
    var HosId : Int?
    var HosImage : [String]?
    var HosREGNno : String?
    var Phone2 : String?
    var Phone3 : String?
    var MobileNo : String?
    var ClinicTimings : [ClinicTimings]?
    var QueryHospital : String?
    var HospitalID : Int?
    var Phone : String?
    var About : String?
    var Images : String?
    var DoctorID : Int?
    var BranchID : Int?
    var Day : Int?
    var Photos : [String]?
    var ActualAddress : String?
    var ClinicG : String?
    var CityNo : Int?
    var Amount : String?
    var DiscountedAmount : String?
    var OfferRate : String?
}

struct ClinicTimings:Decodable
{
    var Timing : String?
}

struct RatingReview:Decodable
{
    var Comment : String?
    var Reply : String?
    var ReviewDate : String?
    var UserName : String?
    var OveralRate : Double?
    var Clean : Int?
    var Polite : Int?
    var Staff : Int?
    var Diagnos : Int?
    var Explained : Int?
    var Recommend : String?
    var UserDet : UserDet?
    var DoctorDet : DoctorDet?
}

struct UserDet:Decodable
{
    var UserName : String?
    var UserImage : String?
    var UserDetailNo : Int?
}

struct DoctorDet:Decodable
{
    var DoctorName : String?
    var DoctorImage : String?
    var DoctorTitle : String?
    var DoctorNo : Int?
}

// rating
struct DoctorRatingStruct: Decodable
{
    var Success : Int
    var Message : String
    var Rating : [Rating]?
    var UsersComment : [Rating]?
    var OverallRating : String?
    var RecommendedPercentage : Double?
    var TotalRecommended : Double?
}

struct Rating:Decodable
{
    var OverallRate : Double?
    var Cleanliness : Double?
    var Politeness : Double?
    var StaffCourteous : Double?
    var DiseaseDiagnosis : Double?
    var DiseaseExplained : Double?
    var Name : String?
    var Comments : String?
    var Recommendation : String?
    var Reply : String?
    var CommentedDate : String?
    var UserImagePath : String?
    var RatingID : Int?
    var Gender : String?
    var PendingReply : String?
    var AppointmentNo : Int?
    var AppointmentDate : String?
    var AppointmentTime : String?
    var AppointmentStatus : String?
    var DrImage : String?
    var Recommend : Int?
    var TotalRecommend : Double?
}

struct DoctorsHosptialTimings : Decodable
{
    var DoctorHospitals : [DoctorHospitals]?
    var Message : String
    var Success : Int
    var Name : String
    var Fees : Int?
    var DrImage : String
    var Degree : String?
    var Experience : Int?
    var Specialities : String?
    var DrAutoBooking : String?
    var BookingAlertMessage : String?
}

struct DoctorHospitals : Decodable
{
    var HospitalId : Int?
    var Hospital : String?
    var Address : String?
    var Lattitude : Double?
    var Longitude : Double?
    var Message : String?
    var BookingAmount : Int?
    var DiscountPercent : String?
    var FinalBookingAmount : String?
    var HosTiming : HosTiming?
    var Holiday : String?
    var ClinicContactNo : String?
    var ClinicEmail : String?
    var AddressOne : String?
    var AddressTwo : String?
    var AddressThree : String?
    var AddressFour : String?
    var IsScheduleAvailable : String?
    var DoctorName : String?
    var Qualification : String?
    var CityName : String?
    var AreaName : String?
    var CityNo : Int?
    var AreaNo : Int?
    var ActualAddress : String?
    var DrAutoBooking : Bool?
    var SDay : Int?
    var DoctorID : Int?
    var Status : Bool? = true
}

struct HosTiming : Decodable
{
    var Morning : String?
    var AfterNoon : String?
    var Evening : String?
    var Night : String?
}

struct AppointmentBookedStruct : Decodable
{
    var Success : Int?
    var Message : String?
    var TockenNumber : Int?
    var AppointmentNo: String?
    var AppointmentNoforSMS: Int?
    var ModelState: String?
}
struct LabBasicSearchStruct : Decodable
{
    var Result : Result1?
    var Id : Int
    //var Exception : String
    var Status : Int
    var IsCanceled : Bool
    var IsCompleted : Bool
    var CreationOptions : Int
    //var AsyncState : String
    var IsFaulted : Bool
}

struct GetLabByIdStruct : Decodable
{
    var Result : Result12?
    var Id : Int
    //var Exception : String
    var Status : Int
    var IsCanceled : Bool
    var IsCompleted : Bool
    var CreationOptions : Int
    //var AsyncState : String
    var IsFaulted : Bool
}

struct Result1:Decodable
{
    var TotalPage : Int?
    var LabData : [LabData]?
    //var CommonSponsorData : String
    var Success : Int
    var Message : String?
    //var ModelState : String
}

struct Result12:Decodable
{
    var TotalPage : Int?
    var LabData : LabData?
    //var CommonSponsorData : String
    var Success : Int
    var Message : String?
    //var ModelState : String
}

struct LabData : Decodable
{
    var LABFACILITIES : [String]?
    var TIMINGS : [String]?
    var LABIMAGES : [LABIMAGES]?
    var SHOWBOOKTEST : String?
    var DIAGNOSTICLABNO : Int?
    var ISHOMESAMPLE : Bool?
    var HOMESAMPLECONTACTNO : String?
    var BOOKTESTONLINE : Bool?
    var SPONSERREQUIRED : Bool?
    var BRANCHES : Bool?
    var ISMAINBRANCH : Bool?
    var ISDISOUNTAVAILABLE : Bool?
    var DISCOUNTAVAILABLECOTENT : String?
    var ACCREDITEDBY : String?
    var HOMEVISITMINAMOUNT : Float?
    var REPORTDELIVERYATHOME : String?
    var REPORTDELIVERYMINAMOUNT : Float?
    var FIRMNAME : String?
    var MOBILENO : String?
    var EMAIL : String?
    var CONTACTNAME : String?
    var CONTACTPERSON : String?
    var BUILDINGNO : String?
    var STREET : String?
    var AREANAME : String?
    var AREANO : Int?
    var PINCODE : Int?
    var PHONENO1 : String?
    var PHONENO2 : String?
    var LANDMARK : String?
    var CITYNAME : String?
    var CITYNO : Int?
    var SALEREPID : Int?
    var ACCREDITEDLICENCEHOLDERNAME : String?
    var ACCREDITEDLICENCENO : String?
    var DATEOFYEARESTABLISHED : String?
    var WEBSITE : String?
    var ABOUT : String?
    var LATITUDE : Double?
    var LONGITUDE : Double?
    var BILLRECEIPTNO : String?
    var TIMESTRING : String?
    var LOGOFILENAME : String?
    var VISITINGCARDFILENAME : String?
    var LICENCEPROOFFILENAME : String?
    var SUBCRIPTIONPLANAMOUNT : Int?
    var SUBSCRIPTIONFREQUENCY : String?
    var APPGUID : String?
    var ROWORDER : Int?
    var TOTALCOUNT : Int?
    var DISPLAYTYPE : String?
    var ADDRESSONE : String?
    var ADDRESSTWO : String?
    var ADDRESSONEID : String?
    var ADDRESSTWOID : String?
    var PHONENUMBERS : String?
    var ISFAVOURITE : String?
    var TIMEAVAILABLE : String?
    var LABNAMEID : String?
    var DISTANCEKM : Double?
    var USERDISTANCEKM : Double?
}

struct LABIMAGES:Decodable
{
    var DiagnosticLabNo : Int?
    var FileType : String?
    var ImageNo : Int?
    var ImageUrl : String?
}

// medical data struct
struct PharmacyBasicSearchStruct : Decodable
{
    var Result : Result2?
    var Id : Int?
    //var Exception : String?
    var Status : Int?
    var IsCanceled : Bool?
    var IsCompleted : Bool?
    var CreationOptions : Int?
    //var AsyncState : String?
    var IsFaulted : Bool?
}

struct GetPharmacyByIdStruct : Decodable
{
    var Result : Result21?
    var Id : Int?
    //var Exception : String?
    var Status : Int?
    var IsCanceled : Bool?
    var IsCompleted : Bool?
    var CreationOptions : Int?
    //var AsyncState : String?
    var IsFaulted : Bool?
}

struct Result2 : Decodable {
    var TotalPage : Int?
    var MedicalData : [MedicalData]?
    //var CommonSponsorData : String?
    var Success : Int?
    var Message : String?
    //var ModelState : String?
}

struct Result21 : Decodable {
    var TotalPage : Int?
    var MedicalData : MedicalData?
    //var CommonSponsorData : String?
    var Success : Int?
    var Message : String?
    //var ModelState : String?
}

struct MedicalData : Decodable {
    var TIMINGS : [String]?
    var MEDICALSHOPIMAGES : [LABIMAGES]?
    var SHOWUPLOADPRESCRIPTION : String?
    var MEDICALNO : Int?
    var DRUGLICENCEHOLDERNAME : String?
    var DRUGLICENCENO : String?
    var ISHOMEDELIVERY : Bool?
    var HOMEDELIVERYCONTACTNO : String?
    var UPLOADPRESCRIPTION : Bool?
    var FIRMNAME : String?
    var MOBILENO : String?
    var EMAIL : String?
    var CONTACTNAME : String?
    var CONTACTPERSON : String?
    var BUILDINGNO : String?
    var STREET : String?
    var AREANAME : String?
    var AREANO : Int?
    var PINCODE : Int?
    var PHONENO1 : String?
    var PHONENO2 : String?
    var LANDMARK : String?
    var CITYNAME : String?
    var CITYNO : Int?
    var SALEREPID : Int?
    var ACCREDITEDLICENCEHOLDERNAME : String?
    var ACCREDITEDLICENCENO : String?
    var DATEOFYEARESTABLISHED : String?
    var WEBSITE : String?
    var ABOUT : String?
    var LATITUDE : Double?
    var LONGITUDE : Double?
    var BILLRECEIPTNO : String?
    var TIMEString : String?
    var LOGOFILENAME : String?
    var VISITINGCARDFILENAME : String?
    var LICENCEPROOFFILENAME : String?
    var SUBCRIPTIONPLANAMOUNT : Int?
    var SUBSCRIPTIONFREQUENCY : String?
    var APPGUID : Int?
    var ROWORDER : Int?
    var TOTALCOUNT : Int?
    var DISPLAYTYPE : String?
    var ADDRESSONE : String?
    var ADDRESSTWO : String?
    var ADDRESSONEID : String?
    var ADDRESSTWOID : String?
    var PHONENUMBERS : String?
    var ISFAVOURITE : String?
    var TIMEAVAILABLE : String?
    var LABNAMEID : String?
    var DISTANCEKM : Double?
    var USERDISTANCEKM : Double?
}


struct OpticalsBasicSearchStruct : Decodable {

    var Result : Result3?
    var Id : Int?
    //var Exception : String?
    var Status : Int?
    var IsCanceled : Bool?
    var IsCompleted : Bool?
    var CreationOptions : Int?
    //var AsyncState : String?
    var IsFaulted : Bool?
}

struct Result3 : Decodable {
    
    var TotalPage : Int?
    var OpticalData : [OpticalData]?
    //var CommonSponsorData : String?
    var Success : Int?
    var Message : String?
    //var ModelState : String?
}

struct GetOpticalsByIdStruct : Decodable {
    
    var Result : Result31?
    var Id : Int?
    //var Exception : String?
    var Status : Int?
    var IsCanceled : Bool?
    var IsCompleted : Bool?
    var CreationOptions : Int?
    //var AsyncState : String?
    var IsFaulted : Bool?
}

struct Result31 : Decodable {
    
    var TotalPage : Int?
    var OpticalData : OpticalData?
    //var CommonSponsorData : String?
    var Success : Int?
    var Message : String?
    //var ModelState : String?
}


struct OpticalData : Decodable {
    var TIMINGS : [String]?
    var LABIMAGES : [LABIMAGES]?
    var SHOWOPTICALTEST : String?
    var OPTICALSNO : Int?
    var OPTICALLICENCEHOLDERNAME : String?
    var OPTICALSLICENCENO : String?
    var COMPUTERISEDTESTINGYORN : String?
    var TESTINGCONTACTNO : String?
    var FIRMNAME : String?
    var MOBILENO : String?
    var EMAIL : String?
    var CONTACTNAME : String?
    var CONTACTPERSON : String?
    var BUILDINGNO : String?
    var STREET : String?
    var AREANAME : String?
    var AREANO : Int?
    var PINCODE : Int?
    var PHONENO1 : String?
    var PHONENO2 : String?
    var LANDMARK : String?
    var CITYNAME : String?
    var CITYNO : Int?
    var SALEREPID : Int?
    var ACCREDITEDLICENCEHOLDERNAME : String?
    var ACCREDITEDLICENCENO : String?
    var DATEOFYEARESTABLISHED : String?
    var WEBSITE : String?
    var ABOUT : String?
    var LATITUDE : Double?
    var LONGITUDE : Double?
    var BILLRECEIPTNO : String?
    var TIMEString : String?
    var LOGOFILENAME : String?
    var VISITINGCARDFILENAME : String?
    var LICENCEPROOFFILENAME : String?
    var SUBCRIPTIONPLANAMOUNT : Int?
    var SUBSCRIPTIONFREQUENCY : String?
    var APPGUID : Int?
    var ROWORDER : Int?
    var TOTALCOUNT : Int?
    var DISPLAYTYPE : String?
    var ADDRESSONE : String?
    var ADDRESSTWO : String?
    var ADDRESSONEID : Int?
    var ADDRESSTWOID : Int?
    var PHONENUMBERS : String?
    var ISFAVOURITE : String?
    var TIMEAVAILABLE : String?
    var LABNAMEID : String?
    var DISTANCEKM : Double?
    var USERDISTANCEKM : Double?
}
struct LabSearchByName : Decodable
{
    var Result : Result4?
    var Id : Int?
    //var Exception : String?
    var Status : Int?
    var IsCanceled : Bool?
    var IsCompleted : Bool?
    var CreationOptions : Int?
    //var AsyncState : String?
    var IsFaulted : Bool?
}

struct Result4 : Decodable {
    
    var DisplayList : [DisplayList]?
    var Success : Int?
    var Message : String?
    var ModelState : String?
}

struct DisplayList : Decodable
{
    var DrpText : String?
    var DrpLangText : String?
    var DrpValue : Int?
    var CityNo : Int?
    var CityName : String?
    var AreaName : String?
    var title : String?
    var Latitude : Double?
    var Longitude : Double?
    var AreaDisKM : Double?
    var SearchDisplay : String?
    var SearchName : String?
    var SearchMatches : Int?
}

// get cities
struct TopCitiesStruct : Decodable
{
    var City : [City]
    var RecentAreas : [RecentAreas]
    var Success : Int
    var Message : String
}

struct City : Decodable {
    
    var CityID : Int?
    var CityName : String?
    var LanguageCityName : String?
    var BookingFee : String?
}

struct RecentAreas : Decodable {
    
    var DrpText : String?
    var DrpValue : Int?
    //var Type : String?
    var title : String?
    var imgsrc : String?
    var AreaNo : Int?
    var AreaName : String?
    var CityNo : Int?
    var SearchType : String?
    var SearchDisplay : String?
    var CityName : String?
    var SearchName : String?
    var SearchMatches : Int?
    var Latitude : Double?
    var Longitude : Double?
    var GroupType : String?
}

// fav reply
struct FavStruct : Decodable {
    
    var Result: LoginResponse?
}


// Clinic struct
struct ClinicProfileStruct : Decodable
{
    var Success : Int?
    var Message : String?
    var ProfileDetails : ProfileDetails?
    var ClinicReviews : [ClinicReviews]?
}

struct ProfileDetails: Decodable {
    var HospitalNo : Int?
    var AboutClinic : String?
    var HospitalName : String?
    var ClinicAddress : String?
    var ClinicRating : Float?
    var RecommendationCount : Float?
    var RecommendedPercentage : Float?
    var ClinicSpeciality : [String]?
    var ClinicContactNo : String?
    var ClinicLogo : String?
    var ClinicPhotos : [String]?
    var ClinicTimings : [ClinicTimings]?
    var Latitude : Double?
    var Longitude : Double?
    var ProfileLink : String?
    var DoctorList : [DoctorList]?
    var Services : [Services]?
    var Facilities : [Services]?
}

struct DoctorList: Decodable
{
var ID : Int?
var Name : String?
var Degree : String?
var Speciality : String?
var Address : String?
var Expereience : Int?
var Image : String?
var Fees : Int?
var Gender : String?
var Language : String?
var DrRating : Float?
var DrRecommendedCount : Float?
var DrRecPercentage : Float?
var DrFavourite : String?
}

struct Services: Decodable
{
    var `Type` : String?
var Detail : String?
}

struct ClinicReviews: Decodable
{
var Comment : String?
var Reply : String?
var ReviewDate : String?
var OveralRate : Float?
var Cleanliness : Float?
var Politeness : Float?
var StaffCourteous : Float?
var DiseaseDiagnos : Float?
var DiseaseExplained : Float?
var Recommended : String?
var UserDetail : UserDetail1?
var DoctorDetail : DoctorDetail?
}

struct UserDetail1: Decodable {
    var UserName : String?
    var UserImage : String?
}

struct DoctorDetail: Decodable {
    var DoctorName : String?
    var DoctorImage : String?
}

// Language Struct

struct LanguagesStruct : Decodable {
    var Success: Int?
    var LangList : [LangList]?
}

struct LangList : Decodable {
    var DrpText : String?
    var DrpValue : Int?
    var DropText : String?
    var DropValue : Int?
    var SubDropText : String?
    var DrpLangText : String?
}

// doctor Search By speciality
struct SearchBySpecialityStruct : Decodable {
    
    var Success : Int?
    var Message : String?
    var SearchList : [SearchList]?
}

struct SearchList : Decodable {
    
    var DrpText : String?
    var DrpValue : Int?
    var `Type` : String?
    var title : String?
    var imgsrc : String?
    var AreaNo : Int?
    var AreaName : String?
    var CityNo : Int?
    var SearchType : String?
    var SearchDisplay : String?
    var CityName : String?
    var SearchName : String?
    var SearchMatches : Int?
    var Latitude : Double?
    var Longitude : Double?
    var GroupType : String?
}

// Doctor Basic search struct

struct DoctorBasicSearchStruct : Decodable
{
    var Success : Int?
    var Message : String?
    var CurrentPageNumber : Int?
    var TotalPages : Int?
    var NearestShown : String?
    var NearByMessage : String?
    var AvlDoctors : [AvlDoctors]?
    var AdDetails : [AdDetails]?
}

struct AdDetails: Decodable
{
    var Ad_Location : String?
    var Ad_Redirectpath : String?
}

struct HosDetail : Decodable
{
    var HospitalID : Int?
    var HospitalName : String?
    var AL4_Area : String?
    var ClinicPincode : String?
    var DistanceKMFromUser : String?
    var DistanceKm : Double?
    var ClinicPhotos : [String?]
    var ClinicTimings : [ClinicTimings]
}

struct Timings : Decodable
{
    var Speciality : String?
    var IndexID : Int?
}

struct Hospitals : Decodable
{
    var Timings : [Timings]
    var Number : Int?
    var HospitalName : String?
    var Address : String?
    var AddressOne : String?
    var AddressTwo : String?
    var AddressThree : String?
    var MobileNo : String?
    var Photos : [String?]
}

struct AvlDoctors : Decodable
{
    var AdDetails : [AdDetails]?
    var HosDetail : [HosDetail]?
    var ID : Int?
    var Name : String?
    var Degree : String?
    var Speciality : String?
    var Address : String?
    var Expereience : Int?
    var Image : String?
    var Fees : Int?
    var Gender : String?
    var Language : String?
    var AL1_DoorNo : String?
    var AL2_LandMark : String?
    var AL3_Street : String?
    var AL4_Area : String?
    var AL5_City : String?
    var AL6_Pincode : Int?
    var MobileNo : String?
    var OfficeNo : String?
    var Latitude : Double?
    var Longitude : Double?
    var DrRating : Double?
    var DrRecommendedCount : Int?
    var DrRecPercentage : Double?
    var DrFavourite : String?
    var DoctorRegisteredAs : String?
    var DoctorAvailable : String?
    var HomeVisit : String?
    var ResultType : String?
    var About : String?
    var ClinicNo : Int?
    var ClinicName : String?
    var ClinicAreaName : String?
    var ClinicCityName : String?
    var ClinicPincode : String?
    var ClinicPhotos : [String?]?
    var ClinicImage : String?
    var ClinicRating : Double?
    var ClinicRecommendedCount : Int?
    var ClinicRecommendedPercentage : Double?
    var ClinicSpeciality : String?
    var DistanceKMFromUser : String?
    var ClinicTimings : [ClinicTimings]?
    var Is24HourService : String?
    var EmergencyCareAvailable : String?
    var EmergencyMobile : String?
    var EmergencyPhone : String?
    var HomeVisitMobile : String?
    var HomeVisitPhone : String?
    var ClinicHomeVAvailable : String?
    var ClinicHomeVMobile : String?
    var DistanceKm : Double?
    var QueryHospital : String?
    var `Type` : String?
    var EncryptedID : String?
    var DrApproved : Bool?
    var RecommendedPercentage : Double?
    var DrTitle : String?
    var Phone1 : String?
    var Phone2 : String?
    var Phone3 : String?
    var ClinicMobileNo : Int?
    var ProfileView : String?
    var Hospitals : [Hospitals]?
    var Category : String?
    var Sno : Int?
    var TOTALPAGECOUNT : Int?
    var ROWORDER : Int?
    var displaytype : String?
    var Profileurl : String?
}

