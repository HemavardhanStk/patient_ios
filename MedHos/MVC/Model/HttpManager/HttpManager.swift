//
//  HttpManager.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire
import Foundation

struct AddInvoiceResponse: Decodable {
    let Success : Int
    let Message: String?
}

class HttpManager: NSObject {
    
    static let shared: HttpManager = HttpManager()
    
    private var alamoFireManager = Alamofire.SessionManager.default as SessionManager
    
    let decoder = JSONDecoder()
    
    private var isSessionCancelled = false
    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func setAFconfig(){
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 4
        configuration.timeoutIntervalForRequest = 4
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    func cancelAllRequests() {
        
        self.isSessionCancelled = true
        alamoFireManager.session.invalidateAndCancel()
        setAFconfig()
    }
    
    // MARK: Load Methods
    
    func loadAPICall(path: String,
                     params: Dictionary<String, Any>,
                     httpMethod: HTTPMethod,
                     controller: UIViewController,
                     isBackgroundCall: Bool = false,
                     completion: @escaping (_ response: Dictionary<String, AnyObject>) -> Void, failure: @escaping () -> Void) {
        
        if !connectedToNetwork() {
            
            failure()
            if !isBackgroundCall {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    AlertHelper.shared.showAlert(message: MSG_NETWORK_ERROR, controller: controller)
                })
            }
            return
        }
        
        if !isBackgroundCall {
            LoadingIndicator.shared.show(controller)
        }
        
        if let url = URL.init(string: "\(BASE_URL)\(path)") {
            
            var headers: HTTPHeaders = [:]
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Basic c2Vuc3VyZWFuZHJvaWQ6QW5kcm9pZDEyMyM="
            
            print("REQUEST: \(url) \(params) \(headers)")
            
            
            alamoFireManager.request(url,
                                     method: httpMethod,
                                     parameters: params.count != 0 ? params : nil,
                                     encoding: JSONEncoding.default,
                                     headers: headers).responseJSON { response in
                
                DispatchQueue.main.async {
                    
                    if !isBackgroundCall {
                        LoadingIndicator.shared.hide(controller)
                    }
                    
                    print("RESPONSE SUCCESS ===>> \(response.result.value as Any)")
                    
                    if (response.result.error == nil) {
                        
                        if let responseDict = response.result.value as? Dictionary<String, AnyObject> {
                            completion(responseDict)
                        }
                    } else {
                        
                        print("RESPONSE FAILURE ===>> \(response.result.error as Any)")
                        failure()
                        if !isBackgroundCall && !self.isSessionCancelled {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                AlertHelper.shared.showAlert(message: MSG_SERVER_ERROR, controller: controller)
                            })
                        }
                    }
                }
            }
        }
    }

    // File Upload
    
    func uploadImageWithParametersAnd(endUrl: String,
                                      photo: [UIImage]?,
                                      dataArray: [Data] = [],
                                      parameters: [String : Any]?,
                                      headers: HTTPHeaders?,
                                      jsonName: String,
                                      controller: UIViewController,
                                      isBackgroundCall: Bool = false,
                                      completion: @escaping (_ success: Bool, _ addInvoiceResponse: AddInvoiceResponse?,_ response: Dictionary<String, AnyObject>) -> Void ) {
        
        if !connectedToNetwork() {
            
            if !isBackgroundCall {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    AlertHelper.shared.showAlert(message: MSG_NETWORK_ERROR, controller: controller)
                })
            }
            return
        }
        
        if !isBackgroundCall {
            LoadingIndicator.shared.show(controller)
        }
        
        if let url = URL.init(string: "\(BASE_URL)\(endUrl)") {
        
        var header: HTTPHeaders = [:]
        header["Content-Type"] = "multipart/form-data"
        header["Authorization"] = "Basic c2Vuc3VyZWFuZHJvaWQ6QW5kcm9pZDEyMyM="
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters!, options: []) else {
             return
        }
        
       let param = [jsonName:httpBody]
            
            print("REQUEST: \(url) \(param) \(String(describing: parameters)) \(header)")
        
        alamoFireManager.upload(multipartFormData: { (multipartFormData) in
            if photo?.count != 0 {
                for i in 0...photo!.count - 1 {
                    if let data = photo![i].jpegData(compressionQuality: 0.5) {
                        multipartFormData.append(data, withName: "FileName", fileName: "image.jpg", mimeType: "image/jpg")
                    }
                }
            }
            
            if dataArray.count != 0 {
                for i in 0..<dataArray.count {
                    multipartFormData.append(dataArray[i] as Data, withName: "FileName", fileName: "pdfName.pdf", mimeType: "application/pdf")
                }
            }
            
            for (key, value) in param {
                multipartFormData.append(value, withName: "\(key)")
            }
            
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header) { (result) in
        DispatchQueue.main.async {
            
            switch result {
                
            case .failure(let error):
                print("UploadImageController.requestWith.Alamofire.usingThreshold:", error)
                completion(false, nil, [:])
                
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON(completionHandler: { (response) in
                    
                    if !isBackgroundCall {
                        LoadingIndicator.shared.hide(controller)
                    }
                    
                    switch response.result {
                        
                    case .failure(let error):
                        
                        print("UploadImageController.requestWith.Alamofire.upload.responseJSON:", error)
                        
                        completion(false, nil, [:])
                        
                    case .success( _):
                        
                        print("UploadImageController.requestWith.Alamofire.upload.responseJSON Succes")
                        guard let data = response.data else { return }
                        
                        print("RESPONSE SUCCESS ===>> \(response.result.value as Any)")
                        
                        do {
                            let addInvoiceResponse = try self.decoder.decode(AddInvoiceResponse.self, from: data)
                            
                            if let responseDict = response.result.value as? Dictionary<String, AnyObject> {
                                completion(true, addInvoiceResponse,responseDict)
                            }
                            
                            completion(true, addInvoiceResponse, [:])
                            
                        } catch let jsonError {
                            
                            print("Error serializing json.ProfileController.getProfile:", jsonError)
                            completion(false, nil, [:])
                        }
                    }
                })
              }
           }
        }
      }
    }
    
}

