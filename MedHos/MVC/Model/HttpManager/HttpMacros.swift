//
//  HttpMacros.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation

let MSG_NETWORK_ERROR                           = "No network connection"
let MSG_SERVER_POINT_ERROR                      = "Could not point to server"
let MSG_SERVER_ERROR                            = "Could not connect to server"
let MSG_REQUEST_CANCEL_ERROR                    = "Your request was cancelled"

let BASE_URL                                    = "http://service.sensure.in/api/"       // TEST
//let BASE_URL                                    = "http://jsonservice.medhos.in/api/"    // LIVE

// Index
let PATH_CheckVersionControl                    = "user/CheckVersionControl"
let PATH_SupportDetails                         = "user/SupportDetails"
let PATH_User_RegisterAndLogin                  = "user/User_RegisterAndLogin"
let PATH_ResendOTPLoginRegister                 = "user/ResendOTPLoginRegister"
let PATH_VerifyUserLoginOTP                     = "user/VerifyUserLoginOTP"
let PATH_GetOTP                                 = "user/GetOTP"

// Home page
let PATH_GetSearchSummaryDetails                = "user/GetSearchSummaryDetails"
let PATH_GetCartBucketResult                    = "DLab/GetCartBucketResult"
let PATH_GetLanguages                           = "user/GetLanguages"

// Clinic Profile
let PATH_GetClinicProfile                       = "user/GetClinicProfile"

// Doctor
let PATH_ShowSpeciality                         = "user/ShowSpeciality"
let PATH_SearchBySpecialization                 = "user/SearchBySpecialization"
let PATH_BasicSearch                            = "user/BasicSearch"
let PATH_HomeVisitBooking                       = "user/HomeVisitBooking"
let PATH_EmergencyBooking                       = "user/EmergencyBooking"
let PATH_GetDrProfile                           = "user/GetDrProfile"
let PATH_UserViewIndividualsRating              = "user/UserViewIndividualsRating"
let PATH_ViewBookingAllHospitals                = "user/ViewBookingAllHospitals"
let PATH_AppoinmentBooking                      = "user/AppoinmentBooking"
let PATH_AppointmentSMStoUser                   = "user/AppointmentSMStoUser"

// Lab
let PATH_DiagnosticLabBasicSearch               = "DLab/DiagnosticLabBasicSearch"
let PATH_SearchByLabName                        = "DLab/SearchByLabName"
let PATH_GetDiagnosticLabTestList               = "DLab/GetDiagnosticLabTestList"
let PATH_AddTesttoCart                          = "DLab/AddTesttoCart"
let PATH_RemoveTestfromCart                     = "DLab/RemoveTestfromCart"
let PATH_GetPackageDetailfromID                 = "DLab/GetPackageDetailfromID"
let PATH_GetCartDataforUser                     = "DLab/GetCartDataforUser"
let PATH_GetDiagnosticLabBookingTimeSlot        = "DLab/GetDiagnosticLabBookingTimeSlot"
let PATH_BookingDiagnosticLab                   = "DLab/BookingDiagnosticLab"
let PATH_DiagnosticLabbookingwithPrescription   = "Upload/DiagnosticLabbookingwithPrescription"
let PATH_DiagnosticLabAppointmentSMStoUser      = "DLab/DiagnosticLabAppointmentSMStoUser"

// Multi Profile View
let PATH_GetLabDetailsByID                      = "DLab/GetLabDetailsByID"
let PATH_GetMedicalShopDetailsByID              = "Pharmacy/GetMedicalShopDetailsByID"
let PATH_GetOpticalDetailsByID                  = "Opticals/GetOpticalDetailsByID"
let PATH_AddUpdateLabFavourites                 = "DLab/AddUpdateLabFavourites"
let PATH_AddUpdateMedicalFavourites             = "Pharmacy/AddUpdateMedicalFavourites"
let PATH_AddUpdateOpticalsFavourites            = "Opticals/AddUpdateOpticalsFavourites"
let PATH_ScanDoctorProfileQR                    = "user/ScanDoctorProfileQR"

// My Favorites
let PATH_GetUserMyFavourites                    = "user/GetUserMyFavourites"
let PATH_AddDeleteDrFavourites                  = "user/AddDeleteDrFavourites"

// Health Reminder
let PATH_AddUpdateHealthReminder                = "user/AddUpdateHealthReminder"
let PATH_GetReminderList                        = "user/GetReminderList"

// Health Record
let PATH_AddUpdateMedicalRecords                = "user/AddUpdateMedicalRecords"
let PATH_DeleteMedRecordFiles                   = "user/DeleteMedRecordFiles"
let PATH_GetMedicalRecords                      = "user/GetMedicalRecords"
let PATH_DeactivateMedicalRecord                = "user/DeactivateMedicalRecord"

// Single Selection
let PATH_GetMedicalAppointment                  = "user/GetMedicalAppointment"

// Appointments
let PATH_GetUpcomeAppointment                   = "user/GetUpcomeAppointment"
let PATH_PastAppointments                       = "user/PastAppointments"
let PATH_GetDueAppointments                     = "user/GetDueAppointments"
let PATH_DueReminderUpdate                      = "user/DueReminderUpdate"
let PATH_CancelBooking                          = "user/CancelBooking"
let PATH_GetPatientPrescription                 = "user/GetPatientPrescription"

// Feedback
let PATH_AddRating                              = "user/AddRating"
let PATH_UserPendingReviews                     = "user/UserPendingReviews"
let PATH_UserGetAllRating                       = "user/UserGetAllRating"

// Manage Lab
let PATH_ViewLabTestDetailsPatient              = "DLab/ViewLabTestDetailsPatient"
let PATH_CancelLabTest                          = "DLab/CancelLabTest"
let PATH_GetLabPatientBookedList                = "DLab/GetLabPatientBookedList"
let PATH_GetLabPatientCompletedList             = "DLab/GetLabPatientCompletedList"
let PATH_GetLabPatientCancelledList             = "DLab/GetLabPatientCancelledList"

// Family
let PATH_UserFamilyReg                          = "user/UserFamilyReg"
let PATH_ListUserFamily                         = "user/ListUserFamily"

// Your Location
let PATH_UserDefaultLocationUpdate              = "user/UserDefaultLocationUpdate"

// User Profile
let PATH_GetUserProfile                         = "user/GetUserProfile"
let PATH_UpdateUserDetails                      = "user/UpdateUserDetails"
let PATH_AddUpdateUserAccount                   = "Upload/UploadFiles"

// FAQ
let PATH_GetFAQ                                 = "user/GetFAQ"
let PATH_UserAddSupport                         = "user/UserAddSupport"
let PATH_ChangeMobileNo                         = "user/ChangeMobileNo"
let PATH_GetEmergencyDataforUser                = "user/GetEmergencyDataforUser"
let PATH_NotificationCheck                      = "user/NotificationCheck"

// Location PopOver
let PATH_GetBookingFeeforCity                   = "user/GetBookingFeeforCity"
