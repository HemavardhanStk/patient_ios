//
//  StringValues.swift
//  MedHos
//
//  Created by Hemavardhan on 03/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation

let STR_PLEASE_ENTER = "Please enter "
let STR_VALID = "valid "
let STR_YES = "Yes"
let STR_NO = "No"

let KEY_QUESTION = "_que"
let KEY_ANSWER = "ans"

let EDIT_LOCATION = "Click Edit to get Location"

// MARK: - Index
let TITLE_APP_TOUR                  = "App Tour"
let TITLE_SIGN_UP                   = "Sign up"
let TITLE_SIGN_IN                   = "Sign in"
let TITLE_FORGOT_PASSWORD           = "Forgot Password"
let TITLE_RESET_PASSWORD            = "Reset Password"
let TITLE_OTP                       = "OTP"
let TITLE_TERMS_AND_CONDITIONS      = "Terms and Conditions"
let TITLE_PRIVACY_POLICY            = "Privacy Policy"

// MARK: - Home
let TITLE_MYFAVOURITE               = "My Favourite"
let TITLE_HEALTHRECORD              = "Health Record"
let TITLE_EDITRECORD                = "Edit Record"
let TITLE_ADDHEALTHRECORD           = "Add Health Record"
let TITLE_HEALTHREMINDER            = "Health Reminder"
let TITLE_EDITREMINDER              = "Edit Reminder"
let TITLE_MEDICAL_REMINDER          = "Medical Reminder"
let TITLE_ADDHEALTHREMINDER         = "Add Health Reminder"
let TITLE_DOCTOR_PROFILE            = "Doctor Profile"
let TITLE_PRE_BOOKING               = "PreBooking"
let TITLE_APPOINTMENT_FOR           = "Appointment For"
let TITLE_CHOOSE_APPOINTMENT        = "Choose Appointment"
let TITLE_PHARMACY                  = "Pharmacy"
let TITLE_DIAGONSTIC_LAB            = "Diagonstic Lab"
let TITLE_OPTICALS                  = "Opticals"

// MARK: - Manage
let TITLE_MANAGE                    = "Manage"
let TITLE_UPCOMING                  = "Upcoming Appointments"
let TITLE_PAST                      = "Past Appointments"
let TITLE_DUE                       = "Due Appointments"
let TITLE_EDITFEEDBACk              = "Edit FeedBack"
let TITLE_PENDING_FEEDBACk          = "Pending FeedBacks"
let TITLE_COMPLETED_FEEDBACk        = "Completed FeedBacks"
let TITLE_FAMILYMEMBERS             = "Family Members"
let TITLE_ADDFAMILY                 = "Add Family"

// MARK: - Account
let TITLE_ACCOUNT                   = "Account"
let TITLE_PROFILE                   = "Profile"
let TITLE_CHANGE_PASSWORD           = "Change Password"
let TITLE_CHANGE_MOBILE             = "Change Mobile"
let TITLE_CHANGE_EMAIL              = "Change Email"
let TITLE_NOTIFICATION              = "Notification"
let TITLE_EMERGENCY_NUMBERS         = "Emergency Numbers"
let TITLE_FAQ                       = "FAQ"
let TITLE_HELP                      = "Help"
let TITLE_CONTACT_US                = "Contact Us"
