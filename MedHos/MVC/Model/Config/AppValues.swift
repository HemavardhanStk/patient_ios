//
//  AppValues.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 22/02/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import Foundation
import UIKit

let MOBILE_LENGTH = 10
let PINCODE_LENGTH = 6

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
