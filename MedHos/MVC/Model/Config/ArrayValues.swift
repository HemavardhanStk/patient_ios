//
//  ArrayValues.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation

let GENDER_TYPE_ARRAY = ["Male", "Female", "Others"]
let GENDER_TAG_TYPE_ARRAY = ["M", "F", "T"]
let DURATION_TYPE_ARRAY = ["5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"]
let YES_NO_TYPE_ARRAY = ["Yes", "No"]
let NUMBERIC_TAG_ARRAY = ["1", "2"]
let YES_NO_TYPE_ALPHA_TAG_ARRAY = ["Y", "N"]
let MARITAL_STATUS_ARRAY = ["Married","UnMarried"]
let MARITAL_STATUS_ALPHA_TAG_ARRAY = ["M","N"]
let BLOOD_GROUP_ARRAY = ["A+","A-","B+","B-","AB+","AB-","O+","O-"]
let LANGUAGE_ARRAY = ["English","Tamil"]
