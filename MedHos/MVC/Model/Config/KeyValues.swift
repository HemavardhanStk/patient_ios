//
//  KeyValues.swift
//  tests
//
//  Created by Hemavardhan on 14/02/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation

let APP_NAME = "MedHos"
// Google API Key
let kMapsAPIKey = "AIzaSyCRbV2n3ZdO6ORL8dKsTW7NNlElWo1TiNU"

let IMAGE = "Image"
let TITLE = "Title"
let DESCRIPTION = "Description"
let ID = "Id"
let TEXT = "Text"
let TYPE = "Type"
let PLACEHOLDER = "Placeholder"
let SUB_PLACEHOLDER = "SubPlaceholder"
let VALUE_ONE = "Value One"
let VALUE_TWO = "Value Two"
let VALUE_THREE = "Value Three"
let STATUS = "Status"
let SELECTED = "Y"
let UN_SELECTED = "N"
let OPEN = "Open"
let CLOSE = "Close"
let COUNT = "Count"
let ONLINE = "Online"
let OFFLINE = "Offline"
let IMAGE_URL = "Image Url"
let CROPPED_IMAGE = "CroppedImage"

// URL
let TERMS_AND_CONDITION_URL = "https://www.medhos.in/MainHome/TermsConditionsMobile"
let PRIVACY_POLICY_URL = "https://www.medhos.in/MainHome/PrivacyPolicyMobile"

let DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
let APP_DISPLAY_DATE_FORMAT = "dd-MM-yyyy"
let APP_DISPLAY_TIME_FORMAT = "hh:mm a"
