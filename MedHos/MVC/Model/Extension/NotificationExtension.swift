//
//  NotificationExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 25/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation

extension Notification.Name {
    public struct Task {
        public static let ReloadLabPreBooking = Notification.Name(rawValue: "ReloadLabPreBooking")
        public static let ReloadDoctorCategories = Notification.Name(rawValue: "ReloadDoctorCategories")
        public static let ReloadDoctorBasicSearch = Notification.Name(rawValue: "ReloadDoctorBasicSearch")
        public static let ReloadLabBasicSearch = Notification.Name(rawValue: "ReloadLabBasicSearch")
        public static let ReloadHealthRecord = Notification.Name(rawValue: "ReloadHealthRecord")
        public static let ReloadHealthReminder = Notification.Name(rawValue: "ReloadHealthReminder")
        public static let ReloadUpcomingAppointments = Notification.Name(rawValue: "ReloadUpcomingAppointments")
        public static let ReloadPendingFeedback = Notification.Name(rawValue: "ReloadPendingFeedback")
        public static let ReloadLabPackageTestList = Notification.Name(rawValue: "ReloadLabPackageTestList")
        public static let ReloadLabCartList = Notification.Name(rawValue: "ReloadLabCartList")
        public static let ReloadBookedTests = Notification.Name(rawValue: "ReloadBookedTests")
        public static let ReloadFamilyList = Notification.Name(rawValue: "ReloadFamilyList")
        public static let AccountPageScrollToTop = Notification.Name(rawValue: "AccountPageScrollToTop")
        public static let ReloadPatientProfile = Notification.Name(rawValue: "ReloadPatientProfile")
    }
}
