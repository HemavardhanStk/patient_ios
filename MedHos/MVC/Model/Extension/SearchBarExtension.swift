//
//  SearchBarExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 03/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    func setSearchBarStyle() {
        
        self.backgroundColor = UIColor.clear
        self.isTranslucent = true
        self.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        if let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField {
            textFieldInsideSearchBar.clearButtonMode = .never
        }
    }
    
    func addToolBar(_ controller: UIViewController) {
        
        let space = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem.init(title: "Done", style: .done, target: controller, action: #selector(searchDoneButtonTapped))
        doneBtn.tag = self.tag
        let toolBar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: 50))
        toolBar.barStyle = .blackOpaque
        toolBar.items = [space, doneBtn]
        self.inputAccessoryView = toolBar
    }
    
    @objc func searchDoneButtonTapped(sender: UIBarButtonItem) {
        
        
    }
    
}
