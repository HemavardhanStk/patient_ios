//
//  StringExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 06/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension String {
    
    func convertToAttributedString(_ string: String) -> NSMutableAttributedString {
        
        let attString = NSMutableAttributedString(string: "\(string)\n\n\(self)")
        attString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_BOLD, size: 15)!, range: NSMakeRange(0, string.count))
        attString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSMakeRange(0, string.count))
        attString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: APP_FONT_NAME_NORMAL, size: 14)!, range: NSMakeRange(string.count + 2,self.count))
        attString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(netHex: APP_PRIMARY_COLOR), range: NSMakeRange(string.count + 2, self.count))
        
        return attString
    }
    
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        let myRange = start..<end
        return String(self[myRange])
    }
}
