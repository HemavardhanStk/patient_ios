//
//  ButtonExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 27/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setPrimaryButtonStyle(_ title: String) {
        
        self.backgroundColor = UIColor(netHex: APP_PRIMARY_COLOR)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitleColor(UIColor.lightGray, for: .highlighted)
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = UIFont.init(name: "Avenir-Heavy", size: 16)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
    
}

