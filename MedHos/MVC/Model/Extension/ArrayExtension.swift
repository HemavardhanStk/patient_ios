//
//  ArrayExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 03/06/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
