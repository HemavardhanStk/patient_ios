//
//  ColorExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 03/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    convenience init(color: String) {
        
        let colorVal = color.replacingOccurrences(of: "#", with: "")
        if let color: Int = Int(colorVal, radix: 16) {
            self.init(netHex: color)
        } else {
            self.init(netHex: APP_PRIMARY_COLOR)
        }
    }
}

