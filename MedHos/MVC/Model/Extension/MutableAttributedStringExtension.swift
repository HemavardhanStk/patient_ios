//
//  MutableAttributedStringExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 08/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}
