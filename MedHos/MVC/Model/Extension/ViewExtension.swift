//
//  ViewExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 03/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

extension UIView {
    
    func setRoundedCorner(_ cornerRadius: CGFloat = 8) {
        
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
}

    func dropShadow() {
        self.layer.shadowRadius = 6.0
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: -8, height: 1)
    }
    
}

extension UIView {
    
    func roundCorners(_ corners:UIRectCorner,_ cormerMask:CACornerMask, radius: CGFloat) {
        if #available(iOS 11.0, *){
            self.clipsToBounds = false
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = cormerMask
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = self.frame
            rectShape.position = self.center
            rectShape.path = UIBezierPath(roundedRect: self.bounds,    byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
            self.layer.mask = rectShape
        }
    }
    
}
