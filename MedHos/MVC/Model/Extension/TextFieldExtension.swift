//
//  TextFieldExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

extension ACFloatingTextfield {
    
    func setTextFieldStyle() {
        
        self.selectedLineColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        self.selectedPlaceHolderColor = UIColor.init(netHex: APP_PRIMARY_COLOR)
        self.font = UIFont.init(name: APP_FONT_NAME_NORMAL, size: 14)
    }
    
    func setTextFieldBottomLineColor(_ color: UIColor, selectedLineColor: UIColor) {
        
        self.lineColor = color
        self.selectedLineColor = selectedLineColor
    }
    
    func addPickerView(_ controller: UIViewController, array: [String] = []) {
        
        let pickerView = UIPickerView.init(frame: CGRect.init(x: 0, y: 0, width: controller.view.frame.size.width, height: 300))
        pickerView.sizeToFit()
        pickerView.dataSource = controller as? UIPickerViewDataSource
        pickerView.delegate = controller as? UIPickerViewDelegate
        pickerView.tag = self.tag
        pickerView.showsSelectionIndicator = true
        if array.count != 0 {
            if let text = self.text,
                let index: Int = array.index(of: text) {
                pickerView.selectRow(index, inComponent: 0, animated: true)
            }
        }
        self.inputView = pickerView
        
        self.addToolBar(controller)
        self.setDropDownStyle()
    }
    
    func addDatePickerView(_ controller: UIViewController) -> UIDatePicker {
        
        let datePicker = UIDatePicker.init()
        datePicker.datePickerMode = .date
        datePicker.tag = self.tag
        datePicker.addTarget(controller, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        self.inputView = datePicker
        
        self.addToolBar(controller)
        self.setDropDownStyle()
        
        return datePicker
    }
    
    func addToolBar(_ controller: UIViewController) {
        
        let space = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem.init(title: "Done", style: .done, target: controller, action: #selector(doneButtonTapped))
        doneBtn.tag = self.tag
        let toolBar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: 50))
        toolBar.barStyle = .blackOpaque
        toolBar.items = [space, doneBtn]
        self.inputAccessoryView = toolBar
    }
    
    @objc func doneButtonTapped(sender: UIBarButtonItem) {
        
        
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        
    }
    
    func setDropDownStyle() {
        
        self.setLeftPadding(10)
        
        let imageView = UIImageView()
        let image: UIImage? = UIImage(named: "ic_dropdown")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let size: CGFloat = 15
        imageView.frame = CGRect(x: 0, y: (self.bounds.height - size) / 2, width: size, height: size)
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 30, height: self.bounds.height)
        view.addSubview(imageView)
        
        self.rightView = view
        self.rightView?.isUserInteractionEnabled = false
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    func setImageOnRight(ImageName: String, Size: CGFloat) {
        
        self.setLeftPadding(10)
        
        let imageView = UIImageView()
        let image: UIImage? = UIImage(named: ImageName)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let size: CGFloat = Size
        imageView.frame = CGRect(x: 0, y: (self.bounds.height - size) / 2, width: size, height: size)
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 30, height: self.bounds.height)
        view.addSubview(imageView)
        
        self.rightView = view
        self.rightView?.isUserInteractionEnabled = false
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    func setLeftPadding(_ value: CGFloat) {
        
        let paddingView = UIView(frame: CGRect.init(x: 0, y: 0, width: value, height: self.bounds.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
