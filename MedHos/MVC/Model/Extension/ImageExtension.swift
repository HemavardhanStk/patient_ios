//
//  ImageExtension.swift
//  MedHos
//
//  Created by Hemavardhan on 03/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

extension UIImage {
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

extension UIImageView {

    func loadImageFromUrl(_ imageUrl: String, contentMode: UIView.ContentMode = .scaleAspectFit, imageTransition: UIImageView.ImageTransition = UIImageView.ImageTransition.flipFromLeft(1.0)) {

        if let url = URL.init(string: imageUrl) {
            self.contentMode = contentMode
            self.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: imageTransition, runImageTransitionIfCached: true, completion: { (data) in

            })
        }
    }
}

extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIImageView {
    
    func cm_setImage(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit, placeholderImage: String = "" ) {
        if placeholderImage != "" {
            self.image = UIImage(named: placeholderImage)
        }
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
}

extension UIImageView {
    
    func viewInFullScreen() {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewInFullScreenTapGesture))
        
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
}

extension UIImageView {
    
    @objc private func viewInFullScreenTapGesture(_ tapGestureRecognizer: UITapGestureRecognizer) {

        if let controller = tapGestureRecognizer.view?.parentContainerViewController() {
            let newImageView = UIImageView()
            newImageView.image = self.image
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            controller.view.addSubview(newImageView)
            controller.navigationController?.isNavigationBarHidden = true
            controller.tabBarController?.tabBar.isHidden = true
        }
    }
    
    @objc private func dismissFullscreenImage(_ tapGestureRecognizer: UITapGestureRecognizer) {
        
        if let controller = tapGestureRecognizer.view?.parentContainerViewController() {
            controller.navigationController?.isNavigationBarHidden = false
            controller.tabBarController?.tabBar.isHidden = false
            tapGestureRecognizer.view?.removeFromSuperview()
        }
    }
    
}

