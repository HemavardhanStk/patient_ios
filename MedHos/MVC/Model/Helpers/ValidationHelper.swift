//
//  ValidationHelper.swift
//  MedHos
//
//  Created by Hemavardhan on 27/03/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//


import UIKit
import Alamofire

class ValidationHelper: NSObject {
    
    override init () { }
    
    static let shared: ValidationHelper = ValidationHelper()
    
    // MARK: - Textfield Validation Helper
    
    func isValidEmailID(_ value: String) -> Bool {
        
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        let result = test.evaluate(with: value)
        
        return result
    }
    
    func isValidMobileNumber(_ value: String) -> Bool {
        
        let regEx = "^[0-9]{10}$"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        let result = test.evaluate(with: value)
        
        return result
    }
    
    func isValidPassword(_ value: String) -> Bool {
        
        let regEx = "^.(?=.{6,})(?=.\\d)(?=.[a-z])(?=.[A-Z])(?=.[@#$%&!~_()^]).*$"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        let result = test.evaluate(with: value)
        
        return result
    }
    
    func isNilCheckValidationSuccess(infoArray: [Dictionary<String, String>], nonMandatoryFields: [Int] = [], controller: UIViewController) -> Bool {
        
        var isValidationSuccess = true
        var message = "Please enter "
        var fieldInfoArray = infoArray
        let nonMandatoryFieldArray = nonMandatoryFields.reversed()
        
        for index in nonMandatoryFieldArray {
            if fieldInfoArray.count > index {
                fieldInfoArray.remove(at: index)
            }
        }
        
        for item in fieldInfoArray {
            if item[TEXT]?.count == 0 {
                isValidationSuccess = false
                if let placeholder = item[PLACEHOLDER] {
                    message = message + placeholder
                }
                break
            }
        }
        
        if !isValidationSuccess {
            AlertHelper.shared.showAlert(message: message, controller: controller)
        }
        
        return isValidationSuccess
    }
    
    // MARK: - API Response Validation Helper
    
    func isAPIValidationSuccess(_ response: Dictionary<String, AnyObject>, controller: UIViewController, isBackgroundCall: Bool = false) -> Bool {
        
        var isValidationSuccess = false
        var message: String = ""
        
        if let status = response["Success"] as? Int {
            let statusValue = "\(status)"
            
            if statusValue == "0" {
                isValidationSuccess = true
            } else {
                if let errorMessgae = response["Message"] as? String {
                    message = errorMessgae
                }
            }
        }
        
        if !isValidationSuccess && !isBackgroundCall {
            AlertHelper.shared.showAlert(message: message, controller: controller)
        }
        
        return isValidationSuccess
    }
    
    func isAPIValidationSuccessForProfileDashboard(_ response: Dictionary<String, AnyObject>, controller: UIViewController, isBackgroundCall: Bool = false) -> Bool {
        
        var isValidationSuccess = false
        var message: String = ""
        
        if let status = response["Success"] as? Int {
            let statusValue = "\(status)"
            
            if statusValue == "0" {
                isValidationSuccess = true
            } else if statusValue == "1" {
                isValidationSuccess = true
            } else {
                if let errorMessgae = response["Message"] as? String {
                    message = errorMessgae
                }
            }
        }
        
        if !isValidationSuccess && !isBackgroundCall {
            AlertHelper.shared.showAlert(message: message, controller: controller)
        }
        
        return isValidationSuccess
    }
    
}

