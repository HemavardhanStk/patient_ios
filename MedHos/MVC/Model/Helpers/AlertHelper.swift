//
//  AlertHelper.swift
//  MedHos Doctor
//
//  Created by Balaji GUNASEKAR on 15/02/18.
//  Copyright © 2018 Balaji G. All rights reserved.
//

import UIKit

class AlertHelper: NSObject {

    override init () { }
    
    static let shared: AlertHelper = AlertHelper()
    var alert: UIAlertController!
    
    func showAlert(title: String = APP_NAME, message: String, controller: UIViewController) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert);
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil));
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithHandler(message: String, handler: String, withCancel: Bool, controller: UIViewController) {
        
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: UIAlertController.Style.alert);
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction) in
            if controller.responds(to: Selector((handler))) {
                controller.perform(Selector((handler)), with: nil, afterDelay: 0)
            }
            else {
                NSLog("Unable to find respective method")
            }
        }))
        
        if withCancel {
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        }
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithTwoWayHandler(message: String, ButtonName1: String, ButtonName2: String, handler1: String, handler2: String, controller: UIViewController) {
        
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: UIAlertController.Style.alert);
        
        alert.addAction(UIAlertAction(title: ButtonName1, style: .default, handler: {(action:UIAlertAction) in
            if controller.responds(to: Selector((handler1))) {
                controller.perform(Selector((handler1)), with: nil, afterDelay: 0)
            }
            else {
                NSLog("Unable to find respective method")
            }
        }))
        
        alert.addAction(UIAlertAction(title: ButtonName2, style: .default, handler: {(action:UIAlertAction) in
            if controller.responds(to: Selector((handler2))) {
                controller.perform(Selector((handler2)), with: nil, afterDelay: 0)
            }
            else {
                NSLog("Unable to find respective method")
            }
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
}
