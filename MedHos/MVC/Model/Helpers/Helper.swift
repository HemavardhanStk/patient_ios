//
//  Helper.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class Helper: NSObject {
    
    override init () { }
    
    static let shared: Helper = Helper()
    
    // MARK: - Date
    
    func getCurrentDateString(_ format: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: Date())
    }
    
    func getString(_ date: Date, format: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    func getDate(_ dateStr: String, format: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if let date = dateFormatter.date(from: dateStr) {
            return date
        } else {
            return Date()
        }
    }
    
    func convert(date dateValue: String, from fromFormat: String, to toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        
        if let date = dateFormatter.date(from: dateValue) {
            
            dateFormatter.dateFormat = toFormat
            let convertedDate = dateFormatter.string(from: date)
            
            return convertedDate
        }
        
        return ""
    }
    
    // MARK: - Calendar Dates
    
    func getNextDate(_ date: Date, format: String) -> (Date, String, String, String) {
        
        let oneDay: Double = 60 * 60 * 24
        let nextDate = date.addingTimeInterval(oneDay)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let convertedDate = dateFormatter.string(from: nextDate)
        var dayStr = ""
        var dateStr = ""
        var monthStr = ""
        let array = convertedDate.components(separatedBy: " ")
        if array.count > 1 {
            dayStr = array[0]
            dateStr = array[1]
            if array.count > 2
            {
                monthStr = array[2]
            }
        }
        return (nextDate, dayStr, dateStr, monthStr)
    }
    
    
    // MARK: - Devive
    
    func isiPhoneX() -> Bool {
        
        let height = UIScreen.main.bounds.height
        if height == 812 {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Gender
    
    func getGenderTag(_ title: String) -> String {
        
        var tag = ""
        if let index: Int = GENDER_TYPE_ARRAY.index(of: title) {
            tag = GENDER_TAG_TYPE_ARRAY[index]
        }
        
        return tag
    }
    
    func getGenderValue(_ tag: String) -> String {
        
        var title = ""
        if let index: Int = GENDER_TAG_TYPE_ARRAY.index(of: tag) {
            title = GENDER_TYPE_ARRAY[index]
        }
        
        return title
    }
    
    func setGenderImage(_ imageView: UIImageView, gender: String) {
        
        imageView.contentMode = .scaleAspectFill
        if gender == "M" {
            imageView.image = UIImage.init(named: "ic_male")
        } else if gender == "F" {
            imageView.image = UIImage.init(named: "ic_female")
        } else {
            imageView.image = UIImage.init(named: "ic_user_manual")
        }
    }
    
    // MARK: - Marital Status
    func getMartialStatus(_ title: String) -> String {
        if title == MARITAL_STATUS_ARRAY[0]
        {
            return MARITAL_STATUS_ALPHA_TAG_ARRAY[0]
        }
        return MARITAL_STATUS_ALPHA_TAG_ARRAY[1]
    }
    
    func getMartialStatusText(_ title: String) -> String {
        if title == MARITAL_STATUS_ALPHA_TAG_ARRAY[0]
        {
            return MARITAL_STATUS_ARRAY[0]
        }
        return MARITAL_STATUS_ARRAY[1]
    }
    
    // MARK: - Preferred Language
    func getPreferredLanguage(_ title: String) -> String {
        
        if title == LANGUAGE_ARRAY[0]
        {
            return NUMBERIC_TAG_ARRAY[1]
        }
        return NUMBERIC_TAG_ARRAY[0]
    }
    
    func getPreferredLanguageText(_ title: String) -> String {
        if title == NUMBERIC_TAG_ARRAY[1]
        {
            return LANGUAGE_ARRAY[0]
        }
        return LANGUAGE_ARRAY[1]
    }
    
    // MARK: - Yes No Type
    
    func getYesNoTypeValue(_ tag: String, array: [String]) -> String {
        
        var value = ""
        if let index: Int = array.index(of: tag) {
            value = YES_NO_TYPE_ARRAY[index]
        }
        return value
    }
    
    func getYesNoTypeTag(_ value: String, array: [String]) -> String {
        
        var tag = ""
        if let index: Int = YES_NO_TYPE_ARRAY.index(of: value) {
            tag = array[index]
        }
        return tag
    }
}

