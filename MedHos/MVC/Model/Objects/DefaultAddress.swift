//
//  DefaultAddress.swift
//  MedHos
//
//  Created by Hemavardhan on 03/07/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class DefaultAddress: NSObject, NSCoding {
    
    var latitude: String
    var longitude: String
    var buildingNo: String
    var streetName: String
    var areaName: String
    var cityName: String
    var pincode: String
    var stateName: String
    var countryName: String
    
    init(latitude: String, longitude: String, buildingNo: String, streetName: String, areaName: String, cityName: String, pincode: String, stateName: String, countryName: String) {
        
        self.latitude       = latitude
        self.longitude      = longitude
        self.buildingNo     = buildingNo
        self.streetName     = streetName
        self.areaName       = areaName
        self.cityName       = cityName
        self.pincode        = pincode
        self.stateName      = stateName
        self.countryName    = countryName
    }
    
    required init(coder decoder: NSCoder) {
        
        self.latitude       = decoder.decodeObject(forKey: "latitude") as? String ?? ""
        self.longitude      = decoder.decodeObject(forKey: "longitude") as? String ?? ""
        self.buildingNo     = decoder.decodeObject(forKey: "buildingNo") as? String ?? ""
        self.streetName     = decoder.decodeObject(forKey: "streetName") as? String ?? ""
        self.areaName       = decoder.decodeObject(forKey: "areaName") as? String ?? ""
        self.cityName       = decoder.decodeObject(forKey: "cityName") as? String ?? ""
        self.pincode        = decoder.decodeObject(forKey: "pincode") as? String ?? ""
        self.stateName      = decoder.decodeObject(forKey: "stateName") as? String ?? ""
        self.countryName    = decoder.decodeObject(forKey: "countryName") as? String ?? ""
        
    }
    
    func encode(with coder: NSCoder) {
        
        coder.encode(latitude, forKey: "latitude")
        coder.encode(longitude, forKey: "longitude")
        coder.encode(buildingNo, forKey: "buildingNo")
        coder.encode(streetName, forKey: "streetName")
        coder.encode(areaName, forKey: "areaName")
        coder.encode(cityName, forKey: "cityName")
        coder.encode(pincode, forKey: "pincode")
        coder.encode(stateName, forKey: "stateName")
        coder.encode(countryName, forKey: "countryName")
        
    }
    
}

class UserAddress {
    
    static let shared: UserAddress = UserAddress()
    
    func loadUserDefaultAddress(_ response: Dictionary<String, AnyObject>) {
        
        let defaultAddress = DefaultAddress(latitude: "", longitude: "", buildingNo: "", streetName: "", areaName: "", cityName: "", pincode: "", stateName: "", countryName: "")
        
        if let dict = response["UserDetail"] as? Dictionary<String, AnyObject>,
            let latitude = dict["Latitude"] as? Double,
            let longitude = dict["Longitude"] as? Double,
            latitude != 0.0, longitude != 0.0 {
            
            defaultAddress.latitude = "\(latitude)"
            defaultAddress.longitude = "\(longitude)"
            
            if let buildingNo = dict["BuildingName"] as? String {
                defaultAddress.buildingNo = buildingNo
            }

            if let streetName = dict["StreetName"] as? String {
                defaultAddress.streetName = streetName
            }

            if let areaName = dict["AreaName"] as? String {
                defaultAddress.areaName = areaName
            }

            if let cityName = dict["CityName"] as? String {
                defaultAddress.cityName = cityName
            }

            if let pincode = dict["Pincode"] as? String {
                defaultAddress.pincode = pincode
            }

            if let stateName = dict["StateName"] as? String {
                defaultAddress.stateName = stateName
            }
            
            if let countryName = dict["CountryName"] as? String {
                defaultAddress.countryName = countryName
            }

        }
        
        if defaultAddress.countryName.count != 0 {
            
            let defaultAddressString = defaultAddress.buildingNo + "," + defaultAddress.streetName + "," + defaultAddress.areaName + "," + defaultAddress.cityName + "-" + defaultAddress.pincode + "," + defaultAddress.stateName + "," + defaultAddress.countryName
            
            Session.shared.setUserDefaultAddressString(defaultAddressString)
        }
        
        Session.shared.setUserDefaultAddress(defaultAddress)
        
    }
    
}
