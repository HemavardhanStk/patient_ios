//
//  User.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    
    var id: String
    var name: String
    var age: String
    var phone: String
    var email: String
    var image: String
    var gender: String
    
    init(id: String, name: String, age: String, phone: String, email: String, image: String, gender: String) {
        
        self.id = id
        self.name = name
        self.age = age
        self.phone = phone
        self.email = email
        self.image = image
        self.gender = gender
    }
    
    required init(coder decoder: NSCoder) {
        
        self.id = decoder.decodeObject(forKey: "id") as? String ?? ""
        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
        self.age = decoder.decodeObject(forKey: "age") as? String ?? ""
        self.phone = decoder.decodeObject(forKey: "phone") as? String ?? ""
        self.email = decoder.decodeObject(forKey: "email") as? String ?? ""
        self.image = decoder.decodeObject(forKey: "image") as? String ?? ""
        self.gender = decoder.decodeObject(forKey: "gender") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(age, forKey: "age")
        coder.encode(phone, forKey: "phone")
        coder.encode(email, forKey: "email")
        coder.encode(image, forKey: "image")
        coder.encode(gender, forKey: "gender")
    }
}

class UserInfo {
    
    static let shared: UserInfo = UserInfo()
    
    // MARK: - Login
    
    func loadUserBasicInfo(_ response: Dictionary<String, AnyObject>) {
        
        let user = User(id: "", name: "", age: "", phone: "", email: "", image: "", gender: "")
        
        if let dict = response["UserDetail"] as? Dictionary<String, AnyObject> {
            
            if let id = dict["User_IDs"] as? Int {
                user.id = "\(id)"
            }
            
            if let name = dict["UserName"] as? String {
                user.name = name
            }
            
            if let age = dict["Age"] as? String {
                user.age = "\(age)"
            }
            
            if let phone = dict["UserPhone"] as? String {
                user.phone = phone
            }
            
            if let phone = dict["EmailID"] as? String {
                user.email = phone
            }
            
            if let image = dict["UserImage"] as? String {
                user.image = image
            }
            
            if let gender = dict["UserGender"] as? String {
                user.gender = gender
            }

        }
        
        if let value = response["SupportContactNo"] {
            Session.shared.setSupportMobileNumber("\(value)")
        }
        
        if let value = response["InviteMessage"] as? String {
            Session.shared.setInviteMessage(value)
        }
        
        if let value = response["AccessToken"] as? String {
            Session.shared.setAccessToken(value)
        }
        
        Session.shared.setUserInfo(user)
    }
    
    func showSigninViewController() {
        
        Session.shared.setLoginStatus(LOGIN_STATUS.LOG_IN.rawValue)
        
        let viewController = AppController.shared.getLoginViewController()
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: viewController)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    func showTabBarViewController() {
        
        Session.shared.setLoginStatus(LOGIN_STATUS.HOME.rawValue)
        
        let viewController = AppController.shared.getRAMAnimatedTabBarController()
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: viewController)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    // MARK: - Logout
    
    func showLogoutAlert(_ controller: UIViewController) {
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to logout?", preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {(action:UIAlertAction) in
            self.logoutUser(controller)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    func logoutUser(_ controller: UIViewController) {
        
        Session.shared.setLoginStatus(LOGIN_STATUS.LOG_IN.rawValue)
        let user = User.init(id: "", name: "", age: "", phone: "", email: "", image: "", gender: "")
        Session.shared.setUserInfo(user)
        
        controller.dismiss(animated: false, completion: nil)
        
        let viewController = AppController.shared.getLoginViewController()
        let navigationController:UINavigationController = UINavigationController.init(rootViewController: viewController)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
}

