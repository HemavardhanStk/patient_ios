//
//  Session.swift
//  MedHos
//
//  Created by Hemavardhan on 04/04/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.


import Foundation

class Session: NSObject {
    
    var userDefaults: UserDefaults
    
    static let shared: Session = Session()
    
    override init() {
        
        self.userDefaults = UserDefaults()
    }
    
    func saveValue() {
        
        self.userDefaults.synchronize()
    }
    
    // MARK: - Session Methods
    
    // MARK: String Value
    
    private func setString (key: String, value: String) {
        
        self.userDefaults.set(value, forKey: key)
        self.saveValue()
    }
    
    private func getString (key: String, defaultValue: String)-> String {
        
        var value: String? = self.userDefaults.object(forKey: key) as? String
        
        if value == nil {
            value = defaultValue.isEmpty ? "" : defaultValue
        }
        
        return value!
    }
    
    // MARK: Int Value
    
    private func setInt (key: String, value: Int) {
        
        self.userDefaults.set(value, forKey: key)
        self.saveValue()
    }
    
    private func getInt (key: String)-> Int {
        
        return self.userDefaults.integer(forKey: key)
    }
    
    // MARK: BOOL Value
    
    private func setBool (key: String, value: Bool) {
        
        self.userDefaults.set(value, forKey: key)
        self.saveValue()
    }
    
    private func getBool (key: String)-> Bool {
        
        return self.userDefaults.bool(forKey: key)
    }
    
    // MARK: Array of Dict
    
    private func setArrayofDict (key: String, value: [Dictionary<String, Any>]) {
        
        self.userDefaults.set(value, forKey: key)
        self.saveValue()
    }
    
    private func getArrayofDict (key: String)-> [Dictionary<String, Any>] {
        
        if let array: [Dictionary<String, Any>] = self.userDefaults.object(forKey: key) as? [Dictionary<String, Any>] {
            return array
        }
        return [Dictionary<String, Any>]()
    }
    
    // MARK: Array value
    
    private func setArray (key: String, value: NSMutableArray) {
        
        self.userDefaults.set(value, forKey: key)
        self.saveValue()
    }
    
    private func getArray (key: String)-> NSMutableArray {
        
        if let array: NSMutableArray = self.userDefaults.object(forKey: key) as? NSMutableArray {
            return array
        }
        return NSMutableArray()
    }
    
    // MARK: Data Value
    
    private func setData (key: String, value: Data) {
        
        self.userDefaults.set(value, forKey: key)
        self.saveValue()
    }
    
    private func getData (key: String)-> Data {
        
        if let value = self.userDefaults.data(forKey: key) {
            return value
        } else {
            return Data()
        }
    }
    
    // MARK: - Project Oriented Methods
    
    
    // MARK: - Index
    
    func setRememberMeStatus (_ value: Bool) {
        self.setBool(key: "RememberMeStatus", value: value)
    }
    
    func getRememberMeStatus ()-> Bool {
        return self.getBool(key: "RememberMeStatus")
    }
    
    func setAppUrl (_ value: String) {
        self.setString(key: "AppUrl", value: value)
    }
    
    func getAppUrl ()-> String {
        return self.getString(key: "AppUrl", defaultValue: "")
    }
    
    func setUserMobileAndUserId (mobile: String, userId: String) {
        self.setString(key: "Mobile", value: mobile)
        self.setString(key: "UserId", value: userId)
    }
    
    func getUserMobileAndUserId ()-> (String, String) {
        let mobile = self.getString(key: "Mobile", defaultValue: "")
        let userId = self.getString(key: "UserId", defaultValue: "")
        return (mobile, userId)
    }
    
    func setLoginStatus (_ value: String) {
        self.setString(key: "LoginStatus", value: value)
    }
    
    func getLoginStatus ()-> String {
        return self.getString(key: "LoginStatus", defaultValue: "")
    }
    
    func setUserInfo(_ user: User) {
        
        let userData = NSKeyedArchiver.archivedData(withRootObject: user)
        self.setData(key: "UserInfo", value: userData)
    }
    
    func getUserInfo() -> User {
        
        let data = self.getData(key: "UserInfo")
        
        if data.count == 0 {
            
            let user = User(id: "", name: "", age: "", phone: "", email: "", image: "", gender: "")
            return user
        }
        else {
            
            let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            return user!
        }
    }
    
    func setUserDefaultAddress(_ userAddress: DefaultAddress) {
        
        let userData = NSKeyedArchiver.archivedData(withRootObject: userAddress)
        self.setData(key: "UserAddress", value: userData)
    }
    
    func getUserDefaultAddress()-> DefaultAddress {
        
        let data = self.getData(key: "UserAddress")
        
        if data.count == 0 {
            
            let defaultAddress = DefaultAddress(latitude: "", longitude: "", buildingNo: "", streetName: "", areaName: "", cityName: "", pincode: "", stateName: "", countryName: "")
            return defaultAddress
        }
        else {
            
            let defaultAddress = NSKeyedUnarchiver.unarchiveObject(with: data) as? DefaultAddress
            return defaultAddress!
        }
    }
    
    func setUserDefaultAddressString (_ value: String) {
        self.setString(key: "UserDefaultAddressString", value: value)
    }
    
    func getUserDefaultAddressString ()-> String {
        return self.getString(key: "UserDefaultAddressString", defaultValue: EDIT_LOCATION)
    }
    
    func setUserMobileNumber (_ value: String) {
        self.setString(key: "UserMobileNumber", value: value)
    }
    
    func getUserMobileNumber ()-> String {
        return self.getString(key: "UserMobileNumber", defaultValue: "")
    }

    func setUserEmailId (_ value: String) {
        self.setString(key: "UserEmailId", value: value)
    }

    func getUserEmailId ()-> String {
        return self.getString(key: "UserEmailId", defaultValue: "")
    }

    func setDeviceToken (_ value: String) {
        self.setString(key: "DeviceToken", value: value)
    }
    
    func getDeviceToken ()-> String {
        return self.getString(key: "DeviceToken", defaultValue: "")
    }
    
    func setUpcomingCount (_ value: String) {
        self.setString(key: "UpcomingCount", value: value)
    }
    
    func getUpcomingCount ()-> String {
        return self.getString(key: "UpcomingCount", defaultValue: "")
    }
    
    func setDueCount (_ value: String) {
        self.setString(key: "DueCount", value: value)
    }
    
    func getDueCount ()-> String {
        return self.getString(key: "DueCount", defaultValue: "")
    }

//    func setCartCount (_ value: String) {
//        self.setString(key: "CartCount", value: value)
//    }
//
//    func getCartCount ()-> String {
//        return self.getString(key: "CartCount", defaultValue: "")
//    }
    
    // MARK: - App Version
    
    func getVersion() -> String {
        var version = "0.0"
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            version = text
        }
        return version
    }
    
    // MARK: - Support Mobile Number
    
    func setSupportMobileNumber (_ value: String) {
        self.setString(key: "SupportMobileNumber", value: value)
    }
    
    func getSupportMobileNumber ()-> String {
        return self.getString(key: "SupportMobileNumber", defaultValue: "")
    }
    
    // MARK: - Invite Message
    
    func setInviteMessage (_ value: String) {
        self.setString(key: "InviteMessage", value: value)
    }
    
    func getInviteMessage ()-> String {
        return self.getString(key: "InviteMessage", defaultValue: "")
    }
    
    // MARK: - Access Token
    
    func setAccessToken (_ value: String) {
        self.setString(key: "AccessToken", value: value)
    }
    
    func getAccessToken ()-> String {
        return self.getString(key: "AccessToken", defaultValue: "")
    }
}


