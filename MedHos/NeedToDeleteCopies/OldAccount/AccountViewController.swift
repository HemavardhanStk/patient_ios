//
//  OldAccountViewController.swift
//  tests
//
//  Created by Hemavardhan on 15/02/19.
//  Copyright © 2019 Hemavardhan. All rights reserved.
//

import UIKit


class OldAccountViewController: UIViewController {
    
    //Variable Initialization
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    let AccountPageHeaders = ["Change Settings","Help","General"]
    let ChangeSettingsHeaders = ["Your Location","Mobile No","App Language"]
    let ChangeSettingsIcons = ["ic_location_icon","ic_mobileno_icon","ic_app-lan-icon"]
    let HelpHeaders = ["FAQ","Call","Contact Us","Emergency Numbers"]
    let HelpIcons = ["ic_faq_icon","ic_call_icon","ic_contact_icon","ic_emerency_icon"]
    let GeneralHeaders = ["Notifications","Refer to Friends","Rate Us","Sign Out"]
    let GeneralIcons = ["ic_notification_icon","ic_refer-to-friend_icon","ic_rate_us_icon","ic_sign-out_icon"]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userMobileNumberLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(netHex: APP_BG_COLOR)
        
        self.userImageView.setRoundedCorner(20)
        
        self.userNameLabel.font = UIFont(name: APP_FONT_NAME_BOLD, size: 15)
        self.userMobileNumberLabel.font = UIFont(name: APP_FONT_NAME_NORMAL, size: 13)
        
        self.userNameLabel.textColor = .white
        self.userMobileNumberLabel.textColor = .white
        
        let user = Session.shared.getUserInfo()
        
        if let url = URL(string: user.image) {
            self.userImageView.af_setImage(withURL: url)
        }
        
        self.userNameLabel.text = user.name
        self.userMobileNumberLabel.text = user.phone
        PrimaryViewSetUp()
        self.navigationController?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToTop), name: Notification.Name.Task.AccountPageScrollToTop, object: nil)
    }
  
    func PrimaryViewSetUp()
    {
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 25
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 40
        self.tableView.separatorStyle = .none
        self.tableView.dataSource = self
        self.tableView.delegate = self
      
        self.tableView.bounces = true
        self.tableView.register(UINib(nibName: "AccountTableViewCell", bundle: nil), forCellReuseIdentifier: "AccountTableViewCell")
        self.tableView.register(UINib(nibName: "ManageHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ManageHeaderTableViewCell")
        view.backgroundColor = #colorLiteral(red: 0.9387328696, green: 0.9387328696, blue: 0.9387328696, alpha: 1)
    }
    
   }
